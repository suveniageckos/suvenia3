@extends('voyager::master')

@section('page_title', __('Stores We Like'))

@section('content')

<div class="container-fluid">
        <h1 class="page-title">
                <i class="voyager-star-half"></i> Stores We Like Blocks
            </h1>
            @forelse($store_blocks as $store_block)

            @empty
            @can('add', app('App\StoresWeLikeBlock'))
            <a href="{{ route('admin:stores_we_like_block:add') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
             @endcan
             @endforelse


<div class="page-content browse container-fluid">

<div class="row">
<div class="col-md-12">

<div class="panel panel-bordered">
<div class="panel-body">
<div class="row">
    @forelse($store_blocks as $store_block)
        <div class="col-md-3 text-center">
            <div class="card p-3">
                <p style="margin:2px; text-transform:capitalize;"><b>Store Ids</b></p>
                <p style="margin:2px; text-transform:capitalize;"><input type="text" class="form-control" readonly value="{{$store_block->store_ids  }}"></p>
                    <p style="margin:2px; text-transform:capitalize;">
                            <a href="{{route('admin:stores_we_like_block:edit', ['id'=> $store_block->id ])}}" class="btn btn-info btn-sm">Edit this</a>
                            <form action="{{route('admin:stores_we_like_block:delete', ['id'=> $store_block->id ])}}" method="POST">
                                @csrf
                                <button class="btn btn-danger btn-sm">Delete this</button>
                            </form>
                    </p>
            </div>
        </div>
    @empty
        <div class="col-md-12 text-center">
            <p><i class="voyager-star-half" style="font-size:100px;"></i></p>
             <b>You do not have any template</b>
        </div>
    @endforelse
</div>
</div>
</div>


</div>
</div>


</div>
</div>

@endsection
