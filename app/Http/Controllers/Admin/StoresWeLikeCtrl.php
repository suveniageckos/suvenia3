<?php

namespace App\Http\Controllers\Admin;

use App\StoresWeLikeBlock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoresWeLikeCtrl extends Controller
{
    public function index(){
        $store_blocks = StoresWeLikeBlock::all();
        return view('vendor.voyager.stores-we-like-blocks.index', ['store_blocks'=> $store_blocks]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $store_blocks = new StoresWeLikeBlock();
            $store_blocks->store_ids = $request->store_ids;
            $store_blocks->save();
            $red = action([StoresWeLikeCtrl::class, 'index']);
           return redirect($red);
        }
        return view('vendor.voyager.stores-we-like-blocks.add', []);
    }

    public function edit(Request $request, $id){
        $store_block = StoresWeLikeBlock::where('id', $id)->first();
        if($request->isMethod('post')){
            $store_block = StoresWeLikeBlock::where('id', $id)->first();
            $store_block->store_ids = $request->store_ids;
            $store_block->save();
            $red = action([StoresWeLikeCtrl::class, 'index']);
           return redirect($red);
        }
        return view('vendor.voyager.stores-we-like-blocks.edit', ['store_block'=> $store_block]);
    }

    public function delete(Request $request, $id){
        if($request->isMethod('post')){
            $store_block = StoresWeLikeBlock::where('id', $id)->first();
            $store_block->delete();
            $red = action([StoresWeLikeCtrl::class, 'index']);
            return redirect($red);
        }
    }
}
