@extends('layouts.layout')

@section('title', 'Return Policy')

@section('content')
<div class="uk-container uk-container-small uk-margin-top uk-margin-bottom">

    <div class="d-block uk-text-center mb-5">
<h3 class="mb-2 uk-text-uppercase">Return Policy</h3>
<div class="divider-bar"></div>
</div>

    <div class="row">
    <div class="col-md-12">
        <p>We source and make use of high quality products, suppliers and factorues. However, if you're not 100% satisfied, kindly let us know and we will fix your satisfaction just right. Our policy lasts 15 days. If 15 days have gone by since your purchase, unfortunately we can’t offer you a rerun or refund. To be eligible for either, your item must be unused and in the same condition that you received it. It must also be in the original packaging. To complete your return, we require that you attach your receipt or proof of purchase.</p>
        <h4 class="uk-text-bold">Reruns</h4>
        <p>We only replace items or offer reruns if they are defective or damaged. If you have a complaint requiring a rerun, send us an email at <a href="mailto:sales@suvenia.com">sales@suvenia.com</a> and send your item to: Suvenia.com, 36, Ayodele Okeowo Street, Ifako, Gbagada, Lagos</p>

        <h4 class="uk-text-bold">Shipping</h4>

    <p>To return your product, you should mail your product to: Suvenia.com, 36, Ayodele Okeowo Street, Ifako, Gbagada, Lagos. Depending on where you live, the time it may take for your rerun product to reach you, may vary. We'll bear the cost for shipping returns and reruns, if the fault is clearly from defect or low-quality item and print.</p>


    </div>
    </div>

</div>


<div id="modal-close-default" uk-modal>
        <div class="uk-modal-dialog uk-modal-body big-dialog">
            {{-- <div class=" uk-modal-header"> --}}
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
            {{-- </div> --}}

                        <div class="row">
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <span class="fas fa-store"></span>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                        <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fas fa-paint-brush"></i>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                            <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fab fa-accessible-icon"></i>
                                            </div>

                                             <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                            <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                 </div>
                            </div>

                        </div>

            </div>
            </div>
@endsection
