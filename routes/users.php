<?php
Route::group(['prefix' => 'app/user', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/login', 'UserCtrl@login')->name('app:user:login');
    Route::match(['post', 'get'], '/logout', 'UserCtrl@logout')->name('app:user:logout');
    Route::match(['post', 'get'], '/sign-up', 'UserCtrl@register')->name('app:user:register');
    Route::match(['post', 'get'], '/verify/email/{token}', 'UserCtrl@verify_email')->name('app:user:verify');
    Route::match(['post', 'get'], '/forgot-password', 'UserCtrl@forgot_password')->name('app:user:forgot_password');
    Route::match(['post', 'get'], '/reset-password/{token}', 'UserCtrl@reset_password')->name('app:user:reset_password');
    Route::match(['post', 'get'], '/resend/email-verification', 'UserCtrl@resend_email_verify')->name('app:user:resend_verification');

    //Social Authentication
    Route::match(['post', 'get'], '/social/{provider}', 'UserCtrl@social')->name('app:user:social');
    Route::match(['post', 'get'], '/social/callback/{provider}', 'UserCtrl@social_callback')->name('app:user:social_callback');
    Route::match(['post', 'get'], '/social/step/complete', 'UserCtrl@complete_social')->name('app:user:complete_social');

});

Route::group(['prefix' => 'dashboard/buyer', 'middleware' => ['web', 'assign.guard:buyers,app:user:login'], 'namespace'=> 'Dashboard'], function () {
    Route::match(['post', 'get'], '/', 'BuyerCtrl@index')->name('app:dashboard:index');
    Route::match(['post', 'get'], '/my-orders', 'BuyerCtrl@my_orders')->name('app:dashboard:my_orders');
    Route::match(['post', 'get'], '/my-customers', 'BuyerCtrl@customer_orders')->name('app:dashboard:my_customers');
    Route::match(['post', 'get'], '/profile-settings', 'BuyerCtrl@profile_settings')->name('app:dashboard:profile_settings');
    Route::match(['post', 'get'], '/withdraw', 'BuyerCtrl@withdraw')->name('app:dashboard:withdraw');
    Route::match(['post', 'get'], '/my-products', 'BuyerCtrl@my_products')->name('app:dashboard:my_products');
    Route::match(['post', 'get'], '/saved-items', 'BuyerCtrl@saved_items')->name('app:dashboard:saved_items');
    Route::match(['post', 'get'], '/delete/saved-item/{id}', 'BuyerCtrl@delete_saved_items')->name('app:dashboard:delete_saved_items');
    Route::match(['post', 'get'], '/reviews', 'BuyerCtrl@reviews')->name('app:dashboard:reviews');
    Route::match(['post', 'get'], '/add-review/{id}', 'BuyerCtrl@add_product_review')->name('app:dashboard:add_product_review');
    Route::match(['post', 'get'], '/payment-history', 'BuyerCtrl@payment_history')->name('app:dashboard:payment_history');

    Route::match(['post', 'get'], '/profile-settings', 'BuyerCtrl@profile_settings')->name('app:dashboard:profile_settings');
    Route::match(['post', 'get'], '/change-password', 'BuyerCtrl@change_password')->name('app:dashboard:change_password');
    Route::match(['post', 'get'], '/design-services', 'BuyerCtrl@design_services')->name('app:dashboard:design_services');
    Route::match(['post', 'get'], '/design-service/{id}', 'BuyerCtrl@single_design_service')->name('app:dashboard:single_design_service');

    //shipping
    Route::match(['post', 'get'], '/shipping', 'BuyerCtrl@shipping')->name('app:dashboard:shipping');

    //Notifications
    Route::match(['post', 'get'], '/notifications', 'BuyerCtrl@all_notifications')->name('app:dashboard:all_notifications');
    Route::match(['post', 'get'], '/notifications/{id}', 'BuyerCtrl@all_notifications')->name('app:dashboard:all_notifications');
    Route::match(['post', 'get'], '/notifications/{id}', 'BuyerCtrl@delete_notifications')->name('app:dashboard:delete_notifications');

    Route::match(['post', 'get'], '/user/set-modal/add-address/{id}', 'BuyerCtrl@add_address_zone_modal')->name('app:dashboard:add_address_zone_modal');
    Route::match(['post', 'get'], '/user/set-modal/edit-address/{id}/{address_id}', 'BuyerCtrl@edit_address_zone_modal')->name('app:dashboard: edit_address_zone_modal');
    Route::match(['post', 'get'], '/user/set-modal/select-address/{id}', 'BuyerCtrl@set_default_zone_modal')->name('app:dashboard:set_default_zone_modal');
    Route::match(['post', 'get'], '/user/set-modal/select-address/{id}', 'BuyerCtrl@set_default_zone_modal')->name('app:dashboard:set_default_zone_modal');
    Route::match(['post', 'get'], '/user/add-shipping-address/{zone_id}', 'BuyerCtrl@add_shipping_address')->name('app:dashboard:add_shipping_address');
    Route::match(['post', 'get'], '/user/edit-shipping-address/{zone_id}/{address_id}', 'BuyerCtrl@edit_shipping_address')->name('app:dashboard:edit_shipping_address');
    Route::match(['post', 'get'], '/user/delete-shipping-address/{id}', 'BuyerCtrl@delete_shipping_address')->name('app:dashboard:delete_shipping_address');
    Route::match(['post', 'get'], '/user/shipping/select-address/{zone_id}', 'BuyerCtrl@select_shipping_address')->name('app:dashboard:select_shipping_address');
    Route::match(['post', 'get'], '/user/shipping/select-zone/{zone_id}', 'BuyerCtrl@select_zone')->name('app:dashboard:select_zone');

});

Route::group(['prefix' => 'dashboard/api', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/profile-settings/upload/{id}', 'BuyerCtrl@profile_settings_save_image')->name('app:dashboard:profile-settings_upload');
    Route::match(['post', 'get'], '/profile-settings/upload-delete/{id}', 'BuyerCtrl@profile_settings_delete_image')->name('app:dashboard:profile-settings_upload_delete');

    Route::match(['post', 'get'], '/edit-store/upload', 'BuyerCtrl@edit_store_save_image')->name('app:dashboard:edit_store_upload');
    Route::match(['post', 'get'], '/edit-store/upload-delete', 'BuyerCtrl@edit_store_delete_image')->name('app:dashboard:edit_store_upload_delete');
});
