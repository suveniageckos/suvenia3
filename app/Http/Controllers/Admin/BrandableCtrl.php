<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Brandable as Brandable;
use App\Helpers\Utils as Utils;
use App\Http\Controllers\Controller;
use App\ProductCategory as Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BrandableCtrl extends Controller
{
    public function index(Request $request)
    {
        $brandables = Brandable::get();
        return view('vendor.voyager.eco-brandables.index', ['brandables' => $brandables]);
    }

    public function add(Request $request){

        if ($request->isMethod('post')) {
 
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|min:2|unique:eco_brandables',
                'price' => 'required|numeric',
                'description' => 'required|min:6',
                'front_image' => 'image',
                'back_image' => 'image|sometimes',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 500);

            } else {

                $data = $request->all();
            
                $brandable = new Brandable();

                $brandable->name = $request->name;
                $brandable->user_id = Auth::user()->id;
                $brandable->price = $request->price;
                $brandable->description = $request->description;

                $front_data = json_decode($request->front_dimension, true);
                $back_data = $request->enable_back_view ? json_decode($request->back_dimension, true) : [];
               
                $data_full = [
                    'backViewEnabled' => $request->enable_back_view ? true : false,
                    'sizeEnabled' => $request->enable_sizes ? true : false,
                    'description' => $request->description,
                    'price' => $request->price,
                    'name' => $request->name,
                    'category' => $request->category,
                    'front' => [
                        'left' => $front_data['left'],
                        'top' => $front_data['top'],
                        'width' => $front_data['width'],
                        'height' => $front_data['height'],
                        'image_url' => '',
                    ],
                    'back' => !$request->enable_back_view ? [] : [
                        'left' => $back_data['left'],
                        'top' => $back_data['top'],
                        'width' => $back_data['width'],
                        'height' => $back_data['height'],
                        'image_url' => '',
                    ],

                ];

                if ($request->hasFile('front_image') && $request->file('front_image')->isValid()) {
                    $front_image_path = Storage::putFile('brandables', $request->file('front_image'));
                    $brandable->front_image = $front_image_path;
                    $brandable->front_image_path = $front_image_path;
                    $data_full['front']['image_url'] = $front_image_path;
                }

                if ($request->hasFile('back_image') && $request->file('back_image')->isValid()) {
                    $back_image_path = Storage::putFile('brandables', $request->file('back_image'));
                    $brandable->back_image = $back_image_path;
                    $brandable->back_image_path = $back_image_path;
                    $data_full['back']['image_url'] = $back_image_path;
                }

                $brandable->data = json_encode($data_full, true);

               
                if ($brandable->save()) {
                    $redirectUrl = action([BrandableCtrl::class, 'index']);
                    return response()->json(['message' => $data_full, 'redirect_url' => $redirectUrl, 'isPaused'=> true], 200);
                }

            }

        }
        $categories = Category::where('is_sub', true)->get();

        return view('vendor.voyager.eco-brandables.add', ['categories' => $categories]);
    }


    /**
 * edit_brandable method
 *
 * id Request $request
 * @return void
 */
public function edit_brandable(Request $request, $id)
{
    $brandable = Brandable::where('id', $id)->first();
    $categories = Category::where('is_sub', true)->get();

    if ($request->isMethod('post')) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:2|unique:eco_brandables,name,' . $id,
            'price' => 'required|numeric',
            'description' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 500);

        } else {
            $data = $request->all();
            $brandable->name = $request->name;
            $brandable->user_id = Auth::user()->id;
            $brandable->price = $request->price;
            $brandable->description = $request->description;

            $front_data = json_decode($request->front_dimension, true);
            $back_data = $request->enable_back_view ? json_decode($request->back_dimension, true) : [];
           
            $data_full = [
                'backViewEnabled' => $request->enable_back_view ? true : false,
                'sizeEnabled' => $request->enable_sizes ? true : false,
                'description' => $request->description,
                'price' => $request->price,
                'name' => $request->name,
                'category' => $request->category,
                'front' => [
                    'left' => $front_data['left'],
                    'top' => $front_data['top'],
                    'width' => $front_data['width'],
                    'height' => $front_data['height'],
                    'image_url' => '',
                ],
                'back' => !$request->enable_back_view ? [] : [
                    'left' => $back_data['left'],
                    'top' => $back_data['top'],
                    'width' => $back_data['width'],
                    'height' => $back_data['height'],
                    'image_url' => '',
                ],

            ];

            if ($request->hasFile('front_image') && $request->file('front_image')->isValid()) {
                $front_image_path = Storage::putFile('brandables', $request->file('front_image'));
                $brandable->front_image = Storage::url($front_image_path);
                $brandable->front_image_path = $front_image_path;
                $data_full['front']['image_url'] = Storage::url($front_image_path);
            }else{
                $data_full['front']['image_url'] = $brandable->front_image;
            }

            if ($request->hasFile('back_image') && $request->file('back_image')->isValid()) {
                $back_image_path = Storage::putFile('brandables', $request->file('back_image'));
                $brandable->back_image = Storage::url($back_image_path);
                $brandable->back_image_path = $back_image_path;
                $data_full['back']['image_url'] = Storage::url($back_image_path);
            }else{
                $data_full['back']['image_url'] = $brandable->back_image;
            }

            $brandable->data = json_encode($data_full, true);


            if ($brandable->save()) {
                $redirectUrl = action([BrandableCtrl::class, 'index']);
                return response()->json(['message' => $data_full, 'redirect_url' => $redirectUrl, 'isPaused'=> true], 200);
            }

        }

    }

    return view('vendor.voyager.eco-brandables.edit', ['brandable' => $brandable, 'categories' => $categories]);
}


    /**
 * delete_brandable method
 *
 * id Request $request
 * @return void
 */
public function delete_brandable(Request $request, $id)
{
    $brandable = Brandable::where([['id', $id]])->first();
   
    if(Storage::exists($brandable->front_image_path)){
        Storage::delete($brandable->front_image_path);
    }
    if(Storage::exists($brandable->back_image_path)){
        Storage::delete($brandable->back_image_path);
    }
   
    if ($brandable->delete()) {
        return back()->with('success', 'Product has been deleted.');
    }

}

}
