<?php
return [
    'permissions' => [
        'User' => [
            'add_user' => 'Can Edit User',
            'view_user' => 'Can View Users',
            'delete_user' => 'Can Delete Users',
            'edit_user' => 'Can Edit Users',
        ],
    'Configuration' => [
            'edit_config' => 'Can Edit Configurations',
        ],
        
    'Media' => [
            'upload_media' => 'Can Upload Image',
        ],

    ],

    'facebook' => [
        'client_id' => env('APP_ENV') == 'production' ? '2018129491839312' : '1631923760229181',
       'client_secret' => env('APP_ENV') == 'production' ? 'd48b77ce895dddb5af8c3cd5486f368a' : 'e3494d79126fa7fcf9dd689c8b860257',
       'redirect' => env('APP_ENV') == 'production' ? 'https://suvenia.com/auth/social/callback/facebook' : 'http://127.0.0.1:8000/auth/social/callback/facebook',
       'redirect_seller' => env('APP_ENV') == 'production' ? 'https://suvenia.com/seller/social/callback/facebook' : 'http://127.0.0.1:8000/seller/social/callback/facebook',
       'redirect_designer' => env('APP_ENV') == 'production' ? 'https://suvenia.com/designer/social/callback/facebook' : 'http://127.0.0.1:8000/designer/social/callback/facebook'
       ],
   
       'linkedin' => [
       'client_id' => env('APP_ENV') == 'production' ? '777x8qm699ig57' : '77byjm4kt8d9xu',
       'client_secret' => env('APP_ENV') == 'production' ? 'ZzMsOVHb5y5gUNcY': 'hFw6rCrdNWJLJnFA',
       'redirect' => env('APP_ENV') == 'production' ? 'https://suvenia.com/auth/social/callback/linkedin' : 'http://127.0.0.1:8000/auth/social/callback/linkedin',
       ],
   
       'twitter' => [
       'client_id' => env('APP_ENV') == 'production' ? 'sziagftBUdOCExOwdyAIazE3Z' : 'rBZ587iutGKUF1czNOhk14nts',
       'client_secret' => env('APP_ENV') == 'production' ? 'sBG0ZX6Zfuv1c6il5bH7XBIZXgkgkfjpq3VdszhYUnZTUxrP9o' : '9JBQx2BGEQpCp527l9e2opJghZPT0kxxL6W4kNKO33AO0AzPbo',
       'redirect' => env('APP_ENV') == 'production' ? 'https://suvenia.com/auth/social/callback/twitter' : 'http://127.0.0.1:8000/auth/social/callback/twitter',
       'redirect_seller' => env('APP_ENV') == 'production' ? 'https://suvenia.com/seller/social/callback/twitter' : 'http://127.0.0.1:8000/seller/social/callback/twitter',
       'redirect_designer' => env('APP_ENV') == 'production' ? 'https://suvenia.com/designer/social/callback/twitter' : 'http://127.0.0.1:8000/designer/social/callback/twitter'
       ],
       'instagram'=> [
           'client_id'=> 'a20357e8d2e64a8788bf27c6765c534e',
           'client_secret'=> 'ac01920471394dc88121077483f7e841',
           'redirect' => env('APP_ENV') == 'production' ? 'https://suvenia.com/dashboard/influencer/social/link/instagram' : 'http://127.0.0.1:8000/dashboard/influencer/social/link/instagram',
           'redirect_seller' => env('APP_ENV') == 'production' ? 'https://suvenia.com/seller/social/callback/instagram' : 'http://127.0.0.1:8000/seller/social/callback/instagram',
       'redirect_designer' => env('APP_ENV') == 'production' ? 'https://suvenia.com/designer/social/callback/instagram' : 'http://127.0.0.1:8000/designer/social/callback/instagram'
       ]
]; 
