<?php
namespace App\Http\Controllers;

use App\Analytic as Analytic;
use App\Faq as Faq;
use App\Http\Controllers\Controller;
use App\Order as Order;
use App\OrderItem as OrderItem;
use App\OrderMetric as OrderMetric;
use App\OrderShipping as OrderShipping;
use App\Payment as Payment;
use App\Product;
use App\ProductCategory as Category;
use App\Review as Review;
use App\SavedItem as SavedItem;
use App\SellerOrder as SellerOrder;
use App\ShippingAddress as Shipping;
use App\ShippingLocation as Location;
use App\ShippingZone as Zone;
use App\Store as Store;
use App\Coupon as Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Agent;
use LaraCart;
use Victorybiz\GeoIPLocation\GeoIPLocation;

use App\Repo\Cart\CartInterface;
use App\Repo\Shipping\ShippingInterface;
use App\Repo\Order\OrderInterface;

class ShopCtrl extends Controller
{

    protected $cart;

    protected $order;

    protected $shipping;

    public function __construct(
        CartInterface $cart,
        ShippingInterface $shipping,
        OrderInterface $order
    ){
        $this->orders = $order;
        $this->cart = $cart;
        $this->shipping = $shipping;
    }

    public function product_search(Request $request)
    {
        if ($request->ajax()) {
            $products = Product::with(['photos', 'categories'])
                ->where('name', 'like', $request->q . '%')
                ->Orwhere('slug', 'like', $request->q . '%')
                ->Orwhere('description', 'like', $request->q . '%')
                ->limit(5)
                ->get();
            return View::make('partials.search', ['products' => $products]);
        }

        if ($request->isMethod('post')) {
            return redirect()->route('app:ecommerce:product_search', ['q' => $request->q]);
        }

        if ($request->isMethod('get')) {
            $qry = Product::with(['photos', 'categories'])
                ->where('name', 'like', $request->q . '%')
                ->Orwhere('slug', 'like', $request->q . '%')
                ->Orwhere('description', 'like', $request->q . '%');

            $product_count = $qry->count();
            $products = $qry->paginate(10);
            return view('shop.search', ['products' => $products, 'product_count' => $product_count]);
        }

    }

    public function product_catlogue(Request $request, $param)
    {
        $category = Category::with(['children', 'products'])->where('slug', $param)->firstOrFail();
        $all_stores = Store::limit(20)->inRandomOrder()->get();
        $default_prods = [];
        $stores_prod = [];
        $color_prods = [];

        if ($request->q_store) {
            $store = Store::where('id', $request->q_store)->first();
            $ids = json_decode($store->meta, true);
            $f_qry = $category->products->whereIn('id', $ids['products'])->pluck('id');
            $stores_prod = $f_qry->all();
        }

        if ($request->q_col) {
            $products_with_color = [];
            $all_Prods = Product::with('design')->get();
            foreach ($all_Prods as $pro) {
                $allCols = json_decode(json_decode($pro->design->colors, true));
                $chosenColor = base64_decode($request->q_col);
                if (in_array($chosenColor, $allCols)) {
                    $products_with_color[] = $pro->id;
                }
            }
            $f_qry_col = $category->products->whereIn('id', $products_with_color)->pluck('id');
            $color_prods = $f_qry_col->all();
        }

        if (!$request->q_col && !$request->q_store) {
            $default_prods = $category->products->pluck('id')->all();
        }

        $productIds = array_unique(array_merge($color_prods, $stores_prod, $default_prods));

        $products = Product::with('photos')->whereIn('id', $productIds)->paginate(12);

        return view('shop.catalogue', ['category' => $category, 'products' => $products, 'stores' => $all_stores]);

    }

    public function single_product(Request $request, $category, $param)
    {
       // LaraCart::destroyCart();
        //echo json_encode(LaraCart::getItems());
        $category = Category::where('slug', $category)->first();
        $Sortref = explode('_', $param);
        $ref = end($Sortref);
        $product = Product::with('photos', 'user')->where('ref', $ref)->first();
        $this->__set_product_views($product, $request);
        $faqs = Faq::where('product_id', $product->id)->get();
        $reviews = Review::where('product_id', $product->id)->get();
        return view('shop.details', ['category' => $category, 'product' => $product, 'faqs' => $faqs, 'reviews' => $reviews]);
    }

    public function ask_question(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'question' => 'required|max:400|min:4',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {
                $faq = new Faq();
                $faq->product_id = $request->product_id;
                $faq->question = $request->question;
                $faq->save();
                return back()->with('success', 'Question saved successfully.');
            }
        }
    }

    public function favorite_product(Request $request, $task)
    {
        if ($request->isMethod('get')) {
            if ($task == 'add') {
                $saved = new SavedItem();
                $saved->product_id = $request->id;
                // $saved->user_id = Auth::user()->id;
                $saved->user_id = Auth::guard('buyers')->user()->id;
                $saveStatus = SavedItem::where('product_id', $request->id)->where('user_id', $saved->user_id)->first();
                if(!$saveStatus){
                $saved->save();
                return back()->with('success', 'Item saved successfully.');
                }else{
             return back()->with('error', 'Item has already been saved.');
            }
         }
            if ($task == 'rem') {
                $saved = SavedItem::where('product_id', $request->id)->first();
                $saved->delete();
            }

        }
    }


    public function __set_product_views($product, $request)
    {
        $analytics = Analytic::where([['category', 'PRODUCTS'], ['client_id', $request->ip()], ['path', url()->full()]]);
        $agent = new Agent();
        $geoip = new GeoIPLocation();

        if ($analytics->get()->count() < 1) {
            $analytic = new Analytic();
            $analytic->user_id = Auth::check() ? Auth::user()->id : '';
            $analytic->client_id = $request->ip();
            $analytic->device = $agent->device();
            $analytic->lang = json_encode($agent->languages());
            $analytic->platform = $agent->platform();
            $analytic->browser = $agent->browser();
            $analytic->model_id = $product->id;
            $analytic->category = 'PRODUCTS';
            $analytic->country = $geoip->getCountry();
            $analytic->page_name = 'ng';
            $analytic->path = url()->full();
            $analytic->is_mobile = !$agent->isPhone() ? false : $agent->isPhone();
            $analytic->save();

        }
    }

    public function buy_direct(Request $request, $code)
    {
        if ($code) {
            $__data = json_decode(base64_decode($code), true);
            if (array_key_exists('id', $__data) && !is_null($__data['id'])) {
                $product = Product::with(['photos', 'user', 'brandable', 'design'])->where('id', $__data['id'])->first();
                //print_r($product);
                LaraCart::addLine(
                    $product->id,
                    $product->name,
                    $__data['qty'],
                    $product->brandable->price + $product->design->total_earning,
                    [
                        "size" => $__data['size'],
                        "color" => $__data['color'],
                    ],
                    $taxable = false
                );

                return redirect()->route('app:ecommerce:step_shipping');

            }

        } else {
            return back()->with('error', 'Invalid direct purchase link!');
        }
    }

    /**
     * Adds a single product to cart
     *
     * @param Request $request
     * @param Integer $id
     * @return void
     */
    public function add_to_cart(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $data = $this->cart->add_to_cart($id);
            $count = $this->cart->count_cart_items();
            return response()->json(['message' => $data['message'], 'cart_count' => $count, 'redirect_url' => route('app:ecommerce:cart_review')], 200);
        }
    }

    public function cart_review(Request $request)
    {
       //Laracart::destroyCart();
        //LaraCart::removeCoupons();
        $request->session()->put('CURRENT_COUPON_CODES', json_encode([]));
        return view('shop.cart_step.cart_review');
    }

    public function apply_coupon(Request $request)
    {
        //LaraCart::removeCoupons();

        if($request->isMethod('post')){
            if(empty($request->coupon_code)){
                return response()->json(['errors'=> 'You must provide a Coupon Code!'], 500);
            }
            $verify_coupon = Coupon::where([['name', $request->coupon_code], ['is_closed', false]]);
            if(!$verify_coupon->first()){
                return response()->json(['errors'=> 'Invalid Coupon Code!'], 500);
            }else{
                $dd = $verify_coupon;
                $check_if_used = DB::table('eco_user_coupon')->where([['user_id', Auth::user()->id], ['coupon_id', $dd->first()->id]]);
                if(!$verify_coupon->first()){
                    return response()->json(['errors'=> 'Oops! You have used this Coupon Code!'], 500);
                }else{
                    $coupon = $verify_coupon->first();
                    //$coupon_array = json_decode(session('CURRENT_COUPON_CODES'));
                    /*$coupon_array[] = [
                        'id'=> $coupon->product_id,
                        'code'=> $coupon->name,
                        'value'=> $coupon->amount,
                    ];*/

                    //$allcoupon = array_merge($coupon_array, $coupon_array_new);
                    //$total_amount = collect($coupon_array)->sum('value');

                   $couponInst = new \LukePOLO\LaraCart\Coupons\Fixed($coupon->name, $coupon->amount,  [
                       'product_id'=> $coupon->product_id,
                       'coupon_name'=> $coupon->name,
                       'coupon_id'=> $coupon->id
                    ]);
                    LaraCart::addCoupon($couponInst);
                   // print_r($coupon->name);
                    return response()->json(['message'=> 'Coupon added!', 'redirect_url'=> route('app:ecommerce:cart_review')], 200);
                }
            }
        }
    }

    public function delete_cart_item(Request $request, $hash)
    {
        if ($request->isMethod('post')) {
            $this->cart->delete_from_cart($hash);
            return back()->with('success', 'Item removed from cart');
        }
    }

    public function update_cart_item(Request $request, $hash)
    {
        if ($request->isMethod('post')) {
            $this->cart->update_cart($hash);
            return back()->with('success', 'Item updated successfully!');
        }
    }

    public function step_shipping(Request $request)
    {
        $zones = Zone::with('addresses')->get();
        return view('shop.cart_step.shipping', [
            'zones' => $zones,
        ]);
    }

    public function add_address_zone_modal(Request $request, $id)
    {
        $states = Location::where('zone_id', $id)->get();
        return View::make('shop.cart_step.snippet.zone_address', ['id' => $id, 'states' => $states]);
    }

    public function edit_address_zone_modal(Request $request, $id, $address_id)
    {
        $states = Location::where('zone_id', $id)->get();
        $address = Shipping::where('id', $address_id)->first();
        return View::make('shop.cart_step.snippet.edit_zone_address', ['id' => $id, 'states' => $states, 'address_id' => $address_id, 'address' => $address]);
    }

    public function set_default_zone_modal(Request $request, $id)
    {
        $addresses = Shipping::where('zone_id', $id)->get();
        return View::make('shop.cart_step.snippet.select_address', ['addresses' => $addresses, 'id' => $id]);
    }

    public function add_shipping_address(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 500);
            } else {

                $this->shipping->add_address($request, $id);

                return response()->json(['message' => 'Shipping details saved successfully', 'redirect_url' => url()->previous()], 200);

            }

        }
    }

    public function edit_shipping_address(Request $request, $id, $address_id)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 500);
            } else {

                $this->shipping->edit_address($request, $id, $address_id);

                return response()->json(['message' => 'Shipping details updated successfully', 'redirect_url' => url()->previous()], 200);

            }

        }
    }

    public function delete_shipping_address(Request $request, $id)
    {
        $this->shipping->delete_address($id);
        return back()->with('success', 'Address deleted sucessfully!');
    }

    public function select_shipping_address(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $this->shipping->select_address($id);
            return response()->json(['message' => 'Changes saved successfully', 'redirect_url' => url()->previous()], 200);
        }
    }

    public function select_zone(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $shipping = Shipping::where([['zone_id', $id], ['is_default', true]])->first();
            //$checkAddress = Shipping::where([['zone_id', $id]])->count();

            $count = LaraCart::count();
            $shippingPrice = 0;
            if (isset($shipping->location_id)) {
                $location = Location::where('id', $shipping->location_id)->first();

                if (isset($location) && count(json_decode($location->data, true)) > 0) {
                    foreach (json_decode($location->data, true) as $key => $value) {
                        $range = explode(",", $key);
                        $low = intval($range[0]);
                        $high = intval($range[1]);
                        if ($this->nBetween($count, $high, $low)) {
                            $shippingPrice = $value;
                        }
                    }
                }
                $request->session()->put('CartShippingAddressID', $shipping->id);
            } else {
                $request->session()->put('CartShippingAddressID', 0);
            }

            $request->session()->put('CartShippingPrice', $shippingPrice);
            $request->session()->put('CartZoneID', $id);
            Zone::where('is_default', true)->update(['is_default' => false]);
            $zone = Zone::where('id', $id)->first();
            $zone->is_default = true;
            $zone->save();

            return response()->json(['message' => 'Shipping Selected Successfuly!', 'redirect_url' => url()->previous()], 200);
        }
    }

    public function nBetween($varToCheck, $high, $low)
    {
        if ($varToCheck < $low) {
            return false;
        } elseif ($varToCheck > $high) {
            return false;
        } else {
            return true;
        }

    }

    public function process_shipping_price_for_payment(Request $request)
    {
        if ($request->isMethod('post')) {
            //$request->session()->pull('CartZoneID');
            $zone = Zone::where('id', $request->__zone_id)->first();
            if (!$zone) {
                return back()->with('error', 'Please select a shipping zone.');
            } else {
                $location = Shipping::where([['zone_id', $zone->id], ['is_default', true]])->first();
                if ($zone->has_option && !isset($location)) {
                    return back()->with('error', 'Please select or add an address for your shipping zone.');
                }
                if (!$request->session()->exists('CartZoneID')){
                    $request->session()->put('CartZoneID', $zone->id);
                }

            }

            return redirect()->route('app:ecommerce:step_pay');
        }
    }

    public function step_pay(Request $request)
    {
        if ($request->session()->exists('CartZoneID')) {
            $zone = Zone::where('id', session('CartZoneID'))->first();
        } else {
            return back()->with('error', 'Invalid Request!');
        }
        return view('shop.cart_step.pay', ['zone' => $zone]);
    }

    public function process_payment(Request $request, $type)
    {
        if ($request->isMethod('post')) {
            $order_id = 'SUV-' . str_random(10);
            $total_amount = LaraCart::total($formatted = false) + session('CartShippingPrice');

            try {
                $decrypted_type = decrypt($type);
            } catch (DecryptException $e) {
                return back()->with('error', 'Invalid token!');
            }
            //TODO ORDER: SAVE ORDER METRICS HERE...

            if ($decrypted_type == 'PAYSTACK') {
                $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
                try
                {
                    $tranx = $paystack->transaction->initialize([
                        'amount' => $total_amount * 100, // in kobo
                        'email' => Auth::user()->email, // unique to customers
                        'reference' => $order_id, // unique to transactions
                    ]);
                    // $order = $this->order->quick_order($total_amount, $order_id);
                    $order = $this->orders->quick_order($total_amount, $order_id);
                    event(new \App\Events\Shop\ProductOrdered($order));

                } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                    die($e->getMessage());
                }
                return redirect($tranx->data->authorization_url);
            } elseif ($decrypted_type == 'PAYLATER') {
                // $order = $this->order->quick_order($total_amount, $order_id);
                $order = $this->orders->quick_order($total_amount, $order_id);
                event(new \App\Events\Shop\ProductOrdered($order));
                return redirect()->route('app:ecommerce:payment_notify_status', ['status' => encrypt($order_id)]);
            }

        }

    }

    public function complete_paylater_payment(Request $request, $id){
        if($request->isMethod('POST')){
            $order = Order::where('id', $id)->first();
            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
            try
            {
                $tranx = $paystack->transaction->initialize([
                    'amount' => $order->price * 100, // in kobo
                    'email' => Auth::user()->email, // unique to customers
                    'reference' => $order->order_ref, // unique to transactions
                ]);

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                die($e->getMessage());
            }
            return redirect($tranx->data->authorization_url);
        }
    }

    public function payment_verify(Request $request)
    {
        if ($request->reference) {
            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
            try {

                $tranx = $paystack->transaction->verify([
                    'reference' => $request->reference, // unique to transactions
                ]);
                if ('success' === $tranx->data->status) {
                    $order = Order::where('order_ref', $request->reference)->first();
                    event(new \App\Events\Shop\ProductOrderCompleted($order));
                    return redirect()->route('app:ecommerce:payment_notify_status', ['status' => encrypt($order->order_ref)]);
                }
            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                // return redirect()->route('shop:cart_payment_error');
            }
        }
    }

    public function payment_notify(Request $request, $status)
    {
        if (!$status) {
            // return redirect()->route()->with('error', 'Invalid Link!');
            return back()->with('error', 'Invalid Link!');
        } else {
            try {
                $pid = decrypt($status);
            } catch (DecryptException $e) {
                // return redirect()->route()->with('error', 'Invalid Link!');
                return back()->with('error', 'Invalid Link!');
            }
            LaraCart::destroyCart();
            $request->session()->forget('CartZoneID');
            $request->session()->forget('CartShippingPrice');
            $request->session()->forget('CartShippingAddressID');
            $payment = Payment::where('reference', $pid)->first();
        }
        return view('shop.cart_step.payment_notify', ['payment' => $payment]);
    }

}
