<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrderCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\OrderDetails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class SendMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrderCompleted  $event
     * @return void
     */
    public function handle(ProductOrderCompleted $event)
    {
        Mail::to(Auth::user()->email)->send(new OrderDetails($event->order));
    }
}
