<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerOffer extends Model
{

    public function gig()
    {
        return $this->belongsTo("App\DesignerGig", "gig_id");
    }

}
