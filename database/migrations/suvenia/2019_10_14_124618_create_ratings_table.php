<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ratings', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('rating');
			$table->string('ratingable_type', 191);
			$table->bigInteger('ratingable_id')->unsigned();
			$table->string('author_type', 191);
			$table->bigInteger('author_id')->unsigned();
			$table->timestamps();
			$table->index(['author_type','author_id']);
			$table->index(['ratingable_type','ratingable_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ratings');
	}

}
