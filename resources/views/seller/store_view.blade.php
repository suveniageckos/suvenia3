@extends('layouts.store_view')

@php
$store_meta = json_decode($store->meta, true);
$store_products = isset($store_meta['products']) ? $store_meta['products'] : [] ;
@endphp

@section('content')
    <div class="uk-container uk-container-expand" style="padding:0;">
        <div style="position: relative;">
            {{-- @if(!empty($store_meta['store_upload_banner']) || !empty($store_meta['store_banner_system']))
        <img style="height:500px;" src="{{ (isset($store_meta['default_banner']) && $store_meta['default_banner'] == 'SYSTEM') ? asset($store_meta['store_banner_system']) :   asset($store_meta['store_upload_banner']) }}" class="uk-width-1-1 img-banner" alt="" uk-responsive>
            @else
         <img style="height:500px;" src="{{ asset('img/default_banner.png') }}" class="uk-width-1-1" alt="" uk-responsive>
           @endif --}}

            @if(!empty($store_meta['store_upload_banner']) || !empty($store_meta['store_banner_system']))
            @if ($store_meta['default_banner'] == 'USER')
            <img src="{{ asset($store_meta['store_upload_banner']) }}" class="uk-width-1-1" alt="" uk-responsive>
            @else
            {{-- @elseif($store_meta['default_banner'] == 'SYSTEM') --}}
            <img src="{{ asset($store_meta['store_banner_system']) }}" class="uk-width-1-1 img-banner" alt="" uk-responsive>
           @endif
           @else
           <img style="height:500px;" src="{{ asset('img/default_banner.png') }}" class="uk-width-1-1" alt="" uk-responsive>
           @endif

           <div class="banner-desc" style="padding: 15px">
            <p style="text-align:justify;">{{ str_limit($store->description, 200, '...') }}</p>
            <p><a href="{{ route('app:base:store_view', ['slug'=> $store->slug]) }}" class="btn btn-secondary" style="font-size:1em; color:seashell; padding-buttom:30px;">SHOP NOW</a></p>
            </div>
        </div>
    </div>

    <div class="uk-container uk-container-medium">
        <div class="row mt-3">
            @php $products = \App\Product::whereIn('id', $store_products)->paginate(12); @endphp
            @forelse($products as $product)
            <div class="col-md-3">
                @include('partials.single_product', ['product'=> $product])
            </div>
            @empty
            <div class="uk-align-center ">
                <p>You have not added any product(s) to your store</p>
                <img src="{{asset('img/cart_store.png')}}" width="150">
            </div>
            @endforelse
        </div>
       <div class="row justify-content-center align-items-center">{!! $products->render() !!}</div>
    </div>


    <!-- Footer -->
<footer class="page-footer font-small special-color-dark pt-4">
<hr>

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3 ">
        <a href="{{ $store->facebook}}" target="_blank" class="btn btn-info" style="color:white;" ><span uk-icon="icon: facebook"></span></a>
        <a href="{{ $store->twitter}}" target="_blank" class="btn btn-info" style="color:white;" ><span uk-icon="icon: twitter"></span></a>
        <a href="{{ $store->instagram}}" target="_blank" class="btn btn-info" style="color:white;" ><span uk-icon="icon: instagram"></span></a>


    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->

@endsection
