<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInfluencerSocialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('influencer_social', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('influencer_id');
			$table->integer('social_id');
			$table->text('link', 65535);
			$table->boolean('is_default');
			$table->integer('followers')->nullable();
			$table->integer('post_count')->nullable();
			$table->integer('engagements')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('influencer_social');
	}

}
