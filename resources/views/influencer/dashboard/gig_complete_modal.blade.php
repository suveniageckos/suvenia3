<div class="row">
    <div class="col-md-12">
    <form action="{{ route('app:influencer:dashboard:gig_complete', ['id'=> $gig->id]) }}" method="post" data-post>
            <div class="form-group row job_url">
                <label class="col-sm-3 col-form-label font-weight-bold" style="font-size:14px;">Instagram Link</label>
                <div class="col-sm-9">
                    <input type="url" name="job_url" placeholder="Job Url" class="form-control">
                </div>
                <small class="help-block ml-4 mt-3"></small>
            </div>
            <div class="form-group uk-text-center mt-4">
                <button type="submit" class="btn btn-info btn-sm">SUBMIT</button>
            </div>
        </form>
    </div>
</div>