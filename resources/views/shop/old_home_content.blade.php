
<div class="uk-position-relative uk-visible-toggl uk-light" uk-slideshow="min-height: 236; max-height: 236; animation: push">

        <ul class="uk-slideshow-items">
            <li>
                <a href="">
                <img src="{{ $utils->get_image('site.hpadsbanner1') }}" alt="" uk-responsive>
                </a>
            </li>
            
            <li>
                <a href="">
                <img src="{{ $utils->get_image('site.hpadsbanner2') }}" alt="" uk-responsive>
                </a>
            </li>
            
        </ul>
    
        <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hove" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class=" uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hove" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
    
</div>

<div class="uk-section p-4">
        @php $_in_season = \App\Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->get(); @endphp

        <div class="uk-container uk-container-small">
            <div class="top-gear" style="position: relative; border:0px solid red;">
            <div class="app-sec-header sec-left">
                <h1 class="title">drinkware</h1>
                <div class="sub-title">For Your Home and Office</div>
            </div>
            <div style="position: absolute; right:0; bottom:0;"><a href="" class="see_link">SEE ALL</a></div>
            </div>

            <div class="row mt-4">

                @foreach($_in_season->take(4) as $product)
                <div class="col-md-3">
                    @include('partials.single_product', ['product'=> $product])
                </div>
                @endforeach

            </div>

        </div>

</div>

<div class="uk-section p-4">
        @php $_in_season = \App\Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->get(); @endphp

        <div class="uk-container uk-container-small">
            <div class="top-gear" style="position: relative; border:0px solid red;">
            <div class="app-sec-header sec-left">
                <h1 class="title">apparrel</h1>
                <div class="sub-title">For Your Trendy Look</div>
            </div>
            <div style="position: absolute; right:0; bottom:0;"><a href="" class="see_link">SEE ALL</a></div>
            </div>

            <div class="row mt-4">

                @foreach($_in_season->take(4) as $product)
                <div class="col-md-3">
                    @include('partials.single_product', ['product'=> $product])
                </div>
                @endforeach

            </div>

        </div>

</div>

<div class="uk-margin-top">
<a href="" target="_blank">
        <img src="{{ $utils->get_image('site.hpcreatedesign') }}" class="uk-width-1-1" alt="" uk-responsive>
    </a>
</div>
 
<div class="uk-section bg-white p-4">
        @php $_in_season = \App\Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->get(); @endphp

        <div class="uk-container uk-container-small">
            <div class="app-sec-header sec-center">
                <h1 class="title">celebrations</h1>
                <div class="underline"></div>
            </div>

            <div class="row mt-4">

                @foreach($_in_season->take(4) as $product)
                <div class="col-md-3">
                    @include('partials.single_product', ['product'=> $product])
                </div>
                @endforeach

            </div>

        </div>

</div>
