
@extends('layouts.dashboard_seller')

@section('title', 'All Stores' )

@push('PAGE_STYLES')

@endpush

@section('content')

        <div class="row border-bottom">
        <div class="col-md-9">
            <h2 class="dash-heading">My Stores</h2>
        </div>
        <div class="col-md-3 pb-2">
            <button class="btn btn-app-info btn-lg button-format-style" uk-toggle="target: #modal-close-default">CREATE NEW STORE</button>
        </div>

        </div>

        <div class="row bg-white p-4">
            @forelse($stores as $store)
            @php
            $store_meta = json_decode($store->meta, true);
          // print_r($store_meta);
            @endphp
            <div class="col-md-4 uk-margin-small-top uk-margin-small-bottom">
                <div  class="card uk-inline-clip uk-transition-toggle" tabindex="0">
                    <div class="card-body" style="height: 350px;">
                        @if($store_meta['default_brand'] == 'IMAGE')
                        <img src="{{ !is_null($store_meta['store_logo']) ? asset('store_img/' . $store_meta['store_logo']) : asset('store_img/store.png') }}"  uk-responsive width="150" class="uk-position-center">
                        @endif
                        @if($store_meta['default_brand'] == 'TEXT')
                            <h3 class="uk-position-center">{{ empty($store_meta['store_logo_text']) ? 'LOGO HERE' : $store_meta['store_logo_text'] }}</h3>
                        @endif
                    </div>
                    <div class="uk-transition-slide-bottom uk-position-cover  uk-overlay uk-overlay-primary">
                        <ul class="mt-3 mr-2 uk-position-top-right">
                            <li class="d-inline-block"><a href="{{ route('app:seller:dashboard:edit_store', ['id'=> $store->id ]) }}" class="mr-1" style="color: #fff;"><span class="mr-1" uk-icon="file-edit" style="font-size:13px;">Edit</span></a></li>

                            <li class="d-inline-block"><a data-url="{{ route('app:seller:dashboard:delete_store', ['id'=> $store->id ]) }}" class="data-delete-item" style="color: #fff;" href=""><span class="mr-1" uk-icon="trash" style="font-size:13px;">Delete</span></a></li>
                        </ul>
                    <div class="uk-position-center"><a href="{{ route('app:base:store_view', ['slug'=> $store->slug ])}}" target="_blank" class="btn btn-outline-light btn-radius" style="padding:15px 50px 15px 50px; font-size:1.3em;">Visit Store</a></div>
                    </div>
                </div>
                <p class="text-store-product-count m-0">{{ $store->products->count() . ' ' . str_plural('Product', $store->products->count()) }}</p>
                <p class="text-store-product text-format-big-font m-0 ">{{ $store->name }}</p>
            {{-- <p class="m-0"><a href="javascript:;" class="text-store-product-link" data-url="{{ route('app:seller:dashboard:fetch_promote_store_form', ['id'=> $store->id]) }}" modal-form data-title="Promote Store">Promote Store</a></p> --}}
            <a href="#" class="text-store-product-link" uk-toggle="target: #modal-close-default2">Promote Store</a>
            </div>
            @empty
            <div class="col-md-12 uk-text-center">
                <p>You do not have any stores</p>
            </div>
            @endforelse
        </div>

{{-- Create Store --}}
<div id="modal-close-default" uk-modal>
    <div class="uk-modal-dialog uk-modal-body big-dialog">
        <div class=" uk-modal-header">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h4 class="uk-modal-title" style="font-size: 20px; font-weight: 600;">Create Store</h4>
        </div>
        <div class="">

            <div class="row">
            <div class="col-sm-7">

            <div class="uk-card uk-card-default uk-card-body uk-width-1-0@m">
                <form action="{{ route('app:seller:dashboard:create_store') }}" method="POST" data-post>
                    @csrf
                    <div class="form-group name">
                    <label for="">Store Name</label>
                    <input type="text" class="form-control textfield-format text-border-radius" id="tb1" placeholder="Store Name" name="name">
                    <p class="help-block m-0"></p>
                    </div>
                    <div class="form-group">
                    <label for="">Your store URL</label>
                    <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text static_field" id="basic-addon1">https://suvenia.com/store/</span>
                            </div>
                            <input type="text" class="form-control textfield-format store_text_format text-border-radius" id="tb2" name="store_url"  placeholder="store url" readonly aria-describedby="basic-addon1">
                          </div>
                          <p class="help-block m-0"></p>
                    </div>

                    <div class="form-group description">
                        <label for="store-description">Store Description</label>
                        <textarea class="uk-textarea text-border-radius" name="description" rows="4" placeholder="Write something about your store"></textarea>
                        <p class="help-block m-0"></p>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-app-info button-format-style">CREATE STORE</button>
                    </div>
                </form>
            </div>
            </div>
            <div class="col-sm-5">
                <div class="uk-margin-medium-top">
                <div class="uk-text-large uk-nav-center tip-text-style">TIPS</div>
                <br>
                <div class="row uk-margin-bottom">
                <div class="col-2">
                <span class="uk-badge uk-border-circle badge-large circle_format_style">1</span>
                </div>
                <div class="col-10">
                <span>Provide your store name and click “Create store” </span>
                </div>
                </div>
                <div class="row uk-margin-bottom">
                <div class="col-2">
                <span class="uk-badge uk-border-circle badge-large circle_format_style">2</span>
                </div>
                <div class="col-10">
                <span>Once the name is available your are redirected to personalize your store. </span>
                </div>
                </div>
                <div class="row uk-margin-bottom">
                <div class="col-2">
                <span class="uk-badge uk-border-circle badge-large circle_format_style">3</span>
                </div>
                <div class="col-10">
                <span>Create products for your store or add already created products. </span>
                </div>
                </div>
                <div class="row uk-margin-bottom">
                <div class="col-2">
                <span class="uk-badge uk-border-circle badge-large circle_format_style">4</span>
                </div>
                <div class="col-10">
                <span>You need to create at least four (4) products for your store to go live. </span>
                </div>
                </div>
                <div class="row uk-margin-bottom">
                <div class="col-2">
                <span class="uk-badge uk-border-circle badge-large circle_format_style">5</span>
                </div>
                <div class="col-10">
                <span>Your store is free. Each time your product sells you make money. That easy! </span>
                </div>
                </div>
                </div>
            </div>
            </div>
            </div>

        </div>
        </div>


{{-- Promote Store --}}
        <div id="modal-close-default2"  uk-modal>
                <div class="uk-modal-dialog uk-modal-body">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <h4 class="uk-modal-title" style="font-size: 20px; font-weight: 600;">Coming Soon!</h4>
                    <div class="">
                        <p>Promote Store is under contruction and will soon be activated. But you can promte your individual products through your product page.</p>
                    </div>
                    </div>
        </div>

@endsection
