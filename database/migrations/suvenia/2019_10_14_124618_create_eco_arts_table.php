<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoArtsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_arts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('ref', 191);
			$table->string('parent_ref', 191)->nullable();
			$table->boolean('is_parent')->nullable();
			$table->text('photo_url', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_arts');
	}

}
