@extends('layouts.store_view')
@php
$store_meta = json_decode($store->meta, true);
$store_products = isset($store_meta['products']) ? $store_meta['products'] : [] ;
@endphp

@section('content')
    {{-- <div class="uk-container uk-container-expand" style="padding:0;">
        <div style="position: relative;">
            @if($store_meta['default_banner'])
                <img style="height:500px;" src="{{ (isset($store_meta['default_banner']) && $store_meta['default_banner'] == 'SYSTEM') ? asset($store_meta['store_banner_system']) : asset('store_img/' . $store_meta['store_upload_banner']) }}" class="uk-width-1-1" alt="" uk-responsive>
            @else
         <img style="height:500px;" src="{{ asset('img/default_banner.png') }}" class="uk-width-1-1" alt="" uk-responsive>
       @endif
        </div>
    </div> --}}

    <div class="uk-container uk-container-medium uk-grid-match" uk-grid>
            <div>
                <div class="uk-card uk-card-body uk-margin-large-top">
                    <h3 class="uk-card-title">About Our Store</h3>
                    <p>{{$store->description}}</p>
                </div>
            </div>
    </div>



    <!-- Footer -->
<footer class="page-footer font-small special-color-dark pt-4">
<hr>

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <a href="{{ $store->facebook}}" target="_blank" class="btn btn-info" style="color:white;" ><span uk-icon="icon: facebook"></span></a>
        <a href="{{ $store->twitter}}" target="_blank" class="btn btn-info" style="color:white;" ><span uk-icon="icon: twitter"></span></a>
        <a href="{{ $store->instagram}}" target="_blank" class="btn btn-info" style="color:white;" ><span uk-icon="icon: instagram"></span></a>


    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->

@endsection
