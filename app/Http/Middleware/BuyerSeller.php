<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class BuyerSeller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('buyers')->check()) {
            return $next($request);
        }elseif(Auth::guard('sellers')->check()){
            return $next($request);
        }else{
            return redirect()->route('app:base:index')->with('error', 'You do not have access to the requested page!.');
        }
        
    }
} 
