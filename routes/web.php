<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['prefix' => 'admin', 'namespace'=> 'Admin'], function () {
    Route::match(['post', 'get'], '/eco-brandables/add', 'BrandableCtrl@add')->name('admin:brandable:add');
    Route::match(['post', 'get'], '/eco-brandables/{id}/edit', 'BrandableCtrl@edit_brandable')->name('admin:brandable:edit');
    Route::match(['post', 'get'], '/eco-brandables/{id}/delete', 'BrandableCtrl@delete_brandable')->name('admin:brandable:delete');

    Route::match(['post', 'get'], '/templates/add', 'TemplateCtrl@add')->name('admin:template:add');
    Route::match(['post', 'get'], '/templates/{id}/edit', 'TemplateCtrl@edit')->name('admin:template:edit');
    Route::match(['post', 'get'], '/templates/{id}/delete', 'TemplateCtrl@delete')->name('admin:template:delete');
    //influencers
    Route::match(['post', 'get'], '/inlfuencer/{id}/confirm', 'InfluencerCtrl@confirm')->name('admin:influencer:confirm');

    //sellers
    Route::match(['post', 'get'], '/sellers/transfer', 'SellerCtrl@transfer')->name('admin:seller:transfer');

    //Product Blocks
    Route::match(['post', 'get'], '/product-blocks/add', 'ProductBlockCtrl@add')->name('admin:product_block:add');
    Route::match(['post', 'get'], '/product-blocks/edit/{id}', 'ProductBlockCtrl@edit')->name('admin:product_block:edit');
    Route::match(['post', 'get'], '/product-blocks/delete/{id}', 'ProductBlockCtrl@delete')->name('admin:product_block:delete');

    //Stores We Like Blocks
    Route::match(['post', 'get'], '/stores-we-like/add', 'StoresWeLikeCtrl@add')->name('admin:stores_we_like_block:add');
    Route::match(['post', 'get'], '/stores-we-like/edit/{id}', 'StoresWeLikeCtrl@edit')->name('admin:stores_we_like_block:edit');
    Route::match(['post', 'get'], '/stores-we-like/delete/{id}', 'StoresWeLikeCtrl@delete')->name('admin:stores_we_like_block:delete');

    Voyager::routes();
});

include_once "customizer.php";
include_once "shop.php";
include_once "users.php";

include_once "guards/influencers.php";
include_once "guards/sellers.php";
include_once "guards/designers.php";

Route::group(['prefix' => 'app/chat', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/send/chat/{id}/{sender}', 'ChatCtrl@new_chat')->name('app:chat:new_chat');
    Route::match(['post', 'get'], '/all/{id}/{sender}', 'ChatCtrl@all_chats')->name('app:chat:all_chats');
    Route::match(['post', 'get'], '/update/chat/{id}/{sender}', 'ChatCtrl@update_chat')->name('app:chat:update_chat');
    Route::match(['post', 'get'], '/upload/chat-file/{id}/{sender}', 'ChatCtrl@upload_chat_file')->name('app:chat:upload_chat_file');
});


Route::group(['prefix' => 'pages', 'middleware' => ['web']], function() {
    // Route::match(['post', 'get'], '/terms', 'BaseCtrl@terms')->name('app:pages:terms');
    Route::get('/faqs', 'BaseCtrl@faqs')->name('app:pages:faqs');
    Route::get('/return_policy', 'BaseCtrl@return_policy')->name('app:pages:return-policy');
    Route::get('/terms', 'BaseCtrl@terms')->name('app:pages:terms');
    Route::get('/shipping', 'BaseCtrl@shipping')->name('app:pages:shipping');
    Route::get('/why_you_should_use_Suvenia_for_all_merchandising', 'BaseCtrl@about')->name('app:pages:about');
    Route::get('/contact', 'BaseCtrl@contact')->name('app:pages:contact');
});




Route::group(['prefix' => '', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/product/search', 'ShopCtrl@product_search')->name('app:ecommerce:product_search');
    Route::match(['post', 'get'], '/store/{slug}', 'DashboardCtrl@store_view')->name('app:base:store_view');
    Route::match(['post', 'get'], '/store/all/{slug}', 'DashboardCtrl@store_view_all')->name('app:base:store_view:all');
    Route::match(['post', 'get'], '/store/about/{slug}', 'DashboardCtrl@store_view_about')->name('app:base:store_view:about');
    Route::match(['post', 'get'], '/{category}/{param}', 'ShopCtrl@single_product')->name('app:ecommerce:single_product');
    Route::match(['post', 'get'], '/{category}', 'ShopCtrl@product_catlogue')->name('app:ecommerce:product_catlogue');

    //Route::match(['post', 'get'], '/', 'BaseCtrl@index')->name('app:base:index');

    Route::match(['post', 'get'], '/{any?}', 'BaseCtrl@index')->name('app:base:index')->where('any', '^(?!admin\/)[\/\w\.-]*');

});


