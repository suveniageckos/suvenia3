<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoOrderItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_order_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('order_id');
			$table->integer('product_id');
			$table->string('order_ref', 191);
			$table->integer('quantity');
			$table->timestamps();
			$table->float('price', 10, 0);
			$table->text('data', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_order_items');
	}

}
