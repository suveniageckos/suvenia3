<?php
namespace App\Repository\Influencer;

use App\Influencer;
use App\Repository\Influencer\InfluencerInterface;

class InfluencerRepository implements InfluencerInterface{

    /**
     * Save influencer application data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function sign_up($data){
        $influencer = Influencer::where('invite_code', decrypt(request()->code))->first();

        $influencer->invite_code = null;
        $influencer->firstname = $data['firstname'];
        $influencer->lastname = $data['lastname'];
        $influencer->bio = $data['bio'];
        $influencer->accepted_payment = $data['accepted_payment'];
        $influencer->minimum_amount = $data['minimum_amount'];
        $influencer->platform = $data['platform'];
        $influencer->is_approved = true;
        $influencer->specialties = $data['specialties'];
        $influencer->phone = $data['cellphone'];
        $influencer->location_id = $data['location'];
        $influencer->address = $data['address'];
        $influencer->save();
        return $influencer;
    }

    /**
     * Save influencer application data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function influencer_apply($data){
        $influencer = new Influencer();

        $influencer->username = $data['username'];
        $influencer->ref = str_random(10);
        $influencer->slug = str_slug($data['username'], '-');
        $influencer->email = $data['email'];
        $influencer->password = bcrypt($data['password']);
        $influencer->has_password = true;
        $influencer->email_token = str_random(36);
       // $influencer->invite_code = 'SV' . str_random(4);
        $influencer->email_token_expiry = \Carbon\Carbon::now()->addDay(1);
        $influencer->temp_socials = json_encode($data['socials']);
        $influencer->save();
        return $influencer;
    }

    /**
     * Save forgot password data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function forgot_password($data){
        $influencer = Influencer::where('email', $data['email'])->first();
        $influencer->password_token = str_random(36);
        $influencer->password_token_expiry = \Carbon\Carbon::now()->addDay(1);
        $influencer->save();
        return $influencer;
    }

    /**
     * Save reset password data
     *
     * @param Illuminate\Http\Request $data
     * @return mixed
     */
    public function reset_password($data){
        $influencer = Influencer::where('password_token', request()->token)->first();
        $currentTime = \Carbon\Carbon::now();
        if(!$influencer){ 
            return false;
        }elseif($influencer and $currentTime > \Carbon\Carbon::parse($influencer->password_token_expiry)){
            return false;
        }else{
            $influencer->password = bcrypt($data['password']);
            $influencer->password_token = null;
            $influencer->password_token_expiry = null;
            $influencer->save();
            return $influencer;
        }

    }

    /**
     * Confirms the influencers
     *
     * @param Illuminate\Http\Request $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function confirm_influencer($id){
        $influencer = Influencer::where('id', $id)->firstOrFail();
        $influencer->invite_code = 'SUV' . strtoupper(str_random(10));
        $influencer->is_approved  = true;
        $influencer->save(); 
        return $influencer;
    }


}