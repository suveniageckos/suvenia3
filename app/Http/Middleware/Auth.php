<?php
namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth as _Auth;
 
class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!_Auth::check()) {
			return redirect()->route('app:user:login')->with('error', 'You must login to access that page.');
        }
        
        return $next($request);
    } 
}
