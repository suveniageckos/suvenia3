@extends('layouts.login')

@section('title', 'Reset Password' )

@section('content')

    <div class="uk-container mt-5 mb-3">
        <div class="row justify-content-center">

       <div class="col-md-6">

         <div class="card app-box">
           <div class="box-header">
             <p class="title">Reset Password?</p>
             <p class="sub-title">Forget your password? No worry, Let's get you a new one. </p>
           </div>
           <div class="box-body">
               <form class="app-form" action="{{ route('app:influencer:request_reset_password', ['token'=> $token]) }}" method="POST">
                @csrf
                <input type="hidden" name="type" value="{{encrypt('influencers')}}">
                      <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        <input type="password" class="form-control form-control-lg textfield-format" placeholder="Password" name="password"  value="">
                        <p class="form-text help-block">{{ $errors->first('password') }}</p>
                      </div>
                      <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                        <input type="password" class="form-control form-control-lg textfield-format" placeholder="Confirm Password" name="confirm_password"  value="">
                        <p class="form-text help-block">{{ $errors->first('confirm_password') }}</p>
                      </div>

                      <button type="submit" class="btn btn-info btn-lg btn-block rounded">Reset Password</button>
                    </form>

           </div>

         </div>

       </div>

        </div>

       </div>


@endsection
