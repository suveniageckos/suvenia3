Hi! {{$user->username}};
<p>Hi Please follow the link below to verify your email address:</p>
<a href="{{ route('app:user:verify_email', ['token'=> $user->email_token]) }}">Verify</a>