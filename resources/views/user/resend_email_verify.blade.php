@extends('layouts.layout')

@section('title', 'Resend Email Verification' )

@section('content')

<div class="uk-container mt-3 mb-3">
    <div class="row justify-content-center">
   
   <div class="col-md-4">
   
     <div class="card app-box">
       <div class="box-header">
         <p class="title">Resend Verification</p>
         <p class="sub-title">Please provide your email to resend verification</p>
       </div>
       <div class="box-body">
           <form class="app-form" action="{{ route('app:user:resend_verification') }}" method="POST" >
               @csrf 
               <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                  <input type="email" class="form-control" placeholder="Email Address" name="email"  value="{{ old('email') }}">
                <p class="form-text help-block">{{ $errors->first('email') }}</p>
                </div>

                  <button type="submit" class="btn btn-info btn-lg btn-block rounded">SUBMIT</button>
                </form>
   
                <p class="body-info">Already have an account? <a href="{{ route('app:user:login') }}">Sign in now</a></p>
       </div>
      
     </div>
       
   </div>
   
    </div>
</div>

@endsection