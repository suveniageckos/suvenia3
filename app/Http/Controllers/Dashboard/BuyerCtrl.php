<?php

namespace App\Http\Controllers\Dashboard;

use App\AllNotifications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use View;
use DB;
use App\Order as Order;
use App\OrderItem as OrderItem;
use App\Product;
use App\User;
use App\ShippingZone as Zone;
use App\ShippingAddress as Shipping;
use App\ShippingLocation as Location;
use App\OrderShipping as OrderShipping;
use App\Review;
use App\SavedItem;
use App\Payment as Payment;
use Hash;
use App\Rules\PasswordExists;
use App\Rules\AmountEquals;
use App\Rules\NumberBetween;
use App\Helpers\Utils;
use Illuminate\Validation\Rule;

class BuyerCtrl extends Controller
{

    public function index(){

        return view('buyer.index', []);
    }

    public function my_orders(Request $request){
        $duration = isset($request->dur) ? \Carbon\Carbon::now()->subDays(intval(base64_decode($request->dur))) : \Carbon\Carbon::now()->subDays(0);

        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping'])->whereDate('created_at', '<=', $duration)->where('user_id', Auth::user()->id )->paginate(10);

        return view('buyer.my_orders', ['orders'=> $orders]);
    }

    public function saved_items(Request $request){
        $products = SavedItem::with(['product'=> function($q){
            $q->with(['categories', 'photos', 'design'])->get();
        }])->where('user_id', Auth::user()->id)->get();
        //echo json_encode($products);
        return view('buyer.saved_items', ['products'=> $products]);
    }

    public function delete_saved_items(Request $request, $id){
        if($request->isMethod('post')){
            $product = SavedItem::where('product_id', $id)->delete();
            return back()->with('success', 'Item removed successfully!');
        }
    }

    public function reviews(Request $request){
        $payments = Payment::where([['status', 2], ['user_id', Auth::user()->id]])->get();
        $orderIDS = $payments->pluck('reference')->all();
        $order_items = OrderItem::whereIn('order_ref', $orderIDS)->get();
        $products = Product::with('order_items')->whereIn('id', $order_items->pluck('product_id'))->paginate(10);
        return view('buyer.review', ['products'=> $products]);
    }

    public function payment_history(Request $request){
        $payments = Payment::where('user_id', Auth::user()->id)->paginate(10);
        return view('buyer.payment_history', ['payments'=> $payments]);
    }

    public function add_product_review(Request $request, $id){
        if($request->isMethod('get')){
            $product = Product::with(['order_item', 'photos', 'design'])->where('id', $id)->first();
            $review = Review::where([['product_id', $id], ['user_id', Auth::user()->id]])->first();
            return View::make('buyer.add_review', ['product'=> $product, 'review'=> $review]);
        }

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:200',
                'review_content' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()], 500);
            }else{
               // rateMsg
            $product = Product::where('id', $id)->first();
            $product->ratingUnique([
                'rating'=> intval($request->rating)
            ], Auth::user());

            $review = Review::firstOrNew(['product_id' => $id]);
            $review->product_id = $id;
            $review->user_id = Auth::user()->id;
            $review->title = $request->title;
            $review->content = $request->review_content;
            $review->save();
            return response()->json(['message'=> 'Review saved successfully!', 'redirect_url'=> route('app:dashboard:reviews')], 200);

            }

        }

    }


    public function profile_settings(Request $request){
        if($request->isMethod('post')){
            $user = User::where('id', Auth::user()->id)->first();

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|min:4|unique:users,username,' . $user->id,
                'firstname' => 'max:255|min:2',
                'lastname' => 'max:255|min:2',
                'phone' => 'digits:11',
            ]);

            if ($validator->fails()) {

                return response()->json(['errors'=> $validator->errors()], 500);

            } else {
                $user = User::where('id', Auth::user()->id)->first();
                $user->username = $request->username;
                $user->slug = str_slug($request->username, '-');
                $user->phone = $request->phone;
                $user->firstname = $request->firstname;
                $user->lastname = $request->lastname;

                if ($user->save()) {
                    return response()->json(['message'=> 'Account settings updated successfully', 'redirect_url'=> route('app:dashboard:profile_settings')], 200);
                }
             }
           }

        return view('buyer.profile_settings', []);
    }

    public function change_password(Request $request){

        if($request->isMethod('post')){
            $user = User::where('id', Auth::user()->id)->first();

            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'password' => ['max:255', 'min:4', function($attribute, $value, $fail)use($user) {
                        if (!Hash::check($value, $user->password)) {
                            return $fail('Password does not match your old password.');
                        }
                    }],
                    'confirm_password' => 'required|min:4|same:password',
                ]);

                if ($validator->fails()) {
                    return response()->json(['errors'=> $validator->errors()], 500);
                } else {

                    $user = User::where('id', Auth::user()->id)->first();
                    $user->password = bcrypt($request->password);
                    if ($user->save()) {
                        return response()->json(['message'=> 'Password setting updated successfully', 'redirect_url'=> route('app:dashboard:change_password')], 200);
                    }
                }

            }
        }

        return view('buyer.change_password', []);
    }

    public function shipping(Request $request){
        $zones = Zone::get();
        return view('buyer.shipping', ['zones'=> $zones]);
    }

    public function add_address_zone_modal(Request $request, $id){
        $states = Location::where('zone_id', $id)->get();
        return View::make('buyer.snippet.zone_address', ['id'=> $id, 'states'=> $states]);
    }

    public function edit_address_zone_modal(Request $request, $id, $address_id){
        $states = Location::where('zone_id', $id)->get();
        $address = Shipping::where('id', $address_id)->first();
        return View::make('buyer.snippet.edit_zone_address', ['id'=> $id, 'states'=> $states, 'address_id'=> $address_id, 'address'=> $address]);
    }

    public function set_default_zone_modal(Request $request, $id){
        $addresses = Shipping::where('zone_id', $id)->get();
        return View::make('buyer.snippet.select_address', ['addresses'=> $addresses, 'id'=> $id]);
    }

    public function add_shipping_address(Request $request, $id){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=> $validator->errors()], 500);
            }else{

                $shipping = new Shipping();

                //$shipping->user_id = Auth::user()->id;
                $shipping->user_id = Auth::user()->id;
                $shipping->location_id = !$request->location ? 0 : $request->location;
                $shipping->zone_id = $id;
                $shipping->price = 3000;
                $shipping->firstname = $request->firstname;
                $shipping->lastname = $request->lastname;
                $shipping->email = $request->email;
                $shipping->address = $request->address;
                $shipping->phone_number = $request->phone_number;
                $shipping->country = $request->location ? "Nigeria" : $request->country;

                $shipping->save();

                return response()->json(['message'=> 'Shipping details saved successfully', 'redirect_url'=>url()->previous()], 200);

            }

        }
    }

    public function edit_shipping_address(Request $request, $id, $address_id){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()], 500);
            }else{

                $shipping = Shipping::where(['id'=> $address_id])->first();

                //$shipping->user_id = Auth::user()->id;
                $shipping->user_id = Auth::user()->id;
                $shipping->location_id = !$request->location ? 0 : $request->location;
                $shipping->zone_id = $id;
                $shipping->price = 3000;
                $shipping->firstname = $request->firstname;
                $shipping->lastname = $request->lastname;
                $shipping->email = $request->email;
                $shipping->address = $request->address;
                $shipping->phone_number = $request->phone_number;
                $shipping->country = $request->location ? "Nigeria" : $request->country;

                $shipping->save();

                return response()->json(['message'=> 'Shipping details updated successfully', 'redirect_url'=>url()->previous()], 200);

            }

        }
    }

    public function delete_shipping_address(Request $request, $id){
       // $request->session()->forget('CartShippingPrice');
        Shipping::where('id', $id)->delete();
        return back()->with('success', 'Address deleted sucessfully!');
    }

    public function select_shipping_address(Request $request, $id){
        if($request->isMethod('post')){
            DB::table('eco_buyer_addresses')->where([['is_default', true], ['zone_id', $id]])->update(['is_default'=>false]);
            $shipping = Shipping::where('id', $request->shipping_address)->first();
            $shipping->is_default = true;
            $shipping->save();
            return response()->json(['message'=> 'Changes saved successfully', 'redirect_url'=>url()->previous()], 200);
        }
    }

    public function select_zone(Request $request, $id){
        if($request->isMethod('post')){
            $shipping = Shipping::where([['zone_id', $id], ['is_default', true]])->first();
            //$checkAddress = Shipping::where([['zone_id', $id]])->count();

            Zone::where('is_default', true)->update(['is_default'=>false]);
            $zone = Zone::where('id', $id)->first();
            $zone->is_default = true;
            $zone->save();

            return response()->json(['message'=> 'Shipping Selected Successfuly!', 'redirect_url'=> url()->previous()], 200);
        }
    }

    public function design_services(Request $request){
        $orders = \App\DesignerOrder::with('gig', 'seller', 'plan')->where([['user_id', Auth::user()->id], ['user_type', 'BUYER']])->paginate(20);
        return view('buyer.design_orders', ['active_orders'=> $orders]);
    }

    public function single_design_service(Request $request, $id){
        $order = \App\DesignerOrder::with('gig','plan', 'order_request', 'designer')->where('id', $id)->firstOrFail();
        return view('buyer.single_design_order', ['order'=> $order]);
    }

    // public function all_notifications(Request $request)
    // {
    //     $notifications = \App\AllNotifications::where('id', $request->id)->first();
    //     return view('buyer.notifications', ['notifications'=> $notifications]);
    // }

    public function all_notifications(Request $request){
        if($request->isMethod('get')){
            AllNotifications::where('id', $request->id)->update(['read_at'=> true]);
            $notifications = DB::table('all_notifications')->where('owner_id', Auth::user()->id)->latest()->paginate(10);
            return view('buyer.notifications', ['notifications'=> $notifications]);
        }
        $notifications = DB::table('all_notifications')->where('owner_id', Auth::user()->id)->latest()->paginate(10);
        return redirect()->route('buyer.notifications', ['notifications'=> $notifications]);

    }

    public function delete_notifications(Request $request, $id)
    {
        AllNotifications::where('id', $id)->delete();
        return back()->with('success', 'Notification deleted sucessfully!');
    }


}
