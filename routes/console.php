<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;
use App\Helpers\Utils;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('app:setup', function () {
   // Artisan::call('migrate:fresh --path=database/migrations/suvenia --seed');
    Artisan::call('migrate --path=database/migrations/suvenia --seed');
   Artisan::call('config:cache');
})->describe('sets app up');

