<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header app-card-header">
                <div class="row">
                    <div class="col-md-4">
                            <form class="uk-search uk-search-default" style="width: 100%;" action="{{ route('app:dashboard:product_search') }}" method="POST">
                                @csrf
                                    <span uk-search-icon></span>
                                    <input class="uk-search-input" type="search" placeholder="Search Products..." name="q" value="{{ request()->q }}">
                                </form>
                    </div>

                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <ul class="list-unstyled m-1">
                            <li class="d-inline"><a href="{{ route('app:ecommerce:product_list') }}" class="btn app-btn-info" target="_blank">CREATE PRODUCT</a></li>
                        <li class="d-inline"><a href="{{ route('app:dashboard:product_delete') }}" class="btn app-btn-danger" id="boxTriggerButton">DELETE PRODUCT</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="mb-2">

                </div>
                <div class="table-responsive">
                    <table class="table app-table">
                        <thead>
                        <tr>
                            <th>
                              <p><input class="form-check-input ml-1" type="checkbox" id="selectAll"></p>

                            </th>
                            <th></th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Total Sold</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            {!! $table !!}
                        </tbody>
                    </table>
                </div>

                {{ $products->appends(['q' => request()->q])->links() }}
            </div>
    </div>
</div>
</div>
