<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Hash;
use Validator;
use App\Designer;
use App\DesignerOrder as Order;
use App\DesignerOffer as Offer;
use App\DesignerRequest;
use App\DesignerWithdrawal as Withdraw;
use App\Helpers\Utils;
use App\Rules\AmountEquals;
use App\Rules\NumberBetween;
use Illuminate\Http\Request;
use App\Rules\PasswordExists;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\DesignerCategory as Category;
use App\DesignerGig as Gig;
use App\DesignerPlan as Plan;
use App\DesignerAttachment;
use App\Chat;
use Notification;
use App\Notifications\DesignerSendOffer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class DesignerCtrl extends Controller
{
    public function __construct(){
        $this->auth = Auth::guard('designers');
    }

    public function index(Request $request){
        $dimers = [
            [
                'title'=> 'Total Income',
                'value'=> 'N' . Order::where([['designer_id', $this->auth->user()->id], ['status', 2]])->sum('amount')
            ],
            [
                'title'=> 'Withdrawn',
                'value'=> 'N' . Withdraw::where([['user_id', $this->auth->user()->id], ['status', 2]])->sum('amount')
            ],
            [
                'title'=> 'Pending Income',
                'value'=> 'N' . Order::where([['designer_id', $this->auth->user()->id], ['status', 0]])->sum('amount')
            ],
            [
                'title'=> 'Available For Withdrawal',
                'value'=> 'N' . $this->auth->user()->balance
            ],

        ];


        $orders = Order::where([['designer_id', $this->auth->user()->id], ['status', 0]])->get();
        $active_orders = Order::with(['designer'])->where([['user_id', $this->auth->user()->id], ['status', 0]])->limit(5)->get();


        // $connected = @fsockopen('https://www.blog.suvenia.com/wp-json/wp/v2/posts?per_page=1');
        // if($connected){

        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://www.blog.suvenia.com/wp-json/wp/v2/posts?per_page=1');
        $response = $request->getBody();
        $blog_details = json_decode($response);

        foreach($blog_details  as $value){
          $id = $value->featured_media;

        $url = 'http://blog.suvenia.com/wp-json/wp/v2/media/'. $id;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = json_decode($res->getBody(), true);
        $imgUrl = $data['media_details']['sizes']['medium']['source_url'];
        $image_url = str_replace('https://', 'http://', $imgUrl);

     }

        $client2 = new \GuzzleHttp\Client();
        $request2 = $client2->get('https://www.vzine.suvenia.com/wp-json/wp/v2/posts?per_page=1');
        $response2 = $request2->getBody();
        $vzine_details = json_decode($response2);

        // }else{
        //     $vzine_details = [];
        //     $blog_details = [];
        //     $image_url = "";
        // }


        // dd($blog_details);
        return view('designer.dashboard.index', [
            'dimers'=> $dimers,
            'orders'=> $orders,
            'active_orders'=> $active_orders,
            'blog_details' => $blog_details,
            'vzine_details' => $vzine_details,
            'image_url' => $image_url
            ]);
    }


    public function designer_all_gigs(Request $request){
        $gigs = Gig::with(['orders',])->where('user_id', $this->auth->user()->id)->paginate(20);
        return view('designer.dashboard.gigs.all_gigs', ['gigs'=> $gigs]);
    }


    public function designer_add_gig(Request $request){
        $categories = Category::get();
        if($request->isMethod('POST')){
            $validator = Validator::make($request->all(), [
				'gig_title' => 'required|min:4',
				'gig_category' => 'required',
				'service_type' => 'required',
				'tags' => 'required',
				'gig_description' => 'required',
				'gig_requirement' => 'required',
				'plans.0.plan_name' => 'required',
				'plans.1.plan_name' => 'required',
				'plans.2.plan_name' => 'required',
				'plans.0.plan_description' => 'required',
				'plans.1.plan_description' => 'required',
				'plans.2.plan_description' => 'required',
				'plans.0.plan_design_concepts' => 'required',
				'plans.1.plan_design_concepts' => 'required',
				'plans.2.plan_design_concepts' => 'required',
				'plans.0.delivery_time' => 'required',
				'plans.1.delivery_time' => 'required',
                'plans.2.delivery_time' => 'required',
                //'plans.0.is_printable' => 'required',
				//'plans.1.is_printable' => 'required',
				//'plans.2.is_printable' => 'required',
				'plans.0.price' => 'required|integer',
				'plans.1.price' => 'required|integer',
				'plans.2.price' => 'required|integer',
				'app_files' => 'required',
            ]);

            $niceNames = [
                'plans.0.plan_name'=> 'First Plan Name',
                'plans.1.plan_name'=> 'Second Plan Name',
                'plans.2.plan_name'=> 'Third Plan Name',
                'plans.0.plan_description'=> 'First Plan Description',
                'plans.1.plan_description'=> 'Second Plan Description',
                'plans.2.plan_description'=> 'Third Plan Description',
                'plans.0.plan_design_concepts'=> 'First Plan Design Concepts',
                'plans.1.plan_design_concepts'=> 'Second Plan Design Concepts',
                'plans.2.plan_design_concepts'=> 'Third Plan Design Concepts',
                'plans.0.delivery_time'=> 'First Plan Delivery Time',
                'plans.1.delivery_time'=> 'Second Plan Delivery Time',
                'plans.2.delivery_time'=> 'Third Plan Delivery Time',
                'plans.0.is_printable'=> 'The First Plan Printing Decision',
                'plans.1.is_printable'=> 'The Second Plan Printing Decision',
                'plans.2.is_printable'=> 'The Third Plan Printing Decision',
                'plans.0.price'=> 'First Plan Price',
                'plans.1.price'=> 'Second Plan Price',
                'plans.2.price'=> 'Third Plan Price',
            ];
            $validator->setAttributeNames($niceNames);

			if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()->first()], 500);
            }else{
                $gig = new Gig();
                $gig->title = $request->gig_title;
                $gig->ref = str_random(10);
                $gig->user_id = $this->auth->user()->id;
                $gig->category_id = $request->gig_category;
                $gig->service_type = $request->service_type;
                $gig->tags = $request->tags;
                $gig->description = $request->gig_description;
                //$gig->description = 'hello';
                $gig->reqirements = $request->gig_requirement;
                //$gig->reqirements = 'req';
                $gig->save();

                foreach($request->plans as $plan){
                    $planIns = new Plan();
                    $planIns->gig_id = $gig->id;
                    $planIns->user_id = $this->auth->user()->id;
                    $planIns->name = $plan['plan_name'];
                    $planIns->description = $plan['plan_description'];
                    $planIns->revisions = $plan['plan_revisions'];
                    $planIns->design_concepts = $plan['plan_design_concepts'];
                    $planIns->has_source_file = isset($plan['source_file']) ? true : false;
                    $planIns->is_printable = isset($plan['is_printable']) ? true : false;
                    //$planIns->enables_commercial_use = $plan['enables_commercial_use'] ? true : false;
                    $planIns->enables_commercial_use = false;
                    $planIns->duration = $plan['delivery_time'];
                    $planIns->price = $plan['price'];
                    $planIns->save();

                }
                foreach($request->app_files as $file){
                    $utils  = new Utils();
                    $upload = $utils->full_base64_upload($file, str_random(10), $gig->ref, 'designers/gigs');
                    $att = new DesignerAttachment();
                    $att->user_id = Auth::user()->id;
                    $att->gig_id = $gig->id;
                    $att->path = $upload['path'];
                    $att->public_url = $upload['public_url'];
                    $att->save();
                }

                return response()->json(['message'=> 'Gig created successfully!', 'redirect_url'=> route('app:designer:dashboard:all_gigs')], 200);

            }
        }
        return view('designer.dashboard.gigs.new_gig', ['categories'=> $categories]);
    }

    public function designer_edit_gig(Request $request, $id){
        $gigE = Gig::where('id', $id)->with('plans', 'photos')->firstOrFail();
      //  echo json_encode($gigE->photos);
        $categories = Category::get();
        if($request->isMethod('POST')){
            $validator = Validator::make($request->all(), [
				'gig_title' => 'required|min:4',
				'gig_category' => 'required',
				'service_type' => 'required',
				'tags' => 'required',
				'gig_description' => 'required',
				'gig_requirement' => 'required',
				'plans.0.plan_name' => 'required',
				'plans.1.plan_name' => 'required',
				'plans.2.plan_name' => 'required',
				'plans.0.plan_description' => 'required',
				'plans.1.plan_description' => 'required',
				'plans.2.plan_description' => 'required',
				'plans.0.plan_design_concepts' => 'required',
				'plans.1.plan_design_concepts' => 'required',
				'plans.2.plan_design_concepts' => 'required',
				'plans.0.delivery_time' => 'required',
				'plans.1.delivery_time' => 'required',
				'plans.2.delivery_time' => 'required',
				//'plans.0.is_printable' => 'required',
				//'plans.1.is_printable' => 'required',
				//'plans.2.is_printable' => 'required',
				'plans.0.price' => 'required|integer',
				'plans.1.price' => 'required|integer',
				'plans.2.price' => 'required|integer',
				'app_files' => 'nullable',
            ]);

            $niceNames = [
                'plans.0.plan_name'=> 'First Plan Name',
                'plans.1.plan_name'=> 'Second Plan Name',
                'plans.2.plan_name'=> 'Third Plan Name',
                'plans.0.plan_description'=> 'First Plan Description',
                'plans.1.plan_description'=> 'Second Plan Description',
                'plans.2.plan_description'=> 'Third Plan Description',
                'plans.0.plan_design_concepts'=> 'First Plan Design Concepts',
                'plans.1.plan_design_concepts'=> 'Second Plan Design Concepts',
                'plans.2.plan_design_concepts'=> 'Third Plan Design Concepts',
                'plans.0.delivery_time'=> 'First Plan Delivery Time',
                'plans.1.delivery_time'=> 'Second Plan Delivery Time',
                'plans.2.delivery_time'=> 'Third Plan Delivery Time',
                'plans.0.is_printable'=> 'The First Plan Printing Decision',
                'plans.1.is_printable'=> 'The Second Plan Printing Decision',
                'plans.2.is_printable'=> 'The Third Plan Printing Decision',
                'plans.0.price'=> 'First Plan Price',
                'plans.1.price'=> 'Second Plan Price',
                'plans.2.price'=> 'Third Plan Price',
            ];
            $validator->setAttributeNames($niceNames);

			if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()->first()], 500);
            }else{
                $gig = Gig::where('id', $id)->with('plans')->firstOrFail();
                $gig->title = $request->gig_title;
                //$gig->ref = str_random(10);
                $gig->user_id = $this->auth->user()->id;
                $gig->category_id = $request->gig_category;
                $gig->service_type = $request->service_type;
                $gig->tags = $request->tags;
                $gig->description = $request->gig_description;
                //$gig->description = 'hello';
                $gig->reqirements = $request->gig_requirement;
                //$gig->reqirements = 'req';
                $gig->save();

                foreach($request->plans as $plan){
                    $planIns = Plan::where('gig_id', $id)->first();
                    $planIns->gig_id = $gig->id;
                    $planIns->user_id = $this->auth->user()->id;
                    $planIns->name = $plan['plan_name'];
                    $planIns->description = $plan['plan_description'];
                    $planIns->revisions = $plan['plan_revisions'];
                    $planIns->design_concepts = $plan['plan_design_concepts'];
                    $planIns->has_source_file = isset($plan['source_file']) ? true : false;
                    $planIns->is_printable = isset($plan['is_printable']) ? true : false;
                    //$planIns->enables_commercial_use = $plan['enables_commercial_use'] ? true : false;
                    $planIns->enables_commercial_use = false;
                    $planIns->duration = $plan['delivery_time'];
                    $planIns->price = $plan['price'];
                    $planIns->save();

                }
                if($request->app_files){
                    foreach($request->app_files as $file){
                        $utils  = new Utils();
                        $upload = $utils->full_base64_upload($file, str_random(10), $gig->ref, 'designers/gigs');
                        $att = new DesignerAttachment();
                        $att->user_id = Auth::user()->id;
                        $att->gig_id = $gig->id;
                        $att->path = $upload['path'];
                        $att->public_url = $upload['public_url'];
                        $att->save();
                    }
                }


                return response()->json(['message'=> 'Gig updated successfully!', 'redirect_url'=> route('app:designer:dashboard:all_gigs')], 200);

            }
        }

       return view('designer.dashboard.gigs.edit_gig', ['gig'=> $gigE, 'categories'=> $categories]);
    }


    public function designer_delete_single_image(Request $request, $id){
        if($request->isMethod('POST')){
            $image = DesignerAttachment::where('id', $id)->firstOrFail();
            if(file_exists($image->path)){
                unlink($image->path);
                $image->delete();
            }
        }
    }

    public function designer_delete_gigs(Request $request, $id){
        if($request->isMethod('POST')){
            if($id){
                $gig = Gig::where('id', $id)->firstOrFail();
                 $images = DesignerAttachment::where('gig_id', $id)->get();
                 if($images){
                    if(file_exists(public_path('storage/designers/gigs/' . $gig->ref))){
                        //unlink(public_path('storage/designers/gigs/' . $gig->ref));
                        Storage::disk('public')->deleteDirectory('designers/gigs/' . $gig->ref);
                    }
                    foreach($images as $image){
                        $image->delete();
                    }
                 }
                 Plan::where('gig_id', $id)->delete();
                 $gig->delete();
                 return back()->with('success', 'Gig deleted successfully!');
            }else{
                abort(404);
            }

        }

    }

    public function all_requests(Request $request){
        $offers = Offer::where([['user_id', $this->auth->user()->id]])->get();
        $reqs = DesignerRequest::with('seller')->where('gig_id', null)->whereNotIn('id', $offers->pluck('request_id'))->paginate(15);

       // dd($reqs);
        return view('designer.dashboard.request', ['requests'=> $reqs]);
    }



    public function send_offer(Request $request, $id){
       if($request->isMethod('POST')){
          if(!$id){
              abort(500);
          }else{
            $check = Offer::where([['user_id', $this->auth->user()->id], ['request_id', $id]])->first();
            if($check){
                return back()->with('success', 'You have already sent an offer to this Request');
            }else{
                $offer = new Offer();
                $offer->user_id = $this->auth->user()->id;
                $offer->request_id = $id;
                $offer->gig_id = $request->gig;
                $offer->ref = 'OFFER_' . str_random(5);
                $offer->save();


                // $user = User::find(1);

                // $data = [
                //      'user_id' => $offer->user_id,
                //      'request_id' => $offer->request_id,
                //      'user_type' => 'SELLER',
                //      'email' => $user->email
                // ];
                //Notification::send($user, new DesignerSendOffer($data));


                return back()->with('success', 'Your offer was sent successfully!');
            }
          }
       }
    }

    public function send_offer_modal(Request $request, $id){
        if($request->isMethod('POST')){
            $reqs = DesignerRequest::with('seller')->where('id', $id)->first();
            $gigs = Gig::with('plans')->where('user_id', $this->auth->user()->id)->get();
            return View::make('designer.dashboard.offer_modal', ['request'=> $reqs, 'gigs'=> $gigs]);
        }
    }

    public function designer_all_orders(Request $request){
        $active_orders = Order::with('gig', 'seller', 'plan')->where([['designer_id', $this->auth->user()->id], ['status', 0]])->paginate(20);
        return view('designer.dashboard.orders.all_orders', ['active_orders'=> $active_orders]);
    }

    public function designer_view_order(Request $request, $id){
        $order = Order::with('gig','plan', 'order_request')->where('id', $id)->firstOrFail();
        return view('designer.dashboard.orders.view_order', ['order'=> $order]);
    }

    public function designer_delete_orders(Request $request){

    }

    public function upload_gallery(Request $request){

    }

    public function designer_order_send_message(Request $request){
        if($request->isMethod('POST')){
            $chat = new Chat();
            $chat->sender = $request->sender;
            $chat->reciever = $request->reciever;
            $chat->sender_type = $request->sender_type;
            $chat->reciever_type = $request->reciever_type;
            $chat->department = 'DESIGNER';
            $chat->model_id = $request->id;
            $chat->content = $request->message;
            $chat->type = 'TEXT';
            $chat->save();
            return response()->json(['message'=> $chat->content], 200);
        }
    }

    public function designer_order_send_attachment(Request $request){

    }

    public function withdrawal(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                 'amount' => ['required', new NumberBetween(1000, 50000), new AmountEquals($this->auth->user()->balance)]
             ]);
             if ($validator->fails()) {
                 return response()->json(['errors'=> $validator->errors()], 500);
             }else{
                 $withdraw = new Withdraw();
                 $withdraw->user_id = $this->auth->user()->id;
                 $withdraw->code = $this->auth->user()->transfer_code;
                 $withdraw->amount = $request->amount;
                 $withdraw->ref = 'SUV-' . str_random(20);
                 $withdraw->status = 1;
                 $withdraw->save();
                 $userQuery = \App\Influencer::where('id', $this->auth->user()->id)->first();
                 $userQuery->decrement('balance', $request->amount);

                 return response()->json(['message'=> 'Your request has been recieved! After validation, your payment would be made within 5 working days.', 'redirect_url'=> route('app:designer:dashboard:withdraw')], 200);
             }
        }
        $dimers = [
            [
                'title'=> 'Current Balance',
                'value'=> 'N' . $this->auth->user()->balance
            ],
            [
                'title'=> 'Lifetime Balance',
                'value'=> 'N' . Order::where([['user_id', $this->auth->user()->id], ['status', 2]])->sum('amount')
            ],
        ];
        $withdrawals = Withdraw::where('user_id', $this->auth->user()->id)->paginate(10);

        return view('designer.dashboard.withdraw', ['withdrawals'=> $withdrawals, 'dimers'=> json_decode(json_encode($dimers), false)]);

    }

    public function profile_settings(Request $request){
        if($request->isMethod('post')){
            $user = Designer::where('id', $this->auth->user()->id)->first();

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|min:4|unique:sellers,username,' . $user->id,
                'firstname' => 'max:255|min:2',
                'lastname' => 'max:255|min:2',
                'phone' => 'digits:11',
                'bio' => 'max:300|min:20',
                'image' => 'required',
            ]);

            if ($validator->fails()) {

                return response()->json(['errors'=> $validator->errors()], 500);

            } else {
                $designer = Designer::where('id', $this->auth->user()->id)->first();
                $designer->username = $request->username;
                $designer->slug = str_slug($request->username, '-');
                $designer->phone = $request->phone;
                $designer->firstname = $request->firstname;
                $designer->lastname = $request->lastname;
                $designer->bio = $request->bio;

                if ($request->hasFile('image') && $request->file('image')->isValid()) {
                    if(Storage::exists($designer->image_path)){
                        Storage::delete($designer->image_path);
                    }
                    $image_path = Storage::putFile('designers', $request->file('image'));
                    $designer->photo_url = Storage::url($image_path);
                    $designer->image_path = $image_path;
                }


                if ($designer->save()) {
                    return response()->json(['message'=> 'Account settings updated successfully', 'redirect_url'=> route('app:designer:dashboard:profile_settings')], 200);
                }
             }
           }

        return view('designer.dashboard.profile_settings', []);
    }

    public function change_password(Request $request){

        if($request->isMethod('post')){
            $designer = Designer::where('id', $this->auth->user()->id)->first();

            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'password' => ['max:255', 'min:4', function($attribute, $value, $fail)use($designer) {
                        if (!Hash::check($value, $designer->password)) {
                            return $fail('Password does not match your old password.');
                        }
                    }],
                    'confirm_password' => 'required|min:4|same:password',
                ]);

                if ($validator->fails()) {
                    return response()->json(['errors'=> $validator->errors()], 500);
                } else {

                    $designer = Designer::where('id', $this->auth->user()->id)->first();
                    $designer->password = bcrypt($request->password);
                    if ($designer->save()) {
                        return response()->json(['message'=> 'Password setting updated successfully', 'redirect_url'=> route('app:designer:dashboard:change_password')], 200);
                    }
                }

            }
        }

        return view('designer.dashboard.change_password', []);
    }

    public function bank_details(Request $request){
        if($request->isMethod('post')){
			$utils = new Utils();
			$designer = Designer::where('id', $this->auth->user()->id)->first();
			$validator = Validator::make($request->all(), [
				'bank' => 'required',
				'bank_name' => 'required',
				//'bank_bvn' => 'required|digits:11',
				'bank_account' => 'required|digits:10',
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);

			}else{

            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));

            try
			{
			  $tranx = $paystack->transferrecipient->create([
				'name' => $request->bank_name,
                'description'=> 'Transfer Reciepent',
                'bank_code'=> $request->bank,
                'currency'=> 'NGN',
                'account_number'=> $request->bank_account
			  ]);

               if($tranx->status === true){
               $designer->bank_sort = $request->bank;
				$designer->bank_name = $request->bank_name;
				//$user->bank_bvn = $request->bank_bvn;
				$designer->bank_account = $request->bank_account;
                $designer->transfer_code = $tranx->data->recipient_code;
				if($designer->save()){
					return response()->json(['message'=> 'Your Profile updated was successfull.', 'redirect_url'=> route('app:seller:dashboard:bank_details')], 200);
				}
               }

			}catch(\Yabacon\Paystack\Exception\ApiException $e){
				//die($e->getMessage());
                return response()->json(['errors'=> str_replace("'", " ", str_after($e->getMessage(), ':'))], 500);
			}

		}
      }
        return view('designer.dashboard.bank_details', []);
    }

    public function rate_designer(Request $request, $id){
        if($request->isMethod('POST')){
            $order = Order::where('id', $id)->first();
            $designer = Designer::where('id', $order->designer_id)->first();
            if($order->user_type == 'SELLER' && Auth::guard('sellers')->check()){
                $user = \App\Seller::find(Auth::guard('sellers')->user()->id);
            }elseif($order->user_type == 'SELLER' && Auth::check()){
                $user = \App\Seller::find(Auth::user()->id);
            }

            $designer->ratingUnique([
                'rating'=> intval($request->rate)
            ], $user);
            return response()->json(['data'=>1], 200);
        }

    }

    public function approve_order(Request $request, $id){
        if($request->isMethod('POST')){
            $order = Order::where('id', $id)->first();
            $chats = \App\Chat::where('model_id', $order->id);
            $images = \App\DesignerAttachment::whereIn('id', $chats->pluck('attachment_id'));
            foreach($images->get() as $image){
                $file = $image->path;
                if(Storage::exists($file)){
                    Storage::delete($file);
                }
            }
            $images->delete();
            $chats->delete();
            $order->is_completed = true;
            $order->save();
            return back()->with('success', 'Order Approved succeesfully.');

        }
    }

}
