<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInflencerGigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inflencer_gigs', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('product_id')->nullable();
			$table->bigInteger('store_id')->nullable();
			$table->bigInteger('user_id');
			$table->integer('influencer_id')->nullable();
			$table->bigInteger('order_id')->nullable();
			$table->string('duration', 191);
			$table->string('status', 191)->nullable();
			$table->boolean('is_closed')->nullable()->default(0);
			$table->dateTime('closed_at')->nullable();
			$table->text('description', 65535)->nullable();
			$table->boolean('is_shippable')->default(0);
			$table->string('content_link', 225)->nullable();
			$table->text('shipping_address', 65535)->nullable();
			$table->string('promotion_platform', 225)->nullable();
			$table->string('promotion_format', 225)->nullable();
			$table->integer('number_of_influencers')->nullable();
			$table->integer('number_of_followers')->nullable();
			$table->string('audience_category', 191)->nullable();
			$table->integer('audience_location')->nullable();
			$table->bigInteger('total_price')->nullable();
			$table->string('job_url', 225)->nullable();
			$table->boolean('job_status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inflencer_gigs');
	}

}
