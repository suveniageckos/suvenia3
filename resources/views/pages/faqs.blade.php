@extends('layouts.layout')

@section('title', 'Frequently asked questions')

@section('content')
<div class="uk-container uk-container-small uk-margin-top uk-margin-bottom">

    <div class="d-block uk-text-center mb-5">
<h3 class="mb-2 uk-text-uppercase">Frequently Asked Questions</h3>
<div class="divider-bar"></div>
</div>

    <div class="row">
    <div class="col-md-12">

        <ul uk-accordion>

        <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left mt-3"><b>What can Suvenia.com do for me?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Suvenia.com allows you to discover, create, buy and sell personlaized items such as apparels, drinkwares, hats and accessories. If you are looking for any specific item category, and do not find, do not hesitate to contact us, as we would often go out of our way to make it available for you. Our goal is to make Suvenia.com a platform that allows you create products at the speed of thought.</p>
        <p></b> You can buy products from the marketplace simply by searching through the site, create your own desisgns and order when you don't find what you like or sell ideas and designs on products if you are a creative individual or brand. You can also search for your favorite stores directly to find products you love.
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Can I buy single product on Suvenia?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Yes, Suvenia is actually designed to eliminate all the barriers around buying single personalised items. However, you can still buy a bulk of a particular item by increasing the quantity of the items on the product page </p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Can I have my store on Suvenia.com?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Yes. A Suvenia store allows you to bring together all the different designs and products you have created into one storefront. Check out this <a href="{{ route('app:seller:index') }}" target="_blank">page</a> for more details on the store </p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Product Categories you will find on Suvenia.com?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Suvenia.com offers Apparels (T-Shirts, V-necks, Polos, Sweatshirts, Hoodies), Drinkwares (Mugs, Bottles), Hats (Trucker caps, Snap backs), and Accessories (Throw Pillows, Aprons, e.t.c.). More product categories for you to personalise continue to get added.</p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>What methods of payment do you accept?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <ol>
        <li><p>Paystack (We accept cards – MasterCard, Visa, Verve, or you can simply do a direct transfer using the paystack payment gateway) </p></li>
        <li><p>Pay Later (We allow you to make payment at a later time beyond when you make your order. This also applies to those who are shipping to an international location, where we have to manually compute shipping costs based on location. Simply select the "Pay Later" or "International" option when checking out. </p></li>
        </ol>
        </div>
        </li>

        <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Where do you ship to?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>We deliver to all 36 states in Nigeria and Internationally with our trusted delivery partners. All shipping costs within Nigeria are automatically computed upon checkout. International shipping costs are computed via an offline system and would be communicated to you after checking out, if you are shipping to an international location. Alternatively, you can pick up your order directly from our office <a href="https://www.google.com.ng/maps/dir/''/suvenia.com/@6.5573471,3.3166194,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x103b8d7a6191281f:0x2312e70626e46619!2m2!1d3.38666!2d6.557352">here</a> </p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>What is the average delivery Time</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>We do not stock items and only produce items on demand. You can expect your order in 3-7 business days except for rush orders. You will be continually advised of the status of your order.</p>
        </div>
        </li>

        <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Can I get a refund when I am not satsisfied?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>We do source for quality raw materials and ensure that we always maintain high standards in our processes. While we would be sad to see an item returned, in the rare instance that it happens, we would make sure your complaints are addressed quickly, offer a replacement, and in the last resort offer and process your refunds. </p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>What print quality can I expect? </b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Except explicitly stated otherwise, we use screen printing process for apparels. Learn more about screen printing process <a href="https://www.youtube.com/watch?v=Aru1wZEKrkE">here</a>. For hats, we use either <a href="https://www.youtube.com/watch?v=d6mqndD9xok">vinyl printing</a> or <a href="https://www.youtube.com/watch?v=CcYC_7rqyN0">embroidery</a>. For accessories & drinkwares, we use a digital printing process. Please factor this in when also creating or uploading your designs. </p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>What are the design specifications you accept? </b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>All product design specifications are listed on the product customizer page. You can use our customizer using JPEG, PNG or SVG files with the sizes indicated. If you are unable to edit your design to fit our specification, please kindly chat with us in the live chat icon page. We are able to accept Adobe Illustrator files (vector art with paths) in an AI or EPS format with all fonts converted to outlines. We will also accept vector PDFs and Jpeg for orders placed off the platform. We would contact you if there are any complexities concerning your submitted artwork that require modification. </p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>What kind of product quality can I expect?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p> All products are made in Nigeria, and sourced from the highest grade suppliers and factories. We also ensure that all products are made ethically and in compliance with environmental laws.</p>
        </div>
        </li>

        <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Do your run promotions and discount sales?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p> Suvenia.com runs periodic promotions sitewide for all registered users. In addition, store owners and sellers also offer periodic coupons to their loyal customers. .</p>
        </div>
        </li>

        <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Customer User Dashboard</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Every Customer is entitled to his/her own page and it is confidential. Your user dashboard summarises details of all your transactions as well as records of your purchases. You can also transit to a seller from your current user dashboard, by simply creating a store.</p>
        </div>
        </li>

         <li class="uk-ope uk-background-default p-2">
        <h3 class="uk-accordion-title uk-text-left text-16 mt-3"><b>Can I buy items in bulk on Suvenia?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Yes, you can buy whatever quantities you desire. Bulk purchases often qualify for further discounts. However, to truly enjoy a bulk discount, do kindly chat with us in the live-chat section below, via Whatsapp on <a href="tel:+2349092223340">+2349092223340</a> or Email at <a href="mailto:sales@suvenia.com">Sales@suvenia.com</a>.</p> . Please note that for a purchase to qualify as bulk, it must contain at least 50pcs of any particular item or combination of items.</p>
        </div>
        </li>

         <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>Can I order offline?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Yes, you can by phone <a href="tel:09092223340">09092223340</a> or Email at <a href="mailto:sales@suvenia.com">Sales@suvenia.com</a>. However, our steps are easy to follow, check out our <a href="http://blog.suvenia.com/">blog</a>  to learn more about how to use Suvenia.com </p>
        </div>
        </li>

        <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>How do sellers get paid their margins?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p> As a seller, on your dashboard, you can view the transactions you have acquired over a period of time, you can choose to continue to accumulate them or withdraw them anytime you like. Withdrawals take at least 24hrs to process. If you have purchased a service such as "Featured Store", "Featured Product", or "Advertising" within the period under consdieration, the sum total of these services is deducted from your actual earnings. </p>
        </div>
        </li>

        <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>What are the terms and conditions?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>Please kindly refer to the general terms of service <a href="https://suvenia.com/pages/terms">here</a> and if you are a seller, please find the seller terms <a href="https://suvenia.com/page/seller_terms">here</a> </p>
        </div>
        </li>

        <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>Who is responsible for the marketing of my products?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p> The responsibility for the growth of your store and the visibility of your products rests with you. Suvenia however provides support through activities such as advertising campaigns, photo-shoot, trade show exhibitions for your product, feature in e-mail newsletters, and selection to participate in Suvenia.com events. These marketing activities are not guaranteed and conditions for selection to get your store and product featured in, continue to vary and are set by Suvenia.com. </p>
        </div>
        </li>

          <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>What can I do with my user dashboard?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p> Every buyer has his/her own dashboard where previous purchases can be monitored, designers can be briefed, and a buyer can transition to a seller. Every sellers dashboard contains a purchase and sales history, products list, store lists, promotions platform, as well as a feature for blogging.</p>
        </div>
        </li>

        <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>Can I change or cancel my order after I have placed it?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>We are unable to make any changes to your order once production has begun. However, if you still want to cancel your order, please kindly get in touch to confirm if your production has commenced or still in queue. We might be able to work something out, if the latter is the case.</p>
        </div>
        </li>

        <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>What if I’m unhappy with my order?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>If you’re unhappy with your order because the product is defective or ‘not as promised’ or the imprint quality isn’t ‘spot on’, simply contact our customer care representative and we’ll rerun your order or refund your money. We’ll even pay the shipping to get the problem product returned.</p>
        </div>
        </li>
        <li class="uk-ope uk-background-default uk-padding-small">
        <h3 class="uk-accordion-title uk-text-left text-16"><b>Do You Offer Rush Service?</b></h3>
        <div class="uk-accordion-content uk-text-left">
        <p>At certain times and for some products, we are able to offer rush services. We recommend that you don't take this route too often, but if you are pressed for time, we'll stick by you to make sure we deliver on time! Please bear in mind that this would come at an added cost .</p>
        </div>
        </li>


        </ul>

    </div>
    </div>

</div>


<div id="modal-close-default" uk-modal>
        <div class="uk-modal-dialog uk-modal-body big-dialog">
            {{-- <div class=" uk-modal-header"> --}}
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
            {{-- </div> --}}

                        <div class="row">
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <span class="fas fa-store"></span>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                        <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fas fa-paint-brush"></i>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                            <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fab fa-accessible-icon"></i>
                                            </div>

                                             <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                            <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                 </div>
                            </div>

                        </div>

            </div>
            </div>
@endsection
