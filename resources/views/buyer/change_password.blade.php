@extends('layouts.dashboard')

@section('title', 'Change Password' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-6 uk-text-center">
                        <a href="{{ route('app:dashboard:profile_settings') }}" class="card_head_nav_item {{  $utils->makeActive(['app:dashboard:profile_settings'], 'active') }}">Personal Details</a>
                        </div>
                        <div class="col-md-6 uk-text-center">
                        <a href="{{ route('app:dashboard:change_password') }}" class="card_head_nav_item {{  $utils->makeActive(['app:dashboard:change_password'], 'active') }}">Change Password</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                        <form action="{{ url()->current() }}" method="POST" data-post>
                                <div class="w-75 m-auto">
                                    <div class="row justify-content-center">
                                        <div class="col-md-12 form-group password">
                                                <label class="app-label">Password</label>
                                            <input type="password" class="form-control app-form" name="password" value="">
                                                <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-12 form-group confirm_password">
                                                <label class="app-label">Confirm Password</label>
                                            <input type="password" class="form-control app-form" name="confirm_password" value="">
                                                <span class="help-block"></span>
                                        </div>
                                            
                                        <div class="col-md-12 form-group uk-text-center">
                                            <button class="btn btn-info" type="submit">UPDATE</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </form>
                </div>
        </div>
    </div>
   
</div>


@endsection

