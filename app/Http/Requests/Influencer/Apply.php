<?php

namespace App\Http\Requests\Influencer;

use Illuminate\Foundation\Http\FormRequest;

class Apply extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /** 
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255|min:4|unique:influencers',
            'password' => 'required|min:4',
            'confirm_password' => 'required|min:4|same:password',
            'email' => 'required|min:4|email|unique:influencers',
            'socials.instagram' => 'required|min:4|url',
            'socials.twitter' => 'required|min:4|url',
            'tos' => 'required|boolean',
        ];
    }

    /**
     * Set the validation field attributes
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'socials.instagram'=> 'Instagram',
            'socials.twitter'=> 'Twitter',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'tos.required' => 'You must agree to our terms and conditions',
        ];
    }


}
