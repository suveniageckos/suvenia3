<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignerPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designer_plans', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('gig_id');
			$table->bigInteger('user_id');
			$table->string('name', 191)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('revisions', 191)->nullable();
			$table->boolean('has_source_file')->default(0);
			$table->boolean('is_printable')->default(0);
			$table->boolean('enables_commercial_use')->default(0);
			$table->string('design_concepts', 191)->nullable();
			$table->float('price');
			$table->integer('duration');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designer_plans');
	}

}
