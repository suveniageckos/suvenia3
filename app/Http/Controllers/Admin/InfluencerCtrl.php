<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Mail\ConfirmInfluencer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Repository\Influencer\InfluencerInterface;

class InfluencerCtrl extends Controller
{
    public $influencer;
    
    public function __construct(InfluencerInterface $influencer){
        $this->influencer = $influencer;
    }

    /**
     * confirms an influencer
     *
     * @param Request $request
     * @param integer $id
     * @return Illuminate\Http\RedirectResponse
     */
    public function confirm(Request $request, $id){
        $model = $this->influencer->confirm_influencer($id);
        try{
            Mail::to($model->email)->later(\Carbon\Carbon::now()->addSeconds(2), new ConfirmInfluencer($model));
            return back();
        }
        catch(Exception $e){
            if(Mail::failures()){
                return back()->with(['message' => "Error sending Email", 'alert-type' => 'error']);
            }
        }
    }
}
