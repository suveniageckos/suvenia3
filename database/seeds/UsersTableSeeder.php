<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'admin@app.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$i2PFDQmwQh.ZX8RP.jKxhOFr72i543QAkoH5uO/oDN7llzt/g8Q.q',
                'remember_token' => 'dLp5i1Ar0ttzOGuQtNrnmUBgWdWN4H2NXjcos3vB5fB4chfRKt7Hn2rmfPDT',
                'settings' => NULL,
                'created_at' => '2019-07-25 19:48:20',
                'updated_at' => '2019-07-25 19:48:21',
                'ref' => NULL,
                'username' => NULL,
                'slug' => NULL,
                'firstname' => NULL,
                'lastname' => NULL,
                'email_token' => NULL,
                'email_token_expiry' => NULL,
                'has_password' => 0,
                'email_verified' => 0,
                'facebook' => NULL,
                'twitter' => NULL,
                'linkedin' => NULL,
                'photo_url' => NULL,
                'phone' => NULL,
                'balance' => '0.00',
                'earnings' => '0.00',
                'bank_sort' => NULL,
                'bank_bvn' => NULL,
                'bank_account' => NULL,
                'bank_name' => NULL,
                'transfer_code' => NULL,
                'last_login' => NULL,
            ),
        ));
        
        
    }
}