<?php

namespace App\Http\Requests\Influencer;

use Illuminate\Foundation\Http\FormRequest;

class SignUp extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'invite_code' => 'required|exists:influencer',
            'firstname' => 'required|max:255|min:4',
            'lastname' => 'required|max:255|min:4',
            'cellphone' => 'required|digits:11',
            'accepted_payment' => 'required',
            'address' => 'required',
            'location' => 'required',
            'minimum_amount' => 'required',
            'specialties' => 'required',
            'platform' => 'required',
            'bio' => 'required|min:4|max:300',
        ];
    }
}
