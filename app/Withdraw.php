<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{

    protected $table = 'eco_withdrawals';

    public function seller(){
        return $this->belongsTo("App\Seller", "user_id");
    }

    public function scopeActiveRequest($query)
    {
        return $query->where('status', false);
    }


}
