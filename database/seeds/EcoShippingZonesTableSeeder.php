<?php

use Illuminate\Database\Seeder;

class EcoShippingZonesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('eco_shipping_zones')->delete();
        
        \DB::table('eco_shipping_zones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Pick up at suvenia',
                'payment_method' => 'PAYLATER',
                'description' => '36, Ayodele Okeowo after Deeper Life Church beside Beauty Republic Make up Studio, Ifako, Gbagada, Lagos, Nigeria',
                'is_default' => 1,
                'has_option' => 0,
                'created_at' => NULL,
                'updated_at' => '2019-06-11 15:30:57',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Ship to Nigeria',
                'payment_method' => 'PAYSTACK',
                'description' => 'Add address to get your Orders delivered to your desired address',
                'is_default' => 0,
                'has_option' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-06-11 15:30:57',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Ship to Outside Nigeria',
                'payment_method' => 'PAYLATER',
                'description' => 'Add address to get your Orders delivered to your desired address',
                'is_default' => 0,
                'has_option' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-06-10 17:57:38',
            ),
        ));
        
        
    }
}