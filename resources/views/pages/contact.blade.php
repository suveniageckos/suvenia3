@extends('layouts.layout')

@section('title', 'Frequently asked questions')

@section('content')


<div class="section uk-section-small">
        <div class="container uk-margin-top">
        <div class="d-block uk-text-center mb-2">
        <h3 class="mb-2 uk-text-uppercase">Get in touch</h3>
        <div class="divider-bar"></div>
        </div>

        <div class="uk-grid-match uk-child-width-expand@s uk-text-center" uk-grid>

            <div>
                <div class="uk-card uk-card-body bg_thumb_cont">
                <img src="{{asset('img/social.png')}}" alt="www.suvenia.com" class="img-responisve" uk-responsive width="100">
                <h4 style="color:#2C2C2C;" class="text-bold">Email &amp; Social</h4>
                <p><a href="mailto:sales@suvenia.com" style="color:#2C2C2C;">sales@suvenia.com</a></p>
                <p>
                 <a href="https://www.facebook.com/suveniadotcom" class="uk-icon-button uk-icon-facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="https://twitter.com/Suveniadotcom"  class="uk-icon-button uk-icon-twitter" target="_blank"><i class="fab fa-twitter"></i></a>
                <a href="https://www.instagram.com/suveniadotcom/" class="uk-icon-button uk-icon-instagram" target="_blank"><i class="fab fa-instagram"></i></a>

               </p>
                </div>
            </div>

           <div>
                <div class="uk-card uk-card-body bg_thumb_cont">
                <img src="{{asset('img/phone.png')}}" alt="www.suvenia.com" class="img-responisve" uk-responsive width="100">
                <h4 style="color:#2C2C2C;" class="text-bold">Call us on:</h4>
                <p><a href="tel:09092223340" style="color:#2C2C2C;" class="text-bold">09092223340</a></p>
               <p style="text-muted">Mon-Fri 9am-6pm</p>
                </div>
             </div>

           <div>
                <div class="uk-card uk-card-body bg_thumb_cont">
                <img src="{{asset('img/location.png')}}" alt="www.suvenia.com" class="img-responisve" uk-responsive width="100">
                <h4 style="color:#2C2C2C;" class="text-bold">Find us at:</h4>
                 <p style="text-muted">36, Ayodele Okeowo
        after Deeper Life Church
        Soluyi, Gbagada, Lagos, Nigeria</p>
                </div>
             </div>

        </div>
        </div>
        </div>

        <div class="uk-section p-0 mb-0">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.717060671916!2d3.384471314024548!3d6.557357324589873!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8d7a6191281f%3A0x2312e70626e46619!2sSuvenia.com!5e0!3m2!1sen!2sng!4v1532972394804" width="" height="450" frameborder="0" style="border:0" allowfullscreen class="uk-width-1-1"></iframe>
        </div>


        <div id="modal-close-default" uk-modal>
                <div class="uk-modal-dialog uk-modal-body big-dialog">
                    {{-- <div class=" uk-modal-header"> --}}
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
                    {{-- </div> --}}

                                <div class="row">
                                    <div class="col-md-4">
                                            <div class="service-box">
                                                    <div class="icon-ball d-inline-block">
                                                        <span class="fas fa-store"></span>
                                                    </div>

                                                    <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                                <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="service-box">
                                                    <div class="icon-ball d-inline-block">
                                                        <i class="fas fa-paint-brush"></i>
                                                    </div>

                                                    <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                                    <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="service-box">
                                                    <div class="icon-ball d-inline-block">
                                                        <i class="fab fa-accessible-icon"></i>
                                                    </div>

                                                     <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                                    <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                         </div>
                                    </div>

                                </div>

                    </div>
                    </div>

@endsection
