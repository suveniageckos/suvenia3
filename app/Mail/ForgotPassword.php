<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $type;

    /**
     * Create a new message instance. 
     *
     * @return void
     */
    public function __construct($user, $type)
    {
        $this->user = $user;

        $this->type =  $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->from(setting('site.site_email'), setting('site.title') . ' ' . setting('site.description'))
                ->subject(setting('site.title') . ' - Forgot Password')

             ->view('mails.forgot_password');
    }
}
