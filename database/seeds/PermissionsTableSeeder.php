<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2019-07-25 18:33:09',
                'updated_at' => '2019-07-25 18:33:09',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2019-07-25 18:33:12',
                'updated_at' => '2019-07-25 18:33:12',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'browse_designers',
                'table_name' => 'designers',
                'created_at' => '2019-07-26 08:38:58',
                'updated_at' => '2019-07-26 08:38:58',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'read_designers',
                'table_name' => 'designers',
                'created_at' => '2019-07-26 08:38:58',
                'updated_at' => '2019-07-26 08:38:58',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'edit_designers',
                'table_name' => 'designers',
                'created_at' => '2019-07-26 08:38:58',
                'updated_at' => '2019-07-26 08:38:58',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'add_designers',
                'table_name' => 'designers',
                'created_at' => '2019-07-26 08:38:58',
                'updated_at' => '2019-07-26 08:38:58',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'delete_designers',
                'table_name' => 'designers',
                'created_at' => '2019-07-26 08:38:58',
                'updated_at' => '2019-07-26 08:38:58',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'browse_influencers',
                'table_name' => 'influencers',
                'created_at' => '2019-07-26 18:47:17',
                'updated_at' => '2019-07-26 18:47:17',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'read_influencers',
                'table_name' => 'influencers',
                'created_at' => '2019-07-26 18:47:17',
                'updated_at' => '2019-07-26 18:47:17',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'edit_influencers',
                'table_name' => 'influencers',
                'created_at' => '2019-07-26 18:47:17',
                'updated_at' => '2019-07-26 18:47:17',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'add_influencers',
                'table_name' => 'influencers',
                'created_at' => '2019-07-26 18:47:17',
                'updated_at' => '2019-07-26 18:47:17',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'delete_influencers',
                'table_name' => 'influencers',
                'created_at' => '2019-07-26 18:47:17',
                'updated_at' => '2019-07-26 18:47:17',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'browse_eco_brandables',
                'table_name' => 'eco_brandables',
                'created_at' => '2019-07-27 08:27:42',
                'updated_at' => '2019-07-27 08:27:42',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'read_eco_brandables',
                'table_name' => 'eco_brandables',
                'created_at' => '2019-07-27 08:27:42',
                'updated_at' => '2019-07-27 08:27:42',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'edit_eco_brandables',
                'table_name' => 'eco_brandables',
                'created_at' => '2019-07-27 08:27:42',
                'updated_at' => '2019-07-27 08:27:42',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'add_eco_brandables',
                'table_name' => 'eco_brandables',
                'created_at' => '2019-07-27 08:27:42',
                'updated_at' => '2019-07-27 08:27:42',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'delete_eco_brandables',
                'table_name' => 'eco_brandables',
                'created_at' => '2019-07-27 08:27:42',
                'updated_at' => '2019-07-27 08:27:42',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'browse_eco_categories',
                'table_name' => 'eco_categories',
                'created_at' => '2019-07-27 19:59:02',
                'updated_at' => '2019-07-27 19:59:02',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'read_eco_categories',
                'table_name' => 'eco_categories',
                'created_at' => '2019-07-27 19:59:02',
                'updated_at' => '2019-07-27 19:59:02',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'edit_eco_categories',
                'table_name' => 'eco_categories',
                'created_at' => '2019-07-27 19:59:02',
                'updated_at' => '2019-07-27 19:59:02',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'add_eco_categories',
                'table_name' => 'eco_categories',
                'created_at' => '2019-07-27 19:59:02',
                'updated_at' => '2019-07-27 19:59:02',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'delete_eco_categories',
                'table_name' => 'eco_categories',
                'created_at' => '2019-07-27 19:59:02',
                'updated_at' => '2019-07-27 19:59:02',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'browse_eco_templates',
                'table_name' => 'eco_templates',
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'read_eco_templates',
                'table_name' => 'eco_templates',
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'edit_eco_templates',
                'table_name' => 'eco_templates',
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'add_eco_templates',
                'table_name' => 'eco_templates',
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'delete_eco_templates',
                'table_name' => 'eco_templates',
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'browse_eco_withdrawals',
                'table_name' => 'eco_withdrawals',
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 11:57:24',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'read_eco_withdrawals',
                'table_name' => 'eco_withdrawals',
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 11:57:24',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'edit_eco_withdrawals',
                'table_name' => 'eco_withdrawals',
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 11:57:24',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'add_eco_withdrawals',
                'table_name' => 'eco_withdrawals',
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 11:57:24',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'delete_eco_withdrawals',
                'table_name' => 'eco_withdrawals',
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 11:57:24',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'browse_eco_orders',
                'table_name' => 'eco_orders',
                'created_at' => '2019-08-01 11:51:01',
                'updated_at' => '2019-08-01 11:51:01',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'read_eco_orders',
                'table_name' => 'eco_orders',
                'created_at' => '2019-08-01 11:51:01',
                'updated_at' => '2019-08-01 11:51:01',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'edit_eco_orders',
                'table_name' => 'eco_orders',
                'created_at' => '2019-08-01 11:51:01',
                'updated_at' => '2019-08-01 11:51:01',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'add_eco_orders',
                'table_name' => 'eco_orders',
                'created_at' => '2019-08-01 11:51:01',
                'updated_at' => '2019-08-01 11:51:01',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'delete_eco_orders',
                'table_name' => 'eco_orders',
                'created_at' => '2019-08-01 11:51:01',
                'updated_at' => '2019-08-01 11:51:01',
            ),
        ));
        
        
    }
}