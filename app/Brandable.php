<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Brandable extends Model
{

    protected $table = 'eco_brandables';

    public function products(){
        return $this->hasMany("App\Product");
    }
}
