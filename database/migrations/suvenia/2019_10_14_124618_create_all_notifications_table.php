<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAllNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('all_notifications', function(Blueprint $table)
		{
			$table->bigInteger('id')->unsigned();
			$table->bigInteger('owner_id')->unsigned();
			$table->string('user_type', 191);
			$table->text('message_body', 65535);
			$table->boolean('read_at')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('all_notifications');
	}

}
