@extends('vendor.voyager.layouts.app')

@section('page_title', __('All Brandables'))
@section('templcss')
<link rel="stylesheet" href="{{ mix('css/admin_templates.css') }}">
@endsection

@section('content')
<div id="InfoDiv" asset-base="{{ asset('/') }}"></div>
<div class="container-fluid">
        <h1 class="page-title">
                <i class="voyager-brush"></i> Add Templates
            </h1>
            @can('add', app('App\Template'))
            <a href="{{ route('admin:template:add') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
             @endcan

<div class="page-content browse container-fluid">

<div class="row">
<div class="col-md-12">

<div class="panel panel-bordered">  
<div class="panel-body">
<div class="row"> 

    <div class="row justify-content-center mb-4 border">
        <div class="col-md-6 uk-text-center">
           <ul class="nav toolbarNav">
              <li class="nav-item"><a href="#" id="FlipVertical" class="nav-link"><span class="icon-flip-vertical"></span></a></li>
              <li class="nav-item"><a href="#" id="FlipHorizintal" class="nav-link"><span class="icon-flip-horizontal"></span></a></li>
              <li class="nav-item"><a href="#" id="DuplicateApp" class="nav-link"><span class="icon-duplicate"></span></a></li>
              <li class="nav-item" style="margin-right: 30px;" id="OtherColorCont">
                 <span class="text-14">Color: </span>
                 <input type='text' id="AppWideColor" class="toggleColorPallete"/>
                 </li>
              <li class="nav-item" id="outlineColorCont">
                 <span class="text-14">Outline Color:</span>
                 <input type='text' id="AppWideOutlineColor" class="toggleColorPallete"/> 
                 </li>
           </ul>
        </div>
     </div>

<div class="row">
<div class="col-md-4">
    
            <ul class="list-group list-group-flush">
               <li class="list-group-item uk-background-muted">
                  <div class="" style="">
                     <div class="form-group">
                        <textarea class="form-control app-input input-no-shadow" id="TextField" rows="3" autofocus placeholder="Add Text"></textarea>
                     </div>
                     <button type="button" class="btn btn-info btn-block" id="AddText">Add Text</button>
                  </div>
               </li>
               <li class="list-group-item uk-background-muted">
                  <div class="uk-grid-small" uk-grid>
                     <div class="form-group uk-width-expand">
                        <label class="" for="form-stacked-text"><b>Choose Font</b></label>
                        <select class="form-control" id="FontSelect" dir="ltr">
                           <optgroup>
                              <option value="'Monoton', cursive" class="monoton" selected>Monoton</option>
                              <option value="'Kranky', cursive" class="kranky">Kranky</option>
                              <option value="The Girl Next Door" class="girl_next">Girl</option>
                              <option value="'Fredericka the Great', cursive" class="fredrick">Fredrick</option>
                              <option value="'Press Start 2P', cursive" class="press_start">Press Start</option>
                              <option value="'Cabin Sketch', cursive" class="cabin_sketch">Cabin Sketch</option>
                              <option value="'Kurale', serif" class="kurale">Kurale</option>
                              <option value="'Sigmar One', cursive" class="sigmar">Sigmar</option>
                              <option value="'Paytone One', sans-serif" class="paytone">Paytone</option>
                              <option value="'Luckiest Guy', cursive" class="luckiest">Luckiest</option>
                              <option value="'Audiowide', cursive" class="audiowide">Audio Wide</option>
                              <option value="'Great Vibes', cursive" class="vibes">Vibes</option>
                              <option value="'Philosopher', sans-serif" class="philosopher">Philosopher</option>
                              <option value="'Plaster', cursive" class="plaster">Plaster</option>
                              <option value="'Exo', sans-serif" class="exo">Exo</option>
                              <option value="'Dancing Script', cursive" class="dancing">Dancing</option>
                              <option value="'Pacifico', cursive" class="pacifico">Pacifico</option>
                              <option value="'Lobster', cursive" class="lobster">Lobster</option>
                           </optgroup>
                        </select>
                     </div>
                     <!--<div class="uk-width-1-5">
                        <label class="uk-form-label" for="form-stacked-text"></label>
                        <div class="color-container">
                         <div id="AddTextColor" class="color-box-corner" style="background: #DA7F03;"></div>
                        </div>
                        </div>-->
                  </div>
               </li>
               <li class="list-group-item uk-background-muted">
                  <div class="uk-grid-small" uk-grid>
                     <div class="uk-width-expand@s">
                        <label class="uk-form-label" for="form-stacked-text"><b>Add An Outline</b></label>
                        <select class="form-control" id="TextOutline">
                           <option value="0" selected>No Outline</option>
                           <option value="1">Thin Outline</option>
                           <option value="2">Medium Outline</option>
                           <option value="3">Thick Outline</option>
                        </select>
                     </div>
                     <!--<div class="uk-width-1-5@s">
                        <label class="uk-form-label" for="form-stacked-text"></label>
                        <div class="color-container">
                         <div id="AddTextOutlineColor" class="color-box-corner" style="background: #000000;"></div>
                        </div>
                        </div>-->
                  </div>
               </li>
            </ul>

            <div class="mt-2">
                <button class="btn btn-info btn-block" id="SaveTemplate">SAVE TEMPLATE</button>
            </div>
            <form id="PostTemplate" method="POST" action="{{ route('admin:template:add') }} ">
                {{ csrf_field() }}
                
                <textarea id="templateData" name="data" class="cus-hide"></textarea>
                <textarea id="templatePhoto" name="photo_url" class="cus-hide"></textarea>
            </form>
         
     
</div>
<div class="col-md-8">
<div class="">
    <canvas id="template_canvas" width="250" height="250" class="border"></canvas>
</div>

</div>

</div>

</div> 
</div> 
</div>

 
</div>
</div>


</div>
</div>

@endsection

@push('javascript')
<script src="{{ mix('js/admin_templates.js') }}"></script>
@endpush