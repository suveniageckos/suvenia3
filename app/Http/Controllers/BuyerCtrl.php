<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuyerCtrl extends Controller
{
    public function index(Request $request){
         $categories = Category::with(['children'=> function($q){
             $q->with('children');
         }])->where('parent_id', 0)->get();
 
         $showcase_categories = Category::where('is_sub', true)->get();
         $banner_grids = DB::table('display_block')->where('block_code', 'ID:MASONRY')->get();
 
         return view('buyer.index', [
             'categories'=> $categories, 
             'showcase_categories'=> $showcase_categories,
             'banner_grids'=> $banner_grids
             ]);
    }  


}
