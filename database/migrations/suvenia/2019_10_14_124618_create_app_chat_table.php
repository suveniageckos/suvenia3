<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app_chat', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('sender');
			$table->bigInteger('reciever');
			$table->string('sender_type', 225)->nullable();
			$table->bigInteger('model_id');
			$table->text('content', 65535);
			$table->string('type', 225)->default('TEXT');
			$table->bigInteger('attachment_id')->nullable();
			$table->text('attachment_url', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('app_chat');
	}

}
