@extends('layouts.dashboard_designer')
@inject('Str', '\Illuminate\Support\Str')


@section('title', 'My Requests' )

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">

                    <div class="card-header card-header-info">
                        <div class="row">
                            <div class="col-md-4">
                                    <div><h4 class="header_text m-0">Requests</h4></div>
                            </div>

                            <div class="col-md-4"></div>
                            <div class="col-md-4 uk-text-right">
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row justify-content-center" uk-height-match="target: > div > .uk-card">
                            @forelse($requests as $request)
                                <div class="col-md-4 mb-3">

                                    <div class="uk-card uk-card-default">
                                            <div class="uk-card-header p-3">
                                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                                    <div class="uk-width-auto">
                                                        <img class="uk-border-circle" width="40" height="40" src="{{asset($request->seller->photo_url) }}">
                                                    </div>
                                                    <div class="uk-width-expand">
                                                    <h6 class="uk-margin-remove-bottom" style="font-size:14px;">{{ $request->seller->username  }}</h6>
                                                        <p class="uk-text-meta uk-margin-remove-top" style="font-size:12px;"><time datetime="{{ $request->created_at }}">{{ $request->created_at->toFormattedDateString() }}</time></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="uk-card-body p-3">
                                                <p>{{ $request->description }}</p>
                                                <p class="mb-0">Budget: <span class="text-success">&#8358;{{ number_format($request->budget) }}</span></p>
                                                <ul class="list-unstyled mt-1">
                                                <li class="d-inline-block">{{ $request->offers->count() . ' ' . Str::plural('Offer', $request->offers->count()) }} |</li>
                                                <li class="d-inline-block"> Duration: {{$request->duration}} {{ str_plural('DAY', $request->duration) }}</li>
                                                </ul>
                                            </div>
                                            <div class="uk-card-footer p-3 uk-text-right">
                                                @php
                                                    $sent_offers = $request->offers->pluck('user_id')->toArray();
                                                    $has_sent_offer = in_array(Auth::guard('designers')->user()->id, $sent_offers) ? true : false;
                                                @endphp
                                                @if(!$has_sent_offer)
                                                    <a href="javascript:;" data-url="{{ route('app:designer:dashboard:send_offer_modal', ['id'=> $request->id]) }}" modal-form data-title="Send Offer" class="btn btn-sm btn-outline-info">Send Offer</a>
                                                @else
                                                    <button class="btn btn-sm btn-outline-info" disabled>Send Offer</button>
                                                @endif
                                            </div>
                                    </div>

                                </div>
                            @empty
                            <div class="col-md-12 uk-text-center">
                                <p>Oops! No Offers this moment.</p>
                            </div>
                            @endforelse
                        </div>

                       {{ $requests->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection
