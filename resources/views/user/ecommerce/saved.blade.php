@extends('layouts.layout')

@section('title', 'Saved Items' )

@section('content')

<div class="uk-container mt-2 mb-3">
  <div class="row justify-content-center">
    <div class="col-8">
        @includeIf('partials.user_nav')
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-9">
       
        <table class="uk-table uk-table-small uk-table-divide _saved_item_table mt-4">
            <thead>
                <tr>
                    <th>PRODUCTS</th>
                    <th>Price</th>
                    <th></th>
                </tr> 
            </thead>
            <tbody class="bg-white">
                @foreach($products as $product)
                <tr class="ml-4 border-bottom">
                    <td>
                            <div class="media">
                                    <img src="https://suvenia.com/p-upl/prod-Xv2BKWXbRY.png" alt="" class="mr-2" width="100" height="130">
                                    <div class="media-body">
                                        <p class="saved_item_prod_name m-0 mb-0 uk-text-capitalize">{{ $product->product->name }}</p>
                                        <p class="saved_item_prod_seller m-0">Seller: Frankie</p>
    
                                        <ul class="list-unstyled mt-2">
                                            
                                            <li class="d-inline-block">
                                                <a class="cart_delete_order_link" href="javascript:;" data-url="{{ route('app:user:delete_saved_items', ['hash'=> $product->id]) }}"><span class="icon-delete"></span> Delete</a>    
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                    </td>
                    <td>
                            <p class="m-0 saved_item_price">&#x20a6; {{ number_format(2300, 2) }}</p>
                    </td>
                    <td>
                            @php $__cat = $product->product->categories->first(); @endphp
                        <a href="{{ route('app:ecommerce:single_product', ['category'=> $__cat->slug, 'param'=> $product->product->slug . '_' . $product->product->ref]) }}" class="btn btn-info btn-sm" target="_blank">BUY NOW</a>
                    </td>
                </tr>
               @endforeach
            </tbody>
        </table>

    </div>
  </div>

</div>

@endsection

@push('saved_item_page')

<script src="{{ asset('suv/s/pg_saved_items.bundle.js') }}"></script>
    
@endpush