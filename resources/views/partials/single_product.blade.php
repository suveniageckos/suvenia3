@php
    $photo = $product->photos->first();
    $design_colors = json_decode(json_decode($product->design->colors));
    $get_color = (!isset($selected_color) || empty($selected_color)) ? collect($design_colors)->random() : $selected_color;
@endphp
@php $__cat = $product->categories->first(); @endphp
<div class="product-box p-3">
        @if($product->categories->count() > 0)
        <a href="{{ route('app:ecommerce:single_product', ['category'=> $__cat->slug, 'param'=> $product->slug . '_' . $product->ref]) }}" target="_blank">
        @else
        <a href="javascript:;">
        @endif
        <img src="{{ Storage::url($photo->public_url) }}" uk-responsive width="250" style="background: {{ $get_color }};"></a>
    <div class="caption">
            <p class="owner"><a href="">{{ $product->user->username }}</a></p>
           <!-- <div class="divider"></div>-->
        @if($product->categories->count() > 0)
        <p class="title"><a href="{{ route('app:ecommerce:single_product', ['category'=> $__cat->slug, 'param'=> $product->slug . '_' . $product->ref]) }}" target="_blank">{{ $product->name }}</a></p>

            <!--<p class="category"><a href="">{{ $__cat->name }}</a></p> -->

        @else
        <p class="title"><a href="javascript:;">{{ $product->name }}</a></p>
        @endif

    </div>
    </div>
