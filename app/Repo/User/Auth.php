<?php
namespace App\Repo\User;
use Illuminate\Support\Facades\Auth as _Auth;

class Auth{

    public $user_instance;

    public $user_guard;

    public function __construct($user_type = '@BUYER'){
        switch($user_type){
            case '@INFLUENCER':
            $this->user_instance = '\App\Influencer';
            $this->user_guard = 'influencers';
            break;
            case '@DESIGNER':
            $this->user_instance = '\App\Designer';
            $this->user_guard = 'designers';
            break;
            case '@SELLER':
            $this->user_instance = '\App\Seller';
            $this->user_guard = 'sellers';
            break;
            default:
            $this->user_instance = '\App\Buyer';
            $this->user_guard = 'buyers';
        }
    }

    public function login(){
        $login = new \App\Repo\User\Login($this->user_instance, $this->user_guard);
        return $login;
    }

    public function logout($redirect_url){
        _Auth::guard($this->user_guard)->logout();
    }


}