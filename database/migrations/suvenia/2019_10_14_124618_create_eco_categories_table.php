<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parent_id')->nullable();
			$table->string('name', 191);
			$table->string('slug', 191)->nullable();
			$table->string('ref', 191)->nullable();
			$table->timestamps();
			$table->boolean('is_parent')->default(0);
			$table->text('page_banner', 65535)->nullable();
			$table->boolean('is_sub')->nullable()->default(0);
			$table->text('home_page_grid_banner', 65535)->nullable();
			$table->text('show_case_product', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_categories');
	}

}
