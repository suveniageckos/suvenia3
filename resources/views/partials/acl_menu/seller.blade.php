@php
  $menus = collect([
      [
          'title'=> 'My Dashbooard',
          'route'=> 'app:seller:dashboard:index',
          'icon'=> 'icon-speedometer'
      ],
      [
          'title'=> 'My Products',
          'route'=> 'app:seller:dashboard:my_products',
          'icon'=> 'icon-present'
      ],
      [
          'title'=> 'My Stores',
          'route'=> 'app:seller:dashboard:all_store',
          'icon'=> 'icon-home'
      ],
      /*[
          'title'=> 'My Orders',
          'route'=> 'app:seller:dashboard:my_orders',
          'icon'=> 'icon-basket-loaded'
      ],*/
      [
          'title'=> 'Customer Orders',
          'route'=> 'app:seller:dashboard:customer_orders',
          'icon'=> 'icon-basket'
      ],
      [
          'title'=> 'Withdrawal',
          'route'=> 'app:seller:dashboard:withdrawal',
          'icon'=> 'icon-wallet'
      ],
      [
          'title'=> 'Promotions',
          'route'=> 'app:seller:dashboard:promotions',
          'icon'=> 'icon-chart'
      ],
    //   [
    //       'title'=> 'Seller Zone',
    //       'route'=> 'app:seller:dashboard:seller_zone',
    //       'icon'=> 'icon-people'
    //   ],
      [
          'title'=> 'Profile Settings',
          'route'=> 'app:seller:dashboard:profile_settings',
          'icon'=> 'icon-settings'
      ],

  ]);
@endphp

@if(isset($type) && $type == 'admin_dropdown')

@foreach ($menus->all() as $item)
    <a href="{{ route($item['route']) }}" class="dropdown-item">
        <i class="icon {{ $item['icon']}}"></i> {{ $item['title']}}
    </a>
@endforeach

@endif

@if(isset($type) && $type == 'admin_sidebar_header')

<li class="nav-item super-header">
   <div class="uk-text-center pt-3 uk-light" style="border-bottom: 1px solid #09628a !important;">
       <img src="{{ Auth::guard('sellers')->user()->photo_url ? asset(Auth::guard('sellers')->user()->photo_url) : asset('user/default.png') }}" class="uk-border-circle m-auto" uk-responsive width="70">
       <p class="text-capitalize mt-2 mb-0">{{ Auth::guard('sellers')->user()->username }}</p>
       <p class="text-capitalize mt-0" style="font-size: 12px;">Seller</p>
   </div>
</li>

<li class="nav-item super-header">
    <a href="" class="nav-link active">
        Seller Dashboard <i class="icon icon-minus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endif

@if(isset($type) && $type == 'admin_sidebar_content')
@foreach ($menus->all() as $item)
<li class="nav-item">
        <a href="{{ route($item['route']) }}" class="nav-link">
            <i class="icon {{ $item['icon']}}" style="font-size: 14px;"></i> {{ $item['title']}}
        </a>
</li>
@endforeach

@endif

@if(isset($type) && $type == 'front_end')
@foreach ($menus->all() as $item)
<li class=""><a href="{{ route($item['route']) }}" target="_blank">{{ $item['title'] }}</a></li>
@endforeach
@endif

@if(isset($type) && $type == 'admin_sidebar_footer')

@auth('buyers')
<li class="nav-item super-header">
    <a href="{{ route('app:dashboard:my_orders') }}" class="nav-link active">
        Buyer Dashboard <i class="icon icon-cheveron-right" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@else
<li class="nav-item super-header">
    <a href="#" class="nav-link active"  uk-toggle="target: #modal-close-default3">
        Buyer Dashboard <i class="icon icon-cheveron-right" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endauth




<li class="nav-item super-header">
    <a href="{{ route('app:seller:dashboard:design_services') }}" class="nav-link active">
        Design Services <i class="icon icon-cheveron-right" style="font-size: 14px; float:right;"></i>
    </a>
</li>
<!--<li class="nav-item super-header">
    <a href="{{ route('app:influencer:dashboard:overview') }}" class="nav-link active">
        Influencer <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
<li class="nav-item super-header">
    <a href="{{ route('app:designer:dashboard:index') }}" class="nav-link active">
        Designer <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>-->
@endif
