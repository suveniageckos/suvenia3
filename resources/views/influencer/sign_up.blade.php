@extends('layouts.layout')

@section('title', 'Become An Influencer' )

@section('content')

<div class="uk-container mt-3 mb-3">
    <div class="row justify-content-center">

   <div class="col-md-4 col-sm-12 ">

     <div class="card app-box">
       <div class="box-header">
         <p class="title">COMPLETE YOUR REGISTRATION</p>
         <p class="sub-title">Please fill the form to complete your registration</p>
       </div>
       <div class="box-body">
           <form class="app-form" action="{{ route('app:influencer:process_sign_up', ['code'=> $code]) }}" method="POST" >
               @csrf
               <!--<div class="form-group {{ $errors->has('invite_code') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="Invite Code" name="invite_code"  value="{{ old('invite_code') }}">
                <p class="form-text help-block">{{ $errors->first('invite_code') }}</p>
              </div>-->

              <div class="form-group {{ $errors->has('fullname') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg textfield-format" placeholder="Full name" name="fullname"  value="{{ old('fullname') }}">
                <p class="form-text help-block">{{ $errors->first('fullname') }}</p>
              </div>

              {{-- <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="firstname" name="firstname"  value="{{ old('firstname') }}">
                <p class="form-text help-block">{{ $errors->first('firstname') }}</p>
              </div>

              <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="lastname" name="lastname"  value="{{ old('lastname') }}">
                <p class="form-text help-block">{{ $errors->first('lastname') }}</p>
              </div> --}}

              <div class="form-group {{ $errors->has('cellphone') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg textfield-format" placeholder="Phone Number" name="cellphone"  value="{{ old('cellphone') }}">
                <p class="form-text help-block">{{ $errors->first('cellphone') }}</p>
              </div>

              <div class="form-group {{ $errors->has('bio') ? 'has-error' : ''}}">
                <textarea class="form-control form-control-lg textfield-format" placeholder="Something fun about yourself" name="bio" ></textarea>
                <p class="form-text help-block">{{ $errors->first('bio') }}</p>
              </div>

              <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                <textarea class="form-control form-control-lg textfield-format" placeholder="Address" name="address" ></textarea>
                <p class="form-text help-block">{{ $errors->first('address') }}</p>
              </div>

              <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select textfield-format" name="state">
                  <option value="">State</option>
                  <option value='Abia'>Abia</option>
                  <option value='Adamawa'>Adamawa</option>
                  <option value='AkwaIbom'>AkwaIbom</option>
                  <option value='Anambra'>Anambra</option>
                  <option value='Bauchi'>Bauchi</option>
                  <option value='Bayelsa'>Bayelsa</option>
                  <option value='Benue'>Benue</option>
                  <option value='Borno'>Borno</option>
                  <option value='CrossRivers'>CrossRivers</option>
                  <option value='Delta'>Delta</option>
                  <option value='Ebonyi'>Ebonyi</option>
                  <option value='Edo'>Edo</option>
                  <option value='Ekiti'>Ekiti</option>
                  <option value='Enugu'>Enugu</option>
                  <option value='Gombe'>Gombe</option>
                  <option value='Imo'>Imo</option>
                  <option value='Jigawa'>Jigawa</option>
                  <option value='Kaduna'>Kaduna</option>
                  <option value='Kano'>Kano</option>
                  <option value='Katsina'>Katsina</option>
                  <option value='Kebbi'>Kebbi</option>
                  <option value='Kogi'>Kogi</option>
                  <option value='Kwara'>Kwara</option>
                  <option value='Lagos'>Lagos</option>
                  <option value='Nasarawa'>Nasarawa</option>
                  <option value='Niger'>Niger</option>
                  <option value='Ogun'>Ogun</option>
                  <option value='Ondo'>Ondo</option>
                  <option value='Osun'>Osun</option>
                  <option value='Oyo'>Oyo</option>
                  <option value='Plateau'>Plateau</option>
                  <option value='Rivers'>Rivers</option>
                  <option value='Sokoto'>Sokoto</option>
                  <option value='Taraba'>Taraba</option>
                  <option value='Yobe'>Yobe</option>
                  <option value='Zamfara'>Zamafara</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('state') }}</p>
              </div>

              <div class="form-group {{ $errors->has('accepted_payment') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select textfield-format" name="accepted_payment">
                      <option value="">Accept Payment</option>
                      <option value="1">Yes</option>
                      <option value="0">No</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('accepted_payment') }}</p>
              </div>

{{--
              <div class="form-group {{ $errors->has('minimum_amount') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select" name="minimum_amount">
                      <option value="">Minimum Amount</option>
                      <option value="5000">5000</option>
                      <option value="10000">10000</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('minimum_amount') }}</p>
              </div> --}}


              <div class="form-group {{ $errors->has('specialties') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select textfield-format" name="specialties">
                      <option value="">Specialties</option>
                      <option value="products">Products</option>
                      <option value="movies">Movies</option>
                      <option value="religion">Religion</option>
                      <option value="trends">Trends</option>
                      <option value="music">Music</option>
                      <option value="food">Food</option>
                      <option value="events">Events</option>
                      <option value="lifestyle">Lifestyle</option>
                      <option value="tech">Tech</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('specialties') }}</p>
              </div>


              <div class="form-group {{ $errors->has('platform') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select textfield-format" name="platform">
                      <option value="">Choose Preferred Platform</option>
                      <option value="instagram">Instagram</option>
                      <option value="facebook">Facebook</option>
                      <option value="twitter">Twitter</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('platform') }}</p>
              </div>

              <div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select" name="location">
                      <option value="">Choose Location</option>
                      @foreach($states as $state)
                       <option value="{{ $state->id }}">{{ title_case($state->name) }}</option>
                      @endforeach
                </select>
                <p class="form-text help-block">{{ $errors->first('location') }}</p>
              </div>





                  <button type="submit" class="btn btn-info  btn-block rounded">COMPLETE</button>
                </form>

       </div>

     </div>

   </div>

    </div>
</div>

@endsection
