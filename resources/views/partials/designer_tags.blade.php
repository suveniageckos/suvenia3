<div class="section bg-tags p-3 uk-animation-slide-left-small">
        <div class="uk-container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                        <div class="row no-gutter">
                            @if(!$utils->device()->isMobile() || $utils->device()->isTablet())
                            <div class="col-md-2 uk-text-right"><p class="m-0 tags_title">Related </p></div>
                            @endif
                            <div class="col-md-10">
                                @if(!$utils->device()->isMobile() || $utils->device()->isTablet())
                                <ul class="m-0 list-unstyled">
                                @else
                                <ul class="m-0 list-unstyled uk-text-center">
                                @endif
                                    @foreach($categories as $cat)
                                    
                                    @php $param = $cat->slug . '-' . $cat->ref; @endphp
                                    @if(!$utils->device()->isMobile() || $utils->device()->isTablet())
                                    <li class="d-inline-block">
                                    @else
                                    <li class="d-inline-block mb-3">
                                    @endif
                                    <a class="tag_box {{ $utils->mark_active($cat->ref, $cat_ref)}}" href="{{ route('app:designer:catalogue', ['params'=> $param ]) }}">{{ title_case($cat->title) }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>