@extends('layouts.designer')

@section('title', 'Post Request' )

@section('content')
<div class="uk-container uk-container-small mt-3 mb-4">
    
    <div class="lite-bg-header uk-text-center">
        <h1 class="m-0 header-main">Post A Design Request</h1>
    </div>

    <div class="row justify-content-center mt-3">
        <div class="col-md-7">
                <div class="buy_plan_card p-4 border-0">
                    <form class="app-form" action="{{ route('app:designer:post_request') }}" method="POST" data-post>
                            <div class="col-md-12 form-group">
                                <div class="row" uk-switcher="toggle:div > div > label; connect: #authContainer">
                                    <div class="col-md-6"> 
                                            <div class="duration_button uk-text-center">
                                                <input type="radio" name="user_type" value="BUYER" id="buyer_div" checked>
                                                <label for="buyer_div" class="w-100" onclick="switchTab('#buyer_div')">I AM A BUYER</label>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="duration_button uk-text-center">
                                                <input type="radio" name="user_type" value="SELLER" id="seller_div">
                                                <label for="seller_div" class="w-100" onclick="switchTab2('#seller_div')">I AM A SELLER</label>
                                            </div>
                                    </div>
                                    
                                </div>
                                <script>
                                   function switchTab(id){
                                            $(id).prop('checked', true);
                                            let val = $(id).val();
                                            if(val == 'BUYER' && $('#buyerLogged').length < 1){
                                                $('#postRequestButton').prop('disabled', true);
                                            }else{
                                                $('#postRequestButton').prop('disabled', false);
                                            }
                                    };
                                   function switchTab2(id){
                                            $(id).prop('checked', true);
                                            let val = $(id).val();
                                            if(val == 'SELLER' && $('#sellerLogged').length < 1){
                                                $('#postRequestButton').prop('disabled', true);
                                            }else{
                                                $('#postRequestButton').prop('disabled', false);
                                            }
                                    };
                                </script>

                                <div class="row">
                                        <ul id="authContainer" class="uk-switcher col-md-12">
                                            <li>
                                                @auth
                                                <div class="bg-light p-3" id="buyerLogged">
                                                        <div class="media">
                                                        <img src="{{ Auth::user()->photo_url }}" class="align-self-center mr-3 uk-border-circle" alt="{{ Auth::user()->username }}" width="50" uk-responsive>
                                                        <div class="media-body">
                                                            <p class="mt-0">{{ Auth::user()->username }}</p>
                                                        </div>
                                                        </div>
                                                </div>
                                                @else
                                                <p class="text-muted mt-0 mb-1" style="font-size: 14px;">You are currently not signed in</p>
                                                    <a href="{{ route('app:user:login') }}" class="btn btn-info btn-sm">Sign In or Register</a>
                                                @endif
                                            </li>
                                            <li>
                                                @auth('sellers')
                                                <div class="bg-light p-3" id="sellerLogged">
                                                        <div class="media">
                                                        <img src="{{ Auth::guard('sellers')->user()->photo_url }}" class="align-self-center mr-3 uk-border-circle" alt="{{ Auth::guard('sellers')->user()->username }}" width="50" uk-responsive>
                                                        <div class="media-body">
                                                            <p class="mt-0">{{ Auth::guard('sellers')->user()->username }}</p>
                                                        </div>
                                                        </div>
                                                </div>
                                                @else
                                                <div class="bg-light p-3">
                                                    <p class="text-muted mt-0 mb-1" style="font-size: 14px;">You are currently not signed in as a seller</p>
                                                    <a href="{{ route('app:seller:login') }}" class="btn btn-info btn-sm">Sign In or Register</a>
                                                </div>
                                                @endif
                                            </li>
                                            
                                        </ul>
                                       
                                </div>
                            </div>

                            <div class="col-md-12 form-group title">
                                <label class="app-label">Title</label>
                                <input type="text" class="form-control" name="title" value="" placeholder="E.g Create Logo">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-12 form-group description">
                                <label class="app-label">Description <span class="text-muted" style="font-size:12px;">(Include every neccesary details needed to complete your request)</span></label>
                                <textarea name="description" class="form-control" cols="30" rows="5" placeholder="Give a brief summary of your project"></textarea>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-12 form-group request_file border-bottom pb-2">
                                <div class="row no-gutters">
                                    <div class="col-md-4">
                                        <div class="js-upload" uk-form-custom>
                                                <input type="file" multiple name="request_file">
                                                <button class="btn btn-info btn-sm" type="button" tabindex="-1"><i class="icon-add"></i> Attach File</button>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-muted" style="font-size:12px;">(Attach any images or documents that might be helpful in explaining you brief.)</span>
                                    </div>
                                </div>
                                     
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-12 form-group border-bottom pb-2">
                                <div class="row no-gutters">
                                    <div class="col-md-4 mt-2">
                                            <label>Choose Category</label>
                                        <select class="form-control" name="category">
                                            @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ title_case($category->title) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pl-2 mt-2 duration">
                                        <label>Duration</label>
                                        <ul class="list-unstyled m-0">
                                            <li class="d-inline-block">
                                                <div class="duration_button">
                                                    <input type="radio" name="duration_fe" value="1" id="duration24" checked onclick="setUpDuration(this.id)">
                                                    <label for="duration24">24 Hours</label>
                                                </div>
                                            </li>
                                            <li class="d-inline-block">
                                                <div class="duration_button">
                                                    <input type="radio" name="duration_fe" value="3" id="duration3" onclick="setUpDuration(this.id)">
                                                    <label for="duration3">3 Days</label>
                                                </div>
                                            </li>
                                            <li class="d-inline-block">
                                                <div class="duration_button">
                                                    <input type="radio" name="duration_fe" value="5" id="duration5" onclick="setUpDuration(this.id)">
                                                    <label for="duration5">5 Days</label>
                                                </div>
                                            </li>
                                            <li class="d-inline-block" >
                                                    <input type="number" class="form-control" name="custom_duration" value="" placeholder="Custom" style="width:100px; height:30px; border: solid 1px rgba(151, 151, 151, 0.38);" onchange="unCheckDuration();">
                                            </li>
                                        </ul>
                                        <input type="hidden" name="duration" value="1" id="durationField">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                             </div>
                             <script>
                               function unCheckDuration(){
                                   $('input[name="duration_fe"]').prop('checked', false);
                                   $('#durationField').val($('input[name="custom_duration"]').val());
                               }
                               function setUpDuration(id){
                                $('#durationField').val($('#'+id).val());
                                $('input[name="custom_duration"]').val("");
                               }
                             </script>

                            <div class="col-md-12 form-group budget w-50">
                                <label class="app-label">Budget</label>
                                <input type="number" class="form-control" name="budget" value="" placeholder="Enter Amount">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-12 form-group pt-4">
                               <button type="submit" class="btn btn-info btn-sm" id="postRequestButton" @auth @else disabled @endauth>POST REQUEST</button>
                            </div>
                            
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection 