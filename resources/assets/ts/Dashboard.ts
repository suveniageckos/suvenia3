import Chart from "chart.js";

class Dashboard {
    constructor() {
        this.setGraph();
    }

    public setGraph(){
       
        var ctx = (<any>document).getElementById('bar-chart').getContext('2d');
        var months = <any>$('#bar-chart').attr('data-months');
        var stats = <any>$('#bar-chart').attr('data-stats');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: JSON.parse(months),
                datasets: [{
                    label: 'Total Orders',
                    backgroundColor: 'rgb(216, 216, 216)',
                    borderColor: 'rgb(216, 216, 216)',
                    data: JSON.parse(stats)
                }]
            },
        });
    }
}

$(document).ready(()=>{
if($('#bar-chart').length){
    const __graph = new Dashboard(); 
}
});
 