<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designers', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('ref', 191);
			$table->string('username', 191)->nullable();
			$table->string('slug', 225)->nullable();
			$table->string('firstname', 191)->nullable();
			$table->string('lastname', 191)->nullable();
			$table->string('email', 191)->unique();
			$table->dateTime('email_verified_at')->nullable();
			$table->string('password', 191);
			$table->text('password_token', 65535)->nullable();
			$table->text('password_token_expiry', 65535)->nullable();
			$table->string('email_token', 191)->nullable();
			$table->dateTime('email_token_expiry')->nullable();
			$table->boolean('has_password')->default(0);
			$table->boolean('email_verified')->default(0);
			$table->string('facebook', 191)->nullable();
			$table->string('twitter', 191)->nullable();
			$table->string('linkedin', 191)->nullable();
			$table->text('photo_url', 65535)->nullable();
			$table->text('image_path', 65535)->nullable();
			$table->text('cover_photo_url', 65535)->nullable();
			$table->string('phone', 191)->nullable();
			$table->decimal('balance')->default(0.00);
			$table->decimal('earnings')->default(0.00);
			$table->string('bank_sort', 191)->nullable();
			$table->string('bank_bvn', 191)->nullable();
			$table->string('bank_account', 191)->nullable();
			$table->string('bank_name', 191)->nullable();
			$table->string('transfer_code', 191)->nullable();
			$table->string('portfolio_link', 191)->nullable();
			$table->text('bio', 65535)->nullable();
			$table->string('accepted_payment', 191)->nullable();
			$table->string('minimum_amount', 191)->nullable();
			$table->boolean('is_approved')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designers');
	}

}
