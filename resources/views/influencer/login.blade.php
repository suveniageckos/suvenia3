@extends('layouts.login')

@section('title', 'Login As An Influencer' )

@section('header')
 @include('partials.login_header', ['route_name'=> 'app:influencer:landing_page'])
@endsection

@section('content')
<div class="uk-container mt-3 mb-3">
 <div class="row justify-content-center">

<div class="col-md-4">

  <div class="card app-box">
    <div class="box-header">
      <p class="title">SIGN IN</p>
      <p class="sub-title">Log in to your account to buy products that  interest you </p>
    </div>
    <div class="box-body">
        <form class="app-form" action="{{ route('app:influencer:login') }}" method="POST" >
            @csrf
               <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                 <input type="email" class="form-control form-control-lg" placeholder="Email Address" name="email"  value="{{ old('email') }}">
                 <p class="form-text help-block"></p>
               </div>

               <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                 <input type="password" class="form-control form-control-lg" placeholder="Password" name="password"  value="{{ old('password') }}">
                 <p class="form-text help-block"></p>
               </div>

               <div class="form-group form-row">
                 <div class="col-md-6">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                        <label class="custom-control-label remember-text" for="customControlAutosizing">Remember me</label>
                      </div>
                 </div>
                 <div class="col-md-6">
                   <a href="{{ route('app:influencer:forgot_password') }}" class="forgot-link">Forgot password?</a>
                 </div>
               </div>

               <button type="submit" class="btn btn-info btn-lg btn-block rounded">SIGN IN</button>
             </form>

             <p class="body-info">Don’t have an account? <a href="{{ route('app:influencer:apply') }}">Apply Now</a></p>
    </div>
<!--
    <div class="box-footer mt-2">
      <div class="fab-info">OR</div>
      <p class="m-1 info">Sign In with social media</p>
      <div class="mt-3">
          <a href="{{ route('app:user:social', ['provider'=> 'facebook']) }}" class="uk-icon-button uk-margin-small-right app-auth-icon" uk-icon="facebook"></a>
          <a href="{{ route('app:user:social', ['provider'=> 'twitter']) }}" class="uk-icon-button  uk-margin-small-right app-auth-icon" uk-icon="twitter"></a>
          <a href="{{ route('app:user:social', ['provider'=> 'instagram']) }}" class="uk-icon-button app-auth-icon" uk-icon="instagram"></a>
      </div>
    </div>
  -->
  </div>

</div>

 </div>

 <div class="row mt-4">
    <p class="text-info-offcanva">Didn't get a verification email? <a href="{{ route('app:user:resend_verification') }}">Resend Verification now</a></p>
 </div>

</div>
@endsection
