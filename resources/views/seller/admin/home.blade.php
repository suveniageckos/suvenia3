@extends('layouts.dashboard_seller')

@section('title', 'Dashboard' )

@section('content')

<div class="container-fluid">
        <div class="row">
            @foreach($dimers as $dimer)
            <div class="col-md-3">
                <div class="card p-4 uk-animation-slide-top-small">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-normal mb-2">{{ $dimer->value }}</span>
                        <span class="font-weight-light">{{ title_case($dimer->title) }}</span>
                        </div>

                        <div class="h2 text-muted">
                        <i class="icon icon-{{ $dimer->icon }}"></i>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="row ">
            <div class="col-md-9">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite">
                        <h4 class="header_text m-0">Overview</h4>
                    </div>

                    <div class="card-body p-0">

                        <div class="p-4">
                            <canvas id="bar-chart" width="100%" height="50" data-months="{{ json_encode($month_names) }}" data-stats="{{ json_encode($month_stats) }}"></canvas>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                    <div class="card mb-4 p-4 uk-text-center">
                        <p class="balance_dimer">{{ Auth::user()->balance }}</p>
                        <p class="dimer_desc_grey">Account Balance</p>
                        <p><a href="{{ route('app:seller:dashboard:withdrawal') }}" class="btn btn-info">WITHDRAW</a></p>
                    </div>
                    <div class="card mb-2 p-4 uk-text-center">
                        <p class="dashboard_home_new_prod_txt">Add new products to your Store</p>
                        <p><a href="{{ route('app:ecommerce:product_list') }}" class="btn btn-info" target="_blank">CREATE PRODUCT</a></p>
                    </div>
            </div>
        </div>

        <div class="row">
                <div class="col-md-12">
                    <div class="card uk-animation-slide-top-small">
                        <div class="card-header card-header-lite">
                            <h4 class="header_text m-0">Customer Orders</h4>
                        </div>

                        <div class="card-body">
                            <div class="mb-2">

                            </div>
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Order Date</th>
                                        <th>Customer</th>
                                        <th>Transaction Ref</th>
                                        <th>Total Sold</th>
                                        <th>Profit</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($orders as $order)

                                    <tr>
                                    <td>{{ \Carbon\Carbon::parse($order->created_at)->toDayDateTimeString() }}</td>
                                        <td>{{ $order->user->username }}</td>
                                        <td>{{ $order->order_ref }}</td>
                                        <td>{{ $order->order_items->sum('quantity') }}</td>
                                        <td>&#8358;{{ $order->order_metrics->sum('user_profit') }}</td>
                                        <td><span class="_delivery_status">{{ isset($order->order_shipping) ? $utils->set_shipping_status($order->order_shipping->status) : '' }} </span></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>
        </div>
</div>

@endsection
