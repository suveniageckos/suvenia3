class DesignerCatalogue {
    constructor() {
        this.sortByDuration();
    }   

    public sortByDuration(){
        if($('[duration-sort]').length){
            $(document).on('change', '[duration-sort]', (e)=>{
                e.preventDefault();
                let url: any = $(e.currentTarget).val();
                window.location.href = url;
            });
        }
    }
}

$(function(){
    const ___designer_catelogue = new DesignerCatalogue();
})