<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favicons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('favicons/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">

    <link rel="stylesheet" href="{{ mix('css/dashboard_vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/dashboard_main.css') }}">
    @stack('PAGE_STYLES')

    <script src="{{ mix('js/dashboard_vendor.js') }}"></script>
    <script src="{{ mix('js/dashboard_main.js') }}"></script>
    @stack('PAGE_SCRIPTS')

</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none">
            <!--<i class="fa fa-bars"></i>-->
            <i class="icon-menu" style="font-size:20px;"></i>
        </a>

        <a class="navbar-brand d-md-down-none" href="{{ route('app:base:index') }}">
            <img src="{{ $utils->get_image('site.logo') }}" alt="logo" width="100">
        </a>
        <a class="navbar-brand navbar-brand-mobile d-md-none" href="{{ route('app:base:index') }}">
            <img src="{{ asset('img/logo-icon.png') }}" alt="logo" width="36">
        </a>

        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="icon-menu" style="font-size:20px;"></i>
        </a>

        <ul class="navbar-nav ml-auto">
        <li class="nav-item ">

            <i class="icon-bell" style="font-size: 20px;"></i>
                @php $notify = DB::table('all_notifications')->where([['owner_id', Auth::guard('buyers')->user()->id], ['read_at', 0]])->count(); @endphp
                @if($notify > 0)
                <span class="badge badge-pill badge-danger">
                {{ $notify }}
                </span>
                @endif


    <div uk-dropdown="mode: click" class="p-0">
    <ul class="list-unstyled">
 @php $notifications = DB::table('all_notifications')->where([['owner_id', Auth::guard('buyers')->user()->id], ['read_at', 0]])->get()->all(); @endphp
    @foreach($notifications as $notification)
        <?php $notificationData = json_decode($notification->message_body, true); ?>
        <li class="nav-item p-2 divider">
                <a href="{{ route('app:dashboard:all_notifications', ['id'=> $notification->id]) }}">{{ $notificationData['data'] }}</a>
        </li>
    @endforeach
    </ul>
    </div>
                </li>
{{--
            <li class="nav-item ">
                <a href="#">
                    <i class="icon-envelope" style="font-size: 20px;"></i>
                    <span class="badge badge-pill badge-danger">5</span>
                </a>
                <div uk-dropdown="mode: click" class="p-0">
                    <ul class="list-unstyled">
                        <li class="d-inline-block border-bottom p-3">
                            <div class="d-flex justify-content-between">
                                <div >
                                    <i class="icon-envelope" style="font-size:20px;"></i>
                                </div>
                                <div class="ml-3">
                                        Set the direction of flex items in a flex container with direction utilities
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </li> --}}

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="avatar avatar-sm generalProfilePic" alt="{{ Auth::user()->username }}" src="{{ !is_null(Auth::user()->photo_url) ? asset(Auth::user()->photo_url) : asset('user/default.png') }}" alt="" width="100">
                    <span class="small ml-1 d-md-down-none capitalize">{{ Auth::user()->username }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    @include('partials.acl_menu.user', ['type'=>'admin_dropdown'])
                    <a href="{{ route('app:user:logout') }}" class="dropdown-item post-link">
                            <i class="fa fa-lock"></i> Logout
                    </a>
                </div>
            </li>

        </ul>
    </nav>

    <div class="main-container">

        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    @include('partials.acl_menu.user', ['type'=>'admin_sidebar_header'])
                 </ul>
                <ul class="nav children">
                    @include('partials.acl_menu.user', ['type'=>'admin_sidebar_content'])
                 </ul>
                <ul class="nav">
                    @include('partials.acl_menu.user', ['type'=>'admin_sidebar_footer'])
                 </ul>
            </nav>
        </div>

        <div class="content">

           @yield('content')

        </div>

    </div>
</div>


</body>
</html>
