<?php
namespace App;

use Ghanem\Rating\Traits\Ratingable as Rating;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'eco_products';

    use Rating;

    public function user()
    {
        return $this->belongsTo("App\Seller");
    }
    public function buyer()
    {
        return $this->belongsTo("App\Buyer");
    }
    public function categories()
    {
        return $this->belongsToMany("App\ProductCategory", "eco_category_product", 'product_id','category_id');
    }
    public function brandable()
    {
        return $this->belongsTo("App\Brandable");
    }
    public function design()
    {
        return $this->hasOne("App\Design");
    }
    public function photos()
    {
        return $this->hasMany("App\Photo");
    }
    public function seller_orders()
    {
        return $this->hasMany("App\SellerOrder");
    }
    public function orders()
    {
        return $this->hasMany("App\Order");
    }
    public function order_items()
    {
        return $this->hasMany("App\OrderItem");
    }
    public function coupon()
    {
        return $this->belongsTo("App\Coupon");
    }
    public function faqs()
    {
        return $this->hasMany("App\Faq");
    }
    public function feedbacks()
    {
        return $this->hasMany("App\Feedback");
    }

    public function order_metric()
    {
        return $this->hasMany("App\OrderMetric");
    }
}
