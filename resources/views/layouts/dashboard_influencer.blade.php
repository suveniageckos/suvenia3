<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favicons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('favicons/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="theme-color" content="#ffffff">

    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">

    <link rel="stylesheet" href="{{ mix('css/dashboard_vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/dashboard_main.css') }}">
    @stack('PAGE_STYLES')

    <script src="{{ mix('js/dashboard_vendor.js') }}"></script>
    <script src="{{ mix('js/dashboard_main.js') }}"></script>
    @stack('PAGE_SCRIPTS')

</head>
<body class="sidebar-fixed header-fixed">
        @includeIf('partials.notify')
<div class="page-wrapper">

    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none">
            <!--<i class="fa fa-bars"></i>-->
            <i class="icon-menu" style="font-size:20px;"></i>
        </a>

        <a class="navbar-brand d-md-down-none" href="{{ route('app:influencer:landing_page') }}">
            <img src="{{ $utils->get_image('site.logo') }}" alt="logo" width="100">
        </a>
        <a class="navbar-brand navbar-brand-mobile d-md-none" href="{{ route('app:influencer:landing_page') }}">
            <img src="{{ asset('img/logo-icon.png') }}" alt="logo" width="36">
        </a>

        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="icon-menu" style="font-size:20px;"></i>
        </a>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="">
                    <i class="icon-bell" style="font-size: 20px;"></i>

                </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="avatar avatar-sm generalProfilePic" alt="{{ Auth::guard('influencers')->user()->username }}" src="{{ !is_null(Auth::guard('influencers')->user()->photo_url) ? asset(Auth::guard('influencers')->user()->photo_url) : asset('user/default.png') }}" alt="" width="100">
                    <span class="small ml-1 d-md-down-none capitalize">{{ Auth::guard('influencers')->user()->username }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                @include('partials.acl_menu.influencer', ['type'=>'admin_dropdown'])
                <a href="{{ route('app:influencer:logout') }}" class="dropdown-item post-link">
                        <i class="fa fa-lock"></i> Logout
                </a>
                </div>
            </li>

        </ul>
    </nav>

    <div class="main-container">

        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    @include('partials.acl_menu.influencer', ['type'=>'admin_sidebar_header'])
                 </ul>
                <ul class="nav children">
                    @include('partials.acl_menu.influencer', ['type'=>'admin_sidebar_content'])
                 </ul>
                <ul class="nav">
                    @include('partials.acl_menu.influencer', ['type'=>'admin_sidebar_footer'])
                 </ul>
            </nav>
        </div>

        <div class="content">

           @yield('content')

        </div>

    </div>
</div>


</body>
</html>
