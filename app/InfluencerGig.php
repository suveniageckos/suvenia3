<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class InfluencerGig extends Model
{
    use Notifiable;
    protected $table = 'inflencer_gigs';

    public function product(){
        return $this->belongsTo("App\Product", "product_id");
    }

    public function user(){
        return $this->belongsTo("App\Seller", "user_id");
    }
}
