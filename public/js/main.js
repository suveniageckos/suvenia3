/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/main.js":
/*!*************************************!*\
  !*** ./resources/assets/js/main.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../ts/Post */ "./resources/assets/ts/Post.ts");

__webpack_require__(/*! ../ts/PostLink */ "./resources/assets/ts/PostLink.ts");

__webpack_require__(/*! ../ts/BatchAction */ "./resources/assets/ts/BatchAction.ts");

__webpack_require__(/*! ../ts/_includes/AppWide */ "./resources/assets/ts/_includes/AppWide.ts");

__webpack_require__(/*! ../ts/_includes/ProductDetails */ "./resources/assets/ts/_includes/ProductDetails.ts");

__webpack_require__(/*! ../ts/_includes/Reviews */ "./resources/assets/ts/_includes/Reviews.ts");

__webpack_require__(/*! ../ts/_includes/SavedItem */ "./resources/assets/ts/_includes/SavedItem.ts");

__webpack_require__(/*! ../ts/_includes/cartR */ "./resources/assets/ts/_includes/cartR.ts");

__webpack_require__(/*! ../ts/_includes/shipP */ "./resources/assets/ts/_includes/shipP.ts");

__webpack_require__(/*! ../ts/_includes/userOrders */ "./resources/assets/ts/_includes/userOrders.ts");

__webpack_require__(/*! ../ts/DesignerCatalogue */ "./resources/assets/ts/DesignerCatalogue.ts");

__webpack_require__(/*! ../ts/DataLink */ "./resources/assets/ts/DataLink.ts");

__webpack_require__(/*! ../ts/App */ "./resources/assets/ts/App.ts");

/***/ }),

/***/ "./resources/assets/ts/App.ts":
/*!************************************!*\
  !*** ./resources/assets/ts/App.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var App = /** @class */ (function () {
    function App() {
    }
    ;
    return App;
}());
var _app = new App();


/***/ }),

/***/ "./resources/assets/ts/BatchAction.ts":
/*!********************************************!*\
  !*** ./resources/assets/ts/BatchAction.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var BatchAction = /** @class */ (function () {
    function BatchAction() {
        this.triggerCheckBoxes();
        this.childrenCheckBox = '[data-children-checkbox]';
        this.submitSelected();
        this.confirm_delete();
    }
    BatchAction.prototype.triggerCheckBoxes = function () {
        var cls = this;
        $('[data-master-checkbox]').on('change', function (e) {
            if ($(e.currentTarget).is(':checked')) {
                $(cls.childrenCheckBox).prop('checked', true);
            }
            else {
                $(cls.childrenCheckBox).prop('checked', false);
            }
        });
    };
    BatchAction.prototype.submitSelected = function () {
        var cls = this;
        $(document).on('click', '[data-checkbox-trigger]', function (e) {
            e.preventDefault();
            var data = [];
            $(cls.childrenCheckBox + ':checked').each(function () {
                data.push($(this).val());
            });
            var form = cls.setForm($(e.currentTarget).attr('href'), data);
            $('body').prepend('<div id="__delete_prompt_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center">' +
                '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 16px; font-weight: 600;">DELETE ITEM(S)</h2>' +
                '<p class="m-1" style="color: #333; font-size: 14px;">Do you really want to delete this Item(s)?</p>' +
                form.join(" ") +
                '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>' +
                '<button class="btn btn-info prompt-delete-link">YES</button>' +
                '</div></div>');
            UIkit.modal("#__delete_prompt_modal").show();
            /*var data: any = [];
            $(cls.childrenCheckBox+':checked').each(function(){
                data.push($(this).val());
            });
            let form = cls.setForm($(e.currentTarget).attr('href'), data);
            $(e.currentTarget).append(form.join(" "));
            $('#BatchDeleteForm').submit();*/
        });
    };
    BatchAction.prototype.setForm = function (href, data) {
        var csrf = $('meta[name="csrf-token"]').attr('content');
        var template = [
            '<form id="BatchDeleteForm" action="' + href + '" method="POST" style="display: none;">',
            '<input type="hidden" name="_token" value="' + csrf + '">',
            '<textarea name="deleteIds">' + JSON.stringify(data) + '</textarea>',
            '</form>'
        ];
        return template;
    };
    BatchAction.prototype.confirm_delete = function () {
        $(document).on('click', '.prompt-delete-link', function () {
            $('#BatchDeleteForm').submit();
        });
    };
    return BatchAction;
}());
var __batchAction = new BatchAction();


/***/ }),

/***/ "./resources/assets/ts/DataLink.ts":
/*!*****************************************!*\
  !*** ./resources/assets/ts/DataLink.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var DataLink = /** @class */ (function () {
    function DataLink() {
        if ($('[data-link]').length) {
            $(document).on('click', '[data-link]', function (e) {
                var url = $(e.currentTarget).attr('data-link');
                e.preventDefault();
                window.location.href = url;
            });
        }
    }
    return DataLink;
}());
$(function () {
    var __dataLink = new DataLink();
});


/***/ }),

/***/ "./resources/assets/ts/DesignerCatalogue.ts":
/*!**************************************************!*\
  !*** ./resources/assets/ts/DesignerCatalogue.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var DesignerCatalogue = /** @class */ (function () {
    function DesignerCatalogue() {
        this.sortByDuration();
    }
    DesignerCatalogue.prototype.sortByDuration = function () {
        if ($('[duration-sort]').length) {
            $(document).on('change', '[duration-sort]', function (e) {
                e.preventDefault();
                var url = $(e.currentTarget).val();
                window.location.href = url;
            });
        }
    };
    return DesignerCatalogue;
}());
$(function () {
    var ___designer_catelogue = new DesignerCatalogue();
});


/***/ }),

/***/ "./resources/assets/ts/Post.ts":
/*!*************************************!*\
  !*** ./resources/assets/ts/Post.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Post = /** @class */ (function () {
    function Post(form) {
        this.form = form;
        this.sendAjax();
    }
    Post.prototype.beforeSend = function (xhr, currentForm) {
        var Spinner = $('body').attr("data-sinner");
        currentForm.find(':input').prop('disabled', true);
        //currentForm.find(':input[type="submit"]').html('<img src="'+Spinner+'" width="20" uk-responsive>');
    };
    Post.prototype.sendAjax = function () {
        var form = $(this.form);
        var cls = this;
        $(document).on('submit', this.form, function (e) {
            e.preventDefault();
            var currentForm = $(e.currentTarget);
            var btnText = currentForm.find(':input[type="submit"]').text();
            var dataForm = new FormData($(e.currentTarget).get(0));
            // let arr = currentForm.serializeArray();
            /* $.each(arr, function(key:any, val:any){
                 dataForm.append(val['name'], val['value']);
             });
             if($('input[type="file"]').length > 0){
                 $('input[type="file"]').each(function(ind, file){
                     console.log($('input[type="file"]'));
                 });
                 console.log((<any>$('input[type="file"]')[0]));
             }*/
            $.ajax({
                url: currentForm.attr('action'),
                type: currentForm.attr('method'),
                data: dataForm,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    cls.beforeSend(xhr, currentForm);
                },
                timeout: 20000
            })
                .fail(function (jqXHR, textStatus, errorThrown) {
                cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
            })
                .done(function (data, textStatus, jqXHR) {
                cls.success(data, textStatus, jqXHR, currentForm, btnText);
            });
        });
    };
    Post.prototype.success = function (data, textStatus, jqXHR, curForm, btnText) {
        if (jqXHR.status == 200) {
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            curForm.find(':input[type="submit"]').text(btnText);
            if (data.message) {
                toastr.success(data.message);
            }
            curForm.find(':input[type="submit"]').text(btnText);
            if (!data.isPaused) {
                var wait = setTimeout(function () {
                    window.location.href = data.redirect_url;
                }, 1000);
            }
            else {
                window.location.href = data.redirect_url;
            }
        }
    };
    Post.prototype.errorHandler = function (jqXHR, textStatus, errorThrown, btnText, curForm) {
        // console.log(jqXHR.status);
        if (jqXHR.status == 500 || jqXHR.status == 422) {
            var data = JSON.parse(jqXHR.responseText);
            if (typeof data.errors === 'string' || data.errors instanceof String) {
                toastr.error(data.errors, "Error");
                curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
            }
            else {
                curForm.find('.has-error').removeClass('has-error');
                curForm.find('.help-block').text('');
                $.each(data.errors, function (ind, val) {
                    curForm.find('.' + ind).addClass('has-error');
                    curForm.find('.' + ind + ' .help-block').text(val[0]);
                });
                curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
            }
        }
        else if (jqXHR.statusText == "timeout") {
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            toastr.error('Request Timeout! Please try again.');
            curForm.find(':input').prop('disabled', false);
            curForm.find(':input[type="submit"]').text(btnText);
        }
    };
    return Post;
}());
$(function () {
    var __initPost = new Post("[data-post]");
});


/***/ }),

/***/ "./resources/assets/ts/PostLink.ts":
/*!*****************************************!*\
  !*** ./resources/assets/ts/PostLink.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var PostLink = /** @class */ (function () {
    function PostLink() {
        this._el = $('.post-link');
        this.csrf = $('meta[name="csrf-token"]').attr('content');
        this.setLink();
    }
    ;
    PostLink.prototype.setLink = function () {
        var cls = this;
        $(document).on('click', '.post-link', function (e) {
            e.preventDefault();
            var __tar = $(e.currentTarget);
            var id = cls.generateId(7);
            var template = cls.setTemplate(__tar.attr('href'), id, cls.csrf);
            __tar.after(template.join(" "));
            $('#' + id).submit();
        });
    };
    PostLink.prototype.setTemplate = function (href, id, csrf) {
        var template = [
            '<form id="' + id + '" action="' + href + '" method="POST" style="display: non;">',
            '<input type="hidden" name="_token" value="' + csrf + '">',
            '</form>'
        ];
        return template;
    };
    PostLink.prototype.generateId = function (len) {
        var text = "";
        var charset = "abcdefghjklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ";
        for (var i = 0; i < len; i++)
            text += charset.charAt(Math.floor(Math.random() * charset.length));
        return text;
    };
    return PostLink;
}());
var ___initPostLink = new PostLink();


/***/ }),

/***/ "./resources/assets/ts/_includes/AppWide.ts":
/*!**************************************************!*\
  !*** ./resources/assets/ts/_includes/AppWide.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var AppWidePage = /** @class */ (function () {
    function AppWidePage() {
        this.set_ajax();
        this.set_search();
    }
    AppWidePage.prototype.set_ajax = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    AppWidePage.prototype.set_search = function () {
        $('#appWideSearchInp').on('keyup', function (e) {
            var url = $(e.target).attr('data-url');
            $.ajax({
                url: url,
                type: 'POST',
                data: { q: $(e.target).val() },
                beforeSend: function () {
                },
                timeout: 20000
            })
                .done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    $('#appWideSearchContainer').html(data);
                }
            });
        });
    };
    return AppWidePage;
}());
$(function () {
    var ____init_appWide_page = new AppWidePage();
});


/***/ }),

/***/ "./resources/assets/ts/_includes/ProductDetails.ts":
/*!*********************************************************!*\
  !*** ./resources/assets/ts/_includes/ProductDetails.ts ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ProductDetails = /** @class */ (function () {
    function ProductDetails() {
        this.order_data = {
            size: "",
            qty: 1,
            id: 0,
            color: "",
            has_size: true,
        };
        this.buy_direct_url = $("#__buyUrl").val();
        this.set_ID();
        this.set_ajax();
        this.setRating();
        this.chooseColor();
        this.chooseSize();
        this.setQuantity();
        this.direct_buy();
        this.add_to_cart();
        this.favoriteProduct();
        this.setFaq();
    }
    ProductDetails.prototype.set_ID = function () {
        this.order_data.id = $('#ProductId').val();
        this.order_data.has_size = $('#hasSize').val();
    };
    ProductDetails.prototype.set_ajax = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    ProductDetails.prototype.setRating = function () {
        $('.bar-rated').barrating({
            theme: 'css-stars',
            readonly: true,
            allowEmpty: true,
            initialRating: 0
        });
        $('#bar-rating').barrating({
            theme: 'css-stars',
            allowEmpty: true,
            initialRating: null,
            showSelectedRating: true,
            deselectable: true
        });
        //$('#bar-rating').barrating('clear');
    };
    ProductDetails.prototype.chooseColor = function () {
        var colorInput = $('.color-box-input');
        var cls = this;
        colorInput.each(function (ind, val) {
            var eachBox = this;
            $(eachBox).on('click', function () {
                //console.log($(eachBox).val());
                if ($(eachBox).is(':checked')) {
                    $('.color-box').removeClass('color-active');
                    $(eachBox).parent().addClass('color-active');
                    cls.order_data.color = $(eachBox).val();
                    $('.productImgColor').css('background', $(eachBox).val());
                }
            });
        });
    };
    ProductDetails.prototype.chooseSize = function () {
        var sizeInput = $('.size-box-input');
        var cls = this;
        sizeInput.each(function (ind, val) {
            var eachBox = this;
            $(eachBox).on('click', function () {
                if ($(eachBox).is(':checked')) {
                    $('.size-box').removeClass('active');
                    $(eachBox).parent().addClass('active');
                    cls.order_data.size = $(eachBox).val();
                }
            });
        });
    };
    ProductDetails.prototype.setQuantity = function () {
        var cls = this;
        var qtyInput = $('#numberInputSpiner');
        var currentVal = qtyInput.val();
        var plus = $('#numberAdd');
        var minus = $('#numberMinus');
        qtyInput.on('change', function (e) {
            var currentVal = $(e.currentTarget).val();
            cls.order_data.qty = parseInt(currentVal);
            if (currentVal.val() < 1) {
                $(e.currentTarget).val(1);
                currentVal = parseInt($(this).val());
                cls.order_data.qty = currentVal;
            }
        });
        plus.on('click', function () {
            qtyInput.val(++currentVal);
            cls.order_data.qty = parseInt(currentVal);
        });
        minus.on('click', function () {
            if (currentVal > 1) {
                qtyInput.val(--currentVal);
                cls.order_data.qty = parseInt(currentVal);
            }
        });
    };
    ProductDetails.prototype.__validate = function () {
        var cls = this;
        var ___data = cls.order_data;
        var status = true;
        if (cls.order_data.has_size) {
            if (___data.size == null || ___data.size == "") {
                toastr.error('You must select a size before proceeding!');
                status = false;
            }
        }
        if (___data.color == null || ___data.color == "") {
            toastr.error('You must select a color before proceeding!');
            status = false;
        }
        return status;
    };
    ProductDetails.prototype.direct_buy = function () {
        var cls = this;
        $('#__buy_direct').on('click', function (e) {
            e.preventDefault();
            if (cls.__validate()) {
                window.location.href = cls.buy_direct_url + "/" + cls.b64EncodeUnicode(JSON.stringify(cls.order_data));
            }
        });
    };
    ProductDetails.prototype.add_to_cart = function () {
        var cls = this;
        $('#__add_to_cart').on('click', function (e) {
            e.preventDefault();
            if (cls.__validate()) {
                //window.location.href = $(this).attr('data-url');
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'POST',
                    data: { item: cls.b64EncodeUnicode(JSON.stringify(cls.order_data)) },
                    timeout: 20000
                })
                    .done(function (data, textStatus, jqXHR) {
                    if (jqXHR.status == 200) {
                        $('#__data_cart_counter').empty();
                        $('#__data_cart_counter').text(data.cart_count);
                        $('body').prepend('<div id="__cart_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">' +
                            '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                            '<h3 class="uk-modal-title mb-2" style="color: #000; font-size: 24px; font-weight: 600;">Success!</h2>' +
                            '<p class="m-1" style="color: #333; font-size: 17px;">' + data.message + '</p>' +
                            '<button class="btn btn-white mr-2 uk-modal-close" href="">CONTINUE SHOPPING</button>' +
                            '<a class="btn btn-info" href="' + data.redirect_url + '">VIEW CART AND CHECKOUT</a>' +
                            '</div></div>');
                        //alert(data.message)
                        //uikit.modal.confirm(data.message);
                        UIkit.modal("#__cart_modal").show();
                    }
                });
            }
        });
    };
    ProductDetails.prototype.b64EncodeUnicode = function (str) {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
    };
    ProductDetails.prototype.favoriteProduct = function () {
        var sendAjax = function (url, id, message, callback) {
            $.ajax({
                url: url,
                type: 'POST',
                data: { id: id },
                beforeSend: function () {
                },
                timeout: 20000
            })
                .done(function () {
                callback;
                toastr.info(message);
                window.location.reload();
            });
        };
        $('#productFavorite').on('click', function (e) {
            var url = $(e.currentTarget).attr('data-url');
            var id = $(e.currentTarget).attr('data-id');
            if ($(e.currentTarget).hasClass('active')) {
                sendAjax(url, id, 'Product removed from your saved items successfully!', $(e.currentTarget).removeClass('active'));
            }
            else {
                sendAjax(url, id, 'Product added to your saved item successfully!', $(e.currentTarget).addClass('active'));
            }
        });
    };
    ProductDetails.prototype.setFaq = function () {
        $(window).on('load', function (e) {
            $('#FaqResponse').html('<div class="bg-light mt-2 mb-2 p-3">' + $('#FaqInputTrigger').val() + '</div>');
        });
        $('#FaqInputTrigger').on('change', function (e) {
            var Cont = $(e.currentTarget).val();
            $('#FaqResponse').html('<div class="bg-light mt-2 mb-2 p-3">' + Cont + '</div>');
        });
    };
    return ProductDetails;
}());
$(function () {
    var ____init_product_page = new ProductDetails();
});


/***/ }),

/***/ "./resources/assets/ts/_includes/Reviews.ts":
/*!**************************************************!*\
  !*** ./resources/assets/ts/_includes/Reviews.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ReviewPage = /** @class */ (function () {
    function ReviewPage() {
        this.set_ajax();
        this.appReviewProduct();
        this.setRating();
    }
    ReviewPage.prototype.set_ajax = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    ReviewPage.prototype.setRating = function () {
        $('.bar-rated').barrating({
            theme: 'css-stars',
            readonly: true,
            allowEmpty: true,
            initialRating: 0,
        });
        if ($('#bar-rating').length) {
            $('#bar-rating').barrating({
                theme: 'css-stars',
                allowEmpty: true,
                initialRating: null,
                showSelectedRating: true,
                deselectable: true,
                emptyValue: 0
            });
            //$('#bar-rating').barrating('clear');
        }
    };
    ReviewPage.prototype.appReviewProduct = function () {
        $(document).on('click', '.appReviewProduct', function (e) {
            e.preventDefault();
            var url = $(e.currentTarget).attr('href');
            $.ajax({
                url: url,
                type: 'GET',
                data: {},
                beforeSend: function () {
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
                .done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //console.log(data);
                    $('body').prepend('<div id="__review_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">' +
                        '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Write Your Review</h2>' +
                        '<p class="m-1" style="color: #333; font-size: 17px;">' + data + '</p>' +
                        '</div></div>');
                    UIkit.modal("#__review_modal").show();
                }
            });
        });
    };
    return ReviewPage;
}());
$(function () {
    var ____init_savedItem_page = new ReviewPage();
});


/***/ }),

/***/ "./resources/assets/ts/_includes/SavedItem.ts":
/*!****************************************************!*\
  !*** ./resources/assets/ts/_includes/SavedItem.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CartItem_1 = __webpack_require__(/*! ./includes/CartItem */ "./resources/assets/ts/_includes/includes/CartItem.ts");
var SavedItemPage = /** @class */ (function () {
    function SavedItemPage(__cartItem) {
        if (__cartItem === void 0) { __cartItem = new CartItem_1.CartItem; }
        this.__cartItem = __cartItem;
        this.set_ajax();
        if ($('#SavedItemPage').length) {
            this.__cartItem.__initiate_delete();
            this.__cartItem._delete_cart_item();
        }
    }
    SavedItemPage.prototype.set_ajax = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    return SavedItemPage;
}());
$(function () {
    var ____init_savedItem_page = new SavedItemPage();
});


/***/ }),

/***/ "./resources/assets/ts/_includes/cartR.ts":
/*!************************************************!*\
  !*** ./resources/assets/ts/_includes/cartR.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CartItem_1 = __webpack_require__(/*! ./includes/CartItem */ "./resources/assets/ts/_includes/includes/CartItem.ts");
var CartReviewPage = /** @class */ (function () {
    function CartReviewPage(__cartItem) {
        if (__cartItem === void 0) { __cartItem = new CartItem_1.CartItem; }
        this.__cartItem = __cartItem;
        this.set_ajax();
        if ($('#CartPage').length) {
            this.__cartItem.__initiate_delete();
            this.__cartItem._delete_cart_item();
        }
        this.__initiate_update();
    }
    CartReviewPage.prototype.set_ajax = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    CartReviewPage.prototype.__initiate_update = function () {
        $(document).on('change', '.cart_prod_quantity_input', function (e) {
            var curTarg = $(e.currentTarget);
            $.post(curTarg.attr('data-url'), { qty: curTarg.val() });
            window.location.reload();
        });
    };
    return CartReviewPage;
}());
$(function () {
    var ____init_cR_page = new CartReviewPage();
});


/***/ }),

/***/ "./resources/assets/ts/_includes/includes/CartItem.ts":
/*!************************************************************!*\
  !*** ./resources/assets/ts/_includes/includes/CartItem.ts ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CartItem = /** @class */ (function () {
    function CartItem() {
    }
    CartItem.prototype._delete_cart_item = function () {
        $('.cart_delete_order_link').each(function () {
            $(this).on('click', function () {
                $('body').prepend('<div id="__delete_cart_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center">' +
                    '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                    '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 24px; font-weight: 600;">DELETE ITEM</h2>' +
                    '<p class="m-1" style="color: #333; font-size: 17px;">Do you really want to delete this Item?</p>' +
                    '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>' +
                    '<a class="btn btn-info cart-item-delete-link" href="' + $(this).attr('data-url') + '">YES</a>' +
                    '</div></div>');
                UIkit.modal("#__delete_cart_modal").show();
            });
        });
    };
    CartItem.prototype.__initiate_delete = function () {
        $(document).on('click', '.cart-item-delete-link', function (e) {
            e.preventDefault();
            $.post($(e.currentTarget).attr('href'));
            window.location.reload();
        });
    };
    return CartItem;
}());
exports.CartItem = CartItem;


/***/ }),

/***/ "./resources/assets/ts/_includes/shipP.ts":
/*!************************************************!*\
  !*** ./resources/assets/ts/_includes/shipP.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CartItem_1 = __webpack_require__(/*! ./includes/CartItem */ "./resources/assets/ts/_includes/includes/CartItem.ts");
var CartShippingPage = /** @class */ (function () {
    function CartShippingPage(__cartItem) {
        if (__cartItem === void 0) { __cartItem = new CartItem_1.CartItem; }
        this.__cartItem = __cartItem;
        this.set_ajax();
        if ($('#ShipPage').length) {
            this.__cartItem.__initiate_delete();
            this.__cartItem._delete_cart_item();
        }
        this.add_ship_modal();
        this.set_default_ship_modal();
        this.set_zone();
        this.edit_ahipping_address();
    }
    CartShippingPage.prototype.set_ajax = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    CartShippingPage.prototype.add_ship_modal = function () {
        var cls = this;
        $(document).on('click', '.__ship_add_address_def', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(e.currentTarget).attr('data-url'),
                type: 'GET',
                data: {},
                beforeSend: function () {
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
                .done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //console.log(data);
                    $('body').prepend('<div id="__zone_address_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">' +
                        '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Add Address</h2>' +
                        '<p class="m-1" style="color: #333; font-size: 17px;">' + data + '</p>' +
                        '</div></div>');
                    UIkit.modal("#__zone_address_modal").show();
                }
            });
        });
    };
    CartShippingPage.prototype.set_default_ship_modal = function () {
        $(document).on('click', '.__ship_select_address_def', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(e.currentTarget).attr('data-url'),
                type: 'GET',
                data: {},
                beforeSend: function () {
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
                .done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //console.log(data);
                    $('body').prepend('<div id="__zone_address_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">' +
                        '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Add Address</h2>' +
                        '<p class="m-1" style="color: #333; font-size: 17px;">' + data + '</p>' +
                        '</div></div>');
                    UIkit.modal("#__zone_address_modal").show();
                }
            });
        });
    };
    CartShippingPage.prototype.set_zone = function () {
        $(document).on('change', '._zone_selector', function (e) {
            var inp = $(e.target);
            $.ajax({
                url: $(e.currentTarget).attr('data-url'),
                type: 'POST',
                data: {},
                beforeSend: function () {
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
                .done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    if (data.message) {
                        toastr.success(data.message);
                    }
                    var wait = setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 1000);
                }
            });
        });
    };
    CartShippingPage.prototype.edit_ahipping_address = function () {
        $(document).on('click', '.__ship_edit_trigger', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(e.currentTarget).attr('href'),
                type: 'GET',
                data: {},
                beforeSend: function () {
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
                .done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    $('body').prepend('<div id="__zone_address_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">' +
                        '<button class="uk-modal-close-default" type="button" uk-close></button>' +
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Edit Address</h2>' +
                        '<p class="m-1" style="color: #333; font-size: 17px;">' + data + '</p>' +
                        '</div></div>');
                    UIkit.modal("#__zone_address_modal").show();
                }
            });
        });
    };
    return CartShippingPage;
}());
$(function () {
    var ____init_cSP_page = new CartShippingPage();
});


/***/ }),

/***/ "./resources/assets/ts/_includes/userOrders.ts":
/*!*****************************************************!*\
  !*** ./resources/assets/ts/_includes/userOrders.ts ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var UserOrdersPage = /** @class */ (function () {
    function UserOrdersPage() {
        this.__set_order_filter();
    }
    UserOrdersPage.prototype.__set_order_filter = function () {
        $(document).on('change', '._order_item_filter_select_input', function (e) {
            var val = $(e.target).val();
            window.location.href = val;
        });
    };
    return UserOrdersPage;
}());
$(function () {
    var ____init_userOder_page = new UserOrdersPage();
});


/***/ }),

/***/ 1:
/*!*******************************************!*\
  !*** multi ./resources/assets/js/main.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\suvenia3\resources\assets\js\main.js */"./resources/assets/js/main.js");


/***/ })

/******/ });