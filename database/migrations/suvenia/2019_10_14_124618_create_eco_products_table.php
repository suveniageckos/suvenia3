<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('user_type', 225)->nullable();
			$table->integer('brandable_id')->nullable();
			$table->integer('design_id')->nullable();
			$table->integer('coupon_id')->nullable();
			$table->string('name', 191)->nullable();
			$table->string('slug', 191)->nullable();
			$table->string('ref', 191)->nullable();
			$table->text('description', 65535)->nullable();
			$table->integer('price')->nullable();
			$table->integer('sku')->nullable();
			$table->integer('quantity')->nullable();
			$table->boolean('is_archived')->default(0);
			$table->boolean('is_published')->default(1);
			$table->boolean('is_approved')->default(0);
			$table->dateTime('approval_date')->nullable();
			$table->text('temp_data_design', 65535)->nullable();
			$table->text('temp_data_photos', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_products');
	}

}
