@extends('layouts.designer2')

@section('title', 'Explore Designers' )

@section('content')

@includeIf('partials.designer_tags', ['categories'=> $categories, 'cat_ref'=> $cat_ref])
 
<div class="section bg-white p-2">
<div class="uk-container mt-3">
    <div class="row">
      @if(!$utils->device()->isMobile() || $utils->device()->isTablet())
        <div class="col-md-3 uk-animation-slide-left-small">
            <div class="sortbox bg-white p-4 border">
                <div class="single_sorter border-bottom p-1">
                    <h2 class="title m-1">All Categories</h2>
                    <div class="pane">
                        <ul class="list-unstyled m-0">
                            @foreach($categories as $cat)
                            @php $param = $cat->slug . '-' . $cat->ref; @endphp
                            <li>
                                <div class="d-flex justify-content-between">
                                    <div class="sort_name">
                                        <a href="{{ route('app:designer:catalogue', ['params'=> $param]) }}">{{ title_case($cat->title) }}</a>
                                    </div>
                                    <div class="counter">
                                        ({{ $cat->gigs->count()}})
                                    </div>
                                </div> 
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="single_sorter border-bottom p-1">
                    <h2 class="title m-1">Duration</h2>
                    @php
                       $sort_durations=[
                            [
                                'time'=> 'Any',
                                'count'=> $utils->get_designer_duration_count(0),
                                'value'=> ''
                            ],
                            [
                                'time'=> '24 Hours',
                                'count'=> $utils->get_designer_duration_count(1),
                                'value'=> 1,
                            ],
                            [
                                'time'=> '3 Days',
                                'count'=> $utils->get_designer_duration_count(3),
                                'value'=> 3

                            ],
                            [
                                'time'=> '7 Days',
                                'count'=> $utils->get_designer_duration_count(7),
                                'value'=> 7
                            ],
                        ];
                    @endphp
                    <div class="pane">
                        <ul class="list-unstyled m-0">
                            @foreach (json_decode(json_encode($sort_durations), false) as $duration)
                            @php
                                $buildParam = [
                                    'params'=> $currentUrlParam,
                                    'dur'=> $duration->value,
                                    'format'=> request()->format,
                                    'skills'=> request()->skills,
                                 ];
                                 $mark_selected_dur = request()->dur == $duration->value ? "checked" : "";
                            @endphp
                            <li class="mb-1">
                                <div class="d-flex justify-content-between">
                                    <div class="sort_radio">
                                        <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio{{ $loop->index }}" name="customRadio" duration-sort  class="custom-control-input" value="{{ route('app:designer:catalogue', $buildParam) }}" {{  $mark_selected_dur}}>
                                        <label class="custom-control-label" for="customRadio{{ $loop->index }}">{{ $duration->time }}</label>
                                        </div>
                                    </div>
                                    <div class="counter">
                                        ({{ $duration->count}})
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <!--<div class="single_sorter border-bottom p-1">
                    <h2 class="title m-1">Format</h2>
                    @php
                       $sort_formats=[
                            [
                                'name'=> 'JPG',
                                'count'=> 1000
                            ],
                            [
                                'name'=> 'PDF',
                                'count'=> 1000
                            ],
                            [
                                'name'=> 'PNG',
                                'count'=> 1000
                            ],
                        ];
                    @endphp
                    <div class="pane">
                        <ul class="list-unstyled m-0">
                            @foreach (json_decode(json_encode($sort_formats), false) as $format)
                            <li class="mb-1">
                                <div class="d-flex justify-content-between">
                                    <div class="sort_checkbox">
                                        <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck{{$loop->index}}">
                                        <label class="custom-control-label" for="customCheck{{$loop->index}}">{{ $format->name }}</label>
                                        </div>
                                    </div>
                                    <div class="counter">
                                        ({{ $duration->count}})
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="single_sorter border-bottom p-1">
                    <h2 class="title m-1">Skills</h2>
                    @php
                       $sort_formats=[
                            [
                                'name'=> 'JPG',
                                'count'=> 1000
                            ],
                            [
                                'name'=> 'PDF',
                                'count'=> 1000
                            ],
                            [
                                'name'=> 'PNG',
                                'count'=> 1000
                            ],
                        ];
                    @endphp
                    <div class="pane">
                        <ul class="list-unstyled m-0">
                            @foreach (json_decode(json_encode($sort_formats), false) as $format)
                            <li class="mb-1">
                                <div class="d-flex justify-content-between">
                                    <div class="sort_checkbox">
                                        <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheckSkill{{$loop->index}}">
                                        <label class="custom-control-label" for="customCheckSkill{{$loop->index}}">{{ $format->name }}</label>
                                        </div>
                                    </div>
                                    <div class="counter">
                                        ({{ $duration->count}})
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>-->

            </div>
        </div>
        @endif

        <div class="col-md-9 uk-animation-slide-bottom-small"> 
            <div class="row mb-3">
                <div class="col-md-4">
                    @if($utils->device()->isMobile())
                    <div class="d-inline-block mr-3">
                     <button class="btn btn-sm btn-filter" uk-toggle="target: #FilterOffcanvas">Filter</button>
                    </div>
                    @endif
                <h3 class="m-0 catalogue_title d-inline-block">{{ title_case($category->title) }}</h3>
                </div>
                <div class="col-md-8"></div>
            </div>
            <div class="row" uk-height-match="target: > div > .uk-card">
                @forelse ($category->gigs as $gig)
               
                    <div class="col-md-4 mb-2">
                        @includeIf('partials.single_gig', ['gig' => $gig])
                    </div>
                @empty
                    <div class="col-md-12 mb-2 uk-text-center">
                        @if(request()->q)
                        <p class="text-muted">Oops!! no result was found..</p>
                        @else
                        <p class="text-muted">Oops!! no gigs for this category yet..</p>
                        @endif
                          
                    </div>
                @endforelse 
            </div>
        </div>

    </div>
</div>
</div>

   
<div id="FilterOffcanvas" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <div class="row">
                    <div class="col-md-12">
                            <div class="sortbox bg-white p-4">
                                <div class="single_sorter border-bottom p-1">
                                    <h2 class="title m-1">All Categories</h2>
                                    <div class="pane">
                                        <ul class="list-unstyled m-0">
                                                @foreach($categories as $cat)
                                                @php $param = $cat->slug . '-' . $cat->ref; @endphp
                                                <li>
                                                    <div class="d-flex justify-content-between">
                                                        <div class="sort_name">
                                                            <a href="{{ route('app:designer:catalogue', ['params'=> $param]) }}">{{ title_case($cat->title) }}</a>
                                                        </div>
                                                        <div class="counter">
                                                            ({{ $cat->gigs->count()}})
                                                        </div>
                                                    </div> 
                                                </li>
                                                @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="single_sorter border-bottom p-1">
                                        <h2 class="title m-1">Duration</h2>
                                        @php
                                           $sort_durations=[
                                                [
                                                    'time'=> 'Any',
                                                    'count'=> $utils->get_designer_duration_count(0),
                                                    'value'=> ''
                                                ],
                                                [
                                                    'time'=> '24 Hours',
                                                    'count'=> $utils->get_designer_duration_count(1),
                                                    'value'=> 1,
                                                ],
                                                [
                                                    'time'=> '3 Days',
                                                    'count'=> $utils->get_designer_duration_count(3),
                                                    'value'=> 3
                    
                                                ],
                                                [
                                                    'time'=> '7 Days',
                                                    'count'=> $utils->get_designer_duration_count(7),
                                                    'value'=> 7
                                                ],
                                            ];
                                        @endphp
                                        <div class="pane">
                                            <ul class="list-unstyled m-0">
                                                @foreach (json_decode(json_encode($sort_durations), false) as $duration)
                                                @php
                                                    $buildParam = [
                                                        'params'=> $currentUrlParam,
                                                        'dur'=> $duration->value,
                                                        'format'=> request()->format,
                                                        'skills'=> request()->skills,
                                                     ];
                                                     $mark_selected_dur = request()->dur === $duration->value ? "checked" : "";
                                                @endphp
                                                
                                                <li class="mb-1">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="sort_radio">
                                                            <div class="custom-control custom-radio">
                                                            <input type="radio" id="customRadio{{ $loop->index }}" name="customRadio" duration-sort  class="custom-control-input" value="{{ route('app:designer:catalogue', $buildParam) }}" {{  $mark_selected_dur}}>
                                                            <label class="custom-control-label" for="customRadio{{ $loop->index }}">{{ $duration->time }}</label>
                                                            </div>
                                                        </div>
                                                        <div class="counter">
                                                            ({{ $duration->count}})
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                
                            </div>
                        </div>
                        
            </div>
        </div>
</div>

@endsection  