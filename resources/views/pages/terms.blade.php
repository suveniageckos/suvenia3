@extends('layouts.layout')

@section('title', 'Terms and Conditions' )

@section('content')








<div class="uk-section bg-gray pt-5 mt-3">
        <div class="uk-container uk-container-medium">
            <div class="row">
                <div class="col-12">


                    <div class="uk-child-width-1-1@" uk-grid>
                        <div>
                            <div uk-grid>
                                <div class="uk-width-auto@m">
                                    <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                                        <li><a href="#" style="font-size:1em; text-transform:capitalize !important;">General Terms & Conditions</a></li>
                                        <li><a href="#" style="font-size:1em; text-transform:capitalize !important;">Seller's Terms & Conditions</a></li>
                                        {{-- <li><a href="#" style="font-size:1em; text-transform:capitalize !important;">Influencer's Terms & Conditions</a></li> --}}
                                    </ul>
                                </div>
                                <div class="uk-width-expand@m">

                                    <ul id="component-tab-left" class="uk-switcher">
                                        <li style="text-align:justify;">
                                            <p class="uk-text-lead">GENERAL TERMS &amp; CONDITIONS</p>
                                            <h4 class="uk-text-bold">1. General Information</h4>
                                            (a) This agreement governs your use of Suvenia's site and service, including all orders made or processed for products or services in connection with the site and service. You and Suvenia.com, providing custom, on-demand merchandise printing, sales, and other related services ("Suvenia"), are the parties to this agreement. If you choose to use other features of the site and service, like opening a store to sell custom, print-on-demand products, other terms may apply in addition to these. No exceptions to these terms are effective unless Suvenia has agreed to them in writing.
                                            <p>
                                            (b) Suvenia.com may change these terms from time to time. If Suvenia.com makes changes to these terms, you will be notified and asked to accept the new terms as a condition of continuing to use Suvenia’s site and service. If you disagree with any amendments, you must stop using Suvenia's site and service.
                                            </p>
                                        <h4 class="uk-text-bold">2. Use of the Site and Service Generally</h4>
                                        <p>
                                        (a) Suvenia owns all intellectual property and other rights, title and interest in and to its site and service (except for user-provided content). Your use of the site and service does not grant you any right, title or interest to these properties, except as follows. Suvenia grants you a limited, revocable license to access and use the site and service for its intended purpose: the provision of an online, on-demand, customizable merchandising solution. You may only use the site and service according to Suvenia's terms, rules, and guidelines found on its site, and Suvenia may revoke this license and limit your access to the site and service according to Section 15 (Termination of Access).
                                        </p>
                                        <p>(b) You may not</p>
                                        <ol type="I">
                                            <li>interfere with the site and service by using viruses or any other programs or technology designed to disrupt or damage any software or hardware;</li>
                                            <li>modify, copy, create derivative works from, reverse engineer, decompile or disassemble any technology used to provide the site and service;</li>
                                            <li>use a robot or other automated means to monitor the activity on or copy information or pages from the site and service, except search engines, traffic counters, or similar basic performance monitoring technology;</li>
                                            <li>impersonate another person or entity;</li>
                                            <li>use any meta tags, search terms, keywords, or similar tags that contain Suvenia's name or trademarks;</li>
                                            <li>engage in any activity that interferes with another user's ability to use or enjoy the site and service, including activity that places a disproportionate burden on the site and service compared to ordinary use from a single, ordinary user;</li>
                                            <li>assist or encourage any third party in engaging in any activity prohibited by this agreement; or</li>
                                            <li>use the site and service to promote hate speech, obscenity, or any content that violates Suvenia’s design policy, which Suvenia may change from time to time in Suvenia’s sole discretion.</li>
                                        </ol>

                                        <h4 class="uk-text-bold">3. Stores</h4>
                                        <p>(a) Two store types are available on Suvenia's site. First, there are stores designed and operated by Suvenia itself ("Suvenia Store(s)") and stores designed and operated by independent shop owners ("Partner Store(s)"). Information about the owner of any store is available as a tab in each store.</p>

                                          <p>(b) Independent store owners of Partner Stores are solely responsible for the products and designs offered in those shops, the design of those shops, and the advertising of the articles offered. Suvenia does not use designs or products of shop owners until a customer places an order that includes one of those designs or products.</p>
                                          <h4 class="uk-text-bold">4. Product Sales</h4>

                                          <p>(a) By placing an order using Suvenia's site and service, a customer makes a binding offer for a contract of sale or, as the case may be, a contract for work and materials with Suvenia only (no contract exists between the customer and any applicable shop owner). Suvenia sends an order confirmation via e-mail to the customer. The order confirmation is not an acceptance of the offer, but only acknowledges that the order was received. The offer is only accepted when Suvenia confirms that production has completed in a second e-mail. Suvenia cannot guarantee the continued availability of any products or designs found on its site.</p>

                                          <p>(b) Information, drawings, figures, technical data, specifications of weight, measurements and services contained in brochures, catalogues, newsletters, ads, or price lists are purely informational. Suvenia cannot guarantee the correctness of this information, and if there is any inconsistency between the information described above and the information in an order confirmation email, the order confirmation email controls.</p>

                                          <p>(c) Suvenia reserves the right to reject orders for any reason or no reason. If Suvenia rejects an order, it will notify the customer.</p>

                                         <p>(d) Suvenia's performance of an order is completed when the shipment provider completes delivery to the customer's address, according to the records of the shipment provider. If there is an interruption of delivery, and Suvenia cannot replace the order in a reasonable amount of time, Suvenia will notify the customer immediately.</p>

                                         <p> (e) If there is a product defect or if you are dissatisfied with your order for any reason, Suvenia’s refund policy will apply, which Suvenia may change at any time in its sole discretion.</p>

                                         <h4 class="uk-text-bold"> 5. Delivery & Shipment</h4>
                                         <p>(a) Suvenia warrants that it will ship orders within three weeks after orders are placed. Normally, goods are shipped within a few days and typical delivery times are 3-5 days, but in certain circumstances shipment and delivery can take up to three weeks. Customers and Suvenia may separately agree to more specific delivery times and terms in a separate writing or agreement (such as a request for express shipping).</p>

                                        <p>(b) Suvenia will make delivery using a shipment service provider of its choosing. The customer must pay standard shipping costs which may depend on order value and shipping destinations. Shipping costs are displayed at checkout.</p>

                                        <h4 class="uk-text-bold"> 6. Price</h4>
                                        <p>(a) All prices found on Suvenia's site are final, and may change from time to time. Shipping and handling are billed and shown separately at checkout (or on invoices, if applicable). The shipping address and order amount may affect certain costs, and sales taxes may apply to some orders.</p>

                                        <p>(b) For customers ordering from outside Nigeria, all product prices are net of local taxes and fees. If, according to applicable law, the goods are subject to sales taxes, import duties, import brokerage fees, or other taxes or fees in the recipient's country, then the customer has the sole responsibility to pay these taxes and fees.</p>

                                        <p>(c) Customers must pay the purchase price, applicable sales taxes, and shipping and handling charges immediately upon placing an order, without deduction.</p>

                                        <h4 class="uk-text-bold"> 7. Payment</h4>

                                        <p>(a) Customers may choose to pay by direct debit, bank transfer, advance payment, or other payment methods. Suvenia reserves the right to limit the method of payment chosen by the customer depending on order value, shipment destination, or other objective criteria.</p>

                                        <p>(b) If the customer selects a payment method or provides payment information that makes it impossible or impractical for Suvenia to receive payment (for example, the customer's account lacks sufficient funds, or the customer provided incorrect payment information) through no fault of Suvenia's own, the customer agrees that Suvenia may add an additional charge to the order to recoup costs associated with processing or attempting to process the impossible or impractical transaction.</p>
                                        <p style="font-weight:700;">(c) Suvenia may sub-contract third parties to process payment.</p>

                                        <p>(d) If the customer fails to pay, Suvenia may assign its claims to a debt collection agency and transfer the personal data required for collecting payment to these third parties.</p>

                                        <h4 class="uk-text-bold"> 8. Title to Products</h4>
                                        <p>Until Suvenia receives full payment for an order and the order is shipped, title to the goods remains with Suvenia. Upon transfer of the goods to the carrier, title and risk of loss passes to the customer. The customer should handle products with care until the transfer of ownership is complete (for example, in case of a product return).</p>
                                        <h4 class="uk-text-bold"> 9. Disclaimers</h4>

                                           <p>(a) Suvenia provides the site and service on an "as is" and "as available" basis. Suvenia does not represent or warrant that the site and service or its use: (i) will be uninterrupted, timely, safe, or secure, (ii) will be free of inaccuracies, errors, or vulnerabilities, (iii) will meet your expectations or requirements, or (iv) will operate in the configuration or with the hardware or software you use. Suvenia hereby disclaims any and all express or implied warranties, including without limitation, warranties of fitness for a particular purpose, merchantability, and non-infringement to the fullest extent of the law, except to the extent that this agreement provides otherwise.</p>

                                            <p>(b) You agree that Suvenia has no responsibility for any damages suffered by you in connection with the site and service, and that use of the site and service, including all content, data or software distributed by, downloaded, or accessed from or through the site and service, is at your sole risk. You understand and agree that you will be solely responsible for any damage to your business or your computer system or any loss of data that results from your actions and your use of the site and service. Suvenia does not control or endorse in any respect any information, products, or services offered by third parties through the site and service, and is not responsible for any possible representations and warranties these third parties may make.</p>

                                            <p> (c) Due to normal changes in our industry and in our technical production processes, Suvenia may reasonably deviate from the descriptions and information found in its brochures, catalogues, and other documents with respect to material, color, weight, measurements, design, or other features.</p>

                                            <p>(d) Suvenia may use subcontractors or third parties to provide certain elements of its site and service. You agree that Suvenia will not be liable to you in any way for your use of these services.</p>

                                            <h4 class="uk-text-bold"> 10. Limitation of Liability</h4>
                                        <p>(a) You use Suvenia's site and service at your own risk. Suvenia provides its site and service without any express or implied warranties (See Section 9(a) (Disclaimers) above). Suvenia is not responsible for the actions, content, information, or data of third parties. You release us, our directors, officers, employees, and agents from any claims and damages, known and unknown, arising out of or in any way connected with any claim you have against Suvenia or any related third parties. If you are a California resident, you waive California Civil Code §1542, which says in full: "A general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which if known by him must have materially affected his settlement with the debtor."</p>

                                        <p>(b) Suvenia is not liable to you for any lost profits or other consequential, special, indirect, or incidental damages arising out of or relating to the site and service or any agreement between you and Suvenia, even if advised of the possibility of such damages. Suvenia’s aggregate liability arising out of or in connection with the site and service or any agreement between you and Suvenia may not exceed the lesser $100 or the amount of cash actually exchanged between you and Suvenia within the past six months. Applicable law may not allow the limitation or exclusion of liability described in this Section. In such cases, Suvenia's liability will be limited to the fullest extent permitted by applicable law.</p>

                                        <h4 class="uk-text-bold"> 11. User Representations and Warranties; Suvenia's Right to Refuse Performance</h4>

                                        <p>(a) If you upload designs or make changes to products in any way (for example, adding custom text), you represent and warrant to Suvenia that you have all necessary rights (for example, in trademark, copyright, privacy rights, publicity rights, common law, etc.) to use the text or design without violating the rights of any third party. Suvenia may, in its sole discretion, refuse to print any designs or text that you submit. However, Suvenia is not obligated to review any of your submissions.</p>

                                        <p>(b) You agree to (i) indemnify and (ii) release Suvenia from all liability (including costs and attorney's fees) for claims relating to the alleged or actual infringement of any third-party rights and any law to the extent that such claims relate to your use of Suvenia's site and service. If you are a store owner with Suvenia, Suvenia may offset amounts in your store owner account to satisfy the indemnification payments owed according to this paragraph.</p>
                                        <h4 class="uk-text-bold"> 12. Data Protection</h4>
                                        <p>Suvenia collects and uses your information according to its privacy policy. You acknowledge that you are responsible for the securing your own information (including passwords), keeping your sensitive information confidential, and taking responsibility for actions taken in connection with your user account.</p>

                                        <h4 class="uk-text-bold"> 13. Dispute Resolution</h4>
                                        <p>The formation, construction, and performance of this agreement (and all other agreements incorporating this agreement by reference) must be construed in accordance with the laws of the Federal republic of Nigeria without regard to their choice of law rules. With respect to any disputes, you and Suvenia agree to submit to the personal and exclusive jurisdiction of the state and federal courts located within Lagos, Nigeria. The parties expressly exclude the application of the UN Convention on Contracts for the International Sale of Goods.</p>
                                        <h4 class="uk-text-bold"> 14. Digital Millennium Copyright Act</h4>
                                        <p>Suvenia will respond to legitimate requests under the Digital Millennium Copyright Act ("DMCA"), and retains the right to deny service to any user if in Suvenia’s sole discretion, the usage or content poses a risk of allegations of infringement of the copyright (or other intellectual property right) of others. If you become aware of user content on Suvenia's site and service that infringes your copyright right, you may contact Suvenia to request a takedown of the alleged infringement.</p>

                                        <h4 class="uk-text-bold"> 15. Termination of Access</h4>
                                        <p>(a) Suvenia may refuse to provide its site and service to you, in full or in part, effective immediately and without notice, for any reason or no reason, but especially if Suvenia believes that you (or any others whom Suvenia believes that you act in concert with) have violated or will violate any laws, rights, or term (or the spirit of any term) of any agreement you are a party to with Suvenia, its affiliates, contractual partners, or users; or if Suvenia believes in its sole discretion that your use of the site and service may create any risk (including any legal risk) for Suvenia, its affiliates, contractual partners, or users. Limitation of access may include removal or modification of content that you have uploaded or otherwise sent to or through the site and service.</p>

                                        <p>(b) If Suvenia exercises its rights under Section 15(a), or if you delete your account, this entire agreement and any other agreements you have entered into with Suvenia will survive indefinitely until otherwise terminated according to their terms, if applicable.</p>

                                        <h4 class="uk-text-bold"> 16. Miscellaneous</h4>

                                        <p>(a) Entire Agreement. This agreement (along with other policies found on Suvenia's site and service) contains the entire agreement between the parties and replaces all prior oral and written agreements. No oral modifications, express or implied, may change the terms of this agreement. The parties have not relied on any representations or promises relating to the subject matter of this agreement except those contained within the four corners of this agreement.</p>

                                            <p>(b) Relationship of Parties. This agreement and other agreements relating to the site and service do not constitute a joint venture, partnership, agency, employment or fiduciary relationship between the parties, except when one is expressly stated. Neither party nor its agents have any authority to bind the other party, and the relationship of the parties is that of buyer and seller, or independent contractors in certain circumstances.</p>

                                            <p>(c) Successors, Assignment, and Delegation. This agreement inures to the benefit of and binds the successors, assigns, heirs, executors and administrators of the parties. However, you may not assign or delegate any right or duty under this agreement without written consent from Suvenia. Any attempt to do so is null and void. If there is an involuntary assignment, then Suvenia may reasonably request documentation from your successors or estate to prove that an involuntary assignment of your rights under this agreement has actually taken place.</p>

                                            <p>(d) Means of Notice. Written notices and other communications described herein may be made electronically and are effective when sent or published. You guarantee that the information provided in your orders or user account is accurate and hereby waive all rights or objections relating to not having received notices from Suvenua because of incorrect or incomplete information.</p>

                                            <p>(e) Enforceability and Severability. If any provision of this agreement is held invalid or unenforceable, the remainder of this agreement will remain in full force and effect. If any provision is held invalid or unenforceable with respect to particular circumstances, it will remain in full force and effect in all other circumstances.</p>

                                            <p>(f) Waivers. Waivers are only effective when in writing. If Suvenia waives enforcement of a breach of any term of this agreement, later breaches of the same or other terms are not waived. Accepting late performance of any act or late fulfilment of any condition of this agreement is not a waiver of the act or condition itself.</p>

                                            <p>(g) Interpretation Rules. "Or" when used in a group of phrases or nouns intends to include any combination of all or any of the items in the group, and not merely one member or the other of such a group. When "includes" or "including" begins a list of items, the list is not exclusive. All headings used in this agreement are for convenience only, and are not to be taken into account when interpreting the meaning of any term of this agreement.</p>

                                            <p>(h) Conflicts. If there are any conflicts between this agreement and another agreement between you and Suvenia, then the terms of that other agreement will control only to the extent they are inconsistent. Otherwise, any additional terms are supplementary to the terms of this agreement.</p>

                                            <p>(i) Reservation. Suvenia reserves all rights not expressly granted in this agreement.</p>

                                            <p>(j) No Third-Party Beneficiaries. This agreement does not and is not intended to confer any rights or remedies upon any person(s) other than the parties.</p>

                                            <p>(k) Minimum Age. Persons under the age of 13 may not use the site. Suvenia will not collect, use, or disclose any personal information associated with a person under age 13.</p>

                                            <p>(l) Retroactive Application. You agree that your acceptance of this version of this agreement and all other agreements incorporated by reference apply retroactively to your use of the site and service prior to the effective date of this agreement.</p>

                                            <h4 class="uk-text-bold">Version: 01/2018</h4>
                                    </li>

                                    {{-- Terms for Sellers --}}
                                        <li>
                                            <p class="uk-text-lead">SELLER'S TERMS &amp; CONDITIONS</p>
                                            <p class="uk-text-italic">Effective October 1, 2017</p>
                                           <h4 class="uk-text-bold">1. Sellers and Suvenia in General</h4>
                                            <p>(a) The General Terms and Conditions of Suvenia are incorporated to this agreement by reference.</p>

                                            <p> (b) This agreement governs the establishment, on-going operation, promotion, and maintenance of an online, custom merchandise interface ("store") and the use of Suvenia’s online design marketplace ("marketplace") whereby the user (the "seller") may create or market designs or products for sale using the site and service provided by Suvenia ("Suvenia").</p>

                                            <p>(c) The Seller may create either a standard store or a designer store, or upload designs to Suvenia’s marketplace. Standard stores feature static products that the Seller selects (for example, by combining new or existing designs available on the Suvenia platform with blank Suvenia products like t-shirts), and which end customers cannot customize. Designer stores feature dynamic products which the Seller can create by using new or existing designs available on the Suvenia platform, and which end customers can then customize with their own text, designs, or existing designs available on the Suvenia platform. Suvenia’s marketplace allows customers to browse designs submitted by many Sellers, but which customers cannot customize. The resulting goods are the printed Suvenia products, and for the completed sales of these, the Seller receives a margin as pre-determined by the seller at the time of creating the design.</p>

                                            <p>(d) Suvenia may change these terms from time to time. If Suvenia makes changes to these terms, you will be notified and asked to accept the new terms as a condition of continuing to use Suvenia's site and service. If you disagree with any amendments, you must stop using Suvenia's site and service.</p>

                                            <p> (e) This agreement's terms apply retroactively to all Submitted Content that the Seller submitted to Suvenia's site and service prior to this agreement's effective date.</p>

                                            <h4 class="uk-text-bold">2. Use of Submitted Content</h4>
                                                <p>(a) The Seller hereby permits Suvenia to reproduce, adapt, modify, translate, publish, publicly perform, publicly display, sell, and distribute any and all Submitted Content and designs (including associated information such as tags) that the Seller submits, posts or displays on or through a store or the site and service. The Seller expressly consents that Suvenia may distribute Submitted Content in its own stores and the stores of Suvenia's corporate affiliates, in stores of third-party Sellers, or via any other channels of external marketplace distribution that currently exist or that may later exist (such as Jumia.com, Amazon.com, eBay, etc.), and that the Seller's sole means of compensation therefore is the margin as pre-determined by the seller at the time of creating the design. The Seller acknowledges that external marketplace distribution may place additional restrictions on pricing, advertising, intellectual property rights, and other matters, subject to the terms and conditions of those third-party distribution channels.</p>

                                                <p>(b) Suvenia may use the Seller's Submitted Content for advertising purposes in any and all media now existing or later arising, including for up to six months after the termination of this agreement if these advertisements already existed or were in development before the Seller terminated this agreement. When reasonably possible, Suvenia will link all such depictions to the Seller's store.</p>

                                                <p>(c) Title to intellectual property rights (for example, trademark, copyright, privacy rights, publicity rights, common law, etc.) to a print design provided by the Seller remain with the Seller.</p>

                                                <p>(d) The Seller may terminate this agreement by removing Submitted Content via the Seller Account, or by terminating this agreement according to its terms. If either party terminates this agreement, any products that have already been ordered prior to termination and that would be affected by such a termination will continue to be fulfilled. Due to technical and practical limitations, Submitted Content may remain on Suvenia's site and service for several days following removal or termination, and if Submitted Content is being used by other Suvenia Sellers, removal may be delayed for up to two weeks to allow those Sellers to make other arrangements for the soon-to-be missing content.</p>

                                                <p>(e) Nothing in this agreement is to be construed to impose a duty upon Suvenia to use or exploit Submitted Content in any way. Suvenia may remove Submitted Content, stores or parts thereof, or any other functionality of the site and service at any time in its sole discretion for any reason or no reason, with or without notice to the Seller, and Suvenia is not liable to the Seller for such acts or their consequences, even if foreseeable and even if Suvenia is specifically advised of them in advance</p>

                                                <h4 class="uk-text-bold">3. Submitted Content Rules</h4>

                                        <p>(a) The Seller is solely responsible for the content (e.g., URLs, print designs, images, slogans, background images, text, tags, descriptions, etc.) uploaded, or otherwise added through the Suvenia interface, to a store, marketplace, or products (collectively "Submitted Content").</p>

                                        <p> (b) The Seller guarantees that the Submitted Content was either created by the Seller or that the Seller is otherwise the owner or authorized licensee of all necessary rights in and to the Submitted Content (for example, trademark, copyright, privacy rights, publicity rights, common law, etc.), necessary to effect the intent of this agreement. Suvenia may demand written evidence of such authorization at any time.</p>

                                        <p> (c) The Seller guarantees that, to the best of the Seller's knowledge, no Submitted Content infringes the rights of any third parties (for example, trademark, copyright, privacy rights, publicity rights, common law, etc.).</p>

                                        <p>(d) The Seller guarantees that the Submitted Content does not violate any state or federal laws, especially laws regarding the protection of minors.</p>

                                        <p>(e) If the Seller violates any agreement with Suvenia, violates Suvenia’s design policy (in Suvenia's sole discretion), or if third parties claim that Submitted Content infringes their rights, Suvenia reserves the right to remove or modify the store or Submitted Content to address the problem; to share the Seller's contact information with the complaining third party; to withhold margins from the Seller who uploaded the Submitted Content until the claim is resolved; and to use such margins to offset the costs of the violation or claim for infringement in Suvenia's sole discretion.</p>

                                        <p>(f) If Suvenia believes, in its sole discretion, that Submitted Content is wrongfully used, obviously misappropriated, or likely to create any risk for Suvenia, then Suvenia reserves the right to remove, limit, or modify the Submitted Content or service provided to the Seller to address the perceived risk.</p>

                                        <p>(g) The Seller must immediately inform Suvenia in writing if the Seller receives claims alleging that its Submitted Content infringes third-party rights.</p>

                                        <h4 class="uk-text-bold">4. Contact and Other Information</h4>
                                        <p>Suvenia may require the Seller to provide certain information via the Seller Account (for example, name, mailing address, email address, payment information, etc.). Failure to provide complete, accurate, and up-to-date information in the Seller Account may result in the store or other Submitted Content not being published for end customers, the stopping of orders, and potentially the immediate cancelation of the store or other Submitted Content and the withholding of any existing margins until complete, accurate, and up-to-date information is provided.</p>
                                        <h4 class="uk-text-bold">5. Payment of the Seller</h4>
                                        <p>(a) The Seller may receive a margin for sales of Suvenia products connected with the Seller's account. Margins vary depending on the settings that the Seller chooses in the Seller Account:</p>

                                        <p> I (i) If the Seller uploads designs to a store, the Seller may set a fixed design price for each design. This design price may be earned in addition to other margins.</p>

                                        <p>II (ii) If Suvenia makes other margin systems available later, these margin systems will continue to be governed by this agreement and may be supplemented with information found on Suvenia’s website.</p>

                                        <p>(b) The Seller acknowledges that margins are only credited to the Seller’s account within 14 days after both (1) payment for the order is received and (2) the order is shipped. The Seller receives no credited margin, or may have a credit margin removed or offset in a later payment period, if (1) the end customer cancels the order, requests a refund within Suvenia’s return policies, or requests a replacement; (2) the customer’s payment method fails; or (3) there is a chargeback.</p>

                                        <p> (c) Suvenia will pay the Seller the owed margins monthly according to the timing, account minimum, and other margin payment guidelines found on Suvenia’s website. Suvenia may, in its sole discretion, adjust the account minimum required for payment effective upon notice to the Seller. Depending on the payment methods the Seller chooses, certain processing fees may apply (for example, PayPal fees). If the Seller closes its account carrying less than the minimum payout balance, that remaining amount will be paid within six months after the cancellation of all agreements between Suvenia and the Seller, as long as no new agreement is made by Suvenia and the Seller within this period.</p>

                                        <p>(d) The Seller is responsible for providing Suvenia with its current, valid payment details, including a valid Nigerian account number and Biometric Verification number or a Paypal account name, its full name as shown on a national identity document, and any other information Suvenia requests. The Seller is solely liable for any consequences of providing incorrect and out-of-date information. Suvenia has the option in its sole discretion to withhold payment of margins until the information described in this paragraph is provided, or to withhold a portion of earned margins necessary to comply with applicable tax laws.</p>

                                        <p>(e) The Seller acknowledges that Suvenia may file certain documents with the FIRS in order to comply with applicable law. However, the Seller agrees that Suvenia is not liable for the consequences of any such filing; is in no way responsible for collecting, reporting, or remitting any taxes arising from or relating to the Seller's own income relating to these margins; and that Suvenia has no duty to maintain records for the Seller's benefit, including for the Seller's tax purposes.</p>

                                        <p>(f) If the Seller fails to provide valid information described in paragraph 5(d) above, and does not respond to requests for valid information within a one-year period starting from the first message sent by Suvenia asking for valid information, then the Seller waives all rights and claims to unpaid margins remaining in its account.</p>

                                        <h4 class="uk-text-bold">6. Use of the Store by the Seller</h4>
                                        <p>(a) Suvenia makes a white-label store site available to the Seller (for example, via a Suvenia.com URL), but the Seller may publish or embed the store on its own website. In that case, the store is published via an electronic reference ("link") provided by Suvenia, which the Seller embeds in his website and which refers to its store. The Seller must use the link and website generated by Suvenia without any changes. In particular, the Seller may not change Suvenia's advertisements, branding, terms and conditions, or other similar content.</p>

                                        <p>(b) The Seller agrees that it does not have and will not earn, through use of the site and service, any ownership, interest, or other rights in the URLs provided by Suvenia, any designs or interfaces implemented by Suvenia which the Seller uses or requests, or any rank or placement on search results generally (for example, as a result of search-engine optimization efforts by the Seller).</p>


                                        <h4 class="uk-text-bold">7. Contractual Relationships, Service, & Order restrictions</h4>
                                        <p>(a) Customers purchasing products are customers of Suvenia. This relationship between customers and Suvenia is independent of any possible relationships and contracts between the Seller and the customer who may visit other websites of the Seller. Thus, all customer orders, customer service, and related matters are governed solely by the rules, guidelines, and business practices of Suvenia.</p>

                                        <p>(b) Suvenia is responsible for all processing and execution of orders. To that end, Suvenia creates order forms, books payments, processes cancellations and returns, and is solely responsible for customer service.</p>

                                        <p>(c) Suvenia may reject any orders which do not meet its requirements, and may reject orders from customers who are known for not being credit-worthy or show as being a credit risk.</p>
                                        <h4 class="uk-text-bold">8. Suvenia's Right to Change Services</h4>
                                        <p>Suvenia may change the Store Administration system, store offerings, product offerings, guidelines, base product prices, general pricing, availability, business operations, and order processing at any time.</p>
                                        <h4 class="uk-text-bold">9. Sales Reports</h4>
                                        <p>Suvenia facilitates all sales made in stores and provides the Seller with a summary of its sales statistics. Suvenia may modify the form and content of this information at any time in its sole discretion.</p>
                                        <h4 class="uk-text-bold">10. Term and Termination</h4>
                                        <p>This agreement continues indefinitely until it is terminated by either party. Sellers may terminate by deactivating their stores in the Seller Account provided online by Suvenia. Suvenia may terminate this agreement effective immediately if the Seller violates any term, or the spirit of any term, of this agreement and may also terminate at any time, for any reason or no reason, effective immediately, by notifying the Seller via the email address provided in the Seller Account. Sections 3, 5, 6, 7, and 9 survive termination of this agreement.</p>
                                    </li>

                                    {{-- Influencer Terms --}}
                                        {{-- <li>
                                            <p class="uk-text-lead">INFLUENCER'S TERMS &amp; CONDITIONS</p>

                                            <p>Coming Soon!!</p>

                                        </li> --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
</div>




{{-- <div class="uk-margin-to">
        <div class="uk-container uk-container-expand uk-padding-remove-left uk-padding-remove-right">
    <a href="" target="_blank">
            <img src="" class="uk-width-1-1" alt="" uk-responsive>

        </a>
    </div>
</div> --}}












    <div id="modal-close-default" uk-modal>
        <div class="uk-modal-dialog uk-modal-body big-dialog">
            {{-- <div class=" uk-modal-header"> --}}
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
            {{-- </div> --}}

                        <div class="row">
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <span class="fas fa-store"></span>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                        <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fas fa-paint-brush"></i>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                            <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fab fa-accessible-icon"></i>
                                            </div>

                                             <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                            <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                 </div>
                            </div>

                        </div>

            </div>
            </div>



@endsection
