import Navigo = require("navigo");
import Fabric from './__inc/Fabric';
import Text from './__inc/Text';
import Image from './__inc/Image';
import Toolbar from './__inc/Toolbar';
import Arts from './__inc/Arts';
import Controls from './__inc/Controls';
import Templates from './__inc/Templates';
import Buy from './__inc/Buy';
import Sell from  './__inc/Sell';

class Customizer{

	baseUrl : any;

	BuyPage: any;

	SellPage: any;

	setfabric: any;

	public constructor(
		private $route = new Navigo(null, true, '#/'),
	){ 
		this.setRoutes();
		
		this.setfabric = new Fabric();
		const setText = new Text(this.setfabric);
		const setImage = new Image(this.setfabric);
		const setToolbar = new Toolbar(this.setfabric); 
		const setArts = new Arts(this.setfabric);
		const setControl = new Controls(this.setfabric);
		const setTemplate = new Templates(this.setfabric);
		this.BuyPage = new Buy(this.setfabric, $route);
		this.SellPage = new Sell(this.setfabric, $route);

		this.BuyPage.goToBuy();
	} 

	/**
	 * Set the routes and render the pages
	 */

	public setRoutes() {
		const cls = this;
		let render = (name: any) => {
			let pages = $('page');
			let page = pages.filter('[data-route=' + name + ']');
			if (page.length) {
				pages.removeClass('page-show').addClass('page-hide');
				page.removeClass('page-hide').addClass('page-show');
			}
		};

		this.$route.on({
			'home': () => {
				render('home');

			},
			'sell': () => {
				cls.SellPage.setDesign();
				cls.SellPage.setPriceSlider();
				cls.SellPage.setTags();
				cls.SellPage.postDesign();
				render('sell');
			},
			'buy': () => {
				if(cls.setfabric.isDirty){
					cls.BuyPage.setDesign();
					cls.BuyPage.setSizeCategory();
					cls.BuyPage.duplicateSize();
					cls.BuyPage.chooseColor();
					cls.BuyPage.addToCart();
					render('buy');
				}else{
					toastr.error('You have not made any changes');
					cls.$route.navigate('home');
				}
				
			} 
		});

		this.$route.on(() => {
			render('home');
		}); 

		this.$route.resolve();
	}

}

$(function () {
	const ____init_customizer_page = new Customizer();
}); 