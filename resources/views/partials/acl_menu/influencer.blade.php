@php
  $menus = collect([
      [
          'title'=> 'Overview',
          'route'=> 'app:influencer:dashboard:overview',
          'icon'=> 'icon-speedometer'
      ],
      [
          'title'=> 'Jobs',
          'route'=> 'app:influencer:dashboard:gigs',
          'icon'=> 'icon-camrecorder'
      ],
    //   [
    //       'title'=> 'My Post',
    //       'route'=> 'app:influencer:dashboard:my_posts',
    //       'icon'=> 'icon-plane'
    //   ],
      [
          'title'=> 'My Orders',
          'route'=> 'app:influencer:dashboard:my_orders',
          'icon'=> 'icon-basket'
      ],
      [
          'title'=> 'Withdraw',
          'route'=> 'app:influencer:dashboard:withdraw',
          'icon'=> 'icon-wallet'
      ],
      [
          'title'=> 'Profile Settings',
          'route'=> 'app:influencer:dashboard:profile_settings',
          'icon'=> 'icon-user'
      ],

  ]);
@endphp

@if(isset($type) && $type == 'admin_dropdown')

@foreach ($menus->all() as $item)
    <a href="{{ route($item['route']) }}" class="dropdown-item">
        <i class="icon {{ $item['icon']}}"></i> {{ $item['title']}}
    </a>
@endforeach

@endif

@if(isset($type) && $type == 'admin_sidebar_header')
<li class="nav-item super-header">
    <div class="uk-text-center pt-3 uk-light" style="border-bottom: 1px solid #09628a !important;">
        <img src="{{ Auth::guard('influencers')->user()->photo_url ? asset(Auth::guard('influencers')->user()->photo_url) : asset('user/default.png') }}" class="uk-border-circle m-auto" uk-responsive width="70">
        <p class="text-capitalize mt-3 mb-2">{{ Auth::guard('influencers')->user()->username }}</p>
        <p class="text-capitalize mt-0" style="font-size: 12px;">Influencer</p>
    </div>
 </li>

<li class="nav-item super-header">
    <a href="{{ route('app:influencer:dashboard:overview') }}" class="nav-link active">
        Influencer Dashbaord <i class="icon icon-minus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endif

@if(isset($type) && $type == 'admin_sidebar_content')
@foreach ($menus->all() as $item)
<li class="nav-item">
        <a href="{{ route($item['route']) }}" class="nav-link">
            <i class="icon {{ $item['icon']}}" style="font-size: 14px;"></i> {{ $item['title']}}
        </a>
</li>
@endforeach

@endif

@if(isset($type) && $type == 'front_end')
@foreach ($menus->all() as $item)
<li class=""><a href="{{ route($item['route']) }}" target="_blank">{{ $item['title'] }}</a></li>
@endforeach
@endif

@if(isset($type) && $type == 'admin_sidebar_footer')

<li class="nav-item super-header">
    <a href="{{ route('app:dashboard:my_orders') }}" class="nav-link active">
        Buyer Dashbaord <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>

<li class="nav-item super-header">
    <a href="{{ route('app:designer:dashboard:index') }}" class="nav-link active">
        Designer Services <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endif
