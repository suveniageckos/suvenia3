<?php

namespace App\Admin\Actions;

use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\ServiceProvider;

class AdminActionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addAction(\App\Admin\Actions\Influencer\ApproveAction::class);
        Voyager::addAction(\App\Admin\Actions\Seller\Transfer::class);
        Voyager::addAction(\App\Admin\Actions\Designer\Transfer::class);
        Voyager::addAction(\App\Admin\Actions\Influencer\Transfer::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
