import "spectrum-colorpicker";

class Toolbar{
    constructor(private Canva: any){
        this.setToolBarItem();
        this.changeFont();

    }

    public setToolBarItem(): void {
		let cls = this;
 
		let tools: any = {};

		cls.Canva.activeCanva.on('object:selected', function (evt: any) {
			var activeObject = evt.target.get('type');
			$('.ToolBar').removeClass('hide');
			if (activeObject == 'image') {
				$('#OtherColorCont').hide();
				$('#outlineColorCont').hide();
			} else {
				$('#OtherColorCont').show();
				$('#outlineColorCont').show();
			} 
		});
		cls.Canva.activeCanva.on('selection:cleared', function (evt: any) {
			$('.ToolBar').addClass('hide');
		});
		cls.Canva.activeCanva.on('object:removed', function (evt: any) {
			var activeObject = evt.target;
			// console.log(activeObject);
			// console.log(cls.uploadedImage);
			$('.ToolBar').addClass('hide');
		});

		$('#DuplicateApp').on('click', function (e: any) {
			e.preventDefault();
			var activeObject = cls.Canva.activeCanva.getActiveObject();
			activeObject.clone(function (cloned: any) {
				cls.Canva.activeCanva.discardActiveObject();
				cloned.set({
					top: cloned.top + 20,
					evented: true
				});
				if (cloned.type === 'activeSelection') {
					// active selection needs a reference to the canvas.
					cloned.canvas = cls.Canva.activeCanva;
					cloned.forEachObject(function (obj: void) {
						cls.Canva.activeCanva.add(obj);
					});
					cloned.setCoords();
				} else {
					cls.Canva.activeCanva.add(cloned);
				}

				let setType = cloned.type == 'i-text' ? 'add-text' : 'image' ;
				cls.Canva.setObjectLayers({ index: cloned, icon: setType, text: setType });
				//console.log(cloned.type);
				cls.Canva.activeCanva.setActiveObject(cloned);
				cls.Canva.activeCanva.renderAll();
			});
		});

		$('#FlipVertical').on('click', function (e: any) {
			e.preventDefault();
			var activeObject = cls.Canva.activeCanva.getActiveObject();
			if (activeObject) {
				if (!activeObject.flipY) {
					activeObject.set('flipY', true);
				} else {
					activeObject.set('flipY', false);
				}
				cls.Canva.activeCanva.centerObject(activeObject);
				cls.Canva.activeCanva.renderAll();

			}
		});

		$('#FlipHorizontal').on('click', function (e: any) {
			e.preventDefault();
			var activeObject = cls.Canva.activeCanva.getActiveObject();
			if (activeObject) {

				if (!activeObject.flipX) {
					activeObject.set('flipX', true);
				} else {
					activeObject.set('flipX', false);
				}

				cls.Canva.activeCanva.centerObject(activeObject);
				cls.Canva.activeCanva.renderAll();
			}
		});

		(<any>$('#AppWideColor') ).spectrum({
			color: "#f00",
			change: function(color: any) {
				//color.toHexString(); // #ff0000
					var activeObject = cls.Canva.activeCanva.getActiveObject();
					if (activeObject && activeObject.type == 'i-text') {
						activeObject.fill = color.toHexString();
						activeObject.dirty = true;
						cls.Canva.activeCanva.renderAll();
					} else if (activeObject && activeObject.type == 'path-group') {
						activeObject.set('fill', color.toHexString());
						activeObject.dirty = true;
						cls.Canva.activeCanva.renderAll();
					}

			}
		});

		(<any>$('#AppWideOutlineColor') ).spectrum({
			color: "#f00",
			change: function(color: any) {
				var activeObject = cls.Canva.activeCanva.getActiveObject();
			if (activeObject && activeObject.type === 'i-text') {
				activeObject.stroke = color.toHexString();
				//activeObject.dirty = true;
				cls.Canva.activeCanva.renderAll();
			}
			}
		});

	}

	public changeFont() {
		let fontSelected = $('#FontSelect').val();
		let cls = this;
		$('#FontSelect').css('font-family', < any > fontSelected);
		$('#FontSelect').on('change', function () {
			var cl: any = $(this).val();
			$(this).css('font-family', cl);
			var activeObject = cls.Canva.activeCanva.getActiveObject();
			if (activeObject && activeObject.type === 'i-text') {
				activeObject.fontFamily = (activeObject.fontFamily == cl ? '' : cl);
				activeObject.dirty = true;
				cls.Canva.activeCanva.renderAll();
				cls.Canva.isDirty = true;
			}

		});
	}

}

export default Toolbar;