<page data-route="sell" class="page-hide">
    <div class="uk-container mt-3 mb-3">
    <div class="app-sec-header sec-center mt-2">
       <h1 class="title">Fill out your product information</h1>
       <div class="underline"></div>
    </div>
    <div class="row justify-content-center">
       <div class="col-md-7">
          <a href="home" data-navigo class="btn btn-info-dark btn-sm"><i uk-icon="arrow-left"></i> Back to designer</a>
       </div>
       <div class="col-md-5">
       </div>
    </div>
    <div class="row mt-3">
    <div class="col-7">
       <div class="card sell-card">
          <div class="card-body">
             <form action="{{ route('app:ecommerce:sell_design') }}" method="post" data-post-customizer>
                {{ csrf_field() }}
                <input type="hidden" id="data_base_price" name="base_price">
                <input type="hidden" id="data_percentage" name="percentage">
                <input type="hidden" id="data_price" name="price">
                <input type="hidden" id="data_user_price" name="user_price">
                <input type="hidden" id="data_total_earning" name="total_earning">
                <div class="row mb-4 no-gutters">
                   <div class="col-12">
                      <div class="_perc_title mb-3">Drag to set your profit margin:</div>
                      <p style="margin:0;"><input type="text" class="span2" value="" data-slider-min="0" data-slider-max="200" data-slider-step="1" data-slider-value="0" data-slider-id="RC" id="PriceSlider" data-slider-tooltip="" data-slider-handle="square"  style="width:100%;"/>
                      </p>
                   </div>
                </div>
                <div class="row mb-2">
                   <div class="col-12">
                      <div class="input-group">
                         <div class="input-group-prepend">
                            <span class="input-group-text">Your Profit</span>
                            <span class="input-group-text">&#8358;</span>
                         </div>
                         <input type="number" class="form-control" aria-label="" value="" id="data_earning">
                      </div>
                      <input type="number" class="form-control" aria-label="" id="data_quantity" value="1" style="display:none;">
                   </div>
                   <!--<div class="col-6">
                      <div class="input-group">
                      <div class="input-group-prepend">
                      <span class="input-group-text">If You Sell</span>
                      </div>
                      <input type="number" class="form-control" aria-label="" id="data_quantity" value="1">
                      </div>
                      </div>-->
                </div>
                <div class="row">
                   <div class="col-12 mb-2 name">
                      <label class="sell-label">Product Name</label>
                      <input type="text" class="form-control" placeholder="Product Name" name="name">
                      <p class="mt-1 help-block"></p>
                   </div>
                   <div class="col-12 description">
                      <label class="sell-label">Description</label>
                      <textarea class="form-control" name="description"></textarea>
                      <p class="mt-1 help-block"></p>
                   </div>
                </div>
                <div class="row mb-2">
                   <div class="col-12 mb-2">
                      <label class="sell-label">Tags</label><span class="ml-2 label-decs">comma seperated e.g lagos,ilovedad,suvenia</span>
                      <div class="d-block">
                         {{-- <input type="text" class="form-contr" placeholder="" id="TagsInput" value="lagos,ilovedad,suvenia,IloveNigeria" name="tag"> --}}
                         <input type="text" class="form-contr" placeholder="" id="TagsInput" name="tag">
                      </div>
                      <div class="row">
                         <div class="col-12 mb-2">
                            <button class="btn btn-info-dark" type="submit">SAVE DESIGN</button>
                         </div>
                      </div>
                   </div>
                </div>
             </form>
             </div>
             </div>
          </div>
          <div class="col-5">
             <div class="card" style="border-radius:0; border:0px solid #fff;">
                <ul class="list-group list-group-flush">
                   <li class="list-group-item">
                      <span style="text-tone p-text-16">This Product Costs:</span>
                      <h2 class="p-text-14" style="margin:0;">&#8358;<span id="display_base_price" class="">0</span></h2>
                   </li>
                   <li class="list-group-item">
                      <span style="text-tone p-text-16">You'll Sell At:</span>
                      <h2 class="p-text-14" style="margin:0;">&#8358;<span id="display_price" class="">0</span></h2>
                   </li>
                   <li class="list-group-item">
                      <span style="text-tone p-text-16">You Could Earn Up To:</span>
                      <h2 class="p-text-14" style="margin:0;">&#8358;<span id="display_total_earning" class="">0</span></h2>
                   </li>
                </ul>
             </div>
             <div class="uk-margin-top uk-text-center">
                <!--<img src="" uk-responsive width="300" id="frontimageHolder">
                   <div id="frontimageHolder"></div>
                   <div id="backimageHolder"></div>-->
                <div style="width: 530px; height: 630px; position: relative;" class="frontimageHolder">
                   <img class="ProductFacing" src="">
                   <div class="drawingArea" style="border:0px solid red; z-index:1000;">
                   </div>
                </div>
                <div class="backimageHolder" style="width: 530px; height: 630px;position: relative;">
                   <img class="ProductFacing" src="">
                   <div class="drawingArea"  style="border:0px solid red; z-index:1000;">
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </page>
