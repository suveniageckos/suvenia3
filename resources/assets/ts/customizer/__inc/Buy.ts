import * as _ from "lodash";

class Buy{

    rowBucket: any =  [];

    colorPaneBucket: any =  ['colorpackFirst'];

    public constructor(public Canva: any, public $route: any){

    }

    public goToBuy(){
        $('#SellPageTrigger').on('click', () => {
			this.$route.navigate('sell');
		}); 
    }
 
    public setDesign() {
		let cls = this;
		let data = cls.Canva.activeData;

		$('.frontimageHolder').find('.ProductFacing').attr('src', cls.Canva.designData.front.img_url);
		$('.frontimageHolder').find('.drawingArea').css({
			'position': 'absolute',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'left': data.front.left + 'px',
			'top': data.front.top + 'px',
		});
		cls.Canva.designData.front.left = data.front.left;
		cls.Canva.designData.front.top = data.front.top;
		cls.Canva.designData.front.height = cls.Canva.canvas_front.height;
		cls.Canva.designData.front.width = cls.Canva.canvas_front.width;
		cls.Canva.designData.front.design = cls.Canva.canvas_front.toDataURL();
		$('.frontimageHolder').find('.drawingArea').html('<img src="' + cls.Canva.canvas_front.toDataURL() + '">');

		if (cls.Canva.hasBack == true) {
			$('.backimageHolder').find('.ProductFacing').attr('src', cls.Canva.designData.back.img_url);
			$('.backimageHolder').find('.drawingArea').css({
				'position': 'absolute',
				'width': data.back.width + 'px',
				'height': data.back.height + 'px',
				'left': data.back.left + 'px',
				'top': data.back.top + 'px',
			});
			cls.Canva.designData.back.left = data.back.left;
			cls.Canva.designData.back.top = data.back.top;
			cls.Canva.designData.back.height = cls.Canva.canvas_back.height;
			cls.Canva.designData.back.width = cls.Canva.canvas_back.width;
			cls.Canva.designData.back.design = cls.Canva.canvas_back.toDataURL();
			$('.backimageHolder').find('.drawingArea').html('<img src="' + cls.Canva.canvas_back.toDataURL() + '">');
		} else {
			cls.Canva.designData.back.design = '';
			$('.backimageHolder').find('.drawingArea').html('');
			$('.backimageHolder').find('.ProductFacing').attr('src', '');
		}
		cls.Canva.backgroundSetter('.frontimageHolder', '.backimageHolder', _.last(cls.Canva.PrimaryColor) );
	}

    /**
	 * @description populates size selection dropdown for the buy page.
	 */
	public setSizeCategory(){
		let defSize = JSON.parse( < any > $('#CartInputSizes').val());
		$.each(defSize, function (ind, val) {
			$('#CartSizes').append(
				'<option value="' + val + '">' + val + '</option>'
			);
		});
		$('#CartInputSizes').on('change', function () {
			let sizes = JSON.parse( < any > $(this).val());
			$('#CartSizes').empty();
			$.each(sizes, function (ind, val) {
				$('#CartSizes').append(
					'<option value="' + val + '">' + val + '</option>'
				);
			});
		});
	}

	public duplicateSize(): void {
		const cls = this;
		//console.log(cls.Canva.activeData.sizeEnabled);
		
		if(!cls.Canva.activeData.sizeEnabled){
			$('.size_super_pane').hide();
		}
		$('#AddMoreSize').on('click', function () {
			let id = cls.Canva.generateId(10);
			$('#CartInputSizes').prop('disabled', true);
            $('.CartSizePaneSingle').last().clone().appendTo('#CartSizePane');
            $('.CartSizePaneSingle').last().attr('id', id);
			$('.CartSizePaneSingle').last().find('.color-pane').find('.color-box-input').attr('name','colorpack'+id);
			$('.CartSizePaneSingle').last().find('._deleteSingleCartPane').html(
				'<p style="margin-top:40px;"><a href="javascript:;" class="text-14 text-dark deleteCartRow" data-target="' + id + '">Remove</a></p>'
			);
			cls.rowBucket.push(id);
            //cls.colorPaneBucket.push('colorpack'+id);
            //console.log(cls.colorPaneBucket);
		});
		$(document).on('click', '.deleteCartRow', function () {
			let Toremove = $(this).attr('data-target');
			$('#' + Toremove).remove();
			if ($('.CartSizePaneSingle').length < 2) {
				$('#CartInputSizes').prop('disabled', false);
			}
			const index = cls.rowBucket.indexOf(Toremove);
			cls.rowBucket.splice(index, 1);
            //cls.colorPaneBucket.push(colIndex);
            //_.pull(cls.colorPaneBucket, 'colorpack'+Toremove)
    
		});

    }
    
    public chooseColor(){ 
        var colorInput = $('.color-box-input');
        var cls = this;
        $('body').on('click', '.color-box-input', (e)=>{
             let inp = $(e.currentTarget);
             inp.parents('.colorpack1').find('.boxActive').removeClass('boxActive');
             inp.parents('.colorpack1').find('.color-box-input:checked').prop('checked', false);
             if(inp.parent().hasClass('boxActive')){
                 inp.parents('.colorpack1').find('.color-box-input:checked').prop('checked', false);
             }else{
                inp.parent().addClass('boxActive');
                inp.prop('checked', true);
             }
        });
       
    }

	public addToCart(): void {
        const cls = this;
		let CartVariants: any = [];
		let TotalQuantity = 0;

		$(document).on('submit', '#PurchaseCartForm', function (e) {
			e.preventDefault();
            CartVariants = [];
            let masterColors: any = [];
			let TotalQuantity = 0;
			$('.CartSizePaneSingle').each(function (ind, val) {
				var size = $(val).find('.cart_size').val();
				//var col = $(val).find('.color-box').attr('data-value');
                var quantity = $(val).find('.cart_quantity').val();
                var col = $(val).find('.color-box-input:checked').val();
				CartVariants.push({
					'quantity': quantity,
                    'size': cls.Canva.designData.sizeEnabled ? size : null,
                    'color': col
                });
                masterColors.push(col);
			});

			$('.CartSizePaneSingle').find('.cart_quantity').each(function (ind, val) {
				TotalQuantity += parseInt( < any > $(val).val());
			});

			var form = $('#PurchaseCartForm');
            var btnText = form.find(':input[type="submit"]').text();
            let productId: any = $('#ProductSelect option:selected').attr('data-id');
			var data = form.serializeArray();
        
			data.push({
				name: 'quantity',
				value: < any > TotalQuantity
			});
			data.push({
				name: 'order_variant',
				value: JSON.stringify(CartVariants)
            });
            
            data.push({
				name: 'design_id',
				value: productId
			});
			data.push({
				name: 'design_data',
				value: JSON.stringify(cls.Canva.designData)
			});
			data.push({
				name: 'photos',
				value: JSON.stringify(cls.Canva.imageBucket)
			});
			data.push({
				name: 'colors',
				value: JSON.stringify(masterColors)
			});
			data.push({
				name: 'has_back',
				value: cls.Canva.hasBack
			});
			data.push({
				name: 'name',
				value: cls.Canva.activeData.name
			});
			data.push({
				name: 'user_price',
				value: <any> parseInt('0')
			});
			data.push({
				name: 'category',
				value: cls.Canva.activeData.category
			});
			data.push({
				name: 'description',
				value: cls.Canva.activeData.description
			});
			data.push({
				name: 'brand_description',
				value: cls.Canva.activeData.description
			});

			$.ajax({
					url: form.attr('action'),
					type: form.attr('method'),
					data: data,
					beforeSend: function (xhr) {
						form.find(':input').prop('disabled', true);
						form.find(':input[type="submit"]').text('Working...');
					},
					timeout: 20000
				})
				.done(function (data, textStatus, jqXHR) {
					if (jqXHR.status == 200) {
						form.find('.has-error').removeClass('has-error');
						form.find('.help-block').text('');
						form.find(':input[type="submit"]').text(btnText);
						if (data.message) {
							toastr.success(data.message);
						}
						var wait = setTimeout(function () {
							window.location.href = data.redirect_url;
							//window.location.replace(data.redirect_url);
						}, 1000);
						form.find(':input[type="submit"]').text(btnText);
					}
				})
				.fail(function (jqXHR, textStatus, errorThrown) {

					if (jqXHR.status == 500) {
						var data = JSON.parse(jqXHR.responseText);
						if (typeof data.error === 'string' || data.error instanceof String) {
							toastr.error(data.error, "Error");
							form.find(':input').prop('disabled', false);
							form.find(':input[type="submit"]').text(btnText);

						} else {

							form.find('.has-error').removeClass('has-error');
							form.find('.help-block').text('');
							$.each(data.error, function (ind: any, val: any) {
								form.find('.' + ind).addClass('has-error');
								form.find('.' + ind + ' .help-block').text(val[0]);
							});
							form.find(':input').prop('disabled', false);
							form.find(':input[type="submit"]').text(btnText);
						}

					} else if (jqXHR.statusText == "timeout") {
						form.find('.has-error').removeClass('has-error');
						form.find('.help-block').text('');
						toastr.error('Request Timeout! Please try again.');
						form.find(':input').prop('disabled', false);
						form.find(':input[type="submit"]').text(btnText);
					}
                });
                

		});
	}


}

export default Buy;