<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrdered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

use App\OrderItem;
use App\SellerOrder;
use App\OrderMetric;
use App\Product;
use Laracart;

class SaveOrderItems
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrdered  $event
     * @return void
     */
    public function handle(ProductOrdered $event)
    {
        $order = $event->order;

        foreach (LaraCart::getItems() as $item) {
            $order_item = new OrderItem();
            // $order_item->user_id = Auth::user()->id;
            $order_item->user_id = Auth::guard('buyers')->user()->id;
            $order_item->order_id = $order->id;
            $order_item->product_id = $item->id;
            $order_item->order_ref = $order->order_ref;
            $order_item->quantity = $item->qty;
            $order_item->price = $item->price;
            $order_item->data = json_encode(['color' => $item->color, 'size' => $item->color]);
            $order_item->save();

            $seller_order = new SellerOrder();
            $product = Product::with(['design', 'brandable'])->where('id', $item->id)->first();
            $seller_order->order_id = $order->id;
            $seller_order->user_id = $product->user_id;
            $seller_order->product_id = $product->id;
            $seller_order->is_paid = false;
            $seller_order->quantity = $item->qty;
            $seller_order->user_profit = $product->design->total_earning * $item->qty;
            $seller_order->save();

            $order_metric = new OrderMetric();
            $percentage = 20; //20%
            $order_metric->product_id = $product->id;
            $order_metric->order_item_id = $order_item->id;
            $order_metric->user_id = $product->user_id;
            $order_metric->order_id = $order->id;
            $order_metric->payment_id = 0;
            //$order_metric->user_profit = $product->design->total_earning - $product->design->total_earning * 0.2;
            $order_metric->user_profit = $product->design->total_earning * $item->qty;
            $order_metric->site_profit = $product->brandable->price * 0.2;
            $order_metric->expenses = $product->brandable->price;
            $order_metric->save();

        }
    }
}
