import $ from "jquery";
import fabric from 'fabric';
import "spectrum-colorpicker";
import 'fabric-customise-controls';

$(function($){
  
    var canvas = new fabric.fabric.Canvas('template_canvas');
  
    if($('#EditPage').length){
      var canvas_data = JSON.parse($('#templateData').val());
      canvas.loadFromJSON(canvas_data,canvas.renderAll.bind(canvas));
      canvas.renderAll();
    }
  
    var options = {
      distance: 10, 
      width: canvas.width,
      height: canvas.height,
      param: {
        stroke: '#ebebeb',
        strokeWidth: 1,
        selectable: false
      } 
   };
  
  var gridLen = options.width / options.distance;
  var addedGrid = [];
  for (var i = 0; i < gridLen; i++) {
    var distance   = i * options.distance,
        horizontal = new fabric.fabric.Line([ distance, 0, distance, options.width], options.param),
        vertical   = new fabric.fabric.Line([ 0, distance, options.width, distance], options.param);
    canvas.add(horizontal);
    canvas.add(vertical);
    addedGrid.push(horizontal);
    addedGrid.push(vertical);
    if(i%5 === 0){
      horizontal.set({stroke: '#cccccc'});
      vertical.set({stroke: '#cccccc'});
    };
  };
  canvas.renderAll();
  
  function set_controls(){
    var BaseUrl = $('#InfoDiv').attr('asset-base');
  fabric.fabric.Canvas.prototype.customiseControls({
      tr: {
          action: 'rotate',
          cursor: 'default'
      },
      tl: {
          action: 'moveUp',
          cursor: 'default'
      },
      bl: {
          action: 'remove',
          cursor: 'default'
      },
      br: {
          action: 'scale',
          cursor: 'nwse-resize'
      }
  }, function () {
      canvas.renderAll();
  });
  fabric.fabric.Object.prototype.customiseCornerIcons({
      settings: {
          borderColor: '#4B4B4B',
          cornerSize: 25,
          cornerShape: 'circle',
          cornerBackgroundColor: '#AFAEAE'
      },
      tl: {
          icon: BaseUrl + 'customize-icons/move-up.svg'
      },
      tr: {
          icon: BaseUrl + 'customize-icons/rotate-left.svg'
      },
      bl: {
          icon: BaseUrl + 'customize-icons/delete.svg'
      },
      br: {
          icon: BaseUrl + 'customize-icons/resize-d.svg'
      }
  }, function () {
      canvas.renderAll();
  });
  fabric.fabric.Object.prototype.setControlsVisibility({
      mt: false,
      mb: false,
      mr: false,
      ml: false,
      mtr: false
  });
  
  }
  //init contols
  set_controls();
  
  var changeFont = function () {
    var fontSelected = $('#FontSelect').val();
    var cls = this;
    $('#FontSelect').css('font-family', fontSelected);
    $('#FontSelect').on('change', function () {
        var cl = $(this).val();
        $(this).css('font-family', cl);
        var activeObject = canvas.getActiveObject();
        if (activeObject && activeObject.type === 'i-text') {
            activeObject.fontFamily = (activeObject.fontFamily == cl ? '' : cl);
            activeObject.dirty = true;
            canvas.renderAll();
            //cls.isDirty = true;
        }
    });
  };
  
  changeFont();
  
  var addText = function () {
    var cls = this;
    $('#AddText').on('click', function () {
        var Text = new fabric.fabric.IText($('#TextField').val(), {
            fontFamily: $('#FontSelect').val(),
            fill: '#333',
            fontSize: 40
        });
        //cls.isDirty = true;
        canvas.centerObject(Text);
        canvas.add(Text);
        canvas.setActiveObject(Text);
        canvas.renderAll();
    });
  };
  
  addText();
  
  
  var setColorToggle = function () {
    $(".toggleColorPallete").spectrum({
        showPaletteOnly: true,
        togglePaletteOnly: true,
        togglePaletteMoreText: 'more',
        togglePaletteLessText: 'less',
        color: 'blanchedalmond',
        palette: [
            ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
            ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
            ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
            ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
            ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
            ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
            ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
            ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
        ]
    });
  };
  
  setColorToggle();
  
  var setToolBar = function () {
    var tools = {};
    var cls = this;
    canvas.on('object:selected', function (evt) {
        var activeObject = evt.target.get('type');
        $('.ToolBar').removeClass('hide');
        if (activeObject == 'image' || activeObject == 'path-group') {
            $('#OtherColorCont').hide();
            $('#outlineColorCont').hide();
        }
        else {
            $('#OtherColorCont').show();
            $('#outlineColorCont').show();
        }
    });
    canvas.on('selection:cleared', function (evt) {
        $('.ToolBar').addClass('hide');
    });
    canvas.on('object:removed', function (evt) {
        var activeObject = evt.target;
        $('.ToolBar').addClass('hide');
    });
  };
  
  setToolBar();
  
  var setToolBarItem = function () {
    var cls = this;
    $('#DuplicateApp').on('click', function (e) {
        e.preventDefault();
        var activeObject = canvas.getActiveObject();
        activeObject.clone(function (cloned) {
            canvas.discardActiveObject();
            cloned.set({
                top: cloned.top + 20,
                evented: true
            });
            if (cloned.type === 'activeSelection') {
                cloned.canvas = canvas;
                cloned.forEachObject(function (obj) {
                    canvas.add(obj);
                });
                cloned.setCoords();
            }
            else {
                canvas.add(cloned);
            }
            canvas.setActiveObject(cloned);
            canvas.renderAll();
        });
    });
    $('#FlipVertical').on('click', function (e) {
        e.preventDefault();
        var activeObject = canvas.getActiveObject();
        if (activeObject) {
            if (!activeObject.flipY) {
                activeObject.set('flipY', true);
            }
            else {
                activeObject.set('flipY', false);
            }
            canvas.centerObject(activeObject);
            canvas.renderAll();
        }
    });
    $('#FlipHorizintal').on('click', function (e) {
        e.preventDefault();
        var activeObject = canvas.getActiveObject();
        if (activeObject) {
            if (!activeObject.flipX) {
                activeObject.set('flipX', true);
            }
            else {
                activeObject.set('flipX', false);
            }
            canvas.centerObject(activeObject);
            canvas.renderAll();
        }
    });
    $('#AppWideColor').on('change', function () {
        var activeObject = canvas.getActiveObject();
        if (activeObject && activeObject.type == 'i-text') {
            activeObject.fill = $(this).val();
            activeObject.dirty = true;
            canvas.renderAll();
        }
        else if (activeObject && activeObject.type == 'path-group') {
            activeObject.set('fill', $(this).val());
            activeObject.dirty = true;
            canvas.renderAll();
        }
    });
    $('#AppWideOutlineColor').on('change', function (e) {
        var activeObject = canvas.getActiveObject();
        if (activeObject && activeObject.type === 'i-text') {
            activeObject.stroke = $(this).val();
            activeObject.dirty = true;
            canvas.renderAll();
        }
    });
  };
  setToolBarItem();
  
  $('#SaveTemplate').on('click', function(){
  
    $.each(addedGrid, function(ind, val){
      canvas.remove(val);
    });
  
    //console.log(canvas.toDataURL());
      //window.open(canvas.toDataURL());
      //PostTemplate
      $('#templateData').val(JSON.stringify(canvas.toJSON()));
      //$('#templatePhoto').val(canvas.toSVG());
      $('#templatePhoto').val(canvas.toDataURL());
      $('#PostTemplate').submit();
  
      $.each(addedGrid, function(ind, val){
        canvas.add(val);
      });
  });
  
  
  
  });