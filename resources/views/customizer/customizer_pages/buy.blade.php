<page data-route="buy" class="page-hide">
    <div class="uk-container">
    <div class="app-sec-header sec-center mt-2">
       <h1 class="title">order settings</h1>
       <div class="underline"></div>
    </div>
    <div class="row justify-content-center mt-3">
       <div class="col-md-8">
          <a href="home" data-navigo class="btn btn-info-dark btn-sm">
          <i uk-icon="arrow-left"></i> Back to designer
          </a>
          <div class="col-md-3"></div>
       </div>
       <div class="row mt-3 justify-content-center">
          <div class="col-md-5">
             <div class="card" style="border:0px solid #ccc;">
                <div class="card-body">
                   @if(!$utils->device()->isMobile() || $utils->device()->isTablet())
                   <form class="" method="post" action="{{ route('app:ecommerce:buy_design') }}" id="PurchaseCartForm">
                      @else
                   <form class="" method="post" action="{{ route('app:ecommerce:buy_design') }}" id="PurchaseCartForm">
                      @endif {{ csrf_field() }}
                      <div class="uk-margin cart_size size_super_pane">
                         <label class="text-16">Choose size category</label>
                         <select class="form-control app-input input-no-shadow" name="sizes" id="CartInputSizes">
                            @php $cl_sizes = config('ecommerce_config.sizes') @endphp @foreach($cl_sizes as $key => $val)
                            <option value="{{ json_encode($val) }}">{{ title_case($key) }} &nbsp;&nbsp;</option>
                            @endforeach
                         </select>
                         <span class="help-block"></span>
                      </div>
                      <div class="" id="CartSizePane">
                         <div class="form-row CartSizePaneSingle mb-2">
                            <div class="form-group col-md-6 size_super_pane">
                               <label for="" class="text-16">Choose size</label>
                               <select class="form-control app-input input-no-shadow cart_size" name="cart_sizes" id="CartSizes"></select>
                               <span class="help-block"></span>
                            </div>
                            <div class="form-group col-md-4">
                               <label for="" class="text-16">Quantity</label>
                               <input type="number" class="form-control app-input input-no-shadow cart_quantity" min="1" name="quantity" value="1">
                               <span class="help-block"></span>
                            </div>
                            <div class="form-group col-md-2 _deleteSingleCartPane"></div>
                            <div class="form-group col-md-12 pt-2" style="background: #f5f5f5;">
                               <div class="color-pane colorpack1" id="colorpackFirst">
                                  <ul class="list-unstyled">
                                     @foreach(config('ecommerce_config.colors') as $key => $color) @php $disabledFirst = $loop->first ? 'checked' : ''; @endphp
                                     <li class="d-inline incr">
                                        <label class="btn-color color-box" style="background: {{ $color }}">
                                        <input type="checkbox" class="color-box-input" name="cart-item-color" value="{{ $color }}" style="display:non;" {{ $disabledFirst }}>
                                        <span class="color-icon"></span>
                                        </label>
                                     </li>
                                     @endforeach
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="uk-margin-to">
                         <span>
                         <a href="javascript:;" class="text-14 text-dark" id="AddMoreSize">Add more style</a>
                         </span>
                      </div>
                      <div class="uk-margin">
                         <button type="submit" class="btn btn-info" id="_checkOut">Proceed to checkout</button>
                      </div>
                   </form>
                </div>
             </div>
          </div>
          <div class="col-md-3">
             <div class="uk-margin-top uk-text-center">
                <!--<img src="" uk-responsive width="300" id="frontimageHolderBuy"><img src="" uk-responsive width="300" id="backimageHolderBuy">-->
                <div style="width: 530px; height: 630px; position: relative;" class="frontimageHolder">
                   <img class="ProductFacing" src="">
                   <div class="drawingArea" style="border:0px solid red; z-index:1000;"></div>
                </div>
                <div class="backimageHolder" style="width: 530px; height: 630px;position: relative;">
                   <img class="ProductFacing" src="">
                   <div class="drawingArea" style="border:0px solid red; z-index:1000;"></div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </page>