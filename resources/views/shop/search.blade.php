@extends('layouts.layout')

@section('title', 'Catalogue' )

@section('content')
@php $categories = $utils->get_categories(); @endphp

<div class="uk-container uk-container-smal mt-4 mb-4">

    <div class="row">
        <p class="m-1">{{ $product_count }} {{ str_plural('Item',  $product_count) }}  Found</p>
    </div>

    <div class="row justify-content-center" uk-height-match="target: > div > .card">
            @forelse($products as $product)
            <div class="col-md-3 mb-3">
                    @include('partials.single_product', ['product'=> $product])
            </div>

            @empty

            <div class="col-md-12 mb-3">
                    <p></p>
            </div>

            @endforelse
    </div>

    <div class="row justify-content-center mt-3">
        {{ $products->appends(['q' => request()->q])->links() }}
    </div>

  
</div>
@endsection