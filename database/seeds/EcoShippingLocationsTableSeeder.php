<?php

use Illuminate\Database\Seeder;

class EcoShippingLocationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('eco_shipping_locations')->delete();
        
        \DB::table('eco_shipping_locations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Abia',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Adamawa',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Akwa Ibom',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Bauchi',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Bayelsa',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Benue',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Borno',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Cross River',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Delta',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Ebonyi',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Enugu',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Edo',
                'zone_id' => 2,
                'data' => '{"1,10":2070,"11,20":4657,"21,30":6383,"31,40":8108,"41,60":11558,"61,99":19800}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Ekiti',
                'zone_id' => 2,
                'data' => '{"1,10":2070,"11,20":4657,"21,30":6383,"31,40":8108,"41,60":11558,"61,99":19800}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Gombe',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Imo',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Jigawa',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Kaduna',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Kano',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Kastina',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Kebbi',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Kogi',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Kwara',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Lagos',
                'zone_id' => 2,
                'data' => '{"1,10":575,"11,20":1725,"21,30":4186,"31,40":5474,"41,60":8068,"61,99":14522}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Nasarawa',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Niger',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Ogun',
                'zone_id' => 2,
                'data' => '{"1,10":2070,"11,20":4657,"21,30":6383,"31,40":8108,"41,60":11558,"61,99":19800}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Ondo',
                'zone_id' => 2,
                'data' => '{"1,10":2070,"11,20":4657,"21,30":6383,"31,40":8108,"41,60":11558,"61,99":19800}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Osun',
                'zone_id' => 2,
                'data' => '{"1,10":2070,"11,20":4657,"21,30":6383,"31,40":8108,"41,60":11558,"61,99":19800}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Oyo',
                'zone_id' => 2,
                'data' => '{"1,10":2070,"11,20":4657,"21,30":6383,"31,40":8108,"41,60":11558,"61,99":19800}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Plateau',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Rivers',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Sokoto',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Taraba',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Yobe',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Zamfara',
                'zone_id' => 2,
                'data' => '{"1,10":3220,"11,20":5003,"21,30":6900,"31,40":8625,"41,60":12075,"61,99":20200}',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}