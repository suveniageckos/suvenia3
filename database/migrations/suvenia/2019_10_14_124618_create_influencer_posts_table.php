<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInfluencerPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('influencer_posts', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('product_id');
			$table->integer('influencer_id');
			$table->string('title', 191);
			$table->string('ref', 191);
			$table->string('slug', 191);
			$table->text('content', 65535);
			$table->string('type', 191);
			$table->text('photo_url', 65535)->nullable();
			$table->text('photo_path', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('influencer_posts');
	}

}
