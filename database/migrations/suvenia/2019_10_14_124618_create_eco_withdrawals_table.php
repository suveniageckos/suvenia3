<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoWithdrawalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_withdrawals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('code', 191);
			$table->string('ref', 191);
			$table->string('amount', 191);
			$table->boolean('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_withdrawals');
	}

}
