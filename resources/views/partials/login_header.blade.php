<div class="uk-container uk-container-small mt-5">
    <div class="row justify-content-center">
        <div class="col-md-4 uk-text-center">
            <a href="{{ route($route_name) }}" class=""><img src="{{  asset($utils->get_image('site.logo')) }}" uk-responsive width="150" class="m-auto"></a> 
        </div>
    </div>
</div>