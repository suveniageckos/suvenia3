<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoBuyerAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_buyer_addresses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('location_id');
			$table->integer('zone_id');
			$table->string('price', 191);
			$table->string('firstname', 191);
			$table->string('lastname', 191);
			$table->string('email', 191);
			$table->text('address', 65535);
			$table->string('phone_number', 191);
			$table->string('country', 191);
			$table->timestamps();
			$table->boolean('is_default')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_buyer_addresses');
	}

}
