<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $table = 'eco_coupons';

    public function user(){
        return $this->belongsTo("App\User");
    }
public function products(){
        return $this->hasMany("App\Product");
    }
}
