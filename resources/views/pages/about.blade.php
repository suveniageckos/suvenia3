@extends('layouts.layout')

@section('title', 'Frequently asked questions')

@section('content')


        <style>
                .btn-about:hover{
                    color: #2298a8 !important;
                }
                .text-app{
                    color: #2298a8 !important;
                }

                .icon-box{
                    background: #2298a8;
                    color: #fff;
                    text-align: center;
                    height: 100px;
                    width: 100px;
                    border-radius: 50%;
                     line-height: 6;
                }

                .icon-box img{
                   width: 50px;

                }
            </style>

            <div class="uk-background-blend-multiply uk-background-top-right uk-background-cover uk-light uk-panel uk-flex uk-flex-center uk-flex-middle p-4" style="background-color:#2298a8; background-image: url({{asset('img/team.png')}});" uk-height-viewport>
                <div class="row">
                <div class="col-md-5">
            <iframe width="" height="315" src="https://www.youtube.com/embed/eAODa3lEw2M?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="uk-width-1-1"></iframe>
                </div>
                <div class="col-md-7">
                    <h1 class="m-0">Buy & Sell Customized Items</h1>
                    <h4 class="m-0">Discover amazing t-shirts, mugs, hoodies, hats, accessories and more, or simply design your own.</h4>
                    <p><a class="btn btn-outline-light btn-about" href="#descSect" uk-scroll>LEARN MORE</a></p>
                </div>
                </div>
            </div>

            <div class="uk-section uk-background-muted" id="descSect">
                <div class="uk-container uk-text-center">
                    <h1 class="text-app">Let's Create Awesomeness</h1>
                    <p>Suvenia allows individuals, influencers and brands to create, buy or sell personalised items such as t-shirts, sweatshirts, hoodies, mugs & hats. Our goal is to make Suvenia a platform that allows people create products at the speed of thought. Starting with personalised products, we want to become the leading channel for discovering or distributing consumer items in sub-saharan Africa, both online and offline.</p>
                </div>

                 <div class="uk-container uk-text-center mt-5">
                     <div class="row">

                         <div class="col-md-4">
                            <div class="icon-box" style="margin:0 auto;"><img src="{{asset('img/abt-icons/cart.svg')}}" uk-responsive></div>
                             <div class="mt-3 uk-text-center">
                            <h4 class="mb-1"><b>Buy</b></h4>
                             Suvenia offers an amazing collection of pre-designed items that you can just buy and go. From T-shirts to mugs, you can find custom products made just for you from our marketplace.
                             </div>
                         </div>
                         <div class="col-md-4">
                            <div class="icon-box" style="margin:0 auto;"><img src="{{asset('img/abt-icons/rocket.svg')}}" uk-responsive></div>
                             <div class="mt-3 uk-text-center">
                                 <h4 class="mb-1"><b>Create</b></h4>
                             Can't find what works for you? You can create and personalise your own products by adding unique
                                artwork, photos, messages, and
                                colours to products.
                             </div>
                         </div>
                         <div class="col-md-4">
                            <div class="icon-box" style="margin:0 auto;"><img src="{{asset('img/abt-icons/money.svg')}}" uk-responsive></div>
                             <div class="mt-3 uk-text-center">
                                 <h4 class="mb-1"><b>Sell</b></h4>
                             Sell their designs and creations by
                            publishing them on various products. They
                            set their own royalty rates, have no inventory
                            to maintain, and we manage all customer
                            relationships.
                             </div>
                         </div>


                     </div>
                </div>

            </div>

            <div class="uk-section p-0 mb-0">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.717060671916!2d3.384471314024548!3d6.557357324589873!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8d7a6191281f%3A0x2312e70626e46619!2sSuvenia.com!5e0!3m2!1sen!2sng!4v1532972394804" width="" height="450" frameborder="0" style="border:0" allowfullscreen class="uk-width-1-1"></iframe>
            </div>


            <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body big-dialog">
                        {{-- <div class=" uk-modal-header"> --}}
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
                        {{-- </div> --}}

                                    <div class="row">
                                        <div class="col-md-4">
                                                <div class="service-box">
                                                        <div class="icon-ball d-inline-block">
                                                            <span class="fas fa-store"></span>
                                                        </div>

                                                        <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                                    <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                                <div class="service-box">
                                                        <div class="icon-ball d-inline-block">
                                                            <i class="fas fa-paint-brush"></i>
                                                        </div>

                                                        <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                                        <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                                <div class="service-box">
                                                        <div class="icon-ball d-inline-block">
                                                            <i class="fab fa-accessible-icon"></i>
                                                        </div>

                                                         <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                                        <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                             </div>
                                        </div>

                                    </div>

                        </div>
                        </div>
@endsection
