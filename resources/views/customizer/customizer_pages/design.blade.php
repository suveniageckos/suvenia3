<page data-route="home" class="page-hide">
    <div class="uk-container mt-3 mb-3">
       <div class="row bg-toolbar mt-2 mb-2 ToolBar hide">
          <div class="col-md-4">
             <ul class="list-unstyled mt-3 notImage"> 
                <li class="d-inline toolbar-label">Font: </li>
                <li class="d-inline">
                   <select class="selector d-inline" id="FontSelect" dir="ltr">
                      <optgroup>
                         <option value="'Monoton', cursive" class="monoton" selected>Monoton</option>
                         <option value="'Kranky', cursive" class="kranky">Kranky</option>
                         <option value="The Girl Next Door" class="girl_next">Girl</option>
                         <option value="'Fredericka the Great', cursive" class="fredrick">Fredrick</option>
                         <option value="'Press Start 2P', cursive" class="press_start">Press Start</option>
                         <option value="'Cabin Sketch', cursive" class="cabin_sketch">Cabin Sketch</option>
                         <option value="'Kurale', serif" class="kurale">Kurale</option>
                         <option value="'Sigmar One', cursive" class="sigmar">Sigmar</option>
                         <option value="'Paytone One', sans-serif" class="paytone">Paytone</option>
                         <option value="'Luckiest Guy', cursive" class="luckiest">Luckiest</option>
                         <option value="'Audiowide', cursive" class="audiowide">Audio Wide</option>
                         <option value="'Great Vibes', cursive" class="vibes">Vibes</option>
                         <option value="'Philosopher', sans-serif" class="philosopher">Philosopher</option>
                         <option value="'Plaster', cursive" class="plaster">Plaster</option>
                         <option value="'Exo', sans-serif" class="exo">Exo</option>
                         <option value="'Dancing Script', cursive" class="dancing">Dancing</option>
                         <option value="'Pacifico', cursive" class="pacifico">Pacifico</option>
                         <option value="'Lobster', cursive" class="lobster">Lobster</option>
                      </optgroup>
                   </select>
                </li>
                <li class="d-inline toolbar-label ml-3">Outline: </li>
                <li  class="d-inline">
                   <select class="selector" id="TextOutline">
                      <option value="0" selected>No Outline</option>
                      <option value="1">Thin Outline</option>
                      <option value="2">Medium Outline</option>
                      <option value="3">Thick Outline</option>
                   </select>
                </li>
             </ul>
          </div>
          <div class="col-md-4">
             <ul class="list-unstyled mt-3">
                {{-- <li class="d-inline-block uk-text-center make-pointer">
                   <span class="icon-undo"></span>
                   <div class="d-inline-bloc toolbar-label">Undo</div>
                </li>
                <li class="d-inline-block uk-text-center ml-1 make-pointer">
                   <span class="icon-redo"></span>
                   <div class="d-inline-bloc toolbar-label">Redo</div>
                </li> --}}
                <li class="d-inline-block uk-text-center ml-3 make-pointer" id="DuplicateApp">
                   <span class="icon-duplicate"></span>
                   <div class="d-inline-bloc toolbar-label">Duplicate</div>
                </li>
                <li class="d-inline-block uk-text-center ml-1 make-pointer" id="FlipVertical">
                   <span class="icon-flip-vertical"></span>
                   <div class="d-inline-bloc toolbar-label">Flip Vertical</div>
                </li>
                <li class="d-inline-block uk-text-center ml-1 make-pointer" id="FlipHorizontal">
                   <span class="icon-flip-horizontal"></span>
                   <div class="d-inline-bloc toolbar-label">Flip Horizontal</div>
                </li>
             </ul>
          </div>
          <div class="col-md-4">
             <ul class="list-unstyled mt-4 notImage">
                <li class="d-inline toolbar-label">Font: </li>
                <li class="d-inline">
                   <input type='text' id="AppWideColor" class="toggleColorPallete"/>
                </li>
                <li class="d-inline toolbar-label ml-3">Outline Color: </li>
                <li class="d-inline">
                   <input type='text' id="AppWideOutlineColor" class="toggleColorPallete"/>
                </li>
             </ul>
          </div>
       </div>
       <div class="row">
          <div class="col-3">
             <div class="list-group features-list">
                <a href="javascript:;" class="list-group-item list-group-item-action features-list-item" id="AddText">
                <span class="icon-add-text"></span> <span class="text-t">add text</span>
                </a>
                <a href="#ImageModal" class="list-group-item list-group-item-action features-list-item" uk-toggle>
                <span class="icon-image"></span> <span class="text-t">add image</span>
                </a>
                <a href="#GraphicModal" class="list-group-item list-group-item-action features-list-item" uk-toggle>
                <span class="icon-graphic"></span> <span class="text-t">add graphic</span>
                </a>
                <a href="#TemplateModal" class="list-group-item list-group-item-action features-list-item" uk-toggle>
                <span class="icon-template"></span> <span class="text-t">add template</span>
                </a>
             </div>
             <div class="card custom-layer-card mt-3">
                <div class="card-header">
                   <span class="icon-layers"></span> layers
                </div>
                <div class="card-body" id="LayerPane">
                </div>
             </div>
          </div>
          <div class="col-md-6" style="overflow:auto; border:0px solid red;" >
             <div class="flip-container">
                <div class="flipper">
                   <div style="width: 530px; height: 630px;" id="side_front" class="flip_front">
                      <img class="ProductFacing" src="">
                      <div class="drawingArea" style="">
                         <canvas id="canvas_front" width="" height="" class="hove"></canvas>
                      </div>
                   </div>
                   <div id="side_back" style="width: 530px; height: 630px;" class="flip_back">
                      <img class="ProductFacing" src="">
                      <div class="drawingArea" style="" id="drawarea_back">
                         <canvas id="canvas_back" width="" height="" class="hove"></canvas>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          <div class="col-3">
             <ul class="list-group">
                 <li class="list-group-item right-task-item uk-text-center" id="_FlipToggles">
                   <div class="btn-group" role="group" aria-label="Flip Product">
                        <button type="button" class="btn btn-info active" id="_flipFront">FRONT</button>
                        <button type="button" class="btn btn-info" id="_flipBack">BACK</button>
                    </div>

                </li>
                <li class="list-group-item right-task-item uk-text-center">
                   <select class="custom-select product-select uk-text-capitalize" id="ProductSelect">
                      @foreach ($brandables as $product)
                      <option value="{{ $product->data }}" data-name="{{ str_slug($product->name) }}" data-id="{{ $product->id}}">{{ $product->name }}</option>
                      @endforeach
                   </select>
                </li>
                <li class="list-group-item right-task-item">
                   <p class="color-title m-0">Choose Colour</p>
                   <p class="color-sub-title m-2">You may select up to 4 colours </p>
                   <div class="color-pane m-2">
                      <ul class="list-unstyled">
                         @foreach(config('ecommerce_config.colors') as $key => $color)
                         @php
                         $disabledFirst = $loop->first ? 'disabled checked' : '';
                         @endphp
                         <li class="d-inline incr">
                            <label class="btn-color product-color" id="checkState{{ $loop->index }}" style="background: {{ $color }}">
                            <input type="checkbox" class="product-color-ball" name="checkState{{ $loop->index }}" value="{{ $color }}" {{ $disabledFirst }}>
                            <span class="color-icon {{ $loop->first ? 'icon-checked': '' }}"></span>
                            </label>
                         </li>
                         @endforeach
                      </ul>
                   </div>
                </li>
                <li class="list-group-item right-task-item uk-text-center">
                  @if(Auth::guard('sellers')->check())
                   <a href="sell"  data-navigo class="btn btn-info-dark btn-block mb-1" id="SellPageTrigger">SELL THIS</a>
                   @endif
                   @if(Auth::guard('buyers')->check())
                   <a href="buy" data-navigo class="btn btn-info-dark btn-block">PROCEED TO ORDER</a>
                  @endif
                </li>
             </ul>
          </div>
       </div>
    </div>
 </page>
