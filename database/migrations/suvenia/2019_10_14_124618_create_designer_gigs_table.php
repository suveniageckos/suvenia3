<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignerGigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designer_gigs', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('user_id');
			$table->bigInteger('category_id');
			$table->string('ref', 225);
			$table->string('title', 225);
			$table->text('service_type', 65535);
			$table->text('tags', 65535);
			$table->text('description', 65535);
			$table->text('reqirements', 65535);
			$table->boolean('is_default')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designer_gigs');
	}

}
