<?php
namespace App\Helpers\Social;

class SocialAuth{
	
	static public function driver($request, $config){
		$provider = ucwords($request->provider);
		$cls = "\\App\\Helpers\\Social\\" .  $provider;
		return $cls::driver($request, $config); 
	}

	
	static public function authenticate($request){
		$provider = ucwords($request->provider);
		$cls = "\\App\\Helpers\\Social\\" .  $provider;
		return $cls::authenticate($request);
	}

	
}