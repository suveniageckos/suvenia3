<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_notifications', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('initiator_id')->nullable();
			$table->bigInteger('reciever_id')->nullable();
			$table->string('title', 191)->nullable();
			$table->text('message_body', 65535)->nullable();
			$table->boolean('is_read')->default(0);
			$table->dateTime('read_on')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_notifications');
	}

}
