@extends('voyager::master') @section('page_title', __('Add Brandable')) @section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<link rel="stylesheet" href="{{ mix('css/brandable.css') }}">@endsection @section('content')
<div class="container-fluid">
	<h1 class="page-title">
        <i class="voyager-paint-bucket"></i> Edit A Brandable Design
    </h1>
    <div id="CustomizerEditPage" baseAssetUrl="{{ asset('/') }}"></div>
	<div class="page-content browse container-fluid">
		<div class="row">
        <form action="{{ route('admin:brandable:edit', ['id'=> $brandable->id]) }}" method="POST" data-post>
            @csrf
            
			<div class="col-md-12">
				<div class="panel panel-bordered">
					<div class="panel-body">
						<div class="row">
								<div class="box box-default">
									<div class="box-header with-border mb-3"> <span class="pull-right"><button class="btn btn-info" type="submit">Save Changes</button></span>
									</div>
									<div class="box-body">
                                            <?php
                                            $brandable_data = json_decode($brandable->data, true);
                                            $isBackEnabled = $brandable_data['backViewEnabled'] ? 'checked' : '';
                                            $isSizeEnabled = $brandable_data['sizeEnabled'] ? 'checked' : '';
                                            //base64_encode(file_get_contents($brandable->front_image))
                                        ?>
										<textarea id="FrontDesignAreaDim" name="front_dimension" style="display:none;">{{ json_encode($brandable_data['front'], true) }}</textarea>
										<textarea id="FrontProduct" name="front_product" style="display:none;"></textarea>
										<textarea id="BackDesignAreaDim" name="back_dimension" style="display:none;">{{ json_encode($brandable_data['back'], true) }}</textarea>
										<textarea id="BackProduct" name="back_product" style="display:none;"></textarea>
										<div class="form-group name">
                                                <label for="name" class="control-label">Name</label>
                                                <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $brandable->name }}">
                                                <div class="help-block with-errors"></div>
                                            </div>
        
                                            <div class="form-group price">
                                                <label for="name" class="control-label">Price</label>
                                                <input type="text" class="form-control" name="price" placeholder="Price" value="{{ $brandable->price }}">
                                                <div class="help-block with-errors"></div>
                                            </div>
        
                                            <div class="form-group description">
                                                <label for="description" class="control-label">Description</label>
                                                <textarea class="form-control" name="description">{{ $brandable->description }}</textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group category">
                                                    <label for="description" class="control-label">Category</label>
                                                    <select class="form-control" name="category">
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}" {{ set_category($category->id, $brandable_data) }}>{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                            </div>
										
                                            <div class="funkyradio">

                                                    <div class="funkyradio-default">
                                                        <input type="checkbox" name="enable_sizes" id="radio1" checked value="1" <?= $isSizeEnabled ?>>
                                                        <label for="radio1">Enable Sizes</label>
                                                    </div>
                                                    <p></p><small class="text-muted">Disabling this is useful for products like "MUGS" which do not need sizes.</small>
            
                                            </div>
            
                                            <div class="funkyradio">
                                                <div class="funkyradio-default">
                                                    <input type="checkbox" name="enable_back_view" id="BackViewTrigger" value="1" <?= $isBackEnabled ?>>
                                                    <label for="BackViewTrigger">Enable Back View</label>
                                                </div>
        
                                            </div>
									</div>
								</div>
								<!-- /.box -->
							
						</div>
						<div class="row mt-3">
							<h5 class="text-center text-bold">Front View Product Settings</h5>
							<div class="col-md-4 mb-3">
								<div class="well well-sm text-center"> <small>Upload Front-View Product</small>
									<p style="margin:1px;"><span class="btn btn-info btn-file">
                                            Browse Products <input type="file" class="file-upload"  accept=".png, .jpg, .jpeg" data-target="#frontImagePreview" data-input="#FrontProduct" name="front_image">
                                            </span>
									</p>
								</div>
							</div>
							<div class="col-md-8" style="overflow:auto;">
								<div class="DesignAreaContainer" style="width:530px; height:630px; border:0px solid red; overflow:auto;">
									<div class="designArea"></div>
									<div class="CanvaArea">
										<img src="{{ asset($brandable->front_image) }}" class="image-fixed" id="frontImagePreview">
									</div>
								</div>
							</div>
                        </div>
                        
						<div class="row uk-margin-top" id="BackView" style="position:relative;"> <span class="overlay"></span>
							<h5 class="text-center text-bold">Back ViewProduct Settings</h5>
							<div class="col-md-4">
								<div class="well well-sm text-center"> <small>Upload Back-View Product</small>
									<p style="margin:1px;"><span class="btn btn-info btn-file">
                                        Browse Products <input type="file" class="file-upload"  accept=".png, .jpg, .jpeg" data-target="#backImagePreview" data-input="#BackProduct" name="back_image">
                                        </span>
									</p>
								</div>
							</div>
							<div class="col-md-8" style="overflow:auto;">
								<div class="DesignAreaContainerBack" style="width:530px; height:630px; border:0px solid red; overflow:auto;">
									<div class="designAreaBack"></div>
									<div class="CanvaArea">
										<img src="{{ asset('brands/default_img.png') }}" class="image-fixed" id="backImagePreview">
									</div>
								</div>
							</div>
                        </div>
                        
					</div>
				</div>
            </div>

        </form>

		</div>
	</div>
</div>@endsection @section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ mix('js/brandable.js') }}"></script>@endsection