Hi! {{$influencer->username}};
<p>Your influencer account has been confirmed! Please follow the link below to complete your registration</p>
<a href="{{ route('app:influencer:sign_up', ['code'=> encrypt($influencer->invite_code)]) }}">Complete Registration</a>