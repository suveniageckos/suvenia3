<?php
namespace App\Http\Controllers\Admin;

use App\ProductBlock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductBlockCtrl extends Controller{

    public function index(Request $request){
        $product_blocks = ProductBlock::get();
        return view('vendor.voyager.product-blocks.index', ['product_blocks'=> $product_blocks]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $product = new ProductBlock();
            $product->title = $request->title;
            $product->code = $request->code;
            $product->product_ids = $request->product_ids;
            $product->save();
            $red = action([ProductBlockCtrl::class, 'index']);
           return redirect($red);
        }
        return view('vendor.voyager.product-blocks.add', []);
    }

    public function edit(Request $request, $id){
        $product = ProductBlock::where('id', $id)->first();
        if($request->isMethod('post')){
            $product = ProductBlock::where('id', $id)->first();
            $product->title = $request->title;
            $product->product_ids = $request->product_ids;
            $product->save();
            $red = action([ProductBlockCtrl::class, 'index']);
           return redirect($red);
        }
        return view('vendor.voyager.product-blocks.edit', ['product'=> $product]);
    }

    public function delete(Request $request, $id){
        if($request->isMethod('post')){
            $product = ProductBlock::where('id', $id)->first();
            $product->delete();
            $red = action([ProductBlockCtrl::class, 'index']);
            return redirect($red);
        }
    }


}