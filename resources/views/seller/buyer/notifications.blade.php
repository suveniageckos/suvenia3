@extends('layouts.dashboard')

@section('title', 'My Notifications')

@section('content')

<div class="row justify-content-center">
        <div class="col-12">
            <div class="bg-white mb-3 shadowed-box">
                <div class="p-2 cart_shipping_zone_header_bottom">
                    <p class="cart_shipping_zone_header m-0">NOTIFICATIONS</p>
                </div>
                <div class="row no-gutters">
                    @foreach ($notifications as $item)
                    <div class="col-12 p-4 border-bottom">
                            {!! $item->message_body !!}
                        </div>
                    @endforeach
                    <div class="col-12 p-4">
                        {{ $notifications->links() }}
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection