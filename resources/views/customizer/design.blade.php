@extends('layouts.customizer')

@section('title', 'Designer' )

@section('content')

<!--- Designer Pane --->
@includeIf('customizer.customizer_pages.design')

<!--- Sell Pane --->
@includeIf('customizer.customizer_pages.sell')

<!--- Buy Pane --->
@includeIf('customizer.customizer_pages.buy')

<!--- Modals --->
@includeIf('customizer.customizer_pages.modals')

@endsection
               
@push('customizer_js')
 
@endpush