class UserOrdersPage{
    public constructor(){
        this.__set_order_filter();
    }

    public __set_order_filter(){
        $(document).on('change', '._order_item_filter_select_input', function(e){
            let val = $(e.target).val();
            window.location.href = <any>val; 
        });
    }
}

$(function(){ 
    const ____init_userOder_page = new UserOrdersPage();
}); 