
@if($action == 'add' || $action == 'edit')
<select class="form-control select2" name="parent_id">
    @foreach(\App\ProductCategory::where('is_parent', true)->get() as $category)
<option value="{{ $category->id }}">{{ title_case($category->name ) }}</option>
    @endforeach
</select>
@elseif($action == 'browse' || $action == 'read')
@php $category = \App\ProductCategory::where('id', $content)->first(); @endphp
<p>{{ isset($category) ? $category->name : '' }}</p>
@endif
