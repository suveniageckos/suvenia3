import { CartItem } from "./includes/CartItem";

class SavedItemPage{

    public constructor(public __cartItem = new CartItem){
        this.set_ajax();
        if($('#SavedItemPage').length){
            this.__cartItem.__initiate_delete();
            this.__cartItem._delete_cart_item();
        }
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    } 
 

}

$(function(){ 
    const ____init_savedItem_page = new SavedItemPage();
}); 