<a href="{{ route('app:ecommerce:fetch_arts') }}" class="btn btn-info btn-sm" id="GoBack">Go Home</a>
<ul class=" uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-top" uk-grid>
@foreach($arts as $art)
<li class="mb-1">
<div class="uk-card uk-card-small uk-text-center art_single">
<p style="margin:0;"><img src="{{ asset($art->photo_url) }}" uk-responsive width="140" class="ArtPic"></p>
<p class="art_name text-14" style="display:non; margin:0px;"></p>
</div>

</li>
@endforeach
</ul>
<div class="border-top uk-margin-top uk-text-center" style="text-align:center !important;" id="PaginLink" data-link="{{ route('app:ecommerce:fetch_arts', ['pid'=> $parent_ref]) }}">
<p>{{ $arts->appends(['pid'=> $parent_ref])->links() }}</p> 
</div>