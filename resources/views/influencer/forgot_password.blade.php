@extends('layouts.login')

@section('title', 'Forgot password' )

@section('content')
<div class="uk-container mt-3 mb-3">
 <div class="row justify-content-center">

<div class="col-md-6">

  <div class="card app-box">
    <div class="box-header">
      <p class="title">Forgot Password?</p>
      <p class="sub-title">Forget your password? No worry, Let's get you a new one. </p>
    </div>
    <div class="box-body">
        <form class="app-form" action="{{ route('app:influencer:request_forgot_password') }}" method="POST">
            @csrf
            <input type="hidden" name="type" value="{{encrypt('influencers')}}">
               <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                 <input type="email" class="form-control form-control-lg textfield-format" placeholder="Email Address" name="email"  value="{{ old('email') }}">
                 <p class="form-text help-block">{{ $errors->first('email') }}</p>
               </div>
               <button type="submit" class="btn btn-info btn-lg btn-block rounded">Send a Reset Link</button>
             </form>

    </div>

  </div>

</div>

 </div>


</div>
@endsection
