@extends('vendor.voyager.layouts.app')

@section('page_title', __('Add Store we like'))
@section('templcss')
<link rel="stylesheet" href="{{ mix('css/admin_templates.css') }}">
@endsection

@section('content')
<div class="container-fluid">
             <h1 class="page-title">
                    <i class="voyager-star-half"></i> Add Store  We Like Blocks
                </h1>

<div class="page-content browse container-fluid">

<div class="row">
<div class="col-md-12">

<div class="panel panel-bordered">
<div class="panel-body">

<div class="row">
<form action="{{ route('admin:stores_we_like_block:add') }}" method="POST">
   @csrf
        {{-- <div class="form-group">
            <label for="">Block Title</label>
            <input type="text" class="form-control" placeholder="Block Title" required name="title">
        </div>

        <div class="form-group">
            <label for="">Code</label>
            <input type="text" name="code" class="form-control" placeholder="Code" readonly value="{{ str_random(10) }}">
        </div> --}}


        <div class="form-group">
            <label for="">Product IDS <small>(Seperate Product ID by comma)</small></label>

            <textarea class="form-control" placeholder="Store IDS" name="store_ids"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-info">SUBMIT</button>
        </div>

    </form>
</div>
</div>

</div>


</div>
</div>


</div>
</div>

@endsection

@push('javascript')

@endpush
