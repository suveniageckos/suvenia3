import fabric from 'fabric';
import * as _ from 'lodash';

class Canvas{

    canvas_front: any;

    activeData: any;

	canvas_back: any;

	active_canvas_side: any; 
 
	activeCanva: any;
 
	baseUrl: any; 

	activeCanvaDiv: any;

    hasBack: boolean = true;

    isDirty: boolean = false;
 
    PrimaryColor: any = ['#FFFFFF'];

    designData: any = {
		front: {},
		back: {}
	};
    
    DesDiv: String = 'canvas';

    baseCategory: any;

    fab: any;

    Layers: any = [
	];

	imageBucket: any = [];

    constructor(){

        this.baseUrl = $('body').attr('BaseAppUrl');
        
        this.fab = fabric;
        
    }

    public setObjectLayers(obj: any) {
		const cls = this;
		const ID = cls.generateId(5);
		cls.Layers.push(ID);
		let latest: any = _.last(cls.Layers);
		
		$('#LayerPane').append(
			'<div class="layer layerID" data-id="'+ID+'">' +
			'<ul class="list-unstyled m-0 d-flex">' +
			'<li class="d-inline flex-fill"><span class="icon-' + obj['icon'] + ' layer-type mr-1 mt-5"></span> <span class="layer-name">' + obj['text'] + '</span></li>' +
			'<li class="d-inline flex-fill uk-text-right"><span class="icon-move icon-shade move-layer mr-2"></span><span class="icon-delete delete-layer icon-shade" data-fabric="w"><textarea style="display: none;" class="pane_data">'+ JSON.stringify(obj) +'</textarea></span></li>' +
			'</ul></div>'
		);

		$('.layerID').each(function(ind, val){
			let delBtn = $(this).find('.delete-layer');
			delBtn.off().on('click', function(e){
				//Get pane data
				let ___data = JSON.parse(<any>$(e.currentTarget).find('.pane_data').val());
				console.log(___data);
				if(___data.is_uploaded){
					let imgID = ___data.uploadID;
					//cls.activeCanva.
					//cls.Canva.imageBucket
					let indexJJ = _.findIndex(cls.imageBucket, { 'uploadID': imgID});
					_.pullAt(cls.imageBucket, [indexJJ])
				}

				let pos: any = _.indexOf(cls.Layers, $(val).attr('data-id'));
				let toBeDel = cls.activeCanva.item(pos);
				cls.activeCanva.remove(toBeDel);
				cls.activeCanva.renderAll();
				$(val).remove();
				_.pull(cls.Layers, $(val).attr('data-id'));
				
			});
		});
		

    }
    
    public generateId(len: any){
        var text = "";

            var charset = "abcdefghjklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ";

            for( var i=0; i < len; i++ )
            text += charset.charAt(Math.floor(Math.random() * charset.length));

            return text;
    }
}

export default Canvas;