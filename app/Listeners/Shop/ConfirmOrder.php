<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrderCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use App\OrderShipping;

class ConfirmOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrderCompleted  $event
     * @return void
     */
    public function handle(ProductOrderCompleted $event)
    {
        $order = Order::where('id', $event->order->id)->first();
        $order->status = 2;
        $order->save();
        //confirm order shipping
        $order_shipping = OrderShipping::where('order_id', $event->order->id)->first();
        $order_shipping->status = 2;
        $order_shipping->save();
    }
}
