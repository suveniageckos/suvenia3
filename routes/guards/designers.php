<?php
 Route::group(['prefix' => 'designer', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/', 'DesignerCtrl@landing_page')->name('app:designer:landing_page');
    Route::match(['post', 'get'], '/hire', 'DesignerCtrl@landing_page_hire')->name('app:designer:landing_page_hire');
    Route::match(['post', 'get'], '/sign-up', 'DesignerCtrl@sign_up')->name('app:designer:sign_up');
    Route::match(['post', 'get'], '/forgot-password', 'DesignerCtrl@forgot_password')->name('app:designer:forgot_password');
    Route::match(['post', 'get'], '/reset-password/{token}', 'DesignerCtrl@reset_password')->name('app:designer:reset_password');
    Route::match(['post', 'get'], '/verify/{token}', 'DesignerCtrl@verify_email')->name('app:designer:verify');
    Route::match(['post', 'get'], '/login', 'DesignerCtrl@login')->name('app:designer:login');
    Route::match(['post', 'get'], '/logout', 'DesignerCtrl@logout')->name('app:designer:logout');
    Route::match(['post', 'get'], '/social/{provider}', 'DesignerCtrl@social')->name('app:designer:social');
    Route::match(['post', 'get'], '/social/callback/{provider}', 'DesignerCtrl@social_callback')->name('app:designer:social_callback');
});

Route::group(['prefix' => 'designer/app', 'middleware' => ['web']], function () {
    // Route::match(['post', 'get'], '/sign-up', 'DesignerCtrl@sign_up')->name('app:designer:sign_up');
     Route::match(['post', 'get'], '/gig/{ref}', 'DesignerCtrl@view_gig')->name('app:designer:view_gig'); 
     Route::match(['post', 'get'], '/catalogue/{params}', 'DesignerCtrl@catalogue')->name('app:designer:catalogue');
     Route::match(['post', 'get'], '/buy-plan/{id}', 'DesignerCtrl@buy_plan')->name('app:designer:buy_plan');
     Route::match(['post', 'get'], '/confirm-payment/{param}', 'DesignerCtrl@confirm_payment')->name('app:designer:confirm_payment');
     Route::match(['post', 'get'], '/process-payment', 'DesignerCtrl@process_payment')->name('app:designer:process_payment');
     Route::match(['post', 'get'], '/payment-callback', 'DesignerCtrl@payment_callback')->name('app:designer:payment_callback');
     Route::match(['post', 'get'], '/complete-order', 'DesignerCtrl@complete_order')->name('app:designer:complete_order');
     Route::match(['post', 'get'], '/success', 'DesignerCtrl@order_notify')->name('app:designer:order_notify');
     Route::match(['post', 'get'], '/post/search-services', 'DesignerCtrl@search_services_post')->name('app:designer:search_post');
     Route::match(['post', 'get'], '/search', 'DesignerCtrl@search_services')->name('app:designer:search');
     Route::match(['post', 'get'], '/post-request', 'DesignerCtrl@post_request')->name('app:designer:post_request');
 });      

 Route::group(['prefix' => 'dashboard/designer', 'middleware' => ['web','assign.guard:designers,app:designer:login']], function () {
    Route::match(['post', 'get'], '/', 'Dashboard\DesignerCtrl@index')->name('app:designer:dashboard:index');
    Route::match(['post', 'get'], '/gigs', 'Dashboard\DesignerCtrl@designer_all_gigs')->name('app:designer:dashboard:all_gigs');
    Route::match(['post', 'get'], '/gig/add', 'Dashboard\DesignerCtrl@designer_add_gig')->name('app:designer:dashboard:add_gig');
    Route::match(['post', 'get'], '/gig/add/upload-gallery', 'Dashboard\DesignerCtrl@upload_gallery')->name('app:designer:dashboard:upload_gallery');
    Route::match(['post', 'get'], '/gig/delete/image/single/{id}', 'Dashboard\DesignerCtrl@designer_delete_single_image')->name('app:designer:dashboard:designer_delete_single_image');
    Route::match(['post', 'get'], '/gig/edit/{id}', 'Dashboard\DesignerCtrl@designer_edit_gig')->name('app:designer:dashboard:edit_gig');
    Route::match(['post', 'get'], '/gig/delete/{id}', 'Dashboard\DesignerCtrl@designer_delete_gigs')->name('app:designer:dashboard:delete_gigs');
    
    Route::match(['post', 'get'], '/orders', 'Dashboard\DesignerCtrl@designer_all_orders')->name('app:designer:dashboard:all_orders');
    Route::match(['post', 'get'], '/{id}/order', 'Dashboard\DesignerCtrl@designer_view_order')->name('app:designer:dashboard:view_order'); 
    Route::match(['post', 'get'], '/order/delete', 'Dashboard\DesignerCtrl@designer_delete_orders')->name('app:designer:dashboard:delete_orders');
    Route::match(['post', 'get'], '/send/order/message', 'Dashboard\DesignerCtrl@designer_order_send_message')->name('app:designer:dashboard:order_send_message');
    Route::match(['post', 'get'], '/send/order/attachment', 'Dashboard\DesignerCtrl@designer_order_send_attachment')->name('app:designer:dashboard:order_send_attachment');
    

    Route::match(['post', 'get'], '/withdrawal', 'Dashboard\DesignerCtrl@withdrawal')->name('app:designer:dashboard:withdrawal');
    Route::match(['post', 'get'], '/profile-settings', 'Dashboard\DesignerCtrl@profile_settings')->name('app:designer:dashboard:profile_settings');
    Route::match(['post', 'get'], '/change-password', 'Dashboard\DesignerCtrl@change_password')->name('app:designer:dashboard:change_password');
    Route::match(['post', 'get'], '/bank-details', 'Dashboard\DesignerCtrl@bank_details')->name('app:designer:dashboard:bank_details');
    Route::match(['post', 'get'], '/requests', 'Dashboard\DesignerCtrl@all_requests')->name('app:designer:dashboard:requests');
    Route::match(['post', 'get'], '/request/send-offer/{id}', 'Dashboard\DesignerCtrl@send_offer')->name('app:designer:dashboard:send_offer'); 
    Route::match(['post', 'get'], '/request/send-offer/modal/{id}', 'Dashboard\DesignerCtrl@send_offer_modal')->name('app:designer:dashboard:send_offer_modal'); 
  
});    

Route::group(['prefix' => 'dashboard/designer/abstract/', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/customer/approve/order/{id}', 'Dashboard\DesignerCtrl@approve_order')->name('app:designer:dashboard:approve_order');
    Route::match(['post', 'get'], '/order/rate/{id}', 'Dashboard\DesignerCtrl@rate_designer')->name('app:designer:dashboard:rate_designer');
});
       