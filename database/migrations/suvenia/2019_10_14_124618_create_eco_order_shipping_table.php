<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoOrderShippingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_order_shipping', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('order_id');
			$table->integer('address_id')->nullable();
			$table->integer('zone_id');
			$table->string('price', 191);
			$table->boolean('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_order_shipping');
	}

}
