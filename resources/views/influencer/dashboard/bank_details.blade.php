@extends('layouts.dashboard_influencer')

@section('title', 'Bank Details' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:influencer:dashboard:profile_settings') }}" class="card_head_nav_item {{  $utils->makeActive(['app:influencer:dashboard:profile_settings'], 'active') }}">Personal Details</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:influencer:dashboard:change_password') }}" class="card_head_nav_item {{  $utils->makeActive(['app:influencer:dashboard:change_password'], 'active') }}">Change Password</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:influencer:dashboard:bank_details') }}" class="card_head_nav_item {{  $utils->makeActive(['app:influencer:dashboard:bank_details'], 'active') }}">Bank Details</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="w-75 m-auto">
                        <form action="{{ url()->current() }}" method="POST" data-post>
                            <div class="row justify-content-center">
                                    <div class="col-md-12 form-group bank_name">
                                            <label for="bank_name app-label">Bank Name</label>
                                    <input type="text" class="form-control app-form textfield-format"  placeholder="Bank Name" name="bank_name" value="{{ Auth::guard('influencers')->user()->bank_name }}">
                                            <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-12 form-group bank_account">
                                            <label for="bank_account app-label">Bank Account</label>
                                    <input type="text" class="form-control app-form textfield-format"  placeholder="Bank Account" name="bank_account" value="{{ Auth::guard('influencers')->user()->bank_account }}">
                                            <span class="help-block"></span>
                                    </div>
                                    <!--<div class="form-group bank_bvn">
                                            <label for="bank_bvn">Bank Bvn</label>
                                    <input type="text" class="form-control"  placeholder="Bank Bvn" name="bank_bvn" value="{{ Auth::guard('influencers')->user()->bank_bvn }}">
                                            <span class="help-block"></span>
                                    </div>-->
                                    <div class="col-md-12 form-group bank">
                                            <label for="bank_bvn app-label">Bank</label>
                                            <select class="uk-select app-form textfield-format" id="BankSelect" name="bank">
                                                    @foreach($utils->fetchBanks() as $key => $value)
                                                    <option value="{{ $key }}" bank-code="{{ $value }}" {{ Auth::guard('influencers')->user()->bank_sort == $key ? 'selected' : ''}}>{{ $value }}</option>
                                                    @endforeach
                                                    </select>
                                            <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-12 form-group uk-text-center">
                                            <button class="btn btn-info" type="submit">UPDATE</button>
                                        </div>

                            </div>

                        </form>
                    </div>
                </div><!-- card ends here -->
        </div>
    </div>

</div>


@endsection

