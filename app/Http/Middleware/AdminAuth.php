<?php

namespace Ukalator\Modules\User\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Bouncer;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
			return redirect()->route('app:admin:user:login')->with('error', 'You are not authorized to access the admin page.');
		}elseif(Auth::check() and Bouncer::is(Auth::user())->a('non_admin')){
            return redirect()->route('app:admin:user:login')->with('error', 'You are not authorized to access the admin page.');
        }
        
        return $next($request);
    }
}
