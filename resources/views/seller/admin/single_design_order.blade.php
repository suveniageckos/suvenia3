@extends('layouts.dashboard_seller')

@section('title', 'My Design Order' )

@section('content')
 
        <div class="row mb-3">
            <div class="col-md-9">
               
            </div>
            <div class="col-md-3 uk-text-right">
                <button class="btn btn-info data-prompt " 
                type="button"
                data-url="{{ route('app:designer:dashboard:approve_order', ['id'=> $order->id]) }}" 
                title="Mark As Completed"
                content=' <p style="color: #333; font-size: 14px;">Do you really want to mark this order as completed? Please note this is irreversible.</p>'
                confirm="Mark Complete"
                 >Mark as Completed</button>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card uk-animation-slide-top-small p-3">
                    <h6>How would you rate <strong>{{$order->designer->username}}</strong>?</h6>
                    <div class="media">
                    <img src="{{ asset($order->designer->photo_url) }}" class="mr-3" alt="{{ $order->designer->username }}" width="50">
                    <div class="media-body">
                    @php $rateValues = [1, 2, 3, 4, 5] ;@endphp
                    <select class="" id="rate-designer-bar" data-url="{{ route('app:designer:dashboard:rate_designer', ['id'=> $order->id]) }}">
                    @foreach ($rateValues as $item)
                    <option value="{{ $item }}" {{ $order->designer->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                    @endforeach
                    </select>
                            <small class="text-success" id="rateMsg"></small>
                    </div>
                    </div>
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <p class="order_title m-0">Order</p>
                                        </div>
                                        <div>
                                            <p class="order_number m-0">#{{ $order->reference }}</p>
                                        </div>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-2">
                                <p class="m-0">N{{ $order->amount }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                <p class="m-0"><span class="buyer_text_title">Buyer:</span> <span class="buyer_text_name">{{ $utils->get_design_buyer($order)->username }}</span> <span class="buyer_text_title ml-2">{{ $order->created_at->toDayDateTimeString() }}</span></p>
                                </div>
                                <div class="col-md-9"></div>
                            </div>
                            
                        </div>

                    <div class="card-body">
                        <div class="">
                                <table class="table table-borderless table-sm order_table">
                                        <thead>
                                          <tr>
                                            <th scope="col">Item</th>
                                            <th scope="col">Duration</th>
                                            <th scope="col">Amount</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <th scope="row">{{ $order->order_request->title }}</th>
                                            <td>{{ $order->order_request->duration }}</td>
                                            <td>N{{ $order->amount }}</td>
                                          </tr>
                                          <tr>
                                            <td>{{ $order->plan->revisions }} Revesions</td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td></td>
                                            <th>Total</th>
                                            <th>N{{ $order->amount }}</th>
                                          </tr>
                                          
                                        </tbody>
                                      </table>
                        </div>
                        <div class="border-top p-4 uk-text-center">
                            <h4 class="m-0 _order_req_title">ORDER REQUIREMENTS</h4>
                            <div class="accordion" id="Reqaccordion">
                            <p class="m-0">Check out buyer’s requirements <a href="javascript:;" class="_order_req_link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">show requirements<i uk-icon="triangle-down"></i></a></p>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#Reqaccordion">
                                    <div class="border-top p-4 mt-3">
                                        {{ $order->order_request->description }}
                                    </div>
                                  </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        

    @if(!$order->is_completed)
        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite uk-text-center">
                            <h4 class="header_text m-0">CONVERSATION</h4>
                    </div>

                    <div class="card-body chat-messages-box" id="chartFrame">
                    <ul id="AppChartPane" url-chats="{{ route('app:chat:all_chats', ['id'=> $order->id, 'sender'=> Auth::guard('sellers')->user()->id]) }}" url-new-chat="{{ route('app:chat:new_chat', ['id'=> $order->id, 'sender'=> Auth::guard('sellers')->user()->id]) }}" current-user="{{json_encode(['id'=> Auth::guard('sellers')->user()->id, 'avatar'=> Auth::guard('sellers')->user()->photo_url])}}" url-update-chat="{{ route('app:chat:update_chat', ['id'=> $order->id, 'sender'=> Auth::guard('sellers')->user()->id]) }}" sender-type="SELLER">
                        </ul>
                    </div>
                    <div class="card-footer" style="background: rgba(151, 151, 151, 0.38)">
                            <div class="bg-white p-2 mt-0">
                        <textarea class="form-control message-input" id="__appChatInput"></textarea>
                            </div>
                        <div class="bg-white p-2 mt-0">
                                <div class="d-flex justify-content-between">
                                    <div></div>
                                    <div>
                                        <ul class="m-0 list-unstyled">
                                            <li class="d-inline-block"><button type="button" class="btn btn-outline-secondary btn-sm" href="#FileChatModal" uk-toggle><i class="icon icon-paper-clip" style="font-size: 20px;"></i> Attach</button></li>
                                    <li class="d-inline-block"><button type="button" class="btn btn-outline-info btn-sm" id="__AppchatSubmit"><i class="icon icon-paper-plane" style="font-size: 20px;"></i> Send</button></li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div id="FileChatModal" uk-modal="bg-close: false">
                        <div class="uk-modal-dialog uk-modal-body p-0">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <h3 class="uk-modal-title mb-0  p-3" style="color: #000; font-size: 16px; font-weight: 600;">Upload File</h2>
                            <div class="upload-pane mt-0">
                                    <div id="FileChatUploader"  class="dropzone uk-text-center" data-url="{{ route('app:chat:upload_chat_file', ['id'=> $order->id, 'sender'=> Auth::guard('sellers')->user()->id]) }}">
                                       <div class="fallback">
                                          <input name="file" type="file" multiple />
                                       </div>
                                    </div>
                                    <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>
                                 </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif

@endsection