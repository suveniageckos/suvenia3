@extends('layouts.designer')

@section('title', 'Buy Plan' )

@section('content')

<div class="uk-container uk-container-small mt-3 mb-3">
    <div class="row justify-content-center mb-2">
        <div class="col-10">
            <h2 class="des_buy_pg_title m-0">Set and Customise request</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-7 mb-2">
            <div class="buy_plan_card p-4">
                <div class="row no-gutters">
                    <div class="col-md-2">
                        <img src="{{ asset($plan->gig->photos->first()->public_url) }}" uk-responsive width="100">
                    </div> 
                    <div class="col-md-7 pl-3 pr-3">
                    <p class="m-0 title">{{ $plan->gig->title }}</p>
                    <p class="m-0 user">{{ $plan->gig->user->username }}</p>
                        <!--<div class="mt-2 d-flex justify-content-between w-25">
                            <div class="qty pr-2">Quantity:</div>
                            <div>
                                <select>
                                    @for($i=1; $i<11; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>-->
                    </div>
                    <div class="col-md-3"><p class="__pricing">Price: &#8358;{{ $plan->price }}</p></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h2 class="plan_title m-1">{{ $plan->name }}</h2>
                        <div class="plan_details">
                            
                            <ul class="list-unstyled">
                                    <li class="d-block feature"><i class="icon-checked mr-2 color-check"></i><span> {{ $plan->duration }} Days</span></li>
                                    <li class="d-block feature">
                                            @if($plan->is_printable)
                                            <i class="icon-checked mr-2 color-check"></i>
                                            @else
                                            <i class="icon-cancel mr-2 color-cancel"></i>
                                            @endif
                                            <span>Print-Ready</span>
                                    </li>
                                    <li class="d-block feature">
                                        @if($plan->has_source_file)
                                        <i class="icon-checked mr-2 color-check"></i>
                                        @else
                                        <i class="icon-cancel mr-2 color-cancel"></i>
                                        @endif
                                        <span> Files</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="des_plan_pay_card p-3">
                    <h2 class="title">Summary</h2>
                    <div class="d-flex justify-content-between sub_total_box border-bottom mb-3 pb-3">
                            <div class="title">
                                Sub total
                            </div>
                            <div class="amount">
                                &#8358;{{ $plan->price }}
                            </div>
                    </div>
                    <div class="d-flex justify-content-between sub_total_box mb-5">
                            <div class="title_bold">
                                Total
                            </div>
                            <div class="amount_bold">
                                &#8358;{{ $plan->price }}
                            </div>
                    </div>
                    
                    <div class="mt-2 uk-text-center">
                        <a href="{{ route('app:designer:confirm_payment', ['param'=> $plan->id]) }}" class="btn btn-info-dark">PROCEED</a>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection