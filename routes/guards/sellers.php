<?php
Route::group(['prefix' => 'seller', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/', 'SellerCtrl@seller_home')->name('app:seller:index');
    Route::match(['post', 'get'], '/sign-up', 'SellerCtrl@sign_up')->name('app:seller:sign_up');
    Route::match(['post', 'get'], '/login', 'SellerCtrl@login')->name('app:seller:login');
    Route::match(['post', 'get'], '/logout', 'SellerCtrl@logout')->name('app:seller:logout');
    Route::match(['post', 'get'], '/forgot-password', 'SellerCtrl@forgot_password')->name('app:seller:forgot_password');
    Route::match(['post', 'get'], '/reset-password/{token}', 'SellerCtrl@reset_password')->name('app:seller:reset_password');
    Route::match(['post', 'get'], '/verify/{token}', 'SellerCtrl@verify_email')->name('app:seller:verify');
    Route::match(['post', 'get'], '/social/{provider}', 'SellerCtrl@social')->name('app:seller:social');
    Route::match(['post', 'get'], '/social/callback/{provider}', 'SellerCtrl@social_callback')->name('app:seller:social_callback');

});
 

Route::group(['prefix' => 'dashboard/seller', 'middleware' => ['web', 'assign.guard:sellers,app:seller:login']], function () {
    Route::match(['post', 'get'], '/', 'Dashboard\SellerCtrl@index')->name('app:seller:dashboard:index');
    Route::match(['post', 'get'], '/my-products', 'Dashboard\SellerCtrl@my_products')->name('app:seller:dashboard:my_products');
    Route::match(['post', 'get'], '/my-products/delete', 'Dashboard\SellerCtrl@delete_product')->name('app:seller:dashboard:product_delete');
    Route::match(['post', 'get'], '/my-products/search', 'Dashboard\SellerCtrl@search_product')->name('app:seller:dashboard:product_search');
    Route::match(['post', 'get'], '/product/edit/{id}', 'Dashboard\SellerCtrl@edit_product')->name('app:seller:dashboard:product_edit');
    Route::match(['post', 'get'], '/my-orders', 'Dashboard\SellerCtrl@my_orders')->name('app:seller:dashboard:my_orders');
    Route::match(['post', 'get'], '/customer-orders', 'Dashboard\SellerCtrl@customers_orders')->name('app:seller:dashboard:customer_orders');
    Route::match(['post', 'get'], '/profile-settings', 'Dashboard\SellerCtrl@profile_settings')->name('app:seller:dashboard:profile_settings');
    Route::match(['post', 'get'], '/change-password', 'Dashboard\SellerCtrl@change_password')->name('app:seller:dashboard:change_password');
    Route::match(['post', 'get'], '/bank-details', 'Dashboard\SellerCtrl@bank_details')->name('app:seller:dashboard:bank_details');
    Route::match(['post', 'get'], '/payment-history', 'Dashboard\SellerCtrl@payment_history')->name('app:seller:dashboard:payment_history');
    Route::match(['post', 'get'], '/withdrawal', 'Dashboard\SellerCtrl@withdrawal')->name('app:seller:dashboard:withdrawal');
    Route::match(['post', 'get'], '/promotions', 'Dashboard\SellerCtrl@promotions')->name('app:seller:dashboard:promotions');
    // Route::match(['post', 'get'], '/seller_zone', 'Dashboard\SellerCtrl@seller_zone')->name('app:seller:dashboard:seller_zone');

    //Notifications
    Route::match(['post', 'get'], '/notifications', 'Dashboard\SellerCtrl@all_notifications')->name('app:seller:dashboard:all_notifications');
    Route::match(['post', 'get'], '/notifications/{id}', 'Dashboard\SellerCtrl@all_notifications')->name('app:seller:dashboard:all_notifications');
    Route::match(['post', 'get'], '/notifications/{id}', 'Dashboard\SellerCtrl@delete_notifications')->name('app:seller:dashboard:delete_notifications');


    Route::match(['post', 'get'], '/design-services', 'Dashboard\SellerCtrl@design_services')->name('app:seller:dashboard:design_services');
    Route::match(['post', 'get'], '/design-service/{id}', 'Dashboard\SellerCtrl@single_design_service')->name('app:seller:dashboard:single_design_service');
    Route::match(['post', 'get'], '/design-requests', 'Dashboard\SellerCtrl@design_request')->name('app:seller:dashboard:design_request');
    Route::match(['post', 'get'], '/design-request/{id}', 'Dashboard\SellerCtrl@single_design_request')->name('app:seller:dashboard:single_design_request');
    Route::match(['post', 'get'], '/fetch/promote-product/form/{id}', 'Dashboard\SellerCtrl@fetch_promote_form')->name('app:seller:fetch_promote_form');
    Route::match(['post', 'get'], '/promote-product', 'Dashboard\SellerCtrl@promote')->name('app:seller:dashboard:promote');
    Route::match(['post', 'get'], '/promote-callback', 'Dashboard\SellerCtrl@promote_order_callback')->name('app:seller:dashboard:promote_order_callback');
    Route::match(['post', 'get'], '/promote-product/filter', 'Dashboard\SellerCtrl@promote_filter')->name('app:seller:dashboard:promote_filter');
    //Stores
    Route::match(['post', 'get'], '/stores', 'Dashboard\SellerCtrl@all_store')->name('app:seller:dashboard:all_store');
    Route::match(['post', 'get'], '/fetch/promote-store/form/{id}', 'Dashboard\SellerCtrl@fetch_promote_store_form')->name('app:seller:dashboard:fetch_promote_store_form');
    Route::match(['post', 'get'], '/promote-store', 'Dashboard\SellerCtrl@promote_store')->name('app:seller:dashboard:promote_store');
    Route::match(['post', 'get'], '/create-stores', 'Dashboard\SellerCtrl@create_store')->name('app:seller:dashboard:create_store');
    Route::match(['post', 'get'], '/edit-store/{id}', 'Dashboard\SellerCtrl@edit_store')->name('app:seller:dashboard:edit_store');
    Route::match(['post', 'get'], '/delete-store/{id}', 'Dashboard\SellerCtrl@delete_store')->name('app:seller:dashboard:delete_store');
    Route::match(['post', 'get'], '/preview-store/{id}', 'Dashboard\SellerCtrl@view_store')->name('app:seller:dashboard:edit_store:view_store');
    Route::match(['post', 'get'], '/edit-store/{id}/store-header', 'Dashboard\SellerCtrl@store_header_edit')->name('app:seller:dashboard:edit_store:store_header');
    Route::match(['post', 'get'], '/edit-store/{id}/store-banner', 'Dashboard\SellerCtrl@store_banner_edit')->name('app:seller:dashboard:edit_store:store_banner');
    Route::match(['post', 'get'], '/edit-store/{id}/store-products', 'Dashboard\SellerCtrl@store_products_edit')->name('app:seller:dashboard:edit_store:store_products');
    Route::match(['post', 'get'], '/edit-store/{id}/store-about', 'Dashboard\SellerCtrl@store_about')->name('app:seller:dashboard:edit_store:store_about');
    

});

Route::group(['prefix' => 'dashboard/seller/api', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/edit-store/upload', 'Dashboard\SellerCtrl@edit_store_save_image')->name('app:seller:dashboard:edit_store_upload');
    Route::match(['post', 'get'], '/edit-store/upload-delete', 'Dashboard\SellerCtrl@edit_store_delete_image')->name('app:seller:dashboard:edit_store_upload_delete');
});
