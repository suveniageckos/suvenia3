<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InfluencerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
			$influencer = DB::table('roles')->where('id', Auth::user()->role_id)->first();
            if($influencer->name != '@INFLUENCER'){
                return redirect()->route('app:influencer:login')->with('error', 'You must login as an influncer to access that page.');
            } 
        }else{
            return redirect()->route('app:influencer:login')->with('error', 'You must login as an influncer to access that page.');
        }
        
        return $next($request);
    } 
}

