<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ghanem\Rating\Traits\Ratingable as Rating;

class Designer extends Authenticatable
{
    use Notifiable;
    use Rating;
}
