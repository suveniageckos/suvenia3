@extends('layouts.layout')
@section('title', 'Catalogue' )
@section('content')
@php
    $categories = $utils->get_categories();
    $currentCategory = $category;
@endphp

<div class="uk-container uk-container-smal mt-4 mb-4">
    <div class="row justify-content-cente mt-2 mb-3">
        <div class="col-md-2">
            <p class="cat-text">{{ !$category ? '' : $category->name }}</p>
        </div>

        <div class="col-md-6">
            <div class="form-inline">
                <label class="mr-2 text-cat-label" for="filter">Filter By</label>
                <div uk-form-custom="target: > * > span:first-child" style="background: #fff;" class="mr-3">
                    <select input-select-link>
                        <option value="">All Stores</option>
                        @foreach ($stores as $store)
                        @php
                            $build_params = [
                                'category'=> $currentCategory->slug,
                                //'q_cat'=> base64_encode($category->id),
                                'q_store'=> $store->id,
                                'q_col'=> request()->q_col,
                                'q_price_h'=> request()->q_price_h,
                                'q_price_l'=> request()->q_price_l,
                                'q_new'=> request()->q_new,
                            ];
                            $mark_selected_store = request()->q_store == $store->id ? 'selected' : '' ;
                        @endphp
                        <option value="{{ route('app:ecommerce:product_catlogue', $build_params) }}" {{ $mark_selected_store }}>{{ $store->name }}</option>
                        @endforeach
                    </select>
                    <button class="uk-button uk-button-default uk-button-small uk-text-capitalize" type="button" tabindex="-1">
                        <span></span>
                        <span uk-icon="icon: chevron-down"></span>

                    </button>
                </div>

                <div uk-form-custom="target: > * > span:first-child" style="background: #fff;" class="mr-3">
                    <select input-select-link>
                        <option value="">All Colours</option>
                        @foreach (config('ecommerce_config.colors') as $color => $value)
                        @php
                            $build_params = [
                                'category'=> isset($currentCategory) ? $currentCategory->slug : '',
                                //'q_cat'=> base64_encode($category->id),
                                'q_store'=> request()->q_store,
                                'q_col'=> base64_encode($value),
                                'q_price_h'=> request()->q_price_h,
                                'q_price_l'=> request()->q_price_l,
                                'q_new'=> request()->q_new,
                            ];

                            $mark_selected_color = base64_decode(request()->q_col) == $value ? 'selected' : '';
                        @endphp
                        <option value="{{ route('app:ecommerce:product_catlogue', $build_params) }}" {{ $mark_selected_color}}>{{ $color }}</option>
                        @endforeach
                    </select>
                    <button class="uk-button uk-button-default uk-button-small uk-text-capitalize" type="button" tabindex="-1">
                        <span></span>
                        <span uk-icon="icon: chevron-down"></span>

                    </button>
                </div>

            </div>
        </div>

        <div class="col-md-4">
            <div class="form-inline" style="float:right;">
                <label class="mr-2 text-cat-label" for="filter">Sort By</label>
                <div uk-form-custom="target: > * > span:first-child" style="background: #fff;" class="">
                    <select>
                        <option value="">All Categories</option>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <button class="uk-button uk-button-default uk-button-small uk-text-capitalize" type="button" tabindex="-1">
                        <span></span>
                        <span uk-icon="icon: chevron-down"></span>

                    </button>
                </div>
            </div>

        </div>

    </div>

    <div class="row justify-content-center" uk-height-match="target: > div > .card">
        @forelse($products as $product)
        <div class="col-md-3 mb-3">
            @include('partials.single_product', ['product'=> $product, 'selected_color'=> base64_decode(request()->q_col)])
        </div>
        @empty
        <div class="col-md-3 mb-3 uk-text-center">
            <p>No product found</p>
        </div>
        @endforelse
    </div>

    <div class="row justify-content-center mt-3">
        {{ $products->links() }}
    </div>

</div>


<div id="modal-close-default" uk-modal>
    <div class="uk-modal-dialog uk-modal-body big-dialog">
        {{-- <div class=" uk-modal-header"> --}}
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
        {{-- </div> --}}

                    <div class="row">
                        <div class="col-md-4">
                                <div class="service-box">
                                        <div class="icon-ball d-inline-block">
                                            <span class="fas fa-store"></span>
                                        </div>

                                        <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                    <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                             </div>
                        </div>
                        <div class="col-md-4">
                                <div class="service-box">
                                        <div class="icon-ball d-inline-block">
                                            <i class="fas fa-paint-brush"></i>
                                        </div>

                                        <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                        <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                             </div>
                        </div>
                        <div class="col-md-4">
                                <div class="service-box">
                                        <div class="icon-ball d-inline-block">
                                            <i class="fab fa-accessible-icon"></i>
                                        </div>

                                         <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                        <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                             </div>
                        </div>

                    </div>

        </div>
        </div>

@endsection

@push('product_details_page')

    <script>
        $(()=>{
            $(document).on('change', '[input-select-link]', (e)=>{
                var url= $(e.currentTarget).val();
                window.location.href = url;
            });
        });

    </script>

@endpush
