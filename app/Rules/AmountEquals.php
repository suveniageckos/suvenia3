<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AmountEquals implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
	 
	public $user_balance = 0;
	
    public function __construct($user_balance)
    {
        $this->user_balance = intval($user_balance);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->user_balance < intval($value) || intval($value) == 0){
			return false;
		}
		return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You do not have sufficient balance. Please enter the amount within your balance range.';
    }
}
