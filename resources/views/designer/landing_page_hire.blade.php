@extends('layouts.designer2')

@section('title', 'Become A Designer' )

@section('content')
@php $redirectUrl = url()->current(); @endphp

<div class="uk-cover-container uk-height-large">
        <img src="{{ $utils->get_image('site.designer_landing_page_banner') }}" alt="" uk-cover>
       <!-- <div class="uk-overlay-primary uk-position-cover"></div>-->
        <div class="uk-section uk-position-center">
            <div class="row uk-justify-center p-2">
                <div class="col-md-12">

                    <div class="uk-text-center">
                    <div class="n-centered-hero">
                        <b>GET AMAZING DESIGN WORK DONE FASTER!</b>
                        <p class="mt-1 n-centered-sub-hero">Earn Online with your Creative Skill.</p>
                    </div>
                    <div class="mt-4">
                    <a class="btn btn-info" style="color: #fff;" href="{{ route('app:designer:sign_up') }}">GET STARTED</a>
                    </div>
                    </div>

                </div>
            </div>
        </div>

</div>

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="borde bg-grey" style="position: relative; height:100px;">
        <div class="float-slider" style="padding: 20px 60px;" uk-slider>
                <div class="row">
                    <div class="col-12 uk-text-center">
                    <h2 class="m-0 header">Kickass designs you’ll love</h2>
                    </div>
                </div>
                <div class="row mt-3">
                        <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >

                                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                                    @foreach($categories as $category)
                                    @php $params = $category->slug . '-' . $category->ref; @endphp
                                        <li class="uk-text-center">
                                            <a href="{{ route('app:designer:catalogue', ['params'=> $params]) }}">
                                                <div class="uk-panel">
                                                    <img src="{{ asset($category->photo_url) }}" alt="" width="150" uk-responsive>

                                                </div>
                                            </a>
                                        <a href="{{ route('app:designer:catalogue', ['params'=> $params]) }}" class="_prod_title mt-2">{{ title_case($category->title) }}</a>
                                        </li>
                                    @endforeach
                                </ul>

                        </div>
                </div>
                <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
                <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
        </div>
</div>


<div class="uk-section bg-grey p-4">
        <div class="uk-container uk-container-small">
            <div class="row">
                <div class="col-12">
                    <p class="m-0 after_cat_slide_desc" style="padding:0 30px 0 30px;">Suvenia makes it easy for you to request for amazing designs for all of your personal or business needs, from our community of designers. Fast, affordable and guranteed.</p>
                </div>
            </div>
        </div>
</div>
@endif


<div class="uk-section" style="background: #fff;">
        <div class="uk-container uk-container-medium">

                 <div class="lite-bg-header uk-text-center">
                    <h1 class="header-main text-body-black mb-5">TWO WAYS TO GET YOUR PROJECT DONE!</h1>
                 </div>

                <div class="row justify-content-center mt-3" uk-grid uk-height-match="target: > div > .card_earn">
                   <div class="col-md-4">
                       <div class="card_earn pt-4 pb-3 uk-text-center">
                           <h2 class="m-0 title" style="font-weight:700; font-size:1.2em;">POST A REQUEST</h2>
                           @php
                                $category = $categories->first();
                                $paramSingle = $category->slug . '-' . $category->ref;
                            @endphp
                           <p class="mt-2 p-3 content uk-padding-small">Set up your design request by filling a short form that contain all the necessary details needed for a designer to work on your project. </p>
                           <p class="mt-1"><a href="{{ route('app:designer:post_request') }}" class="btn btn-info-dark">Post a Request</a></p>
                       </div>
                   </div>
                   <div class="col-md-4">
                        <div class="card_earn pt-4 pb-3 uk-text-center">
                            <h2 class="m-0 title" style="font-weight:700; font-size:1.2em;">FIND DESIGNER</h2>
                           <p class="mt-2 p-3 content uk-padding-small">Explore and search for designer. Choose from pool of professional designers to help with your project.</p>
                           <p class="mt-1"><a href="{{ route('app:designer:catalogue', ['params'=>  $paramSingle]) }}" class="btn btn-info-dark">Explore</a></p>
                       </div>
                   </div>
                </div>

        </div>
</div>




<div class="uk-section" style="background: #fff;">
    <div class="uk-container uk-container-medium">

             <div class="lite-bg-header uk-text-center">
                <h1 class="header-main text-body-black mb-5">SOME PROJECTS MADE BY OUR DESIGNERS</h1>
             </div>

             <div class="uk-child-width-1-5@m uk-grid-small uk-grid-match" uk-grid>

                @php
                // $designer_jobs = \App\DesignerOrder::where('is_completed', 1)->first()->limit(5);

                // $_in_season = \App\Product::with(['user','categories'=>function($q){
                //     $q->where('parent_id', '!=', null);
                // }, 'photos'])->where('user_type', 'SELLER')->whereIn('id', explode(",", $product_block->product_ids))->get();

                @endphp

                <div>
                        <div class="uk-card uk-card-default uk-border-rounded">

                            <div class="uk-card-media-top">
                                    {{-- <img src="{{asset("img/dodo_gang.png")}}" alt=""> --}}
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2tgeRjqSG63pd2ZiiPUi814IwvsDNSxo3158OSw6THxaPOsIqKg" style="width: 100%;" alt="">
                            </div>

                            <div class="uk-card-footer">
                                    <div class="uk-grid-small uk-flex-middle" uk-grid>
                                        <div class="uk-width-auto uk-padding-remove-left">
                                            {{-- <img class="uk-border-circle" width="40" height="40" src="{{asset("img/dodo_gang.png")}}"> --}}
                                            <img class="uk-border-circle" width="40" height="40" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHsAAAB7CAMAAABjGQ9NAAAAPFBMVEWVu9////+QuN6Ltdzy9vudwOH4+v3s8vnm7/fi7PbH2u3S4fGZvuCzzeeiw+PN3u+60emsyeXB1uzb5/MK2cn9AAADd0lEQVRoge2b25KrIBBFtUFFAiry//96JCaTq7rbNEmdKvfD1EweZgXobvqiRXHo0KFDDyKiwlVVV1Xu/PsXwS70baPrJN20fXBfwhMNbfmsdvgCnZStX8hJtVWZ6cq/J5/pXuVE02mRnHTKufLXg3469mxk2kJP8EwrV+sbftn2LGdOA4AuS59j5QShyzIDe8PE73ZdHo4uuyzl0RZmW+mFk4bZWprdweiy7GTR1DPYvezCgZB2k3Bwc/hxTwfuRNkVA12WlSibY2rCxkaBxQ6SBw7eI1cNB/s/Z3sW2wuif+pjP40tbrkkeFUtG1OLhsFuZNG/vMeg3Pwq4Ryd5eCi7l3wDF3WzKeF44Zei+epEWZHcfYIs0f5wgRmC0eWSQrNFrV8FQwXReIl0SQHsqU9LEkZCG2yNB6w/EE2b7hKIXdZk6fHBiXpoqn5Pfx3PS4kaxMuve+0GVhz+PZVG36Wx7/+4GuRNUM0fdQyXGcmT1radpOVOhvSQoPx0k7MYmxE3SUpUOE1wDVhPmsaO/HBCaluCiz9BaD848Ybf5mVqH4KL53k4ISUs7OJRXX9hIa+1XVZ67Yf6ApTc1KnrZPBk6rG2x43fwdKk5RSdJvM0V3h1IzVx3gqhie7XhqE0XP5YAb3CZ2KN8MwE97QicKr79V2v9lT9z6QGF884Kc//Huv191O+Eqjo45jNx/29LMb43LRsm98stljqbVpjd6qlXbBK06rYeUb7shbFafaX1PLv914HdQ1hZ8te8fC0SoEEbM4ZM1HtsScn8BVJyJmLsXrI26J5WaMLgMiVieC0V1BxOvASB43M4OV9LAkhpcxW/XbYlwoot6dxPBwwYA6ixNWOd1yRIyOutDVfRPjEudNZhDBPQHmNAwR3FIXN3OGoYubOcPQoUYaT3jbTTaaJ8ERXdzFGE4m72K4k8mlxzeBibL4LZYE3mTCCdMsMG1iPLKFC+x2ZghrcGCDn9DjCHyaT8kmqbMiFth+uefgUIYndISTYdPx6oAG4dqAM42nIqz0jniqY2C22Ug5Hz9fvY5+V2+VVBHsJ2lEY0PxQVf1zMcGoY8y1n/CvfFV5U94DteefKUEe+ipVe28jWZ1TmSi9Y6yvG9y/q+u8/YUTXN5vSS9YNKYeLK+c0Ue7ONXSF3c9FpNeq/m/GLN+YNDhw4d+tM/VBUnSQQKp/gAAAAASUVORK5CYII=">
                                        </div>
                                        <div class="uk-width-expand uk-padding-remove-right">
                                            <h3 class="uk-card-title uk-margin-remove-bottom" style="font-size:14px; font-weight:700;">Art Work</h3>
                                            <p class="uk-text-meta uk-margin-remove-top" style="font-size:12px;">by Chidiebere</p>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>




            </div>
    </div>
</div>






<div class="uk-section bg-app">
    <div class="uk-container uk-container-small">
            <div class="bg-banner-header uk-text-center mb-5">
                    <h1 class="m-0 text-body-white">FAQ</h1>
                </div>
        <div class="row justify-content-center">
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content1" aria-expanded="false" aria-controls="faq-collapse-content1">
                        <div class="d-flex justify-content-between">
                            <div>What can Suvenia.com do for me?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content1">
                        <p class="faqs_text text-muted style="text-align:justify;">Suvenia.com allows you to discover, create, buy and sell personlaized items such as apparels, drinkwares, hats and accessories. If you are looking for any specific item category, and do not find, do not hesitate to contact us, as we would often go out of our way to make it available for you. Our goal is to make Suvenia.com a platform that allows you create products at the speed of thought.</p>
                       <p  class="faqs_text text-muted" style="text-align:justify;">You can buy products from the marketplace simply by searching through the site, create your own desisgns and order when you don't find what you like or sell ideas and designs on products if you are a creative individual or brand. You can also search for your favorite stores directly to find products you love.</p>
                </div>

            </div>
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content2" aria-expanded="false" aria-controls="faq-collapse-content2">
                        <div class="d-flex justify-content-between">
                            <div>What are the design specifications you accept?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content2">
                    <p  class="faqs_text text-muted" style="text-align:justify;">All product design specifications are listed on the product customizer page. You can use our customizer using JPEG, PNG or SVG files with the sizes indicated. If you are unable to edit your design to fit our specification, please kindly chat with us in the live chat icon page. We are able to accept Adobe Illustrator files (vector art with paths) in an AI or EPS format with all fonts converted to outlines. We will also accept vector PDFs and Jpeg for orders placed off the platform. We would contact you if there are any complexities concerning your submitted artwork that require modification.</p>
                </div>

            </div>
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content3" aria-expanded="false" aria-controls="faq-collapse-content3">
                        <div class="d-flex justify-content-between">
                            <div>What kind of product quality can I expect?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content3">
                    <p class="faqs_text text-muted" style="text-align:justify;">All products are made in Nigeria, and sourced from the highest grade suppliers and factories. We also ensure that all products are made ethically and in compliance with environmental laws.</p>
                </div>

            </div>
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content4" aria-expanded="false" aria-controls="faq-collapse-content4">
                        <div class="d-flex justify-content-between">
                            <div>What print quality can I expect?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content4">
                    <p class="faqs_text text-muted" style="text-align:justify;">Except explicitly stated otherwise, we use screen printing process for apparels. Learn more about screen printing process <a href="https://www.youtube.com/watch?v=Aru1wZEKrkE" target="_blank">here</a>. For hats, we use either <a href="https://www.youtube.com/watch?v=d6mqndD9xok" target="_blank">vinyl printing</a> or <a href="https://www.youtube.com/watch?v=CcYC_7rqyN0" target="_blank">embroidery</a>. For accessories & drinkwares, we use a digital printing process. Please factor this in when also creating or uploading your designs.</p>
                </div>

            </div>

        </div>
    </div>
</div>




<div class="uk-section" style="background: #fff;">
    <div class=" uk-text-center">
     <h1 class="header-main text-body-black mb-5">Customer Feedback</h1>
    </div>
    <div class=" uk-margin-large-top" uk-slider >
            <div>
                <div class=" uk-container uk-container-small uk-slider-container">
                    <ul class="uk-slider-items uk-child-width-1-1@m uk-grid">

                        <li>
                            <div class="uk-card uk-card-default">
                                <div class="uk-card-body p-2">
                                <div class="d-flex">
                                    <div>
                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2tgeRjqSG63pd2ZiiPUi814IwvsDNSxo3158OSw6THxaPOsIqKg" class="uk-border-circle uk-padding-small" alt="" width="200">
                                    </div>
                                    <div>
                                        <p class="ml-3 mt-3 store_text_content">Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua labore et dolore magna aliqua.</p>
                                        <p class="ml-3 mt-0" style="font-weight:600;">Chidiebere Ezeka</p>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </li>
                        <li>
                                <div class="uk-card uk-card-default">
                                    <div class="uk-card-body p-2">
                                    <div class="d-flex">
                                        <div>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2tgeRjqSG63pd2ZiiPUi814IwvsDNSxo3158OSw6THxaPOsIqKg" class="uk-border-circle uk-padding-small" alt="" width="200">
                                        </div>
                                        <div>
                                            <p class="ml-3 mt-3 store_text_content">Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua labore et dolore magna aliqua.</p>
                                            <p class="ml-3 mt-0" style="font-weight:600;">Chidiebere Ezeka</p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </li>
                    </ul>


                </div>
            </div>

             <div class=" uk-margin-medium-top">
               <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
             </div>
        </div>

</div>


<div class="uk-section">
    <div class="uk-container uk-container-small">
            <div class="lite-bg-header uk-text-center">
                <h1 class="header-main text-body-black mb-0">Find Freelance Services For Your Business Today</h1>
               <p class="mb-0" style="font-size:1.2em;font-weight: normal;font-style: normal;font-stretch: normal;line-height: normal;letter-spacing: normal;">Suvenia got you covered for all your needs</p>
            </div>
            <div class="row mt-4">


                    <div class="col-md-12 col-12 uk-text-center">
                            <a class="btn btn-info" style="color: #fff;font-weight: normal;font-style: normal;font-stretch: normal;line-height: normal;letter-spacing: normal;" href="{{ route('app:designer:sign_up') }}">Get Started</a>
                    </div>

            </div>

    </div>
</div>
{{--
<div class="uk-section">
    <div class="uk-container uk-container-small">
            <div class="lite-bg-header uk-text-center">
               <h1 class="m-0 header-main">Our freelancers have done awesome work for many great companies.</h1>
            </div>
            <div class="row mt-4">
                @php
                    $companies = [
                        'https://www.freelogodesign.org/Content/img/logo-ex-7.png',
                        'https://www.freelogodesign.org/Content/img/logo-ex-7.png',
                        'https://www.freelogodesign.org/Content/img/logo-ex-7.png',
                        'https://www.freelogodesign.org/Content/img/logo-ex-7.png'
                        // 'https://ya-webdesign.com/images/adidas-logo-png-4.png',
                        // 'https://ya-webdesign.com/images/adidas-logo-png-4.png',
                        // 'https://ya-webdesign.com/images/adidas-logo-png-4.png'

                    ];
                @endphp
                @foreach ($companies as $item)
                    <div class="col-md-3 col-6 uk-text-center">
                    <img src="{{ $item }}" uk-responsive width="100">
                    </div>
                @endforeach
            </div>

    </div>
</div> --}}


@endsection
