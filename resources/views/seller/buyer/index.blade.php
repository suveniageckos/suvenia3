@extends('layouts.dashboard')

@section('title', 'Dashboard' )

@section('content')
 
<div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-normal mb-2">54</span>
                            <span class="font-weight-light">Total Users</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="icon icon-people"></i>
                        </div> 
                    </div> 
                </div>
            </div> 

            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-normal mb-2">$32,400</span>
                            <span class="font-weight-light">Income</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="icon icon-wallet"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-normal mb-2">900</span>
                            <span class="font-weight-light">Downloads</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="icon icon-cloud-download"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-normal mb-2">32s</span>
                            <span class="font-weight-light">Time</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="icon icon-clock"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-lite">
                        <h4 class="header_text m-0">Overview</h4>
                    </div>

                    <div class="card-body p-0">
                            
                        <div class="p-4">
                            <canvas id="bar-chart" width="100%" height="50"></canvas>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        
</div>

@endsection