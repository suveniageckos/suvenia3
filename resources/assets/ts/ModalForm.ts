class ModalForm {
    constructor() {
        this.set_ajax();
        this.add_ship_modal();
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    public add_ship_modal(){
        let cls = this;
        $(document).on('click', '[modal-form]', function(e){
            e.preventDefault();
            let title = $(e.currentTarget).attr("data-title");
            $.ajax({
                url:  $(e.currentTarget).attr('data-url'),
                type: 'POST',
                data: {},
                beforeSend: function(){
                   // $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    //console.log(data);
                    $('body').prepend(
                        '<div id="modalForm" uk-modal="bg-close:false;"><div class="uk-modal-dialog uk-modal-body p-0">'+
                        '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                        '<h3 class="uk-modal-title mb-2 border-bottom p-3" style="color: #000; font-size: 16px; font-weight: 600;">'+title+'</h2>'+
                        '<div class="p-3" style="color: #333; font-size: 17px;">'+data+'</div>'+
                        '</div></div>'
                    );
                    UIkit.modal("#modalForm").show();
                }
            });

        });
    }

}

$(function(){
    let ___ship_modal = new ModalForm();
})
