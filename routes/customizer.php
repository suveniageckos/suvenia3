<?php
Route::group(['prefix' => 'customize', 'middleware' => ['auth.buyer.seller']], function () {
    Route::match(['post', 'get'], '/start-design', 'BrandableCtrl@product_list')->name('app:ecommerce:product_list');
    Route::match(['post', 'get'], '/design', 'BrandableCtrl@design')->name('app:ecommerce:design');
    Route::match(['post', 'get'], '/design/upload-image', 'BrandableCtrl@upload_design')->name('app:ecommerce:upload_design');
    Route::match(['post', 'get'], '/design/fetch-arts', 'BrandableCtrl@fetch_arts')->name('app:ecommerce:fetch_arts');
    Route::match(['post', 'get'], '/design/fetch-templates', 'BrandableCtrl@fetch_templates')->name('app:ecommerce:fetch_templates');
    Route::match(['post', 'get'], '/success/{type}', 'BrandableCtrl@design_success')->name('app:ecommerce:design_success');
    Route::match(['post', 'get'], '/design/sell', 'BrandableCtrl@sell_design')->name('app:ecommerce:sell_design');
    Route::match(['post', 'get'], '/design/buy', 'BrandableCtrl@buy_design')->name('app:ecommerce:buy_design');
    Route::match(['post', 'get'], '/design/process/{id}/{type}', 'BrandableCtrl@process_design')->name('app:ecommerce:process_design');
}); 