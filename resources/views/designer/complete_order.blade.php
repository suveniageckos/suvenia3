@extends('layouts.designer')

@section('title', 'Complete Order')

@section('content')
<div class="uk-container uk-container-small mt-3 mb-3">
        <div class="row justify-content-center mb-2">
            <div class="col-8">
                <h2 class="des_buy_pg_title m-0">Post A Design Request</h2>
            </div>
        </div>
        <div class="row justify-content-center mb-2">
                <div class="col-md-8">
                        <div class="bg-white mb-3 shadowed-box">
                            <form class="app-form des-form" method="POST" action="{{ route('app:designer:complete_order', ['pid'=> request()->pid]) }}" data-post>
                            <div class="row no-gutters border-bottom-pay p-2">
                                <div class="form-group w-100 title">
                                        <label>Choose a name for your request</label>
                                        <input type="text" name="title" class="form-control" placeholder="E.g Create Logo">
                                        <div class="help-block"></div>
                                </div>
                            </div> 
                            <div class="row no-gutters border-bottom-pa p-2">
                                <div class="form-group w-100 description">
                                        <label>Describe your request <span>(Include every neccesary details needed to complete your request)</span></label>
                                        <textarea name="description" class="form-control" placeholder="Give a brief summary of your project" cols="30" rows="5"></textarea>
                                        <div class="help-block"></div>
                                </div>
                                <div class="col-md-12 form-group request_file border-bottom pb-2">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <div class="js-upload" uk-form-custom>
                                                    <input type="file" multiple name="request_file">
                                                    <button class="btn btn-info btn-sm" type="button" tabindex="-1"><i class="icon-add"></i> Attach File</button>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <span class="text-muted" style="font-size:12px;">(Attach any images or documents that might be helpful in explaining you brief.)</span>
                                        </div>
                                    </div>
                                         
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group w-100">
                                    <button class="btn btn-info btn-sm">POST REQUEST</button>
                                </div>
                            </div>
                            </form> 
                                
                        </div>
                </div>
               
        </div>
</div>

@endsection