<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMerchStoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('merch_stores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name', 191)->unique();
			$table->string('slug', 191);
			$table->string('ref', 191)->unique();
			$table->text('store_url', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('facebook', 191)->nullable();
			$table->string('twitter', 191)->nullable();
			$table->string('youtube', 191)->nullable();
			$table->string('instagram', 191)->nullable();
			$table->string('website', 191)->nullable();
			$table->boolean('is_published')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->text('meta', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('merch_stores');
	}

}
