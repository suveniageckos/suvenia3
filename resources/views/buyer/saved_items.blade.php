@extends('layouts.dashboard')

@section('title', 'Saved Items' )


@section('content')

<div class="row justify-content-center">
        <div class="col-12">

            <table class="uk-table uk-table-small uk-table-divide _saved_item_table mt-4">
                <thead>
                    <tr>
                        <th>PRODUCTS</th>
                        <th>PRICE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="bg-white">
                    @forelse($products as $product)
                    <tr class="ml-4 border-bottom">

                        <td>
                            @php
                            $design_colors = json_decode(json_decode($product->product->design->colors));
                            $get_color = collect($design_colors)->random();
                            @endphp

                                <div class="media">
                                        <img src="{{ asset('storage/'.$product->product->photos->first()->public_url) }}" alt="" class="mr-2" width="100" height="130" style="background: {{ $get_color }}">
                                        <div class="media-body">
                                            <p class="saved_item_prod_name m-0 mb-0 uk-text-capitalize">{{ $product->product->name }}</p>
                                            <!--<p class="saved_item_prod_seller m-0">Seller: Frankie</p>-->

                                            <ul class="list-unstyled mt-2">

                                                <li class="d-inline-block">
                                                    <a class="cart_delete_order_link data-delete-item" href="javascript:;" data-url="{{ route('app:dashboard:delete_saved_items', ['hash'=> $product->id]) }}" ><span class="icon-delete"></span> Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                        </td>
                        <td>
                                @php $product_price = \App\Order::where('id', $product->product->id)->first(); @endphp
                                <p class="m-0 saved_item_price">&#x20a6; {{ number_format($product_price->price, 2) }}</p>
                        </td>
                        <td>
                                @php $__cat = $product->product->categories->first(); @endphp
                            <a href="{{ route('app:ecommerce:single_product', ['category'=> $__cat->slug, 'param'=> $product->product->slug . '_' . $product->product->ref]) }}" class="btn btn-info btn-sm" target="_blank">BUY NOW</a>
                        </td>
                    </tr>
                    @empty
                    <tr><td colspan="3" class="uk-text-center"><p class="m-1">You have not saved any products</p></td></tr>
                   @endforelse
                </tbody>
            </table>

        </div>
</div>

@endsection
