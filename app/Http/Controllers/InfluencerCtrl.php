<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Influencer;
use App\Mail\VerifyEmail;
use App\ShippingLocation;
use App\Mail\ForgotPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repo\User\Auth as AuthGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Influencer\Apply;
use App\Http\Requests\AuthResetPassword;
use App\Http\Requests\Influencer\SignUp;
use App\Http\Requests\AuthForgotPassword;
use App\Repository\Influencer\InfluencerInterface;

class InfluencerCtrl extends Controller
{
    public $user;

    public $influencer;

    public function __construct(InfluencerInterface $influencer){
        $this->user = new AuthGuard('@INFLUENCER');
        $this->influencer = $influencer;
    }

    public function landing_page(){
        //Auth::guard('influencers')->logout();
        $service_grids = DB::table('display_block')->where('block_code', 'ID:INFLUENCER:SERVICE')->get();
        $influencers = Influencer::with(['socials'=>function($q){
            $q->with('network')->where('is_default', true)->first();
        }])->where('is_approved', true)->limit(4)->get();

        return view('influencer.landing_page', ['service_grids'=>$service_grids, 'influencers'=> $influencers]);
    }

    /**
     * Process apply Request Form
     *
     * @param Apply $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function process_apply(Apply $request){
        if ($request->isMethod('post')) {
            $data = $request->validated();
            $model = $this->influencer->influencer_apply($data);
            Mail::to($model->email)->later(\Carbon\Carbon::now()->addSeconds(2), new VerifyEmail($model, 'INFLUENCER'));
            return redirect()->route('app:influencer:apply_success', ['pid'=> encrypt($model->ref)]);
        }
    }

    /**
     * displays apply form
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function apply(){
        return view('influencer.apply', []);
    }

    public function apply_success(Request $request){
        if(!$request->pid){
            return abort(404, 'Missing success token!');
        }
        $influencer = Influencer::where('ref', decrypt($request->pid))->first();
        if(!$influencer){
            return abort(404, 'Missing success token!');
        }
        return view('influencer.apply_success', []);
    }

    /**
     * Process password Request Form
     *
     * @param AuthForgotPassword $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function process_sign_up(SignUp $request, $code){
        if($request->isMethod('post')){
            $data = $request->validated();
            $model = $this->influencer->sign_up($data);
            return redirect()->route('app:influencer:login')->with('success', 'Your influencer account is now active, you may now login.');
        }
    }

    /**
     * displays the signup page
     *
     * @param Request $request
     * @param string $code
     * @return Illuminate\Http\Response
     */
    public function sign_up(Request $request, $code){
        $states = ShippingLocation::get();
        return view('influencer.sign_up', ['code'=> $code, 'states'=> $states]);
    }

    public function login(Request $request){

        if ($request->isMethod('post')){
           $login = $this->user->login();
           $login->enter();
           if($login->status){
               return redirect()->route('app:influencer:dashboard:overview')->with('success', $login->message);
           }else{
               return back()->withInput()->with('error', $login->message);
           }
        }
         return view('influencer.login');
    }


/**
 * Process password Request Form
 *
 * @param AuthForgotPassword $request
 * @return Illuminate\Http\RedirectResponse
 */
public function request_forgot_password(AuthForgotPassword $request)
{
    if($request->isMethod('post')){
        $data = $request->validated();
        $model = $this->influencer->forgot_password($data);
        Mail::to($model->email)->later(\Carbon\Carbon::now()->addSeconds(2), new ForgotPassword($model, 'INFLUENCER'));
        return redirect()->route('app:influencer:login')->with('success', 'A password reset link has been successfully sent to your email address.!');

    }
}

/**
 * displays password request form
 *
 * @param Request $request
 * @return Illuminate\Http\Response
 */
public function forgot_password(Request $request)
{
    return view('influencer.forgot_password');
}


/**
 * Process password Request Form
 *
 * @param AuthResetPassword $request
 * @return Illuminate\Http\RedirectResponse
 */
public function request_reset_password(AuthResetPassword $request, $token)
{
    if($request->isMethod('post')){
        $data = $request->validated();
        $model = $this->influencer->reset_password($data);
        if(!$model){
            return redirect()->route('app:influencer:login')->with('error', 'Invalid or Expired Token. Please resend password reset link again');
        }else{
            return redirect()->route('app:influencer:login')->with('success', 'You have updated your password successfully, You can now login!');
        }
    }
}

/**
 * displays password reset form
 *
 * @param Request $request
 * @return Illuminate\Http\Response
 */

public function reset_password(Request $request, $token)
{
    return view('influencer.reset_password', ['token'=> $token]);
}

/**
 * Logs Influencer out of the infuencer platform
 *
 * @param Request $request
 * @return void
 */
public function logout(Request $request){
    if ($request->isMethod('post')){
        $this->user->logout('app:influencer:landing_page');
        return redirect()->route('app:influencer:landing_page')->with('success', 'You have logged out successfully!');
        }
}


}
