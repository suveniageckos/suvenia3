<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{

    protected $table = 'eco_buyer_addresses';

    public function user()
    {
        return $this->belongsTo("App\User");
    }
    public function location()
    {
        return $this->belongsTo("App\ShippingLocation");
    }
    public function zone()
    {
        return $this->belongsTo("App\ShippingZone", "zone_id");
    }
}
