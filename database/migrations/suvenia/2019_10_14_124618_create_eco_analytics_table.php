<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoAnalyticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_analytics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('client_id', 191)->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('model_id')->nullable();
			$table->string('category', 225)->nullable();
			$table->string('device', 191);
			$table->string('platform', 191);
			$table->string('browser', 191);
			$table->boolean('is_mobile');
			$table->string('lang', 191);
			$table->text('country', 65535)->nullable();
			$table->string('path', 191);
			$table->string('page_name', 191);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_analytics');
	}

}
