<?php
namespace App\Helpers\Social;

class Instagram{
    static public function Init($config){
		$provider = new \League\OAuth2\Client\Provider\Instagram([
			'clientId'          => config('user_config.instagram.client_id'),
			'clientSecret'      => config('user_config.instagram.client_secret'),
			'redirectUri'       => config(config($config['redirect_url'])),
			'graphApiVersion'   => 'v2.10',
		]);

		return $provider;
	}

	static public function driver($request, $config){
		$provider = self::Init($config);
		if(is_null($request->code) || !isset($request->code)){
			
			$authUrl = $provider->getAuthorizationUrl([
				//'scope' => ['email'],
			]);
			$request->session()->put('INSTAGRAM_AUTH_STATE', $provider->getState());
			//$this->authRedirectUrl = $authUrl;
			return redirect($authUrl);
			
		}elseif(!is_null($request->state) || ($request->state !== $request->session()->get('INSTAGRAM_AUTH_STATE'))){
			$request->session()->forget('INSTAGRAM_AUTH_STATE');
			return redirect()->route('home')->with('error', 'Error loggin in with instagram, please try again.');
		}

	}

	static public function authenticate($request){
		$provider = self::Init();
		if(!is_null($request->code) || isset($request->code)){
            $token = $provider->getAccessToken('authorization_code', [
				'code' => $request->code
			]);
			$client = new \GuzzleHttp\Client();
			$res = $client->request('GET', 'https://api.instagram.com/v1/users/self', [
				'query' => [
					'access_token'=> $token->getToken()
				]
			]);

			$analytics = $client->request('GET', 'https://api.instagram.com/v1/users/self/media/recent', [
				'query' => [
					'access_token'=> $token->getToken()
				]
			]);

			$dat = json_decode($res->getBody()->getContents(), false);
			$data = $dat->data;

			$ana = json_decode($analytics->getBody()->getContents(), false);
			$commentsCount = 0;
			$likeCount = 0;
			
			foreach($ana->data as $an){
				$commentsCount = $an->comments->count;
				$likeCount = $an->likes->count;
			}

			$userData = [ 
				'id'=> $data->id,
				'email'=> '',
				'username'=> $data->username,
				'firstname'=> $data->full_name,
				'lastname'=> $data->username,
				'profile_url'=> 'https://www.instagram.com/' . $data->username,
				'photo_url'=> $data->profile_picture,
				'bio'=> $data->bio,
				'following'=> $data->counts->follows,
				'followers'=> $data->counts->followed_by,
				'total_posts'=> $data->counts->media,
				'comments'=> $commentsCount,
				'likes'=> $likeCount,
			  ];

			return json_decode(json_encode($userData), false);

		}
	}

}