@extends('layouts.dashboard_designer')

@section('title', 'My Gigs' )

@section('content') 

        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                    
                        <div class="card-header card-header-info">
                                <div class="row">
                                    <div class="col-md-4">
                                            <div><h4 class="header_text m-0">My Gigs</h4></div>
                                    </div>
                                    
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4 uk-text-right">
                                            <a href="{{ route('app:designer:dashboard:add_gig') }}" class="btn app-btn-info btn-sm">CREATE A NEW GIG</a>
                                    </div>
                                </div>
                            </div> 

                    <div class="card-body">
                     
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead> 
                                <tr>
                                    <th>NAME</th>
                                    <th>ORDERS</th>
                                    <th></th>
                                </tr> 
                                </thead>
                                <tbody>
                                    @foreach($gigs as $gig)
                                    <tr>
                                    <td>{{ title_case($gig->title) }}</td>
                                    <td>{{ $gig->orders->count() }}</td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <li><a href="{{ route('app:designer:dashboard:edit_gig', ['id'=> $gig->id]) }}" class="d-inline-block">Edit</a></li>
                                            <li><a href="" class="d-inline-block" data-prompt url="{{ route('app:designer:dashboard:delete_gigs', ['id'=> $gig->id]) }}" title="Delete Gig" message="Are you sure you want to delete this Gig?<br> Note this action is not reversible! click <b>YES</b> to proceed">Delete</a></li>
                                        </ul>
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table> 
                        </div>

                       {{ $gigs->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection