<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrderCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Payment;

class ConfirmPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrderCompleted  $event
     * @return void
     */
    public function handle(ProductOrderCompleted $event)
    {
            $order = $event->order;
            $payment = Payment::where('reference', $order->order_ref)->first();
            $payment->status = 2;
            $payment->save();
    }
}
