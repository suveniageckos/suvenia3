<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrderCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifySite
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrderCompleted  $event
     * @return void
     */
    public function handle(ProductOrderCompleted $event)
    {
        //
    } 
}
