<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignerRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designer_requests', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('user_id');
			$table->string('user_type', 225)->nullable();
			$table->bigInteger('gig_id')->nullable();
			$table->integer('plan_id')->nullable();
			$table->string('title', 191);
			$table->float('budget');
			$table->text('description', 65535);
			$table->timestamp('deadline')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('category_id')->nullable();
			$table->integer('duration')->default(1);
			$table->text('photo_url', 65535)->nullable();
			$table->text('image_path', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designer_requests');
	}

}
