import Dropzone = require("dropzone");

Dropzone.autoDiscover = false;

declare const ClassicEditor: any;
declare const instance: any;

class DesignerGig{
    public constructor(){
        this.set_steps();
        this.setUploader();
    }

    public set_steps(){
        (<any>$('#add-edit-gig')).steps({
            headerTag: "h6",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                finish: "PUBLISH",
                next: "SAVE & CONTINUE",
                previous: "PREVIOUS",
                loading: "Loading ..."
            },
            onFinished: function (event: any, currentIndex: any) { 
                $('#newGigForm').submit();
            },
        }); 
       // alert('hey'); 
    } 

    public setUploader(){
        if($('#appEditor2').length || $('#appEditor2').length){
       var myDropzone = new Dropzone("#drop-area", { 
           url: $('#drop-area').attr('upl-url'),
           paramName: "app_files", // The name that will be used to transfer the file
           maxFilesize: 2,
           maxFiles: 4,
           headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           acceptedFiles: 'image/*',
           addRemoveLinks: true,
           thumbnail:function(file: any){
            if($('#fileLister > li').length > 3){
                toastr.error("You cannot uploaad more than 4 files");
                (<any>myDropzone).removeFile(file);
            }else{
                $('#fileLister').prepend(
                    '<li class="d-block border-bottom p-3"><img width="150" src="'+file.dataURL+'" uk-responsive><textarea name="app_files[]" style="display:none;">'+file.dataURL+'</textarea><p class="m-0 image-gallery" style="font-size: 12px; color: red; cursor: pointer;">remove</p></li>'
                ); 
                (<any>myDropzone).removeFile(file);
            }
            
           }
        });
        
        $(document).on('click', '.image-gallery', (evt)=>{
            let target = $(evt.currentTarget);
            target.parent().remove();
        });
        }
        
    }
}

$(function(){
    const __designer_gig = new DesignerGig();
});