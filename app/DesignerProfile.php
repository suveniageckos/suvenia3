<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerProfile extends Model
{
    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
