<div class="row">
    <div class="col-md-5">
        <div class="d-flex justify-content-between">
            <div>
                <img src="{{ asset('storage/'.$gig->product->photos->first()->public_url) }}" alt="" width="100">
            </div>
            <div>
                <p class="font-weight-bold m-0 text-dark" style="font-size:14px;">{{ title_case($gig->product->name) }}</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4 class="m-0" style="font-size: 14px;">Description</h4>
    </div>
    <div class="col-md-12">
        <div class="mt-1">
                <p class="text-muted m-0" style="font-size:14px;">{{ $gig->description}}</p>
        </div>
    </div>
</div>

@if($gig->content_link)
<div class="row">
    <h4 class="m-0">Content Link</h4>
    <div class="col-md-12">
        <div class="mt-1">
                <a class="text-primary m-0" style="font-size:14px;" href="{{ $gig->content_link}}">{{ $gig->content_link}}</a>
        </div>
    </div>
</div>
@endif

<div class="d-flex justify-content-between mt-4">
        <div>
            {{-- <a href="javascript:;" class="text-primary font-weight-bold uk-modal-close">Reject</a> --}}
            <a href="{{ route('app:influencer:dashboard:reject_gig', ['id'=> $gig->id]) }}" class="text-primary font-weight-bold post-link">Reject</a>
        </div>
        <div>
        <a href="{{ route('app:influencer:dashboard:accept_gig', ['id'=> $gig->id]) }}" class="btn btn-info rounded-pill post-link">Accept</a>
        </div>
</div>
