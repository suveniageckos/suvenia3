
<div class="p-2 cart_shipping_zone_header_bottom">
        <p class="__add_address_link m-0"><a href="" class="__ship_add_address_def" data-url="{{ route('app:dashboard:add_address_zone_modal', ['id'=> $id])}}">ADD NEW ADDRESS</a></p>
    </div>
    <p class="__post_header_address_modal m-3">Saved Addresses</p>
        <form class="data-post mt-1" method="POST" action="{{ route('app:dashboard:select_shipping_address', ['zone_id'=> $id ]) }}" data-post>
            @csrf
        <div class="row no-gutters">
            @foreach ($addresses as $address)
            @php $makeDefault = $address->is_default ? 'checked' : ''; @endphp
            <div class="col-12 shipping_border-righ p-3 cart_ship_border_bottom">
                <div class="row no-gutters">
                    <div class="col-1 d-table uk-text-righ p-1">
                        <div class="custom-control custom-radio align-middle">
                        <input type="radio" value="{{ $address->id }}" name="shipping_address" id="shipping_radio_{{ $address->id }}" class="custom-control-input" {{ $makeDefault }}>
                        <label class="custom-control-label" for="shipping_radio_{{ $address->id }}"></label>
                        </div>
                    </div>
                    <div class="col-7">
                            <p class="m-1 ml-3 cart-address-header">{{ $address->firstname }} {{ $address->lastname }}</p>
                    <p class="m-1 cart-address-text">{{ $address->address }}</p>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                        <li class="d-inline"><a href="{{ route('app:dashboard: edit_address_zone_modal', ['id'=> $id, 'address_id'=> $address->id]) }}" class="_ship_action_edit __ship_edit_trigger"><span class="icon-edit"></span> Edit</a></li>
                            <li class="d-inline ml-2"><a href="{{ route('app:dashboard:delete_shipping_address', ['id'=> $address->id]) }}" class="_ship_action_delete"><span class="icon-delete __ship_delete_trigger"></span> Delete</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-12 shipping_border-righ p-3 uk-text-center">
                    <button type="submit" class="btn btn-info">USE THIS ADDRESS</button>
            </div>
        </div>
        </form>

