<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoBrandablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_brandables', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name', 191)->nullable();
			$table->text('description', 65535)->nullable();
			$table->integer('price')->nullable();
			$table->text('data', 65535)->nullable();
			$table->text('front_image', 65535)->nullable();
			$table->text('back_image', 65535)->nullable();
			$table->text('front_image_path', 65535)->nullable();
			$table->text('back_image_path', 65535)->nullable();
			$table->timestamps();
			$table->text('display_photo', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_brandables');
	}

}
