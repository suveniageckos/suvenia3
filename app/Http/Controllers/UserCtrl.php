<?php
namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Mail\NewEmailVerify;
use App\Mail\ResendEmailVerify;
use App\Mail\VerifyEmail;
use App\Rules\PasswordExists;
//use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Buyer as _User;
use App\Product as Product;
use App\Order as Order;
use App\ShippingZone as Zone;
use App\ShippingAddress as Shipping;
use App\ShippingLocation as Location;
use App\OrderItem as OrderItem;
use App\OrderShipping as OrderShipping; 
use App\SavedItem as SavedItem;
use App\Payment as Payment;
use App\Review as Review; 
use App\Repo\User\Auth as AuthGuard;

class UserCtrl extends Controller
{

    public $user;

    public function __construct(){
        $this->user = new AuthGuard();
    }

    /**
     * handles the index action
     *
     * @param Request $request
     * @return void
     */

    public function index(Request $request)
    {
        // Auth::logout();
        //for unified login experience
        return view('user.index');
    }

    /**
     * Registers new user and dispatches the on user sign up event
     *
     * @param Request $request
     * @return void
     */

    public function register(Request $request)
    {
        if ($request->isMethod('post')) {
            $role = DB::table('roles')->where('name', 'user')->first();

            $data = $request->all();

            $validator = Validator::make($data, [
                'username' => 'required|max:255|min:4|unique:buyers',
                'password' => 'required|min:4',
                'confirm_password' => 'required|min:4|same:password',
                'email' => 'required|min:4|email|unique:buyers',
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user = new _User();
                $data = $request->all(); 

                $user->username = $data['username'];
                $user->name = $data['username'];
                $user->ref = str_random(10);
                $user->slug = str_slug($data['username'], '-');
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->has_password = true;
                $user->email_token = str_random(36);
                $user->email_token_expiry = \Carbon\Carbon::now()->addDay(1);

                if ($user->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new VerifyEmail($user, 'USER'));
                    //dispatch onUserSignUp() event
                    return redirect()->route('app:base:index')->with('success', 'Your registration was successful! A verification link has been sent to your mail, follow the link to verify your email address.');
                }

            }

        }
        return view('user.register');
    }

    /**
     * Handles the user login and dispatches the @method mixed OnUserLogin() events
     *
     * @param Request $request
     * @return void
     */

    public function login(Request $request)
    {
        if ($request->isMethod('post')){
            $login = $this->user->login();
            $login->enter();
            if($login->status){
                return redirect()->route('app:base:index')->with('success', $login->message);
            }else{
                return back()->withInput()->with('error', $login->message);
            }
         }

        return view('user.login');
    }

    /**
     * Handles the logout logic
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        if ($request->isMethod('post')) {
            Auth::guard('buyers')->logout();
            return back()->with('success', 'You have logged out successfully!');
        }
    }

    /**
     * Handles the forgot password callback
     *
     * @param Request $request
     * @return void
     */
    public function forgot_password(Request $request)
    {

        if ($request->isMethod('post')) { 

            $data = $request->all();

            $validator = Validator::make($data, [
                'email' => ['required', 'min:4', 'email', 'exists:buyers'],
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();
 
            } else {

                $user = _User::where('email', $data['email'])->first();
                $user->password_token = str_random(36);
                $user->password_token_expiry = \Carbon\Carbon::now()->addDay(1);

                if ($user->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new ForgotPassword($user, 'USER'));
                    return redirect()->route('app:base:index')->with('success', 'A password reset link has been successfully sent to your email address.!');

                }
            }

        }

        return view('user.forgot_password');
    }

    /**
     * Handles the reset password callback
     * This verifies token and saves the new password
     *
     * @param Request $request
     * @param String $token
     * @return void
     */
    public function reset_password(Request $request, $token)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();

            $currentTime = \Carbon\Carbon::now();
            $user = _User::where('password_token', $token)->first();
            if (!$user) {

                return back()
                    ->withInput()
                    ->with('error', 'Invalid Password Token.');

            } elseif ($user and $currentTime > \Carbon\Carbon::parse($user->password_token_expiry)) {

                return back()
                    ->withInput()
                    ->with('error', 'Your Password Request Token has expired, please request a password reset link again.');

            } else {

                $validator = Validator::make($data, [
                    'password' => 'required|min:4',
                    'confirm_password' => 'required|min:4|same:password',
                ]);

                if ($validator->fails()) {

                    return back()
                        ->withErrors($validator)
                        ->withInput();

                } else {
                    $user->password = bcrypt($data['password']);
                    $user->password_token = null;
                    $user->password_token_expiry = null;
                    if ($user->save()) {

                        return redirect()->route('app:user:login')->with('success', 'Your password reset was successful you can now login with your new password.!');

                    }

                }

            }
        }

        return view('user.reset_password', ['token' => $token]);
    }

    /**
     * This verifies user email address.
     * This happens either after sign up or after account update.
     *
     * @param Request $request
     * @param String $token
     * @return void
     */ 
    public function verify_email(Request $request, $token)
    {

        if ($request->isMethod('get')) {
            $currentTime = \Carbon\Carbon::now();
            $user = _User::where('email_token', $token)->first();
            if (!$user) {
                return redirect()->route('app:base:index')->with('error', 'Invalid Email Verification Token.');

            } elseif ($user and $currentTime > \Carbon\Carbon::parse($user->email_token_expiry)) {

                return redirect()->route('app:base:index')->with('error', 'Email token has expired, please re-verify your email address.');

            } else {

                $user->email_verified = true;
                $user->email_token = null;
                $user->email_token_expiry = null;
                if ($user->save()) {
                    //return response()->json(['isVerified'=> true], 200);
                    return redirect()->route('app:user:login')->with('success', 'Your email has been verified! you may now login.');
                }
            }

        }

        return view('user.verify_email');
    }

    public function resend_email_verify(Request $request)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();

            $validator = Validator::make($data, [
                'email' => ['required', 'min:4', 'email', 'exists:users'],
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user = _User::where('email', $data['email'])->first();
                $user->email_token = str_random(36);
                $user->email_token_expiry = \Carbon\Carbon::now()->addDay(1);

                if ($user->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new ResendEmailVerify($user));
                    return redirect()->route('app:base:index')->with('success', 'A verification link has been sent to your mail, follow the link to verify your email address.');

                }
            }

        }

        return view('user.resend_email_verify');
    }

    /**
     * This handles the user basic account setting update
     *
     * @param Request $request
     * @return void
     */

    public function basic_settings(Request $request)
    {

        $user = _User::where('id', Auth::user()->id)->first();

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|min:4|unique:users,username,' . $user->id,
                'firstname' => 'max:255|min:2',
                'lastname' => 'max:255|min:2',
                'phone' => 'digits:11|required',
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user->username = $request->username;
                $user->slug = str_slug($request->username, '-');
                $user->phone = $request->phone;
                $user->firstname = $request->firstname;
                $user->lastname = $request->lastname;

                if ($user->save()) {
                    return back()
                        ->with('success', 'Account settings updated successfully');
                }
            }

        }

        return view('user.basic_settings', ['user' => $user]);
    }

    /**
     * This handles the user password setting update
     *
     * @param Request $request
     * @return void
     */
    public function password_settings(Request $request)
    {

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'password' => ['max:255', 'min:4', new PasswordExists],
                'confirm_password' => 'required|min:4|same:password',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user = _User::where('id', Auth::user()->id)->first();
                $user->password = bcrypt($request->password);
                if ($user->save()) {
                    return back()
                        ->with('success', 'Password setting updated successfully');
                }
            }

        }

        return view('user.password_settings');
    }

    /**
     * This handles user email settings
     * It sends an email verification link to the user
     *
     * @param Request $request
     * @return void
     */
    public function email_settings(Request $request)
    {
        $user = _User::where('id', Auth::user()->id)->first();

        if ($request->isMethod('post')) {
            $data = $request->all();

            $validator = Validator::make($data, [
                'email' => ['required', 'min:4', 'email', 'unique:users'],
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user->email = $request->email;
                $user->email_token = str_random(36);
                $user->email_token_expiry = \Carbon\Carbon::now()->addDay(1);

                if ($user->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new NewEmailVerify($user));
                    Auth::logout();
                    return redirect()->route('app:base:index')->with('success', 'A verification link has been sent to your mail, follow the link to verify your email address.');

                }

            }
        }

        return view('user.email_settings', ['user' => $user]);
    }

    /**
     * Handles user social authentication and redirects
     *
     * @param Request $request
     * @param String $provider
     * @return void
     */
    public function social(Request $request, $provider)
    {
        return \App\Helpers\Social\SocialAuth::driver($request, [
            'redirect_url'=> 'user_config.facebook.redirect'
        ]);
    }

    /**
     * Handles the social callback and saves user data or log user in if already signed up.
     *
     * @param Request $request
     * @param String $provider
     * @return void
     */
    public function social_callback(Request $request, $provider)
    {
        $auth = \App\Helpers\Social\SocialAuth::authenticate($request);
        $userCheck = _User::where($provider, $auth->id)->orWhere('email', $auth->email)->first();
        if ($userCheck) {
            if (!$userCheck->$provider) {
                $userCheck->$provider = $auth->id;
                if ($userCheck->save()) {
                    Auth::guard('buyers')->loginUsingId($userCheck->id, true);
                }
            } else {
                Auth::guard('buyers')->loginUsingId($userCheck->id, true);
            }
            return redirect()->route('app:base:index')->with('success', 'You have successfully logged in with ' . $provider);

        } else {
            $user = new _User();
            $user->name = $auth->username;
            $user->username = $auth->username;
            $user->slug = str_slug($auth->username);
            $user->ref = str_random(10);
            $user->email = $auth->email;
            $user->email_verified = true;
            $user->photo_url = $auth->photo_url;
            $user->password = 'Not Available';
            $user->$provider = $auth->id;
            $user->has_password = false;
            if ($user->save()) {
                //Bouncer::assign('none_admin')->to($user);
               
                Auth::guard('buyers')->loginUsingId($user->id, true);
                return redirect()->route('app:user:complete_social')->with('success', 'You have successfully logged in with ' . $provider);

            }

        }
    }

    /**
     * Completes first time social authentication
     *
     * @param Request $request
     * @param String $provider
     * @return void
     */
    public function complete_social(Request $request)
    {
        $user = _User::where('id', Auth::user()->id)->first();

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|min:4|unique:users,username,' . $user->id,
                'firstname' => 'max:255|min:2',
                'lastname' => 'max:255|min:2',
                'password' => 'required|min:4',
                'confirm_password' => 'required|min:4|same:password',
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user->username = $request->username;
                $user->slug = str_slug($request->username, '-');
                $user->firstname = $request->firstname;
                $user->lastname = $request->lastname;
                $user->has_password = true;
                $user->password = bcrypt($request->password);

                if ($user->save()) {
                    return redirect()
                        ->route('app:base:index')
                        ->with('success', 'Hurray! Your registration is complete!');
                }
            }

        }

        return view('user.complete_social', ['user' => $user]);

    }


    /**
     * ECOMMERCE INTERLINK METHODS
     */

    public function user_orders(Request $request){

        $duration = isset($request->dur) ? \Carbon\Carbon::now()->subDays(intval(base64_decode($request->dur))) : \Carbon\Carbon::now()->subDays(0);
        //print_r($duration);
        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping'])->whereDate('created_at', '<=', $duration)->where('user_id', Auth::user()->id )->paginate();

        return view('user.ecommerce.orders', ['orders'=> $orders]);
    }

    public function user_shipping_address(Request $request){
        $zones = Zone::get();
        return view('user.ecommerce.shipping', [
            'zones'=> $zones
        ]);
    }

    public function user_saved_items(Request $request){
        $products = SavedItem::with(['product'=> function($q){
            $q->with('categories');
        }])->where('user_id', Auth::user()->id)->get();
        return view('user.ecommerce.saved', ['products'=> $products]);
    }

    public function user_delete_saved_items(Request $request, $id){
        if($request->isMethod('post')){
            $product = SavedItem::where('product_id', $id)->delete();
            return back()->with('success', 'Item removed successfully!');
        }
    }

    public function add_address_zone_modal(Request $request, $id){
        $states = Location::where('zone_id', $id)->get();
        return View::make('user.ecommerce.snippet.zone_address', ['id'=> $id, 'states'=> $states]);
    }

    public function edit_address_zone_modal(Request $request, $id, $address_id){
        $states = Location::where('zone_id', $id)->get();
        $address = Shipping::where('id', $address_id)->first();
        return View::make('user.ecommerce.snippet.edit_zone_address', ['id'=> $id, 'states'=> $states, 'address_id'=> $address_id, 'address'=> $address]);
    }

    public function set_default_zone_modal(Request $request, $id){
        $addresses = Shipping::where('zone_id', $id)->get();
        return View::make('user.ecommerce.snippet.select_address', ['addresses'=> $addresses, 'id'=> $id]);
    }

    public function add_shipping_address(Request $request, $id){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=> $validator->errors()], 500);
            }else{

                $shipping = new Shipping();

                //$shipping->user_id = Auth::user()->id;
                $shipping->user_id = Auth::user()->id;
                $shipping->location_id = !$request->location ? 0 : $request->location;
                $shipping->zone_id = $id;
                $shipping->price = 3000;
                $shipping->firstname = $request->firstname;
                $shipping->lastname = $request->lastname;
                $shipping->email = $request->email;
                $shipping->address = $request->address;
                $shipping->phone_number = $request->phone_number;
                $shipping->country = $request->location ? "Nigeria" : $request->country;

                $shipping->save();

                return response()->json(['message'=> 'Shipping details saved successfully', 'redirect_url'=>url()->previous()], 200);

            }

        }
    }

    public function edit_shipping_address(Request $request, $id, $address_id){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()], 500);
            }else{

                $shipping = Shipping::where(['id'=> $address_id])->first();

                //$shipping->user_id = Auth::user()->id;
                $shipping->user_id = Auth::user()->id;
                $shipping->location_id = !$request->location ? 0 : $request->location;
                $shipping->zone_id = $id;
                $shipping->price = 3000;
                $shipping->firstname = $request->firstname;
                $shipping->lastname = $request->lastname;
                $shipping->email = $request->email;
                $shipping->address = $request->address;
                $shipping->phone_number = $request->phone_number;
                $shipping->country = $request->location ? "Nigeria" : $request->country;

                $shipping->save();

                return response()->json(['message'=> 'Shipping details updated successfully', 'redirect_url'=>url()->previous()], 200);

            }

        }
    }

    public function delete_shipping_address(Request $request, $id){
       // $request->session()->forget('CartShippingPrice');
        Shipping::where('id', $id)->delete();
        return back()->with('success', 'Address deleted sucessfully!');
    }

    public function select_shipping_address(Request $request, $id){
        if($request->isMethod('post')){
            DB::table('eco_buyer_addresses')->where([['is_default', true], ['zone_id', $id]])->update(['is_default'=>false]);
            $shipping = Shipping::where('id', $request->shipping_address)->first();
            $shipping->is_default = true;
            $shipping->save();
            return response()->json(['message'=> 'Changes saved successfully', 'redirect_url'=>url()->previous()], 200);
        }
    }

    public function select_zone(Request $request, $id){
        if($request->isMethod('post')){
            $shipping = Shipping::where([['zone_id', $id], ['is_default', true]])->first();
            //$checkAddress = Shipping::where([['zone_id', $id]])->count();

            Zone::where('is_default', true)->update(['is_default'=>false]);
            $zone = Zone::where('id', $id)->first();
            $zone->is_default = true;
            $zone->save();

            return response()->json(['message'=> 'Shipping Selected Successfuly!', 'redirect_url'=> url()->previous()], 200);
        }
    }

    public function review_product(Request $request){
        $payments = Payment::where('status', true)->get();
        $orderIDS = $payments->pluck('order_id')->all();
        $order_items = OrderItem::whereIn('order_ref', $orderIDS)->get();
        $products = Product::with('order_item')->whereIn('id', $order_items->pluck('product_id'))->paginate(10);
        return view('user.ecommerce.reviews', ['products'=> $products]);
    }

    public function add_product_review(Request $request, $id){
        if($request->isMethod('get')){
            $product = Product::with('order_item')->where('id', $id)->first();
            $review = Review::where([['product_id', $id], ['user_id', Auth::user()->id]])->first();
            return View::make('user.ecommerce.snippet.add_review', ['product'=> $product, 'review'=> $review]);
        }

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:200',
                'content' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()], 500);
            }else{
               // rateMsg
            $product = Product::where('id', $id)->first();
            $product->ratingUnique([
                'rating'=> intval($request->rating)
            ], Auth::user());

            $review = Review::firstOrNew(['product_id' => $id]);
            $review->product_id = $id;
            $review->user_id = Auth::user()->id;
            $review->title = $request->title;
            $review->content = $request->content;
            $review->save();
            return response()->json(['message'=> 'Review saved successfully!', 'redirect_url'=> route('app:user:review_product')], 200);

            }

        }

    }


}
