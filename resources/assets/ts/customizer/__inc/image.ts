import Dropzone = require("dropzone");

Dropzone.autoDiscover = false;

class Image{

    constructor(private Canva: any){
        this.uploadImage();
    }
 
    public uploadImage() {
		/*$('#InitUpload').on('click', ()=>{
			
		});*/
		let imgData: any, imgFilePath: any, imgName: any;
        let cls = this;
        
		let uploadArt = new Dropzone("div#ImgUploader", {
			url: $("div#ImgUploader").attr('uploadUrl'),
			maxFilesize: 10,
			paramName: "file",
			method: 'post',
			maxFiles: 5, 
			addRemoveLinks: true,
			//parallelUploads:1,
			uploadMultiple: false,
			acceptedFiles: 'image/*,application/pdf',
			//autoProcessQueue: false,
			params: {
				_token: $('meta[name="csrf-token"]').attr('content')
			},
			dictDefaultMessage: '<span class="uk-text-middle app-text-upload">Drag Art Here or </span><span class="uk-link">Select File</span>',
			/*init: function () {

			}*/

		});

		uploadArt.on('success', function (file: any, res: any) {
			imgData = res.file;
			imgFilePath = res.image_path;
			imgName = res.name;
		});

		$('#SaveUpload').on('click', () => {
			cls.Canva.fab.fabric.Image.fromURL(imgData, function (myImg: any) {
				var scale = 300 / myImg.width;
				myImg.set({});
				myImg.scaleToWidth(150);
				myImg.scaleToHeight(150);
				cls.Canva.activeCanva.centerObject(myImg);
				cls.Canva.activeCanva.add(myImg); 
				cls.Canva.activeCanva.setActiveObject(myImg);
				cls.Canva.activeCanva.renderAll();
				let thisObject = cls.Canva.activeCanva.getObjects().indexOf(myImg);
				let imgTempData = cls.Canva.generateId(10);
				cls.Canva.setObjectLayers({ index: thisObject, icon: 'image', text: imgName, is_uploaded: true, uploadID: imgTempData});
				cls.Canva.imageBucket.push({'name': imgName, 'tmp_name': imgFilePath, uploadID: imgTempData});
				console.log(cls.Canva.activeCanva);
				cls.Canva.isDirty = true;
				UIkit.modal('#ImageModal').hide();
			});
		});
    }
    
}

export default Image;