<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignerOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designer_orders', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('user_id');
			$table->string('user_type', 225)->nullable();
			$table->bigInteger('designer_id');
			$table->integer('gig_id');
			$table->bigInteger('plan_id');
			$table->bigInteger('payment_id');
			$table->bigInteger('request_id')->nullable();
			$table->float('amount');
			$table->string('reference', 191);
			$table->boolean('status')->default(0);
			$table->timestamps();
			$table->boolean('info_submitted')->default(0);
			$table->boolean('work_delivered')->default(0);
			$table->boolean('work_in_progress')->default(0);
			$table->boolean('is_completed')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designer_orders');
	}

}
