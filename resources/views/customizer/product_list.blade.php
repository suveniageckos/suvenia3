@extends('layouts.customizer')

@section('title', 'Start Design' )

@section('content')
<div class="uk-section bg-designer-ads">
        <div class="uk-container">
            <div class="row justify-content-center">
                <div class="col-8">
                        <p>Our Community of Designers can help with your designs <a class="btn btn-info-dk" href="{{ route('app:designer:landing_page_hire') }}">REQUEST DESIGN</a></p>
                </div>
            </div>
        </div>
</div>


<div class="uk-container uk-container-smal mt-4 mb-4">

        <div class="app-sec-header sec-center">
            <h1 class="title">create design</h1>
            <div class="underline"></div>
        </div>
        <div class="app-sec-sub uk-text-center mb-5 mt-3">
            Choose any product and make it awesome with your personalised design
        </div>

        <div class="row justify-content-center mt-4 mb-3 no-gutters">
            @foreach($brandables as $brandable)
            @php $data = json_decode($brandable->data, true); @endphp
                <div class="col-3 uk-text-center">
                <a href="{{ route('app:ecommerce:design') }}" class="card-brandable" data-mapper="{{ $brandable->data }}">
                    <img src="{{ asset('brand_display/'. str_slug($brandable->name) .'.png')}}" class="" uk-responsive width="207">
                    <p class="mt-1">{{ $brandable->name }}</p>
                    </a>
                </div>
            @endforeach
        </div>
</div>
@endsection

@push('customizer_js')

    <script src="{{ asset('suv/s/pg_customizer_landing.bundle.js') }}"></script>

@endpush
