@extends('layouts.layout')

@section('title', 'Home' )

@section('content')

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="uk-cover-container banner-height">
        <img src="{{ asset($utils->get_image('site.market_place_home_banner')) }}" alt="" uk-cover>
        <!---<div class="uk-overlay-primary uk-position-cover"></div>-->

        <div class="d-flex justify-content-between uk-position-center">
            <div></div>
            <div class="w-100 p-3 mr-5" style="" uk-scrollspy="cls:uk-animation-scale-up;repeat:true;">
                    <div class="home-hero uk-text-right">
                        <b>Discover</b> amazing designs
                    </div>
                    <div class="home-hero-sub uk-text-center">
                            <p class="m-0">MADE FOR YOU</p>
                    </div>
                    <div class="mt-2 uk-text-center m-auto" style="width: 80%;">
                            <form action="{{ route('app:ecommerce:product_search') }}" method="post" autocomplete="off" class="m-auto">
                                    @csrf
                        <div class="input-group">
                        <input type="search" class="form-control banner-search textfield-search-format" placeholder="Find your desired product" name="q" >
                        <div class="input-group-append">
                        <button class="btn" type="submit" style="width:60px; color:#fff; background-color:lightseagreen; "><span class="icon-search" ></span></button>
                        </div>
                        </div>
                    </form>
                    </div>
            </div>
        </div>
</div>


<div class="borde" style="position: relative; height:100px; background: #fff;">
<div class="float-slider" style="padding: 20px 60px;" uk-slider>
        <div class="row">
            <div class="col-12 uk-text-center">
            <h2 class="m-0 header">Everyday products made uniquely for you</h2>
            </div>
        </div>
        <div class="row mt-3">
                <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >

                        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                        @foreach($showcase_categories as $category)
                            <li class="uk-text-center" uk-scrollspy="cls:uk-animation-fade;repeat:true;">
                                <a href="{{ route('app:ecommerce:product_catlogue', ['category'=> $category->slug]) }}">
                                    <div class="uk-panel">
                                        <img src="{{ asset('storage/' . $category->show_case_product) }}" alt="">

                                    </div>
                                </a>
                            <a href="{{ route('app:ecommerce:product_catlogue', ['category'=> $category->slug]) }}" class="_prod_title">{{ title_case($category->name) }}</a>
                            </li>
                        @endforeach
                        </ul>

                </div>
        </div>
        <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
        <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
</div>
</div>


<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small">
            <div class="row">
                <div class="col-12">
                    <p class="m-0 after_cat_slide_desc">You are unique, so why shouldn’t everything else about you be? From Apparels to Accessories, Suvenia makes it easy for you to find something made just for you.</p>
                </div>
            </div>
        </div>
</div>
@else

<div class="uk-cover-container">
        <img src="{{ asset($utils->get_image('site.market_place_home_banner')) }}" alt="">
        <div class="uk-section uk-position-center">
            <div class="row uk-justify-center p-2">
                <div class="col-md-12">

                    <div class="uk-text-center">
                    <div class="n-centered-her">
                        <b class="text-white" style="font-size:16px;">Discover Amazing Designs</b>
                        <p class="mt-0 n-centered-sub-hero text-white" style="font-size:13px;">Made For You</p>
                    </div>
                    <div class="m-0">
                    @php
                      $catDefaultLink = $showcase_categories->first() ? $showcase_categories->first()->name : '' ;
                    @endphp
                    <a class="btn btn-outline-light btn-sm rounded-pill" style="font-size:12px;" href="{{ route('app:ecommerce:product_catlogue', ['category'=> $catDefaultLink]) }}" target="_blank">Explore</a>
                    </div>
                    </div>

                </div>
            </div>
        </div>

</div>

@endif

<div class="uk-section bg-white p-4">
    @include('partials.product_block', ['code'=> 'AK5U2MC0Nq'])
</div>

<div class="uk-margin-to">
        <div class="uk-container uk-container-expand uk-padding-remove-left uk-padding-remove-right">
    <a href="" target="_blank">
            <img src="{{ asset('img/home_banner.png') }}" class="uk-width-1-1" alt="" uk-responsive>

        </a>
    </div>
</div>

<div class="uk-section bg-white p-4">
    @include('partials.product_block', ['code'=> 'KahoRnWY46'])
</div>

<div class="uk-section bg-white p-4">
    @include('partials.product_block', ['code'=> '1Nx4AivOly'])
</div>

<div class="uk-margin-to bg-white">
        <div class="uk-container uk-container-expand uk-padding-remove-left uk-padding-remove-right">
            <div class="app-sec-header sec-center mb-3">
                <h1 class="title text-body-black">more for you</h1>
                <!--<div class="underline"></div>-->
                <div style="height:5px"></div>
             </div>
             @if(!$utils->device()->isMobile() || $utils->device()->isTablet())
                    <div uk-slideshow="animation: push; min-height: 100; max-height: 300">
                        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                            <ul class="uk-slideshow-items">
                                <li>
                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url({{ asset('img/aa.png') }});">
                                       <a href="{{ route('app:ecommerce:product_list') }}" class="create_your_button" style="  color: #000; text-decoration: none;">CREATE YOUR OWN</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url({{ asset('img/cc.png') }});">
                                        <a href="{{ route('app:seller:index') }}" class="create_your_button" style="  color: #000; text-decoration: none;">SELL ON SUVENIA</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url({{ asset('img/bb.png') }});">
                                        <a href="{{ route('app:designer:landing_page') }}" class="create_your_button" style="  color: #000; text-decoration: none;">HIRE A DESIGNER</a>
                                    </div>
                                </li>
                            </ul>

                            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                        </div>
                        <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

                    </div>
            @else

            <div uk-slideshow="animation: push; min-height: 350; max-height: 400; ">

                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slideshow-items">
                                <li>
                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url({{ asset('img/a1.png') }});">
                                        <a href="{{ route('app:ecommerce:product_list') }}" class="create_your_button" style="  color: #000; text-decoration: none;">CREATE YOUR OWN</a>
                                     </div>
                                </li>
                                <li>
                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url({{ asset('img/c1.png') }});">
                                        <a href="{{ route('app:seller:index') }}" class="create_your_button" style="  color: #000; text-decoration: none;">SELL ON SUVENIA</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url({{ asset('img/b1.png') }});">
                                        <a href="{{ route('app:designer:landing_page') }}" class="create_your_button" style="  color: #000; text-decoration: none;">HIRE A DESIGNER</a>
                                    </div>
                                </li>


                    </ul>

                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                </div>

                <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

            </div>

     @endif
        </div>

    </div>




<div class="uk-section bg-white p-4">
        <div class="uk-container">
            <div class="app-sec-header sec-center mb-3">
                <h1 class="title text-body-black">stores we like</h1>
                <!--<div class="underline"></div>-->
                <div style="height:5px"></div>
            </div>

            @if(!$utils->device()->isMobile() || $utils->device()->isTablet())

            <ul class="uk-slider-items uk-child-width-1-3@m uk-grid">

                @php
                    $store_blocks = \App\StoresWeLikeBlock::all();
                    foreach ($store_blocks as $store_block){
                    $store_idds = \App\Store::where('id', explode(",", $store_block->store_ids))->get();
                    foreach ($store_idds as $store_idd){
                    $seller_ids = \App\Seller::where('id', $store_idd->user_id)->get();
                     }
                     }
                @endphp

                @foreach ($store_idds as $store_idd)
                <?php $mystore = json_decode($store_idd->meta, true); ?>
                    <li>
                            <div class="uk-card uk-card-default">
                                    <div class="uk-card-media-top">
                                    <a href="{{ $store_idd->store_url }}" target="_blank">
                                        @if(!empty($mystore['store_upload_banner']) || !empty($mystore['store_banner_system']))
                                        @if ($mystore['default_banner'] == 'USER')
                                        <img src="{{ asset($mystore['store_upload_banner']) }}" alt="" width="100%">
                                        @else
                                        <img src="{{ asset($mystore['store_banner_system']) }}" alt="" width="100%">
                                        @endif
                                        @else
                                        <img src="{{ asset($mystore['default_banner']) }}" alt="" width="100%">
                                        @endif
                                        </a>
                                    </div>

                                    <div class="uk-card-body p-2">
                                            <div class="d-flex">
                                                <div>
                                                    @php foreach($seller_ids as $seller_id) @endphp
                                                 <img src="{{('storage/'. $seller_id->image_path) }}" alt="" width="64">
                                                </div>
                                                <div>

                                                    <a href="{{ $store_idd->store_url }}" target="_blank" class="card__links">
                                                    <p class="mb-0 mt-0 ml-3 store_card_title uk-text-capitalize">{{$store_idd->name}}</p>
                                                    <p class="ml-3 mt-0 store_text_content">{{ str_limit($store_idd->description, 34) }}</p>
                                                    </a>
                                                </div>
                                            </div>
                                    </div>
                            </div>
                    </li>
                @endforeach


    </ul>
            @else


            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>
                <ul class="uk-slider-items uk-grid uk-margin-bottom">
                        @php
                        $store_blocks = \App\StoresWeLikeBlock::all();
                        foreach ($store_blocks as $store_block){
                        $store_idds = \App\Store::where('id', explode(",", $store_block->store_ids))->get();
                        foreach ($store_idds as $store_idd){
                        $seller_ids = \App\Seller::where('id', $store_idd->user_id)->get();
                         }
                         }
                    @endphp

                    @foreach ($store_idds as $store_idd)
                    <?php $mystore = json_decode($store_idd->meta, true); ?>
                    <li class="uk-width-4-5">
                        <div class="uk-panel">
                            <div class="uk-card uk-card-default">
                                <div class="uk-card-media-top">
                                    <a href="{{ $store_idd->store_url }}" target="_blank">
                                        @if(!empty($mystore['store_upload_banner']) || !empty($mystore['store_banner_system']))
                                        @if ($mystore['default_banner'] == 'USER')
                                      <img src="{{ asset($mystore['store_upload_banner']) }}" alt="" width="100%">
                                      @else
                                    <img src="{{ asset($mystore['store_banner_system']) }}" alt="" width="100%">
                                    @endif
                                    @else
                                    <img src="{{ asset($mystore['default_banner']) }}" alt="" width="100%">
                                    @endif
                                    </a>
                                </div>
                                <div class="uk-card-body p-2">
                                        <div class="d-flex">
                                            <div>

                                                @php foreach($seller_ids as $seller_id) @endphp
                                                <img src="{{('storage/'. $seller_id->image_path) }}" alt="" width="64">

                                            </div>
                                            <div>
                                                <a href="{{ $store_idd->store_url }}" target="_blank" class="card__links">
                                                <p class="mb-0 mt-0 ml-3 store_card_title">{{$store_idd->name}}</p>
                                                {{-- <p class="ml-3 mt-0 store_text_content">{{ str_limit($store_idd->description, 25) }}</p> --}}
                                                </a>
                                            </div>
                                        </div>
                                </div>
                        </div>
                        </div>
                    </li>
                    @endforeach


                </ul>

                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

            </div>

            @endif

        </div>

</div>


<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small">
            <div class="app-sec-header sec-center mb-3">
                <h1 class="title text-body-black">our promise to you</h1>
                <!--<div class="underline"></div>-->
                <div style="height:5px"></div>
            </div>

            <div class="row mt-4">

                <div class="col-md-4">
                        <div class="card app-info-card transparent">
                                <p class="m-1"><span class="card-info-icon icon-delivery-truck"></span>
                                <div class="app-card-icon-head">FAST SHIPPING</div>
                                <small>We produce every item on-demand and quickly</small>
                        </div>
                </div>
                <div class="col-md-4">
                        <div class="card app-info-card transparent">
                                <p class="m-1"><span class="card-info-icon icon-five-stars"></span>
                                <div class="app-card-icon-head">100% SATISFACTION</div>
                                <small>Exchange or money back guarantee for all orders</small>
                        </div>
                </div>
                <div class="col-md-4">
                        <div class="card app-info-card transparent">
                                <p class="m-1"><span class="card-info-icon icon-shield"></span>
                                <div class="app-card-icon-head">SECURE PAYMENT</div>
                                <small>Secure online shopping and payment.</small>
                        </div>
                </div>

            </div>

        </div>

</div>


<div class="uk-section bg-light-dark p-4">
        <div class="uk-container uk-container-small">
                <div class="app-sec-header sec-center mb-3">
                        <h1 class="title text-body-black">our blog</h1>
                        <!--<div class="underline"></div>-->
                        <div style="height:5px"></div>
                    </div>


                            @if(!$utils->device()->isMobile() || $utils->device()->isTablet())

                            <ul class="uk-slider-items uk-child-width-1-2@m uk-grid">

                            <li>
                              @foreach ($vzine_details as $vzine)
                                <div class="uk-card uk-card-default">
                                        <div class="uk-card-media-top">
                                            <a href="{{ $vzine->link }}" target="_blank">
                                                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-right" style="background-image: url({{ $vzine->jetpack_featured_media_url }});">
                                                        <span class="uk-badge uk-padding-small uk-text-large">VZINE</span>
                                                    </div>
                                            </a>
                                        </div>
                                        <div class="uk-card-body p-2">
                                                <div class="d-flex">

                                                    <div>
                                                        <a href="{{ $vzine->link }}" target="_blank" class="card__links">
                                                        <small class="uk-text-muted">{{ date('d F, Y', strtotime($vzine->date)) }}</small>
                                                        <p class="mb-0 mt-2 ml-3 mr-3 text-formatted-big">{{ $vzine->title->rendered }}</p>
                                                        {{-- <p class="ml-3 mt-4 mr-3 text-formatted">{!! $vzine->excerpt->rendered !!}</p> --}}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                        <p class="mb-3 mt-2 ml-3"><a href="{{ $vzine->link }}" target="_blank" class="card__links">Discover ></a></p>
                                                </div>
                                        </div>
                                </div>
                                @endforeach
                        </li>


                        <li>
                            @foreach ($blog_details as $blog)


                                <div class="uk-card uk-card-default">
                                        <div class="uk-card-media-top">
                                            <a href="{{ $blog->link }}" target="_blank">

                                                <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-right" style="background-image: url({{ $image_url }});">
                                                    <span class="uk-badge uk-padding-small uk-text-large">BLOG</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="uk-card-body p-2">
                                                <div class="d-flex">

                                                    <div style="text-align:justify;">
                                                    <a href="{{ $blog->link }}" target="_blank" class="card__links">
                                                        <small class="uk-text-muted">{{ date('d F, Y', strtotime($blog->date)) }}</small>
                                                        <p class="mb-0 mt-2 ml-3 mr-3 text-formatted-big">{{ $blog->title->rendered }}</p>
                                                        {{-- <p class="ml-3 mt-4 mr-3 text-formatted">{!! $blog->excerpt->rendered !!}</p> --}}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                        <p class="mb-3 mt-2 ml-3"><a href="{{ $blog->link }}" target="_blank" class="card__links">Discover ></a></p>
                                                </div>
                                        </div>
                                </div>
                                @endforeach
                        </li>
                    </ul>
                @else


                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>
                        <ul class="uk-slider-items uk-grid">
                            <li class="uk-width-4-5">
                            @foreach ($vzine_details as $vzine)
                                <div class="uk-panel">
                                    <div class="uk-card uk-card-default">
                                        <div class="uk-card-media-top">
                                        <a href="{{ $vzine->link }}" target="_blank">
                                                    <div class="uk-background-cover uk-height-medium uk-flex uk-flex-right" style="background-image: url({{  $vzine->jetpack_featured_media_url }});">
                                                        <span class="uk-badge uk-padding-small uk-text-large">VZINE</span>
                                                    </div>
                                            </a>
                                        </div>
                                        <div class="uk-card-body p-2">
                                                <div class="d-flex">

                                                    <div>
                                                            <a href="{{ $vzine->link }}" target="_blank" class="card__links">
                                                           <small>{{ date('d F, Y', strtotime($vzine->date)) }}</small>
                                                            <p class="mb-0 mt-2 ml-3 mr-3 text-formatted-big-mobile">{{ $vzine->title->rendered }}</p>
                                                            {{-- <p class="ml-3 mt-4 mr-3 text-formatted-mobile">{{ $blog->excerpt->rendered }}</p> --}}
                                                            </a>
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                    <p class="mb-3 mt-2 ml-3"><a href="{{ $vzine->link }}" target="_blank" class="card__links">Discover ></a></p>
                                                </div>
                                        </div>
                                </div>
                                </div>
                            @endforeach
                            </li>


                            <li class="uk-width-4-5">
                                @foreach ($blog_details as $blog)
                                    <div class="uk-panel">
                                        <div class="uk-card uk-card-default">
                                            <div class="uk-card-media-top">
                                            <a href="{{ $blog->link }}" target="_blank">
                                                <div class="uk-background-cover uk-height-medium uk-flex uk-flex-right" style="background-image: url({{ $image_url }});">
                                                    <span class="uk-badge uk-padding-small uk-text-large">BLOG</span>
                                                </div>
                                            </a>
                                            </div>
                                            <div class="uk-card-body p-2">
                                                    <div class="d-flex">

                                                        <div>
                                                        <a href="{{ $blog->link }}" target="_blank" class="card__links">
                                                            <small>{{ date('d F, Y', strtotime($blog->date)) }}</small>
                                                            <p class="mb-0 mt-2 ml-3 mr-3 text-formatted-big-mobile">{{ $blog->title->rendered }}</p>
                                                            {{-- <p class="ml-3 mt-4 mr-3 text-formatted-mobile">{{ $blog->excerpt->rendered }}</p> --}}
                                                        </a>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex">
                                                        <p class="mb-3 mt-2 ml-3"><a href="{{ $blog->link }}" target="_blank" class="card__links">Discover ></a></p>
                                                    </div>
                                            </div>
                                    </div>
                                    </div>
                                    @endforeach
                                </li>

                        </ul>

                        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                    </div>

                @endif

        </div>
    </div>





<div class="uk-section bg-grey p-4">
        <div class="uk-container uk-container-small">
            <div class="div mt-4 uk-text-center">
            <p class="m-0 text-formatted">Subscribe to our newsletter now, get all news from our awesome website.</p>
            </div>
            <div class="row mt-4 justify-content-center">
            <div class="col-md-7">
                <form class="app-form">
                <div class="input-group mb-3">
                <input type="email" class="form-control app-input rounded-0 textfield-search-format" placeholder="Enter Your Email Address" aria-label="Subscribe" aria-describedby="Subscribe">
                <div class="input-group-append">
                <button class="btn btn-info-dark" type="button" id="button-addon2">SUBSCRIBE</button>
                </div>
                </div>
            </form>

            </div>
            </div>

        </div>

</div>

    <div id="modal-close-default" uk-modal>
        <div class="uk-modal-dialog uk-modal-body big-dialog">
            {{-- <div class=" uk-modal-header"> --}}
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
            {{-- </div> --}}

                        <div class="row">
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <span class="fas fa-store"></span>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                        <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fas fa-paint-brush"></i>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                            <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fab fa-accessible-icon"></i>
                                            </div>

                                             <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                            <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                 </div>
                            </div>

                        </div>

            </div>
            </div>



@endsection
