@extends('layouts.dashboard_seller')

@section('title', 'Design Services' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:seller:dashboard:design_services') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:design_services'], 'active') }}">My Orders</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:seller:dashboard:design_request') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:design_request'], 'active') }}">My Design Requests</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead> 
                                <tr>
                                    <th>GIG</th>
                                    <th></th>
                                    <th>ORDER NUMBER</th>
                                    <th>DURATION</th>
                                    <th>PRICE</th>
                                    <th></th>
                                </tr> 
                                </thead>
                                <tbody>
                                    @forelse($active_orders as $order)
                                    <tr>
                                        <td>{{ $order->gig->title }}</td>
                                        <td>
                                            <ul class="list-unstyled">
                                                <li class="d-inline-block"><img src="{{ asset($order->designer->photo_url) }}" class="mr-1 uk-border-circle" alt="{{ $order->designer->username }}" width="50"></li>
                                                <li class="d-inline-block">
                                                    <p class="text-secondary mb-0 uk-text-capitalize" style="font-size:12px;">{{ $order->designer->username }}</p>
                                                    @php $rateValues = [1, 2, 3, 4, 5] ;@endphp
                                                    <select class="bar-rated" data-url="{{ route('app:designer:dashboard:rate_designer', ['id'=> $order->id]) }}">
                                                    @foreach ($rateValues as $item)
                                                    <option value="{{ $item }}" {{ $order->designer->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                                                    @endforeach
                                                    </select>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>{{ $order->reference }}</td>
                                        <td>{{ $order->plan->duration }}</td>
                                        <td>&#8358;{{ $order->amount }}</td>
                                        <td><a href="{{ route('app:seller:dashboard:single_design_service', ['id'=> $order->id]) }}" class="text-muted">View</a></td>
                                    </tr>
                                    @empty 
                                    <tr>
                                        <td colspan="100">You Do Not Have Orders Yet</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                       {{ $active_orders->links() }}
                </div>
        </div>
   
</div>


@endsection

 