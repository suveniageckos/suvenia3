<?php

use Illuminate\Database\Seeder;

class EcoBrandablesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('eco_brandables')->delete();
        
        \DB::table('eco_brandables')->insert(array (
            0 => 
            array (
                'id' => 41,
                'user_id' => 1,
                'name' => 't-shirt',
                'description' => 'Comfortable, casual and loose fitting, this t-shirt is a wardrobe staple, and your ideal everyday, casual wear. Made from 100% cotton, it wears well on anyone. Print is on front only, and with full colour printing',
                'price' => 2500,
                'data' => '{"backViewEnabled":true,"sizeEnabled":true,"description":"Comfortable, casual and loose fitting, this t-shirt is a wardrobe staple, and your ideal everyday, casual wear. Made from 100% cotton, it wears well on anyone. Print is on front only, and with full colour printing","price":"2500","name":"t-shirt","category":"53","front":{"left":153,"top":105,"width":225,"height":424,"image_url":"brandables\\/Front-ryajgTsXEb.png"},"back":{"left":145,"top":68,"width":223,"height":465,"image_url":"brandables\\/Back-YsFQaFW6kR.png"}}',
                'front_image' => 'storage/brandables/Front-ryajgTsXEb.png',
                'back_image' => 'storage/brandables/Back-YsFQaFW6kR.png',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 02:57:49',
                'updated_at' => '2019-04-24 02:57:49',
                'display_photo' => NULL,
            ),
            1 => 
            array (
                'id' => 42,
                'user_id' => 1,
                'name' => 'hoodie',
                'description' => 'A modern take on a classic style, this hoodie is made of 100% cotton. Extra thick for added warmth, this hoodie is perfect for outdoor activities on super cold days. Print is on front only, and with full colour printing. Ensure that your texts are spelt correctly legibly. Images should also be as clear as possible and without blurs',
                'price' => 5000,
                'data' => '{"backViewEnabled":true,"sizeEnabled":true,"description":"A modern take on a classic style, this hoodie is made of 100% cotton. Extra thick for added warmth, this hoodie is perfect for outdoor activities on super cold days. Print is on front only, and with full colour printing. Ensure that your texts are spelt correctly legibly. Images should also be as clear as possible and without blurs","price":"5000","name":"hoodie","category":"49","front":{"left":151,"top":122,"width":219,"height":248,"image_url":"brandables\\/Front-m6SGFnidkE.png"},"back":{"left":139,"top":135,"width":250,"height":385,"image_url":"brandables\\/Back-1UWl7TjnAq.png"}}',
                'front_image' => 'storage/brandables/Front-m6SGFnidkE.png',
                'back_image' => 'storage/brandables/Back-1UWl7TjnAq.png',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:00:16',
                'updated_at' => '2019-04-24 03:00:16',
                'display_photo' => NULL,
            ),
            2 => 
            array (
                'id' => 43,
                'user_id' => 1,
                'name' => 'snapback',
                'description' => 'Our custom snapbacks have 6 panels structure, with snap closure. Snapbacks are popular with millennials and entertainers. Print is on front only, and with full colour printing. Ensure that your texts are spelt correctly legibly. Images should also be as clear as possible and without blurs',
                'price' => 3000,
                'data' => '{"backViewEnabled":false,"sizeEnabled":false,"description":"Our custom snapbacks have 6 panels structure, with snap closure. Snapbacks are popular with millennials and entertainers. Print is on front only, and with full colour printing. Ensure that your texts are spelt correctly legibly. Images should also be as clear as possible and without blurs","price":"3000","name":"snapback","category":"54","front":{"left":70,"top":195,"width":379,"height":206,"image_url":"brandables\\/Front-wfbJ243Fnw.png"},"back":[]}',
                'front_image' => 'storage/brandables/Front-wfbJ243Fnw.png',
                'back_image' => '',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:02:30',
                'updated_at' => '2019-04-24 03:02:30',
                'display_photo' => NULL,
            ),
            3 => 
            array (
                'id' => 44,
                'user_id' => 1,
                'name' => 'baseball hat',
                'description' => 'This hat is a creative way to express yourself. Print is on the front only. One Size-adjustable velcro strap. 100% cotton, with five-panel structure.',
                'price' => 2500,
                'data' => '{"backViewEnabled":false,"sizeEnabled":false,"description":"This hat is a creative way to express yourself. Print is on the front only. One Size-adjustable velcro strap. 100% cotton, with five-panel structure.","price":"2500","name":"baseball hat","category":"55","front":{"left":101,"top":156,"width":325,"height":239,"image_url":"brandables\\/Front-kI8GeCIRP6.png"},"back":[]}',
                'front_image' => 'storage/brandables/Front-kI8GeCIRP6.png',
                'back_image' => '',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:06:32',
                'updated_at' => '2019-04-24 03:06:32',
                'display_photo' => NULL,
            ),
            4 => 
            array (
                'id' => 45,
                'user_id' => 1,
                'name' => 'v-neck tee',
                'description' => 'Comfortable, casual and loose fitting, this t-shirt is a wardrobe staple, and your ideal everyday, casual wear. Made from 100% cotton, it wears well on anyone. Print is on front only, and with full colour printing.',
                'price' => 2500,
                'data' => '{"backViewEnabled":true,"sizeEnabled":true,"description":"Comfortable, casual and loose fitting, this t-shirt is a wardrobe staple, and your ideal everyday, casual wear. Made from 100% cotton, it wears well on anyone. Print is on front only, and with full colour printing.","price":"2500","name":"v-neck tee","category":"50","front":{"left":140,"top":124,"width":236,"height":407,"image_url":"brandables\\/Front-A2zWZiBoRr.png"},"back":{"left":139,"top":70,"width":242,"height":467,"image_url":"brandables\\/Back-XQepInKcM2.png"}}',
                'front_image' => 'storage/brandables/Front-A2zWZiBoRr.png',
                'back_image' => 'storage/brandables/Back-XQepInKcM2.png',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:09:04',
                'updated_at' => '2019-04-24 03:09:04',
                'display_photo' => NULL,
            ),
            5 => 
            array (
                'id' => 46,
                'user_id' => 1,
                'name' => 'sweatshirt',
                'description' => 'Brave any outdoor activity in the comfort of this classic crewneck sweatshirt. It\\u2019s both plush and durable in its construction \\u2013 a true staple for any wardrobe that will last. Print is on front only, and with full colour printing, Ensure that your texts are spelt correctly legibly. Images should also be as clear as possible and without blurs.',
                'price' => 4000,
                'data' => '{"backViewEnabled":true,"sizeEnabled":true,"description":"Brave any outdoor activity in the comfort of this classic crewneck sweatshirt. It\\\\u2019s both plush and durable in its construction \\\\u2013 a true staple for any wardrobe that will last. Print is on front only, and with full colour printing, Ensure that your texts are spelt correctly legibly. Images should also be as clear as possible and without blurs.","price":"4000","name":"sweatshirt","category":"52","front":{"left":148,"top":79,"width":246,"height":456,"image_url":"brandables\\/Front-fBAjDrmdC1.png"},"back":{"left":139,"top":45,"width":255,"height":492,"image_url":"brandables\\/Back-a05noVWme9.png"}}',
                'front_image' => 'storage/brandables/Front-fBAjDrmdC1.png',
                'back_image' => 'storage/brandables/Back-a05noVWme9.png',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:11:26',
                'updated_at' => '2019-04-24 03:11:26',
                'display_photo' => NULL,
            ),
            6 => 
            array (
                'id' => 47,
                'user_id' => 1,
                'name' => 'polo',
                'description' => 'Comfortable, casual and loose fitting, this t-shirt is a wardrobe staple, and your ideal everyday, casual wear. Made from 100% cotton, it wears well on anyone. Print is on front only, and with full color printing.',
                'price' => 3500,
                'data' => '{"backViewEnabled":true,"sizeEnabled":true,"description":"Comfortable, casual and loose fitting, this t-shirt is a wardrobe staple, and your ideal everyday, casual wear. Made from 100% cotton, it wears well on anyone. Print is on front only, and with full color printing.","price":"3500","name":"polo","category":"51","front":{"left":136,"top":151,"width":280,"height":403,"image_url":"brandables\\/Front-2i7tEUshiy.png"},"back":{"left":122,"top":118,"width":293,"height":429,"image_url":"brandables\\/Back-dtO5Pf11Kg.png"}}',
                'front_image' => 'storage/brandables/Front-2i7tEUshiy.png',
                'back_image' => 'storage/brandables/Back-dtO5Pf11Kg.png',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:13:36',
                'updated_at' => '2019-04-24 03:13:36',
                'display_photo' => NULL,
            ),
            7 => 
            array (
                'id' => 48,
                'user_id' => 1,
                'name' => 'throw pillow',
                'description' => 'The perfect complement to your couch, custom pillows will make you the envy of the neighborhood.',
                'price' => 7000,
                'data' => '{"backViewEnabled":false,"sizeEnabled":false,"description":"The perfect complement to your couch, custom pillows will make you the envy of the neighborhood.","price":"7000","name":"throw pillow","category":"58","front":{"left":66,"top":127,"width":391,"height":400,"image_url":"brandables\\/Front-u9tFmBovTP.png"},"back":[]}',
                'front_image' => 'storage/brandables/Front-u9tFmBovTP.png',
                'back_image' => '',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:15:22',
                'updated_at' => '2019-04-24 03:15:22',
                'display_photo' => NULL,
            ),
            8 => 
            array (
                'id' => 49,
                'user_id' => 1,
                'name' => 'apron',
                'description' => 'Apron material',
                'price' => 2000,
                'data' => '{"backViewEnabled":false,"sizeEnabled":true,"description":"Apron material","price":"2000","name":"apron","category":"57","front":{"left":122,"top":32,"width":278,"height":546,"image_url":"brandables\\/Front-1tozGcdZZ9.png"},"back":[]}',
                'front_image' => 'storage/brandables/Front-1tozGcdZZ9.png',
                'back_image' => '',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:17:56',
                'updated_at' => '2019-04-24 03:17:56',
                'display_photo' => NULL,
            ),
            9 => 
            array (
                'id' => 51,
                'user_id' => 1,
                'name' => 'mug',
                'description' => 'this mug has got enough light to bright up your day',
                'price' => 1500,
                'data' => '{"backViewEnabled":false,"sizeEnabled":false,"description":"this mug has got enough light to bright up your day","price":"1500","name":"mug","category":"56","front":{"left":84,"top":107,"width":284,"height":371,"image_url":"brandables\\/Front-l0tsbP5ZD0.png"},"back":[]}',
                'front_image' => 'storage/brandables/Front-l0tsbP5ZD0.png',
                'back_image' => '',
                'front_image_path' => NULL,
                'back_image_path' => NULL,
                'created_at' => '2019-04-24 03:21:16',
                'updated_at' => '2019-04-24 03:21:16',
                'display_photo' => NULL,
            ),
        ));
        
        
    }
}