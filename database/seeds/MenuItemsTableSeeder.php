<?php

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'menu_id' => 1,
                'title' => 'Dashboard',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-boat',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2019-07-25 18:33:07',
                'updated_at' => '2019-07-25 18:33:07',
                'route' => 'voyager.dashboard',
                'parameters' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'menu_id' => 1,
                'title' => 'Media',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 5,
                'created_at' => '2019-07-25 18:33:07',
                'updated_at' => '2019-07-25 18:33:07',
                'route' => 'voyager.media.index',
                'parameters' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Users',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-person',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 3,
                'created_at' => '2019-07-25 18:33:07',
                'updated_at' => '2019-07-25 18:33:07',
                'route' => 'voyager.users.index',
                'parameters' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'menu_id' => 1,
                'title' => 'Roles',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-lock',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2019-07-25 18:33:07',
                'updated_at' => '2019-07-25 18:33:07',
                'route' => 'voyager.roles.index',
                'parameters' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'menu_id' => 1,
                'title' => 'Tools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tools',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 9,
                'created_at' => '2019-07-25 18:33:07',
                'updated_at' => '2019-07-25 18:33:07',
                'route' => NULL,
                'parameters' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'menu_id' => 1,
                'title' => 'Menu Builder',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 10,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
                'route' => 'voyager.menus.index',
                'parameters' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'menu_id' => 1,
                'title' => 'Database',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 11,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
                'route' => 'voyager.database.index',
                'parameters' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'menu_id' => 1,
                'title' => 'Compass',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-compass',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 12,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
                'route' => 'voyager.compass.index',
                'parameters' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'menu_id' => 1,
                'title' => 'BREAD',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bread',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 13,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
                'route' => 'voyager.bread.index',
                'parameters' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'menu_id' => 1,
                'title' => 'Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 14,
                'created_at' => '2019-07-25 18:33:08',
                'updated_at' => '2019-07-25 18:33:08',
                'route' => 'voyager.settings.index',
                'parameters' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'menu_id' => 1,
                'title' => 'Hooks',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-hook',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 13,
                'created_at' => '2019-07-25 18:33:12',
                'updated_at' => '2019-07-25 18:33:12',
                'route' => 'voyager.hooks',
                'parameters' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'menu_id' => 1,
                'title' => 'Designers',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-paint-bucket',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 15,
                'created_at' => '2019-07-26 08:38:59',
                'updated_at' => '2019-07-26 08:38:59',
                'route' => 'voyager.designers.index',
                'parameters' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'menu_id' => 1,
                'title' => 'Influencers',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tv',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 16,
                'created_at' => '2019-07-26 18:47:18',
                'updated_at' => '2019-07-26 18:47:18',
                'route' => 'voyager.influencers.index',
                'parameters' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'menu_id' => 1,
                'title' => 'Brandables',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 17,
                'created_at' => '2019-07-27 08:27:42',
                'updated_at' => '2019-07-27 08:27:42',
                'route' => 'voyager.eco-brandables.index',
                'parameters' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'menu_id' => 1,
                'title' => 'Product Categories',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 18,
                'created_at' => '2019-07-27 19:59:02',
                'updated_at' => '2019-07-27 19:59:02',
                'route' => 'voyager.product-categories.index',
                'parameters' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'menu_id' => 1,
                'title' => 'Templates',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-photo',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 19,
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
                'route' => 'voyager.templates.index',
                'parameters' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'menu_id' => 1,
                'title' => 'Seller Withdrawals',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-wallet',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 20,
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 11:57:24',
                'route' => 'voyager.seller-withdrawals.index',
                'parameters' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'menu_id' => 1,
                'title' => 'Product Orders',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-basket',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 21,
                'created_at' => '2019-08-01 11:51:01',
                'updated_at' => '2019-08-01 11:51:01',
                'route' => 'voyager.product-orders.index',
                'parameters' => NULL,
            ),
        ));
        
        
    }
}