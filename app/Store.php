<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{

    protected $table = 'merch_stores';
    
    public function products(){
        return $this->belongsToMany("App\Product", 'merch_store_products', 'store_id', 'product_id');
    }

}
