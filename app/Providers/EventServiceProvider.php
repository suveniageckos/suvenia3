<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\ProductCategoryCreated' => [
            'App\Listeners\UpdateCategoryFields',
        ],
        'App\Events\Shop\ProductOrdered' => [
            'App\Listeners\Shop\SaveShippingDetails',
            'App\Listeners\Shop\SavePayment',
            'App\Listeners\Shop\SaveOrderItems',
            //'App\Listeners\Shop\SendMail',
            //'App\Listeners\Shop\NotifySeller',
        ],
        'App\Events\Shop\ProductOrderCompleted' => [
            'App\Listeners\Shop\ConfirmPayment',
            //'App\Listeners\Shop\SendMail',
            'App\Listeners\Shop\ConfirmSellerOrder',
            'App\Listeners\Shop\NotifySeller',
            'App\Listeners\Shop\NotifySite',
            'App\Listeners\Shop\ConfirmOrder',
        ],
        '\TCG\Voyager\Events\SettingUpdated'=>[
            'App\Listeners\Admin\Settings\DeleteImage'
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
