@extends('layouts.dashboard_seller')

@section('title', 'Design Requests' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:seller:dashboard:design_services') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:design_services'], 'active') }}">My Orders</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:seller:dashboard:design_request') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:design_request'], 'active') }}">My Design Requests</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead> 
                                <tr>
                                    <th>TITLE</th>
                                    <th>DESCRIPTION</th>
                                    <th>BUDGET</th>
                                    <th></th>
                                </tr> 
                                </thead>
                                <tbody>
                                    @forelse($requests as $request)
                                    <tr>
                                        <td>{{ title_case($request->title) }}</td>
                                        <td>{{ title_case($request->description) }}</td>
                                        <td>&#8358;{{ $request->budget }}</td>
                                        <td><a href="{{ route('app:seller:dashboard:single_design_request', ['id'=> $request->id]) }}" class="text-primary">See Offers</a></td>
                                    </tr>
                                    @empty 
                                    <tr>
                                        <td colspan="100">You have not created any request yet</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                       {{ $requests->links() }}
                </div>
        </div>
   
</div>


@endsection

 