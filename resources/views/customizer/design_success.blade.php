@extends('layouts.customizer')

@section('title', 'Success' )

@section('content')

<div class="uk-container mt-3 mb-3">
        <div class="row justify-content-center">
                <div class="col-6 uk-text-center">
                <img src="{{ asset('img/pay_success.svg') }}" uk-responsive width="100">
                <h3 class="m-2 _pay_status_title">Your product was saved successfully!</h3>
                @if($type == 'sell')
                <p class="m-1 _pay_status_text">Our Team will review your product before it goes live.</p>
                <p class="m-1 _pay_status_text">Meanwhile feel free to explore products from other creators or create another product</p>
                <p class="m-2"><a href="{{ route('app:ecommerce:design') }}" class="btn btn-info-dark">CREATE ANOTHER</a>
                        <a href="{{ route('app:base:index') }}" class="btn btn-info-dark">EXPLORE PRODUCTS</a>
                </p>
                @else
                <p class="m-1 _pay_status_text">Our Team will review your product and probably get in touch with you if need be after you complete your order.</p>
                <p class="m-1 _pay_status_text">Meanwhile feel free to <a href="{{ route('app:base:index') }}">explore</a> products from other creators or <a href="{{ route('app:ecommerce:design') }}">create</a> another product</p>
               
                <p class="m-2"><a href="{{ route('app:ecommerce:cart_review') }}" class="btn btn-info-dark">PROCEED TO CHECKOUT</a>
                </p>

                @endif

                </div>
        </div>
</div>
@endsection