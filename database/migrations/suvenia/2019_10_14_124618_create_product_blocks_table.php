<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductBlocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_blocks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 225)->nullable();
			$table->string('code', 225)->nullable();
			$table->text('product_ids')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_blocks');
	}

}
