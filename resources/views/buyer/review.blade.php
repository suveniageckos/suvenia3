@extends('layouts.dashboard')

@section('title', 'Reviews' )

@section('content')

<div class="row justify-content-center">
        <div class="col-12" id="RatePage">
           
            <table class="uk-table uk-table-small uk-table-divide _saved_item_table mt-4">
                <thead>
                    <tr>
                        <th>REVIEWS</th>
                        <th></th>
                    </tr> 
                </thead>
                <tbody class="bg-white">
                   
                    @forelse($products as $product)
                  
                    <tr class="ml-4 border-bottom">
                        <td>
                                <div class="media">
                                        @php 
                                        $get_meta = json_decode($product->order_item->data, true);
                                        $color = isset($get_meta['color']) ? $get_meta['color'] : '#ffffff';
                                        @endphp

                                        <img src="{{ asset($product->photos->first()->public_url) }}" alt="" class="mr-2" width="100" height="130" style="background-color: {{$color}}">
                                        <div class="media-body">
                                            <p class="saved_item_prod_name m-0 mb-0 uk-text-capitalize">{{ $product->name }}</p>
                                        <p class="saved_item_prod_seller m-0">{{ $product->order_item->order_ref }}</p>
                                        <div class="mt-1">
                                       
                                        @php $rateValues = [1, 2, 3, 4, 5] ;@endphp
                                                <select class="bar-rated" >
                                                        @foreach ($rateValues as $item)
                                                        <option value="{{ $item }}" {{ $product->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                                                        @endforeach
                                                </select>
                                        </div>
                                        <div class="mt-1">
                                            <p class="m-0 saved_item_prod_name">How do you feel about this product? Tell us what you like or dislike</p>
                                        </div>
                                           
                                        </div>
                                    </div>
    
                        </td>
                       
                        <td>
                                @php $__cat = $product->categories->first(); @endphp
                            <a href="{{ route('app:dashboard:add_product_review', ['id'=> $product->id]) }}" class="btn btn-info btn-sm appReviewProduct">REVIEW</a>
                        </td>
                    </tr>

                    @empty
                    <tr class="ml-4 border-bottom uk-text-center">
                            <td><p>You do not have any reviews</p></td>
                    </tr>
                   @endforelse

                </tbody>
            </table>
    
        </div>
      </div>


</div>
     
@endsection