@extends('layouts.dashboard_designer')

@section('title', 'Add Gig' )

@section('content')
<form action="{{ route('app:designer:dashboard:add_gig') }}" method="POST" id="newGigForm" gig-post>
  @csrf
<div id="GigCreate"></div>

<div id="add-edit-gig">
@php
$tabs = [
    [
        'name'=> 'Overview',
        'id'=> '_overview'
    ],
    [
        'name'=> 'Pricing',
        'id'=> '_pricing'
    ],
    [
        'name'=> 'Description',
        'id'=> '_description'
    ],
    [
        'name'=> 'Requirement',
        'id'=> '_requirement'
    ],
    [
        'name'=> 'Gallery',
        'id'=> '_gallery'
    ],
    [
        'name'=> 'Finish',
        'id'=> '_finish'
    ]
];
@endphp


<h6 class="col-md-2 col-6 mb-1 "><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[0]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-lite">
                    Basic Settings
                </div>
                <div class="card-body p-3">

                    <div class="row justify-content-left mt-3">
                    <div class="col-md-12">
                            <form action="" method="post" data-post>
                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">GIG TITLE</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="Make You Title Catchy" class="form-control" name="gig_title" value="">
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">CATEGORY</label>
                                        </div>
                                        <div class="col-md-9">
                                             <select class="form-control" name="gig_category">
                                                 @foreach($categories as $category)
                                                 <option value="{{ $category->id }}">{{ title_case($category->title) }}</option>
                                                 @endforeach
                                             </select>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">SERVICE TYPE</label>
                                            <p class="label-sub m-0">Enter the type of services you provide</p>
                                        </div>
                                        <div class="col-md-9">
                                             <input type="text" class="form-control" placeholder="" name="service_type">
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">SEARCH TAGS</label>
                                            <p class="label-sub m-0">Enter search terms, which you feel buyers will use when looking for your service.</p>
                                        </div>
                                        <div class="col-md-9">
                                                <textarea placeholder="Up to 5 search terms" class="form-control" name="tags"></textarea>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                            </form>
                    </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide">
<div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[1]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
        <div class="row justify-content-center">
        <div class="col-md-12">
                <div class="card mt-3 plan-card">
                        @php
                        $gigs = [
                            [
                                'id'=> 1,
                                'title'=> 'Basic',
                                'amount'=> 2000,
                                'delivery_time'=> 4,
                                'revisions'=> 'unlimited',
                                'is_printable'=> true,
                                'source_file'=> false,
                                'design_concepts'=> '3',
                                'description'=> 'I will give you JPEG, PNG files, free 3D mockup',
                                ],
                            [
                                'id'=> 2,
                                'title'=> 'Standard',
                                'amount'=> 2000,
                                'delivery_time'=> 4,
                                'revisions'=> 'unlimited',
                                'is_printable'=> true,
                                'source_file'=> true,
                                'design_concepts'=> '3',
                                'description'=> 'I will give you JPEG, PNG files, free 3D mockup',
                                ],
                            [
                                'id'=> 3,
                                'title'=> 'Premium',
                                'amount'=> 2000,
                                'delivery_time'=> 4,
                                'revisions'=> 'unlimited',
                                'is_printable'=> true,
                                'source_file'=> true,
                                'design_concepts'=> '3',
                                'description'=> 'I will give you JPEG, PNG files, free 3D mockup',
                                ],

                    ];

                        $gigs_obj = json_decode(json_encode($gigs));
                    @endphp
                         @php
                        $plan_compare_data = [
                            ['name'=> '', 'key'=> 'title'   ],
                            ['name'=> '', 'key'=> 'description' ],
                            ['name'=> 'Print-Ready', 'key'=> 'is_printable' ],
                            ['name'=> 'Source File', 'key'=> 'source_file'  ],
                            ['name'=> 'Design Concepts', 'key'=> 'design_concepts'  ],
                            ['name'=> 'Revision', 'key'=> 'revisions'   ],
                            ['name'=> 'Delivery Time', 'key'=> 'delivery_time'  ],
                            ['name'=> 'Price', 'key'=> 'price'  ],
                           // ['name'=> '', 'key'=> 'button'    ],
                        ];
                        $__compare_data = json_decode(json_encode($plan_compare_data));
                    @endphp
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered compare_table mb-0">

                                <tbody>
                                    @foreach($__compare_data as $compare_data)
                                    <tr>
                                        <th class="compare_title">{{ $compare_data->name }} </th>
                                        @foreach($gigs_obj as $gig)
                                        @php
                                        $kkey = $compare_data->key;
                                        $gigIndex = $loop->index;
                                        @endphp
                                            @if($compare_data->key == 'title')
                                            <td class="cell_price tab-title">
                                                <!--<div class="cell_gig_title">{{ $gig->$kkey }}</div>-->
                                                <textarea placeholder="Name your package" class="form-control tab_form" name="plans[{{$gigIndex}}][plan_name]">{{ $gig->$kkey }}</textarea>
                                            </td>
                                            @elseif($compare_data->key == 'description')
                                            <td><textarea placeholder="Describe your package" class="form-control tab_form" name="plans[{{$gigIndex}}][plan_description]"></textarea></td>

                                            @elseif($compare_data->key == 'revisions')
                                            <td>
                                                <select class="form-control tab_form" name="plans[{{$gigIndex}}][plan_revisions]">
                                                    <option value="4">4</option>
                                                    <option value="6">6</option>
                                                    <option value="10">10</option>
                                                    <option value="12">12</option>
                                                    <option value="unlimited">Unlimited</option>
                                                </select>
                                            </td>
                                            @elseif($compare_data->key == 'design_concepts')
                                            <td>
                                                    <select class="form-control tab_form" name="plans[{{$gigIndex}}][plan_design_concepts]">
                                                            <option value="1">1</option>
                                                            <option value="1-2">1-2</option>
                                                            <option value="1-3">1-3</option>
                                                            {{-- <option value="1-5">1-5</option>
                                                            <option value="6-12">6-12</option>
                                                            <option value="13-17">13-17</option>
                                                            <option value="unlimited">Unlimted</option> --}}
                                                    </select>
                                            </td>
                                            @elseif($compare_data->key == 'is_printable')
                                            @php $check1 = str_random(5); @endphp
                                            <td class="uk-text-center">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1{{$check1}}" name="plans[{{$gigIndex}}][plan_printable]">
                                                    <label class="custom-control-label" for="customCheck1{{$check1}}"></label>
                                                </div>
                                            </td>

                                            @elseif($compare_data->key == 'source_file')
                                            @php $check2 = str_random(5); @endphp
                                            <td class="uk-text-center">
                                                    <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck2{{$check2}}" name="plans[{{$gigIndex}}][source_file]">
                                                            <label class="custom-control-label" for="customCheck2{{$check2}}"></label>
                                                    </div>
                                            </td>

                                            @elseif($compare_data->key == 'delivery_time')

                                            <!--<td class="cell_con">{{ $gig->$kkey }} Days</td>-->
                                            <td><input type="datetime" class="tab_form" name="plans[{{$gigIndex}}][delivery_time]"></td>

                                             @elseif($compare_data->key == 'price')
                                            <td><input type="number" class="form-control tab_form" placeholder="Enter A Price" name="plans[{{$gigIndex}}][price]"></td>

                                            @endif


                                        @endforeach
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
        </div>
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[2]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="m-0 tab_header_text mb-2">Description</h4>
            <textarea class="form-control" id="appEditor1" name="gig_description"></textarea>
        </div>

        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[3]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-3">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="m-0 tab_header_text mb-2">Requirements</h4>
            <textarea class="form-control appEditor" id="appEditor2" name="gig_requirements"></textarea>
        </div>

        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[4]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">

        <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="m-0 tab_header_text mb-1">Previous Work</h4>
            <p class="m-0 tab_header_sub mb-3">Add photos of your past work related to this Gigs to make you stand out</p>
            <div class="card">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="header_file border-bottom p-3">
                        ADD FILE
                    </div>
                    <p class="text_desc_file p-3">Accepted File Types (Max file size: 10MB)
                            We accept the following file types: png, jpg, gif</p>
                    <div class="mt-3 p-3">
                            <form action=""
                            class="dropzone"
                            id="drop-area" upl-url="{{ route('app:designer:dashboard:upload_gallery') }}"></form>
                    </div>
                </div>
                <div class="col-md-6 border-left p-2">
                    <ul id="fileLister" class="list-unstyled"></ul>
                </div>
            </div>
        </div>
        </div>
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[5]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-3">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card uk-text-center p-5">
                <h1 class="m-1 complete_title_text">YOU ARE READY</h1>
                <p class="text_complete_sub">Let’s publish your Gig so buyers can see .</p>
            </div>
        </div>

        </div>
</section>

</form>
@endsection
