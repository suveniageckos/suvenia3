/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/brandable.js":
/*!******************************************!*\
  !*** ./resources/assets/js/brandable.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../ts/Post */ "./resources/assets/ts/Post.ts");

$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('#createUseravatar').on('fileuploaded', function (event, data, previewId, index) {
    var response = data.response;
    $('#file_id').val(response.file_id);
  }); //Brandables

  if ($('#CustomizerEditPage').length > 0) {
    var product_data_front_edit = JSON.parse($(':input[name="front_dimension"]').val());
    var product_data_back_edit = JSON.parse($(':input[name="back_dimension"]').val());
    var baseUrl = $('#CustomizerEditPage').attr('baseAssetUrl');
    console.log(product_data_front_edit);
    $('.DesignAreaContainer > .designArea').css({
      left: product_data_front_edit.left,
      top: product_data_front_edit.top,
      width: product_data_front_edit.width,
      height: product_data_front_edit.height
    }); //$('#frontImagePreview').attr('src', baseUrl+product_data_front_edit.image_url);

    $('.DesignAreaContainerBack > .designAreaBack').css({
      left: product_data_back_edit.left,
      top: product_data_back_edit.top,
      width: product_data_back_edit.width,
      height: product_data_back_edit.height
    }); // $('#backImagePreview').attr('src', baseUrl+product_data_back_edit.image_url);
  }

  var DA = $(".designArea");
  var product_data_front = {
    width: DA.outerWidth(),
    height: DA.outerHeight(),
    left: DA.position().left,
    top: DA.position().top
  };
  var product_data_back = {
    width: DA.outerWidth(),
    height: DA.outerHeight(),
    left: DA.position().left,
    top: DA.position().top
  };
  $('.designArea').draggable({
    containment: ".DesignAreaContainer",
    scroll: false,
    stop: function stop(event, ui) {
      console.log($(this).parent());
      var offset = $(this).position();
      var xPos = offset.left;
      var yPos = offset.top;
      product_data_front['left'] = xPos - 16;
      product_data_front['top'] = yPos - 1;
      str('#FrontDesignAreaDim', product_data_front);
    }
  }).resizable({
    stop: function stop(event, ui) {
      var endW = $(this).outerWidth();
      var endH = $(this).outerHeight();
      product_data_front['width'] = endW;
      product_data_front['height'] = endH;
      str('#FrontDesignAreaDim', product_data_front);
    }
  });
  $('.designAreaBack').draggable({
    containment: ".DesignAreaContainerBack",
    scroll: false,
    stop: function stop(event, ui) {
      var offset = $(this).position();
      var xPos = offset.left;
      var yPos = offset.top; //console.log($(this).parent());

      product_data_back['left'] = xPos - 16;
      product_data_back['top'] = yPos - 1;
      str('#BackDesignAreaDim', product_data_back);
    }
  }).resizable({
    stop: function stop(event, ui) {
      var endW = $(this).outerWidth();
      var endH = $(this).outerHeight();
      product_data_back['width'] = endW;
      product_data_back['height'] = endH;
      str('#BackDesignAreaDim', product_data_back);
    }
  });
  $('#BackViewTrigger').on('change', function () {
    if ($(this).is(':checked')) {
      $('#BackView').find('span').removeClass('overlay');
      $('#BackView').find('input').prop('disabled', false);
    } else {
      $('#BackView').find('span').addClass('overlay');
      $('#BackView').find('input').prop('disabled', true);
    }
  });

  var str = function str(input, data) {
    var data = JSON.stringify(data);
    $(input).val(data);
  };

  function readImage(inputElement) {
    var deferred = $.Deferred();
    var files = inputElement.get(0).files;

    if (files && files[0]) {
      var fr = new FileReader();

      fr.onload = function (e) {
        deferred.resolve(e.target.result);
      };

      fr.readAsDataURL(files[0]);
    } else {
      deferred.resolve(undefined);
    }

    return deferred.promise();
  }

  $('.file-upload').each(function () {
    $(this).on('change', function () {
      var $imgTr = $(this);
      readImage($(this)).done(function (base64Data) {
        var target = $imgTr.attr('data-target'); // var input = $imgTr.attr('data-input');

        $(target).attr('src', base64Data); // $(input).val(base64Data);
      });
    });
  });
});

/***/ }),

/***/ "./resources/assets/ts/Post.ts":
/*!*************************************!*\
  !*** ./resources/assets/ts/Post.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Post = /** @class */ (function () {
    function Post(form) {
        this.form = form;
        this.sendAjax();
    }
    Post.prototype.beforeSend = function (xhr, currentForm) {
        var Spinner = $('body').attr("data-sinner");
        currentForm.find(':input').prop('disabled', true);
        //currentForm.find(':input[type="submit"]').html('<img src="'+Spinner+'" width="20" uk-responsive>');
    };
    Post.prototype.sendAjax = function () {
        var form = $(this.form);
        var cls = this;
        $(document).on('submit', this.form, function (e) {
            e.preventDefault();
            var currentForm = $(e.currentTarget);
            var btnText = currentForm.find(':input[type="submit"]').text();
            var dataForm = new FormData($(e.currentTarget).get(0));
            // let arr = currentForm.serializeArray();
            /* $.each(arr, function(key:any, val:any){
                 dataForm.append(val['name'], val['value']);
             });
             if($('input[type="file"]').length > 0){
                 $('input[type="file"]').each(function(ind, file){
                     console.log($('input[type="file"]'));
                 });
                 console.log((<any>$('input[type="file"]')[0]));
             }*/
            $.ajax({
                url: currentForm.attr('action'),
                type: currentForm.attr('method'),
                data: dataForm,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    cls.beforeSend(xhr, currentForm);
                },
                timeout: 20000
            })
                .fail(function (jqXHR, textStatus, errorThrown) {
                cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
            })
                .done(function (data, textStatus, jqXHR) {
                cls.success(data, textStatus, jqXHR, currentForm, btnText);
            });
        });
    };
    Post.prototype.success = function (data, textStatus, jqXHR, curForm, btnText) {
        if (jqXHR.status == 200) {
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            curForm.find(':input[type="submit"]').text(btnText);
            if (data.message) {
                toastr.success(data.message);
            }
            curForm.find(':input[type="submit"]').text(btnText);
            if (!data.isPaused) {
                var wait = setTimeout(function () {
                    window.location.href = data.redirect_url;
                }, 1000);
            }
            else {
                window.location.href = data.redirect_url;
            }
        }
    };
    Post.prototype.errorHandler = function (jqXHR, textStatus, errorThrown, btnText, curForm) {
        // console.log(jqXHR.status);
        if (jqXHR.status == 500 || jqXHR.status == 422) {
            var data = JSON.parse(jqXHR.responseText);
            if (typeof data.errors === 'string' || data.errors instanceof String) {
                toastr.error(data.errors, "Error");
                curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
            }
            else {
                curForm.find('.has-error').removeClass('has-error');
                curForm.find('.help-block').text('');
                $.each(data.errors, function (ind, val) {
                    curForm.find('.' + ind).addClass('has-error');
                    curForm.find('.' + ind + ' .help-block').text(val[0]);
                });
                curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
            }
        }
        else if (jqXHR.statusText == "timeout") {
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            toastr.error('Request Timeout! Please try again.');
            curForm.find(':input').prop('disabled', false);
            curForm.find(':input[type="submit"]').text(btnText);
        }
    };
    return Post;
}());
$(function () {
    var __initPost = new Post("[data-post]");
});


/***/ }),

/***/ 9:
/*!************************************************!*\
  !*** multi ./resources/assets/js/brandable.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\suvenia3\resources\assets\js\brandable.js */"./resources/assets/js/brandable.js");


/***/ })

/******/ });