import Canvas from './canvas';
import * as _ from 'lodash';

class Fabric extends Canvas{

    constructor(){
        super();
        this.InitCanvas();

        if (!localStorage.getItem("mappedData")) {
			this.LoadProduct();
		} else {
			this.LoadProductFromMapped();
        }

        this.changeProduct();
        this.changeProductColor();
        this.switchView();
    }


    public InitCanvas() {
		this.canvas_front = new this.fab.fabric.Canvas('canvas_front');
		this.canvas_back = new this.fab.fabric.Canvas('canvas_back');
    }

    public LoadProductFromMapped() {
		let _data: any = JSON.parse(<any>localStorage.getItem('mappedData'));
		this.activeData = _data;
		if (!_data.backViewEnabled) {
			this.setFrontView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			$('.flip-container').addClass('flipped_front');
			$('#_FlipToggles').hide();
			$('#ProductSelect option[data-name=' + this.slugify(_data.name) + ']').prop("selected", true);
			this.hasBack = false;
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
		} else {
			this.setFrontView(_data);
			this.setBackView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
			this.hasBack = true;
			$('#_FlipToggles').show();
			$('#ProductSelect option[data-name=' + this.slugify(_data.name) + ']').prop("selected", true);
		}
    }

    public LoadProductFromSelect(data: any) {
		let _data = JSON.parse(data);
		this.activeData = _data;
		if (!_data.backViewEnabled) {
			this.setFrontView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.active_canvas_side = 'front';
			$('#_FlipToggles').hide();
			this.hasBack = false;
			this.isDirty = true;
			//this.PrimaryColor = '#FFFFFF';
			this.baseCategory = _data.name;
		} else {
			this.setFrontView(_data);
			this.setBackView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.active_canvas_side = 'front';
			$('#_FlipToggles').show();
			this.isDirty = true;
			this.hasBack = true;
			//this.PrimaryColor = '#FFFFFF';
			this.baseCategory = _data.name;
		}
    }

    public LoadProduct() {
		let _data = JSON.parse(<any>$('#ProductSelect').val());
		this.activeData = _data;
		if (!_data.backViewEnabled) {
			this.setFrontView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.active_canvas_side = 'front';
			$('.flip-container').addClass('flipped_front');
			$('#_FlipToggles').css('display', 'none');
			this.hasBack = false;
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
		} else {
			this.setFrontView(_data);
			this.setBackView(_data);
			this.hasBack = true;
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			$('#_FlipToggles').css('display', 'block');
			this.active_canvas_side = 'front';
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
		}
    }

    public setFrontView(data: any): void {
		const _Pdiv = $('#side_front');
		const _CanvD = $('#' + this.DesDiv + '_front');
		_Pdiv.find('.ProductFacing').attr('src', this.baseUrl + '/storage/' + data.front.image_url);
		_CanvD.css({
			'border': '2px solid #ABABAB'
		});
		_CanvD.after('<div class="CanvaDimension"></div>');
		_CanvD.css({
			position: 'relative',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'originX': 'center',
			'originY': 'center'
		});
		_Pdiv.find('.canvas-container').css({
			'position': 'absolute',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'left': data.front.left + 'px',
			'top': data.front.top + 'px',
		});
		_Pdiv.find('.CanvaDimension').text(data.front.width + 'x' + data.front.height + ' Pixels Printable Area. 120 Dpi');
		this.canvas_front.setWidth(data.front.width);
		this.canvas_front.setHeight(data.front.height);
		this.canvas_front.renderAll();
		this.designData.front.img_url = this.baseUrl + '/storage/' + data.front.image_url;
	}


	public setBackView(data: any): void {
		const _PdivB = $('#side_back');
		const _CanvB = $('#' + this.DesDiv + '_back');
		_PdivB.find('.ProductFacing').attr('src', this.baseUrl + '/storage/' + data.back.image_url);
		_CanvB.css({
			'border': '2px solid #ABABAB'
		});
		_CanvB.after('<div class="CanvaDimension"></div>');
		_CanvB.css({
			'position': 'relative',
			'width': data.back.width + 'px',
			'height': data.back.height + 'px',
			'originX': 'center',
			'originY': 'center'
		});
		_PdivB.find('.canvas-container').css({
			'position': 'absolute',
			'width': data.back.width + 'px',
			'height': data.back.height + 'px',
			'left': data.back.left + 'px',
			'top': data.back.top + 'px',
		});
		_PdivB.find('.CanvaDimension').text(data.back.width + 'x' + data.back.height + ' Pixels Printable Area. 120 Dpi');
		this.canvas_back.setWidth(data.back.width);
		this.canvas_back.setHeight(data.back.height);
		this.canvas_back.renderAll();
		this.designData.back.img_url = this.baseUrl + '/storage/' + data.back.image_url;
    }

    public switchView() {
		let cls = this;
		$('#_flipFront').on('click', function () {

			if (!$('.flip-container').hasClass('flipped_front')) {
				$('.flip-container').removeClass('flipped_back');
				$('.flip-container').addClass('flipped_front');
				cls.activeCanva = cls.canvas_front;
				cls.activeCanvaDiv = $('#side_front');
				cls.active_canvas_side = 'front';
				$('#_flipBack').removeClass('active');
				$(this).addClass('active');
			}

		});

		$('#_flipBack').on('click', function () {
			if (!$('.flip-container').hasClass('flipped_back')) {
				$('.flip-container').removeClass('flipped_front');
				$('.flip-container').addClass('flipped_back');
				cls.activeCanva = cls.canvas_back;
				cls.activeCanvaDiv = $('#side_back');
				cls.active_canvas_side = 'back';
				$('#_flipFront').removeClass('active');
				$(this).addClass('active');
			}
		});

    }

    public changeProduct(): void {
		let cls = this;
		$('#ProductSelect').on('change', function () {
			localStorage.removeItem("mappedData");
			const __data = $(this).val();
			cls.LoadProductFromSelect(__data);

		});
	}

    public backgroundSetter(front: any, back: any, ChosenColor: any) {
		$(front).find('.ProductFacing').css({
			'background': ChosenColor,
		});
		$(back).find('.ProductFacing').css({
			'background': ChosenColor,
		});

	}


	public changeProductColor() {
		let cls = this;
		const validate = (el: any) => {
			if ($(el).length >= 4) {
				return false;
			}
			return true;
		}
		$(document).on('change', '.product-color-ball', (e) => {
			let Icon = $(e.target).next();

			if ($(e.target).is(':checked')) {
				if (validate($('.icon-checked'))) {
					Icon.addClass('icon-checked');
					var ChosenColor = $(e.target).val();
					cls.backgroundSetter('#side_front', '#side_back', ChosenColor);
					cls.isDirty = true;
					cls.PrimaryColor.push(ChosenColor);
				} else {
					toastr.error('You must select only 4 colours');
				}

			} else {
				Icon.removeClass('icon-checked');
				_.pull(cls.PrimaryColor, $(e.target).val());
				var lastCol = _.last(cls.PrimaryColor);
				cls.backgroundSetter('#side_front', '#side_back', lastCol);
			}

		});

    }

    public slugify(str: string) {
		str = str.replace(/^\s+|\s+$/g, ''); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
		var to = "aaaaaeeeeeiiiiooooouuuunc------";
		for (var i = 0, l = from.length; i < l; i++) {
			str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		}

		str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes

		return str;
	}

}

export default Fabric;
