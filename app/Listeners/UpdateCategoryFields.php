<?php

namespace App\Listeners;

use App\Events\ProductCategoryCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateCategoryFields
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductCategoryCreated  $event
     * @return void
     */
    public function handle(ProductCategoryCreated $event)
    {
        //$event->category->ref = str_random(10);
        $event->category->ref = str_random(10);
        if(request()->is_parent){
            $event->category->parent_id = null;
        }

        if(request()->is_sub){
            $event->category->is_parent = false;
        }
    }
}
