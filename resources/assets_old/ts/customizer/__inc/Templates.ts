class Template{

    public constructor(public Canva: any){
        this.placeTemplate();
    }

    public placeTemplate() {
		let cls = this;
		$(document.body).on('click', '.TemplateSingle', function (e) {
			let pic = $(e.target).attr('src');
			let data = JSON.parse(<any>$(e.target).attr('data'));
			cls.Canva.activeCanva.loadFromJSON($(e.target).attr('data'), function (r: any) {
			}, function (o: any, object: any) {

				if (o.type == 'i-text') {
					o.fontFamily = o.fontFamily;
					let Text = new cls.Canva.fab.fabric.IText(<any>o.text, o);
					cls.Canva.activeCanva.setActiveObject(object);
				}

				cls.Canva.isDirty = true;
				cls.Canva.activeCanva.renderAll();
			});
            UIkit.modal('#TemplateModal').hide();
            let thisObject = cls.Canva.activeCanva.getObjects().indexOf(Text);
            cls.Canva.setObjectLayers({ index: thisObject, icon: 'add-text', text: 'Template' });
		});
	}


}

export default Template;