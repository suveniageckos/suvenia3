<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('role_id')->unsigned()->nullable()->index('users_role_id_foreign');
			$table->string('name', 191);
			$table->string('email', 191)->unique();
			$table->string('avatar', 191)->nullable()->default('users/default.png');
			$table->dateTime('email_verified_at')->nullable();
			$table->string('password', 191);
			$table->string('remember_token', 100)->nullable();
			$table->text('settings', 65535)->nullable();
			$table->timestamps();
			$table->string('ref', 191)->nullable();
			$table->string('username', 191)->nullable();
			$table->string('slug', 191)->nullable();
			$table->string('firstname', 191)->nullable();
			$table->string('lastname', 191)->nullable();
			$table->string('email_token', 191)->nullable();
			$table->dateTime('email_token_expiry')->nullable();
			$table->boolean('has_password')->default(0);
			$table->boolean('email_verified')->default(0);
			$table->string('facebook', 191)->nullable();
			$table->string('twitter', 191)->nullable();
			$table->string('linkedin', 191)->nullable();
			$table->text('photo_url', 65535)->nullable();
			$table->string('phone', 191)->nullable();
			$table->decimal('balance')->default(0.00);
			$table->decimal('earnings')->default(0.00);
			$table->string('bank_sort', 191)->nullable();
			$table->string('bank_bvn', 191)->nullable();
			$table->string('bank_account', 191)->nullable();
			$table->string('bank_name', 191)->nullable();
			$table->string('transfer_code', 225)->nullable();
			$table->dateTime('last_login')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
