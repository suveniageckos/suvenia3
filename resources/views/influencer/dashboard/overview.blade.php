@extends('layouts.dashboard_influencer')

@section('title', 'Overviews' )

@section('content')


<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 mb-3">
           <span class="uk-h2"> Dashboard</span>

        </div>
    </div>


    <div class="row">
        {{-- User Profile --}}
            <div class="col-md-4">

                    <div class="uk-child-width-1-1@m" uk-grid>
                            <div>

                                <div class="uk-card uk-card-default">
                                    <div class="uk-card-media-top">
                                    <img class="uk-align-center uk-padding-small uk-border-circle" src="{{ Auth::guard('influencers')->user()->cover_photo_url }}" alt="" width="250">
                                    </div>
                                    <a href="{{ route('app:user:social', ['provider'=>'instagram']) }}" class="btn btn-info">Connect your Instagram Account</a>
                                    <div class="uk-card-body">
                                            <div class="text-bold-order-big">Social</div>
                                            <div class="d-flex">
                                                <div class="p-2"><i class="fa fa-chain""></i> Instagram </div><div class="p-2 ml-auto"><a href="{{ Auth::guard('influencers')->user()->instagram_link }}"><small>{{ substr(Auth::guard('influencers')->user()->instagram_link,0,28) }}</small></a></div>
                                            </div>
                                            <div class="d-flex">
                                                <div class="p-2"><i class="fa fa-users""></i> Followers </div><div class="p-2 ml-auto">{{ Auth::guard('influencers')->user()->instagram_followers }}</div>
                                            </div>
                                            <div class="d-flex">
                                                <div class="p-2"><i class="fa fa-heart""></i> Engagement </div><div class="p-2 ml-auto">{{ Auth::guard('influencers')->user()->instagram_engagements }}%</div>
                                            </div><br><br>
                                            <hr>

                                            <div class="d-flex">
                                            <div class="p-2 text-bold-order-big">Account Balance </div><div class="p-2 ml-auto withdraw-link"><a href="{{ route('app:influencer:dashboard:withdraw') }}">Withdraw <i class="fa fa-arrow-right""></i></a></div>
                                                </div>

                                                <p class="text-bold-order-bigger m-2">{{ Auth::guard('influencers')->user()->balance }} NGN</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
            </div>

            {{-- Available Jobs --}}
       <div class="col-md-8">
         <div class="uk-card uk-card-default uk-width-1-1@m">
            <div class="uk-card-header">
               <div class="uk-grid-small uk-flex-middle" uk-grid>
                    <div class="uk-width-expand ">
                      <div class="d-flex">
                         <div class="p-2 text-bold-order-bigger">Job Feeds </div>
                           <div class="p-2 ml-auto">
                            <div class="uk-inline">
                               <button class="uk-button uk-button-default all-jobs-button" type="button">All Jobs  <span uk-icon="icon: chevron-down"></span></button>
                                <div uk-dropdown="mode: click">
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li class="uk-active">
                                            <a href="#">Opened Jobs</a>
                                        </li>
                                        <li><a href="#">Closed Jobs</a></li>
                                    </ul>
                                </div>


                                 </div>
                             </div>
                         </div>
                    </div>
                 </div>
            </div>
            <div class="uk-card-body">
                <ul class="uk-list uk-list-divider">
                    @forelse ($seller_orders as $seller_order)
                    <li>
                    @if(!$seller_order->gig->is_closed)
                        @php
                            $modal_title = $seller_order->gig->promotion_format == "VIDEO" ? "Video Promotion" : "Photo Promotion";
                         @endphp
                            <a href="javascript:;"  modal-form data-title="{{ $modal_title }}" data-url="{{ route('app:influencer:dashboard:gig_modal', ['id'=> $seller_order->gig->id]) }}">
                        <div class="uk-text-center" uk-grid>
                                <div class="uk-width-auto">
                                <img class="image-border-size" src="{{ asset('storage/'.$seller_order->gig->product->photos->first()->public_url) }}" alt="image">
                                </div>
                                <div class="uk-width-expand">
                                    <div class="float-left col-md-9">
                                    <p class="text-bold-order-big uk-text-left">{{ $seller_order->gig->product->name }}</p>
                                        <p class="text-muted-small uk-text-left">{{ str_limit($seller_order->gig->product->description, 60) }} </p>
                                    </div>
                                    <div class="float-right col-md-3">
                                        <div class="text-bold-order-big">&#8358; {{ $seller_order->gig->product->price }}</div>

                                             @if($seller_order->gig->is_closed == 0)
                                                <div class="text-color-green">Open</div>
                                            @else
                                                <div class="text-color-red">Close</div>
                                            @endif
                                            <br>
                                    <div style="font-size:13px;">{{ $seller_order->gig->created_at->diffForHumans() }}</div>
                                    <div><i uk-icon="icon: users"></i> {{ $seller_order->gig->number_of_influencers ? $seller_order->gig->number_of_influencers . ' influencers' : '0 influencer' }} </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                            @else
                            <div class="uk-text-center" uk-grid>
                                    <div class="uk-width-auto">
                                    <img class="image-border-size" src="{{ asset('storage/'.$seller_order->gig->product->photos->first()->public_url) }}" alt="image">
                                    </div>
                                    <div class="uk-width-expand">
                                        <div class="float-left col-md-9">
                                        <p class="text-bold-order-big uk-text-left">{{ $seller_order->gig->product->name }}</p>
                                            <p class="text-muted-small uk-text-left">{{ str_limit($seller_order->gig->product->description, 60) }} </p>
                                        </div>
                                        <div class="float-right col-md-3">
                                            <div class="text-bold-order-big">N {{ $seller_order->gig->product->price }}</div>

                                                 @if($seller_order->gig->is_closed == 0)
                                                    <div class="text-color-green">Open</div>
                                                @else
                                                    <div class="text-color-red">Close</div>
                                                @endif
                                                <br>
                                        <div style="font-size:13px;">{{ \Carbon\Carbon::parse($seller_order->gig->closed_at)->diffForHumans() }}</div>
                                            <div><i uk-icon="icon: users"></i> {{ $seller_order->gig->number_of_influencers ? $seller_order->gig->number_of_influencers . ' influencers' : '0 influencer' }} </div>


                                        </div>
                                    </div>
                                </div>
                                @endforelse
                        </li>

                        @empty
                        <div class="col-md-12 uk-text-center">
                            <p>Ooops.. no jobs at this time!</p>
                        </div>
                    @endforelse

                     </ul>
                </div>

            </div>

        </div>
    </div>


{{-- Recent Jobs --}}

<div class="row upper-space">
    <div class="col-md-8">
            <div class="uk-card uk-card-default uk-width-1-1@m">
               <div class="uk-card-header">
                  <div class="uk-grid-small uk-flex-middle" uk-grid>
                       <div class="uk-width-expand ">
                         <div class="d-flex">
                            <div class="p-2 text-bold-order-bigger">Recent Jobs</div>
                            </div>
                       </div>
                    </div>
               </div>
               <div class="uk-card-body">

                    <ul class="uk-list uk-list-divider">
                            @forelse ($recent_jobs as $recent_job)
                            <li>
                                <div class="uk-text-center" uk-grid>
                                        <div class="uk-width-auto">
                                        <img class="image-border-size" src="{{ asset('storage/'.$recent_job->gig->product->photos->first()->public_url) }}" alt="image">
                                        </div>
                                        <div class="uk-width-expand">
                                            <div class="float-left col-md-9">
                                            <p class="text-bold-order-big uk-text-left">{{ $recent_job->gig->product->name }}</p>
                                                <p class="text-muted-small uk-text-left">{{ str_limit($recent_job->gig->product->description, 60) }} </p>
                                            </div>
                                            <div class="float-right col-md-3">
                                                <div class="text-bold-order-big">N {{ $recent_job->gig->product->price }}</div>

                                                     @if($recent_job->gig->job_status== 1)
                                                        <div class="text-color-green">Completed</div>
                                                    @else
                                                        <div class="text-color-red">Uncompleted</div>
                                                    @endif
                                                    <br>
                                            <div>{{ $recent_job->gig->created_at->diffForHumans() }}</div>

                                            </div>
                                        </div>
                                    </div>
                                </li>

                                @empty
                                <div class="col-md-12 uk-text-center">
                                    <p>You have no recent job!</p>
                                </div>
                            @endforelse

                             </ul>

                </div>
               </div>

        </div>

        <div class="col-md-4">

                <div class="uk-card uk-card-default uk-width-1-1@m">
                        <div class="uk-card-header">
                           <div class="uk-grid-small uk-flex-middle" uk-grid>
                                <div class="uk-width-expand ">
                                  <div class="d-flex">
                                     <div class="p-2 text-bold-order-bigger">Hot News</div>
                                     </div>
                                </div>
                             </div>
                        </div>
                        <div class="uk-card-body">


                                <ul class="uk-list uk-list-divider">
                                        <li>
                                        <div class="uk-text-center" uk-grid>
                                                <div class="uk-width-auto">
                                                <img class="image-news-border-size" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTogL_kyrSC464D5WLSailNgLZ2KdXx7n3C-vqxe49hvuUQgAVOlw" alt="image">
                                                </div>
                                                <div class="uk-width-expand">
                                                    <div class="float-left">
                                                        <p class="text-bold-order-news uk-text-left">Best way to work with colors</p>
                                                        <p class="text-muted-small uk-text-left">Lorem ipsum dolor dolore magna aliqua. </p>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>

                                </ul>
                         </div>
                        </div>
        </div>
</div>







</div>

{{-- <div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card uk-animation-slide-top-small">

                    <div class="card-body">
                        <div class="row dime_row">
                          @foreach($dimers as $dimer)
                            <div class="col-md-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div>
                                    <img src="{{ asset($dimer->icon) }}" width="100" uk-responsive>
                                    </div>
                                    <div class="uk-text-center ml-2">
                                    <p class="dime_counter m-0">{{ $dimer->value }}</p>
                                    <p class="dime_desc">{{ $dimer->title }}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                            <div class="col-md-6 mb-2 uk-text-right">
                                <div class="d-flex justify-content-between">
                                    <div></div>
                                    <div>
                                            <div class="uk-text-center">
                                                    <p class="m-1">Post Something New</p>
                                                        <a href="{{ route('app:influencer:dashboard:my_posts')}}" class="btn btn-info">POST</a>
                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-9">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite">
                        <h4 class="header_text m-0">Overview</h4>
                    </div>

                    <div class="card-body p-0">

                        <div class="p-4">
                            <canvas id="bar-chart" width="100%" height="50"  data-months="{{ json_encode($month_names) }}" data-stats="{{ json_encode($month_stats) }}"></canvas>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                    <div class="card mb-4 p-4 uk-text-center">
                        <p class="balance_dimer">{{ Auth::guard('influencers')->user()->balance }}</p>
                        <p class="dimer_desc_grey">Account Balance</p>
                        <p><a href="{{ route('app:influencer:dashboard:withdraw') }}" class="btn btn-info">WITHDRAW</a></p>
                    </div>
            </div>
    </div>

    <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite">
                        <h4 class="header_text m-0">Gigs Completed</h4>
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Order Date</th>
                                    <th>Customer</th>
                                    <th>Transaction Ref</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                        @foreach ($seller_orders as $order)

                                        <tr>
                                        <td>{{ \Carbon\Carbon::parse($order->created_at)->toDayDateTimeString() }}</td>
                                            <td>{{ $order->customer->username }}</td>
                                            <td>{{ $order->reference }}</td>
                                            <td>{{ $order->amount }}</td>
                                            <td><span class="_delivery_status">{{ isset($order->status) ? $utils->set_shipping_status($order->status) : '' }} </span></td>


                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
    </div>

</div> --}}

@endsection
