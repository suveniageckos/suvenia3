<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoOrderMetricsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_order_metrics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('order_id');
			$table->integer('payment_id');
			$table->integer('product_id');
			$table->string('user_profit', 191);
			$table->string('site_profit', 191);
			$table->string('expenses', 191);
			$table->timestamps();
			$table->integer('order_item_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_order_metrics');
	}

}
