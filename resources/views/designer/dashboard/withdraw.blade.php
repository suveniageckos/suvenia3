@extends('layouts.dashboard_designer')

@section('title', 'Withdrawal History' )

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <h4 class="header_text m-0">Overview</h4>
                </div>
                <div class="card-body">
                    <div class="row dime_row">
                        @foreach($dimers as $dimer)
                        <div class="col-md-6 uk-text-center">
                            <p class="dime_counter m-0">{{ $dimer->value }}</p>
                            <p class="dime_desc">{{ $dimer->title }}</p>
                        </div>
                        @endforeach
                    </div> 
                </div> 
        </div>
    </div>
    <div class="col-md-4 mb-3">
        <a href="{{ route('app:designer:dashboard:bank_details')}}" class="btn btn-info">UPDATE BANK DETAILS</a>
    </div>
</div>

<div class="row">
        <div class="col-md-12">
            <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-info">
                   <h4 class="header_text m-0">Request Withdrawal</h4>
                </div>

                <div class="card-body">
                        <form action="{{ url()->current() }}" method="POST" data-post>
                            @csrf
                                <div class="form-group amount">
                                        <label for="amount">Amount</label>
                                <input type="number" class="form-control"  placeholder="Amount" name="amount" value="">
                                        <span class="help-block"></span>
                                </div>
                                
                                <div class="form-group">
                                        <button class="btn btn-info">Submit</button>
                                </div>

                    </form>
                </div>
            </div>
        </div> 
</div>

        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-info">
                        <h4 class="header_text m-0">Withdrawal History</h4>
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Transaction Ref</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($withdrawals as $withdrawal)
                                
                                <tr>
                                <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->toDayDateTimeString() }}</td>
                                    <td>{{ $withdrawal->ref }}</td>
                                    <td>&#8358;{{ $withdrawal->amount }}</td>
                                    <td>
                                        @if($withdrawal->status == 0)
                                        <div class="btn status-btn failed">FAILED</div>
                                        @elseif($withdrawal->status == 1)
                                        <div class="btn status-btn pending">PENDING</div>
                                        @elseif($withdrawal->status == 2)
                                        <div class="btn status-btn complete">PAID</div>
                                        @endif

                                    </td>
                                
                                   
                                </tr>
                                @empty
                                <tr><td colspan="4">You Have Not Made Any Withdrawal</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $withdrawals->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection