@php
  $menus = collect([
    //   [
    //       'title'=> 'My Dashbooard',
    //       'route'=> 'app:dashboard:index',
    //       'icon'=> 'icon-speedometer'
    //   ],
      [
          'title'=> 'My Orders',
          'route'=> 'app:dashboard:my_orders',
          'icon'=> 'icon-basket-loaded'
      ],
      [
          'title'=> 'Payment History',
          'route'=> 'app:dashboard:payment_history',
          'icon'=> 'icon-credit-card'
      ],
      [
          'title'=> 'Saved Items',
          'route'=> 'app:dashboard:saved_items',
          'icon'=> 'icon-plus'
      ],
      [
          'title'=> 'Reviews',
          'route'=> 'app:dashboard:reviews',
          'icon'=> 'icon-like'
      ],
      [
          'title'=> 'Shipping Information',
          'route'=> 'app:dashboard:shipping',
          'icon'=> 'icon-plane'
      ],
      [
          'title'=> 'Profile Settings',
          'route'=> 'app:dashboard:profile_settings',
          'icon'=> 'icon-user'
      ]

  ]);
@endphp

@if(isset($type) && $type == 'admin_dropdown')

@foreach ($menus->all() as $item)
    <a href="{{ route($item['route']) }}" class="dropdown-item">
        <i class="icon {{ $item['icon']}}"></i> {{ $item['title']}}
    </a>
@endforeach

@endif

@if(isset($type) && $type == 'admin_sidebar_header')

<li class="nav-item super-header">
        <div class="uk-text-center pt-3 uk-light" style="border-bottom: 1px solid #09628a !important;">
            <img src="{{ Auth::guard('buyers')->user()->photo_url ? asset(Auth::guard('buyers')->user()->photo_url) : asset('user/default.png') }}" class="uk-border-circle m-auto" uk-responsive width="70">
            <p class="text-capitalize mt-2 mb-0">{{ Auth::guard('buyers')->user()->username }}</p>
            <p class="text-capitalize mt-0" style="font-size: 12px;">Buyer</p>
        </div>
</li>

<li class="nav-item super-header">
    <a href="" class="nav-link active">
        Buyer Dashboard <i class="icon icon-minus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endif

@if(isset($type) && $type == 'admin_sidebar_content')
@foreach ($menus->all() as $item)
<li class="nav-item">
        <a href="{{ route($item['route']) }}" class="nav-link">
            <i class="icon {{ $item['icon']}}" style="font-size: 14px;"></i> {{ $item['title']}}
        </a>
</li>
@endforeach

@endif

@if(isset($type) && $type == 'front_end')
@foreach ($menus->all() as $item)
<li class=""><a href="{{ route($item['route']) }}" target="_blank">{{ $item['title'] }}</a></li>
@endforeach
@endif

@if(isset($type) && $type == 'admin_sidebar_footer')

<li class="nav-item super-header">
    <a href="{{ route('app:dashboard:design_services') }}" class="nav-link active">
        Design Services <i class="icon icon-cheveron-right" style="font-size: 14px; float:right;"></i>
    </a>
</li>

<!--<li class="nav-item super-header">
    <a href="{{ route('app:seller:dashboard:index') }}" class="nav-link active">
        Seller <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
<li class="nav-item super-header">
    <a href="{{ route('app:influencer:dashboard:overview') }}" class="nav-link active">
        Influencer <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
<li class="nav-item super-header">
    <a href="{{ route('app:designer:dashboard:index') }}" class="nav-link active">
        Designer <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>-->
@endif
