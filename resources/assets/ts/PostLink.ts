class PostLink{
    _el: any;
    csrf: any;

    public constructor(){
        this._el = $('.post-link');
        this.csrf = $('meta[name="csrf-token"]').attr('content');
        this.setLink();
    };

    public setLink(){
        const cls = this;
        $(document).on('click', '.post-link', (e)=>{
            e.preventDefault();
            var __tar = $(e.currentTarget);
            var id = cls.generateId(7);
            let template = cls.setTemplate(__tar.attr('href'), id, cls.csrf);
            __tar.after(template.join(" "));
            $('#'+id).submit();
        });
    }

    public setTemplate(href: any, id: any, csrf: any){
        var template = [
            '<form id="'+id+'" action="'+href+'" method="POST" style="display: non;">',
            '<input type="hidden" name="_token" value="'+csrf+'">',
            '</form>'
        ];
        return template;
    }

    public generateId(len: any){
        var text = "";

            var charset = "abcdefghjklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ";

            for( var i=0; i < len; i++ )
            text += charset.charAt(Math.floor(Math.random() * charset.length));

            return text;
    }

}
const ___initPostLink = new PostLink();