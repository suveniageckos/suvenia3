<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrdered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\OrderShipping;
use Illuminate\Support\Facades\Auth;

class SaveShippingDetails
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrdered  $event
     * @return void
     */
    public function handle(ProductOrdered $event)
    {
        $order = $event->order;

        $order_shipping = new OrderShipping();
        // $order_shipping->user_id = Auth::user()->id;
        $order_shipping->user_id = Auth::guard('buyers')->user()->id;
        $order_shipping->order_id = $order->id;
        //$order_shipping->order_ref = $order->order_ref;
        $order_shipping->address_id = session('CartShippingAddressID');
        $order_shipping->price = !session('CartShippingPrice') ? 0 : session('CartShippingPrice') ;
        $order_shipping->zone_id = session('CartZoneID');
        $order_shipping->status = 0;
        $order_shipping->save();
    }
}
