<?php
namespace App\Repo\Shipping;

use App\Repo\Shipping\ShippingInterface;

use App\ShippingAddress as Shipping;
use App\ShippingLocation as Location;
use App\ShippingZone as Zone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShippingRepo implements ShippingInterface{

    /**
     * fetchs all shipping address
     *
     * @param mixed $id
     * @return void
     */
    public function get_address($id){

    }

     /**
     * add new address
     *
     * @param Illuminate\Http\Request $data
     * @param int $id
     * @return void
     */
    public function add_address($data, $zone_id){
        $request = $data;

        $shipping = new Shipping();
        $shipping->user_id = Auth::guard('buyers')->user()->id;
        // $shipping->user_id = Auth::user()->id;
        $shipping->location_id = !$request->location ? 0 : $request->location;
        $shipping->zone_id = $zone_id;
        $shipping->price = 3000;
        $shipping->firstname = $request->firstname;
        $shipping->lastname = $request->lastname;
        $shipping->email = $request->email;
        $shipping->address = $request->address;
        $shipping->phone_number = $request->phone_number;
        $shipping->country = $request->location ? "Nigeria" : $request->country;
        $shipping->save();
        return $shipping;
    }

    /**
     * edit shipping address
     *
     * @param [type] $id
     * @return void
     */
    public function edit_address($data, $zone_id, $address_id){
        $request = $data;

        $shipping = Shipping::where(['id' => $address_id])->first();

        $shipping->user_id = Auth::user()->id;
        $shipping->location_id = !$request->location ? 0 : $request->location;
        $shipping->zone_id = $zone_id;
        $shipping->price = 3000;
        $shipping->firstname = $request->firstname;
        $shipping->lastname = $request->lastname;
        $shipping->email = $request->email;
        $shipping->address = $request->address;
        $shipping->phone_number = $request->phone_number;
        $shipping->country = $request->location ? "Nigeria" : $request->country;

        $shipping->save();
        return $shipping;
    }

    /**
     * delete an address
     *
     * @param [type] $id
     * @return void
     */
    public function delete_address($id){
        Shipping::where('id', $id)->delete();
    }


    public function select_address($id){
        DB::table('eco_buyer_addresses')->where([['is_default', true], ['zone_id', $id]])->update(['is_default' => false]);
        $shipping = Shipping::where('id', request()->shipping_address)->first();
        $shipping->is_default = true;
        $shipping->save();
        return $shipping;
    }

    /**
     * fetch a shipping zone
     *
     * @param [type] $id
     * @return void
     */
    public function get_zone($id){

    }

    public function select_zone($id){}

    public function nBetween($varToCheck, $high, $low)
    {
        if ($varToCheck < $low) {
            return false;
        } elseif ($varToCheck > $high) {
            return false;
        } else {
            return true;
        }

    }


}
