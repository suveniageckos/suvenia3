@extends('layouts.dashboard')

@section('title', 'Payment History' )

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                        <div class="card-header card-header-info">
                                <h4 class="header_text m-0">Payment History</h4>
                            </div>
                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead> 
                                <tr>
                                    <th>Date</th>
                                    <th>Transaction Ref</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr> 
                                </thead>
                                <tbody>
                                @forelse ($payments as $payment)
                                
                                <tr>
                                <td>{{ \Carbon\Carbon::parse($payment->created_at)->toDayDateTimeString() }}</td>
                                    <td>{{ $payment->reference }}</td>
                                    <td>&#8358;{{ $payment->amount }}</td>
                                    <td>
                                            @if($payment->status == 0)
                                            <div class="btn status-btn failed">FAILED</div>
                                            @elseif($payment->status == 1)
                                            <div class="btn status-btn pending">PENDING</div>
                                            @elseif($payment->status == 2)
                                            <div class="btn status-btn complete">PAID</div>
                                            @endif
                                    </td>
                                
                                   
                                </tr>
                                @empty
                                <tr><td colspan="4" class="uk-text-center">You Have Not Made Any Payment</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $payments->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection