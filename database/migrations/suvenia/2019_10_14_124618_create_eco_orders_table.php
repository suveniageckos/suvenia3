<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('order_ref', 191);
			$table->string('price', 191);
			$table->boolean('is_invalid')->default(0);
			$table->boolean('status')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_orders');
	}

}
