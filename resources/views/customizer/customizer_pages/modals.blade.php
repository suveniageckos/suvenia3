<div id="ImageModal" uk-modal="bg-close: false">
    <div class="uk-modal-dialog app-modal">
       <button class="uk-modal-close-default" type="button" uk-close></button>
       <div class="uk-modal-header">
          <h2 class="uk-modal-title">Add Image</h2>
       </div>
       <div class="uk-modal-body">
          <p class="m-0 upload-instructions">Accepted File Types (Max file size: 10MB)
             We accept the following file types: png, jpg, pdf
          </p>
          <div class="upload-pane mt-3">
             <div id="ImgUploader"  class="dropzone uk-text-center" uploadUrl="{{ route('app:ecommerce:upload_design') }}">
                <div class="fallback">
                   <input name="file" type="file" multiple />
                </div>
             </div>
             <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>
          </div>
          <div class="uk-margin uk-text-center">
             <label class="m-auto"><input class="uk-checkbox" type="checkbox" checked> <span class="upload-copyright">By Uploading an Image you agree that you hold the right to modify and sell image</span></label>
          </div>
          <div class="submit-pane uk-text-center">
             <button class="btn btn-info-dark" id="SaveUpload">UPLOAD</button>
          </div>
       </div>
    </div>
 </div>
 
 <div id="GraphicModal" uk-modal="bg-close: false">
    <div class="uk-modal-dialog app-modal">
       <button class="uk-modal-close-default" type="button" uk-close></button>
       <div class="uk-modal-header">
          <h2 class="uk-modal-title">Add Graphic</h2>
       </div>
       <div class="uk-modal-body">
          <div id="Artscontent">
             <ul class=" uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-top" uk-grid>
                @foreach($arts as $art)
                @if($art->name !== 'parent')
                <li class="uk-margin-bottom">
                   <div class="uk-card uk-card-small uk-text-center art_single">
                      <p style="margin:0;"><img src="{{ asset('Arts/parent/' . str_slug($art->name, '-') . '.svg') }}" uk-responsive width="100" class="ArtPicParent" data-url="{{ route('app:ecommerce:fetch_arts', ['pid'=> $art->ref]) }}"></p>
                      <p class="art_name text-14" style="display:non; margin:0px;">{{ title_case($art->name) }}</p>
                   </div>
                </li>
                @endif
                @endforeach
             </ul>
             <div class="border-top uk-margin-top uk-text-center" style="text-align:center !important;" id="PaginLink" data-link="{{ route('app:ecommerce:fetch_arts') }}">
                <p></p>
             </div>
          </div>
       </div>
    </div>
 </div>
 <div id="TemplateModal" uk-modal="bg-close: false">
    <div class="uk-modal-dialog app-modal">
       <button class="uk-modal-close-default" type="button" uk-close></button>
       <div class="uk-modal-header">
          <h2 class="uk-modal-title">Add Template</h2>
       </div>
       <div class="uk-modal-body">
          <div id="Templatecontent">
             <ul class=" uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-top" uk-grid>
                @foreach($templates as $template)
                <li class="uk-margin-bottom">
                   <div class="uk-card uk-card-small uk-text-center template_single">
                      <p style="margin:0;"><img src="{{ $template->photo_url }}" uk-responsive width="100" class="TemplateSingle" data-url="{{ route('app:ecommerce:fetch_templates', ['pid'=> $template->ref]) }}" data="{{ $template->data }}"></p>
                      <p class="template_name text-14" style="display:non; margin:0px;">{{ title_case($template->name) }}</p>
                   </div>
                </li>
                @endforeach
             </ul>
          </div>
       </div>
    </div>
 </div>
 </div>