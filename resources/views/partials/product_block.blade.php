@php
$product_block = \App\ProductBlock::where('code', $code)->first();

$_in_season = \App\Product::with(['user','categories'=>function($q){
    $q->where('parent_id', '!=', null);
}, 'photos'])->where('user_type', 'SELLER')->whereIn('id', explode(",", $product_block->product_ids))->get();

@endphp

<div class="uk-container uk-container-small">
    <div class="app-sec-header sec-center">
    <h1 class="title text-body-black">{{ $product_block->title }}</h1>
    </div>

    <div class="row mt-4">

        @foreach($_in_season as $product)

        <div class="col-md-3">
            @include('partials.single_product', ['product'=> $product])
        </div>
        @endforeach

    </div>

</div>
