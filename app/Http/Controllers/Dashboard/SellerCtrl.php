<?php

namespace App\Http\Controllers\Dashboard;

use DB;
use Auth;
use Hash;
use View;
use Validator;
use App\Seller;
use App\OrderItem;
use App\Influencer;
use App\OrderMetric;
use App\Helpers\Utils;
use App\Order as Order;
use App\Store as Store;
use App\AllNotifications;
use App\Design as Design;
use App\Payment as Payment;
use App\Product as Product;
use App\Rules\AmountEquals;
use App\Rules\NumberBetween;
use Illuminate\Http\Request;
use App\Rules\PasswordExists;
use App\Withdraw as Withdraw;
use Illuminate\Validation\Rule;
use App\SellerOrder as SellerOrder;
 use Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class SellerCtrl extends Controller
{
    public $auth;

    public $store_meta = [
        'store_logo_text'=>'',
        'store_logo_font'=>'',
        'store_logo_color'=>'',
        'store_logo'=>'',
        'default_brand'=> 'TEXT',
        'store_theme'=> '',
        'store_theme_color'=> '',
        'store_upload_banner'=> '',
        'store_banner_system'=>'',
        'default_banner'=> 'SYSTEM',
        'products'=>[],
        'featured_products'=>[],
        'display_product'=>[],
    ];

    public function __construct(){
        $this->auth = Auth::guard('sellers');
    }

    public function index(Request $request){
        $month_back = [6,5,4,3,2,1,0];
        $month_names = [];
        $month_stats = [];
        $carbon = \Carbon\Carbon::now();

        $month_back = [6,5,4,3,2,1,0];
        $month_names = [];
        $month_stats = [];
        $carbon = \Carbon\Carbon::now();
        foreach($month_back as $month){
            $month_names[] = $carbon->copy()->startOfMonth()->subMonths($month)->format('F');
            $month_stats[] = SellerOrder::whereMonth('created_at',$carbon->copy()->startOfMonth()->subMonths($month)->format('m'))->where([['user_id', $this->auth->user()->id]])->count();
        }
        $seller_orders = SellerOrder::where('user_id', $this->auth->user()->id)->get();
        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping', 'user', 'order_metrics'])->whereIn('id', $seller_orders->pluck('order_id'))->paginate(10);

        $dimers = [
            [
                'icon'=> 'present',
                'title'=> 'total products',
                'value'=> Product::where([['user_id', $this->auth->user()->id], ['is_archived', false]])->count()
            ],
            [
                'icon'=> 'home',
                'title'=> 'total stores',
                'value'=> Store::where([['user_id', $this->auth->user()->id], ['is_deleted', false]])->count()
            ],
            [
                'icon'=> 'basket-loaded',
                'title'=> 'total orders',
                'value'=> SellerOrder::where([['user_id', $this->auth->user()->id]])->count()
            ],
            [
                'icon'=> 'wallet',
                'title'=> 'amount sold',
                'value'=>  'N' . SellerOrder::where([['user_id', $this->auth->user()->id], ['is_paid', 2]])->sum('user_profit')
            ]


        ];

        return view('seller.admin.home', ['orders'=> $orders, 'dimers'=> json_decode(json_encode($dimers), false), 'month_names'=> $month_names, 'month_stats'=> $month_stats]);
    }

    public function my_products(Request $request){
        $q = $request->q ? $request->q : '';
        $products = Product::with(['order_metric', 'design', 'brandable', 'seller_orders'=>function($q){
            $q->where('is_paid', 2);
        }])->where([['user_id', $this->auth->user()->id], ['user_type', 'SELLER'], ['is_archived', false], ['name', 'like', $request->q . '%']])->paginate(15);

        $dimers = [
            [
                'title'=> 'total product',
                'value'=> Product::where([['user_id', $this->auth->user()->id], ['is_archived', false]])->count()
            ],
            [
                'title'=> 'product order',
                'value'=> SellerOrder::where([['user_id', $this->auth->user()->id]])->count()
            ],
            [
                'title'=> 'profit',
                'value'=> 'N' .  SellerOrder::where([['user_id', $this->auth->user()->id], ['is_paid', 2]])->sum('user_profit')
            ],
        ];
        return view('seller.admin.my_products', ['products'=> $products, 'dimers'=> json_decode(json_encode($dimers), false)]);
    }

    public function fetch_promote_form(Request $request, $id){
            if($request->isMethod('POST')){
            $product = Product::with(['photos'=>function($q){
                $q->first();
            }])->where('id', $id)->first();
            $followers = DB::table('influencer_pricelistings')->get();
            // dd($followers);
             $influencers = Influencer::whereBetween('instagram_followers', explode("-", $followers->first()->follower_range))
             ->limit(4);
            return View::make('seller.admin.promote_form', ['id'=>$id, 'product'=> $product, 'followers'=> $followers, 'influencers'=> $influencers]);

             }
    }

    public function promote(Request $request){
        if($request->isMethod('POST')){
            $validator = Validator::make($request->all(), [
				'description' => 'required|min:10',
				'content_link' => [Rule::requiredIf(function () use ($request) {
                       return  !$request->is_shippable;
                })],
				'promotion_platform' => 'required',
				'promotion_format' => 'required',
				'duration' => 'required',
				'number_of_influencers' => 'required|numeric',
				'number_of_followers' => 'required',
				'audience_category' => 'required',
				'audience_location' => 'required'
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{
            $gig = new \App\InfluencerGig();
            $gig->product_id = $request->product_id;
            $gig->user_id = $this->auth->user()->id;
            $gig->description = $request->description;
            $gig->is_shippable = $request->is_shippable;
            if($gig->is_shippable){
               // $gig->shipping_address = $request->shipping_address;
              $gig->content_link = $request->content_link;
            }
            $gig->promotion_platform = $request->promotion_platform;
            $gig->promotion_format = $request->promotion_format;
            $gig->duration = $request->duration;
            $gig->number_of_influencers = $request->number_of_influencers;
            $gig->number_of_followers = $request->number_of_followers;
            $gig->audience_category = $request->audience_category;
            $gig->audience_location = $request->audience_location;
            $gig->total_price = $request->total_price;
            $gig->save();

             //Notification
             $product = Product::where('id', $gig->product_id)->first();
             $influencers = Influencer::where('instagram_followers', $gig->number_of_followers)->first();
             $photo_url = Photo::where('product_id', $gig->product_id)->first();

             $notify = new AllNotifications();
             foreach($influencers as $influencer){
                 $notify->owner_id = $influencer->id;
                 $notify->user_type = $product->user_type;
                 $notify->message_body = [
                 // 'owner_id' => $product->user_id,
                 'gig_id' => $gig->product_id,
                 'photo_url' =>  $photo_url->public_url,
                 'influencer_id' => Auth::guard('influencers')->user()->id,
                 'influencer_username' => Auth::guard('influencers')->user()->username,
                 'name' => $product->name,
                 'description' => $product->description,
                 'user_type' => $product->user_type,
                 //   'response_type' => 'REJECT',
                 'data' => 'You have a new gig'
             ];

             }

             dd($notify);

            //  $notify->save();


            $order_id = 'SUV-' . str_random(10);

            $paystack = new \Yabacon\Paystack(config('seller_config.paystack_code'));
                 try
                {
                        $tranx = $paystack->transaction->initialize([
                        'amount' => $gig->total_price * 100, // in kobo
                        'email' => $this->auth->user()->email, // unique to customers
                        'reference' => $order_id, // unique to transactions
                    ]);

                    $order = new \App\InfluencerOrder();
                    $order->reference = $order_id;
                    $order->customer_id = $this->auth->user()->id;
                    $order->gig_id = $gig->id;
                    $order->amount = $gig->total_price;
                    $order->status = false;
                    $order->save();

                    $ngig = \App\InfluencerGig::where('id', $gig->id)->first();
                    $ngig->order_id = $order->id;
                    $ngig->save();

                    $payment = new Payment();
                    $payment->user_id = $this->auth->user()->id;
                    $payment->user_type = 'SELLER';
                    $payment->order_id = $order->id;
                    $payment->amount = $gig->total_price;
                    $payment->gateway = 'PAYSTACK';
                    $payment->reference = $order_id;
                    $payment->type = 'INFLUENCER';
                    $payment->save();


                    return response()->json(['message'=> 'Your Product Promotion was saved successfully, Please complete Your Payment to public your request.', 'redirect_url'=> $tranx->data->authorization_url], 200);

                } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                    return response()->json(['error'=>$e->getMessage()], 500);
                }
            }
        }
    }

    public function promote_filter(Request $request){
        if($request->isMethod('POST')){
            $price_listing = DB::table('influencer_pricelistings')->where('id', $request->range_of_followers)->first();
            $influencers = Influencer::whereBetween('instagram_followers', explode("-", $price_listing->follower_range))
                          ->limit($request->number_of_influencers);
            $data = [
                'influencers_range'=> $price_listing->follower_range,
                'influencers_count'=> $influencers->count(),
                'potential_reach'=> number_format($influencers->avg('instagram_followers'), 0),
                'estimated_engagement'=> $influencers->avg('instagram_comments') +  $influencers->avg('instagram_likes')
            ];

            return response()->json(['data'=> $data], 200);
        }
    }

    public function promote_order_callback(Request $request){
        if ($request->reference) {
            $paystack = new \Yabacon\Paystack(config('seller_config.paystack_code'));
            try {

                $tranx = $paystack->transaction->verify([
                    'reference' => $request->reference, // unique to transactions
                ]);
                if ('success' === $tranx->data->status) {
                    $payment = Payment::where('reference', $request->reference)->first();
                    $payment->reference = $request->reference;
                    $payment->status = 2;
                    $payment->save();

                    $order = \App\InfluencerOrder::where('reference', $request->reference)->first();
                    $order->payment_id = $payment->id;
                    $order->status = 2;
                    $order->save();
                    return redirect()->route('app:seller:dashboard:my_products')->with('success', 'Your payment was successfull!');
                }
            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                // return redirect()->route('shop:cart_payment_error');
                abort(500, 'There was an error confirming your payment.');
            }
        }else{
            abort(500);
        }
       // return view('seller.admin.promote_callback', []);
    }

    public function search_product(Request $request){
        if($request->isMethod('post')){
            $q = $request->q;
            return redirect()->route('app:seller:dashboard:my_products', ['q'=> $q]);
        }
    }

    public function delete_product(Request $request){
        if($request->isMethod('post')){
            $ids = collect(json_decode($request->deleteIds, true));
            if($ids->count() > 0){
                $products = Product::whereIn('id', $ids)->update(['is_archived'=> true]);
                return back()->with('success', 'Product(s) deleted successfully!');
            }else{
                return back()->with('error', 'You must select atleast one Product');
            }
        }
    }

    public function edit_product(Request $request, $id){
        if($request->isMethod('post')){
            $product = Product::where('id', $id)->first();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->save();

            $design = Design::where('product_id', $product->id)->first();
            $design->total_earning = $request->earning;
            $design->save();

            return response()->json(['message'=> 'Product Updated Successfully', 'redirect_url'=> route('app:seller:dashboard:product_edit', ['id'=> $product->id]) ], 200);
        }
        if($request->isMethod('get')){
        $product = Product::with(['order_metric', 'design', 'brandable', 'photos'])->where([['id', $id], ['user_id', $this->auth->user()->id], ['is_archived', false]])->first();
        }

        return view('seller.admin.edit_product', ['product'=> $product]);
    }

    public function all_store(Request $request){
        $stores = Store::with('products')->paginate(10);
        return view('seller.admin.all_stores', ['stores'=> $stores]);
    }


    public function fetch_promote_store_form(Request $request, $id){
        if($request->isMethod('POST')){
            $store = Store::where('id', $id)->first();
            $followers = DB::table('influencer_pricelistings')->get();
            $influencers = Influencer::whereBetween('instagram_followers', explode("-", $followers->first()->follower_range));
            return View::make('seller.admin.promote_store', ['id'=>$id, 'store'=> $store, 'followers'=> $followers, 'influencers'=> $influencers]);
        }
    }

    public function promote_store(Request $request){
        if($request->isMethod('POST')){
            $validator = Validator::make($request->all(), [
				'description' => 'required|min:10',
				'content_link' => [Rule::requiredIf(function () use ($request) {
                       return  !$request->is_shippable;
                })],
				'promotion_platform' => 'required',
				'promotion_format' => 'required',
				'duration' => 'required',
				'number_of_influencers' => 'required|numeric',
				'number_of_followers' => 'required',
				'audience_location' => 'required',
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{
            $gig = new \App\InfluencerGig();
            $gig->store_id = $request->store_id;
            $gig->user_id = $this->auth->user()->id;
            $gig->description = $request->description;
            $gig->is_shippable = $request->is_shippable;
            if($gig->is_shippable){
               // $gig->shipping_address = $request->shipping_address;
              $gig->content_link = $request->content_link;
            }
            $gig->promotion_platform = $request->promotion_platform;
            $gig->promotion_format = $request->promotion_format;
            $gig->duration = $request->duration;
            $gig->number_of_influencers = $request->number_of_influencers;
            $gig->number_of_followers = $request->number_of_followers;
            $gig->audience_location = $request->audience_location;
            $gig->total_price = $request->total_price;
            $gig->save();

            $order_id = 'SUV-' . str_random(10);

            $paystack = new \Yabacon\Paystack(config('seller_config.paystack_code'));
                 try
                {
                    $tranx = $paystack->transaction->initialize([
                        'amount' => $gig->total_price * 100, // in kobo
                        'email' => $this->auth->user()->email, // unique to customers
                        'reference' => $order_id, // unique to transactions
                    ]);

                    $order = new \App\InfluencerOrder();
                    $order->reference = $order_id;
                    $order->customer_id = $this->auth->user()->id;
                    $order->gig_id = $gig->id;
                    $order->amount = $gig->total_price;
                    $order->status = false;
                    $order->save();

                    $ngig = \App\InfluencerGig::where('id', $gig->id)->first();
                    $ngig->order_id = $order->id;
                    $ngig->save();

                    $payment = new Payment();
                    $payment->user_id = $this->auth->user()->id;
                    $payment->user_type = 'SELLER';
                    $payment->order_id = $order->id;
                    $payment->amount = $gig->total_price;
                    $payment->gateway = 'PAYSTACK';
                    $payment->reference = $order_id;
                    $payment->type = 'INFLUENCER';
                    $payment->save();

                    return response()->json(['message'=> 'Your Product Promotion was saved successfully, Please complete Your Payment to pulbic your request.', 'redirect_url'=> $tranx->data->authorization_url], 200);

                } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                    return response()->json(['error'=>$e->getMessage()], 500);
                }
            }
        }
    }

    public function promotions()
    {
      $promotions = \App\InfluencerGig::where('user_id', $this->auth->user()->id)->get();
      return view('seller.admin.promotions', ['promotions'=> $promotions]);


        // $user = Seller::where('id', $this->auth->user()->id)->first();
        // $user = Seller::all();
        // foreach($user as $my){
        // $id = $my->id;

        // $my->notify(new Testing(Seller::findOrFail($id)));
        // }


        // $seller_auth = Auth::guard('seller')->user()->id;
        //$store->user_id = $this->auth->user()->id;

        // $details = [
        //      'user_id' => $this->$user->id,
        //      'username' => $this->$user->surname,
        //      'photo_url' => $this->$user->photo_url,
        //      'user_type' => 'SELLER',
        //      'message' => 'You just got a promotional gig',
        //      'email' => $user->email
        // ];

        //  Notification::send($user, new Testing($details));
        // auth()->user()->notify(new Testing($details));

        // return view('seller.admin.promotions');
    }




    public function delete_store(Request $request, $id){
        if($request->isMethod('POST')){
            $stores = Store::where('id', $id)->first();
            if(!$stores){
                return back()->with('error', 'You must select at least one Product');
            }else{
                $stores->delete();
                return back()->with('success', 'Store(s) deleted successfully!');
            }

        }

        return view('seller.admin.all_stores', ['stores'=> $stores]);
        if($request->isMethod('POST')){
            $ids = collect(json_decode($request->deleteIds, true));
            if($ids->count() > 0){
                $products = Product::whereIn('id', $ids)->update(['is_archived'=> true]);
                return back()->with('success', 'Product(s) deleted successfully!');
            }else{
                return back()->with('error', 'You must select atleast one Product');
            }
        }
    }



    public function create_store(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
				'name' => 'required|unique:merch_stores',
                'description' => 'required|min:10',
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{

                $store = new Store();
                $store->user_id = $this->auth->user()->id;
                $store->name = $request->name;
                $store->slug = str_slug($request->name);
                $store->store_url = 'https://suvenia.com/store/'.str_slug($request->name);
                $store->ref = str_random(10);
                $store->description = $request->description;
                $store->meta = json_encode($this->store_meta);
                $store->save();
                return response()->json(['message'=> 'Store created successfully!', 'redirect_url'=> route('app:seller:dashboard:all_store')], 200);
            }
        }
    }

    public function edit_store(Request $request, $id){
        $store = Store::where('id', $id)->first();
        $products = Product::get();

        // $client = new \GuzzleHttp\Client();
        // $request = $client->get('https://api.unsplash.com/photos/?client_id=6daae1fe9860dd3865a9b81ecb3bdaf7151b3dbe9d6ae500af5a6d2d670f565b');
        // $response = $request->getBody();
        // $all_images = json_decode($response);

        $query = 'shopping';
        $page = 1;
        $clientID = '6daae1fe9860dd3865a9b81ecb3bdaf7151b3dbe9d6ae500af5a6d2d670f565b';
        $per_page = 15;
        $orientation = 'landscape';

        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://api.unsplash.com/search/photos?orientation='.$orientation.'&page='.$page.'&query='.$query.'&client_id='.$clientID.'');
        $response = $request->getBody();
        $search = json_decode($response);
        $all_images = $search->results;
        // $all_images = [];

        //  dd($all_images);
       return view('seller.admin.edit_store', ['store'=> $store, 'products'=> $products, 'defaultBanners'=> $all_images]);
    }

    public function view_store(Request $request, $id){
        $store = Store::where('id', $id)->first();
        return view('seller.store_view', ['store'=> $store]);
    }

    public function store_header_edit(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $validator = Validator::make($request->all(), [
				'name' => ['required', Rule::unique('merch_stores')->ignore($store->id)],
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{

            $meta['store_logo_text'] = $data['store_logo_text'];
            $meta['store_logo_font'] = $data['store_logo_font'];
            $meta['store_logo_color'] = $data['store_logo_color'];
            $meta['store_theme_color'] = $data['store_theme_color'];
            $meta['store_theme'] = $data['store_theme'];
            $meta['default_brand'] = $data['default_brand'];
           if(isset($data['store_logo_temp'])){
            $Imageurl  = $this->save_base64($store->ref, public_path('store_img/logos/'),'logos', $data['store_logo_temp']);
            $meta['store_logo'] = $Imageurl;
           }else{
            $meta['store_logo'] = isset($data['store_logo']) ? $data['store_logo'] : '';
           }
            $store->name = $request->name;
            $store->slug = str_slug($request->name);
            $store->meta = json_encode($meta);
            $store->save();
           return response()->json(['message'=> 'Store updated successfully!'], 200);

         }

        }
    }

    public function store_banner_edit(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $cropper = json_decode($request->cropper, true);
            $path = 'app/public/store_img/' . $store->ref . '/' . 'banner-' . $store->ref . '.png';

            if ($request->hasFile('file')) {
                 if(!file_exists(storage_path('app/public/store_img/' . $store->ref))){
                    mkdir(storage_path('app/public/store_img/' . $store->ref));
                 }
                \Image::make($request->file('file'))->crop(intval($cropper['width']), intval($cropper['height']), intval($cropper['x']), intval($cropper['y']))->save(storage_path($path));
                $meta['store_upload_banner'] = asset(Storage::url('store_img/' . $store->ref . '/' . 'banner-' . $store->ref . '.png'));
                $meta['default_banner'] = 'USER';
                // if(isset($data['system_store_banner']) && $data['default_banner'] == 'SYSTEM'){
                //     $meta['store_banner_system'] = $data['system_store_banner'];
                // }

                // $meta['default_banner'] = $data['default_banner'];
                $store->meta = json_encode($meta);
                $store->save();
                return response()->json(['message'=> 'Store updated successfully!'], 200);
            }else{

                $meta['store_banner_system'] = $data['system_store_banner'];

                 $meta['default_banner'] = $data['default_banner'];
                 $store->meta = json_encode($meta);
                 $store->save();
                 return response()->json(['message'=> 'Store updated successfully!'], 200);
            }
          //  $manager = new ImageManager(array('driver' => 'imagick'));

            //return response()->json(['message'=> $request->all()], 200);
        }
    }


    public function store_products_edit(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $meta['products'] = isset($data['store_products']) ? $data['store_products'] : [] ;
            $store->meta = json_encode($meta);
            $store->save();
            return response()->json(['message'=> 'Store updated successfully!'], 200);
        }
    }

    public function store_about(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $validator = Validator::make($request->all(), [
				'facebook' => 'present',
				'twitter' => 'present',
				'instagram' => 'present',
				'website' => 'present',
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{
                $store->description = $request->description;
                $store->facebook = $request->facebook;
                $store->twitter = $request->twitter;
                $store->instagram = $request->instagram;
                $store->website = $request->website;
                $store->save();
                return response()->json(['message'=> 'Store updated successfully!'], 200);
            }
        }
    }

    public function edit_store_save_image(Request $request){
        if($request->isMethod('post')){
            $files = $request->file('files');
            foreach($files as $file){
                $img = file_get_contents($file->getPathName());
                $type = $file->getMimeType();
                $data = 'data:' . $type . ';base64,' . base64_encode($img);
                return response()->json(['img'=> $data ], 200);
            }
        }
    }

    public function save_base64($filename, $path, $sub_folder, $data){
        $imga = substr($data, 5, strpos($data, ';')-5);
        $type = explode('/', $imga);
        if(!file_exists($path . $filename)){
            mkdir($path . $filename);
        }else{
            $old_files = glob($path . $filename . '/*');

            //Loop through the file list.
            foreach($old_files as $file){
            //Make sure that this is a file and not a directory.
            if(is_file($file)){
            //Use the unlink function to delete the file.
            unlink($file);
            }
            }
        }
        $image_path = file_put_contents($path . $filename . '/' . $filename . '.' . $type[1], base64_decode(explode(',', $data)[1]));
        return $sub_folder . '/' . $filename . '/' . $filename . '.' . $type[1];
    }

    public function my_orders(Request $request){
        $duration = isset($request->dur) ? \Carbon\Carbon::now()->subDays(intval(base64_decode($request->dur))) : \Carbon\Carbon::now()->subDays(0);

        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping'])->whereDate('created_at', '<=', $duration)->where('user_id', $this->auth->user()->id )->paginate(10);

        return view('seller.admin.my_orders', ['orders'=> $orders]);
    }

    public function customers_orders(Request $request){
        $seller_orders = SellerOrder::where('user_id', $this->auth->user()->id)->get();
        $duration = isset($request->dur) ? \Carbon\Carbon::now()->subDays(intval(base64_decode($request->dur))) : \Carbon\Carbon::now()->subDays(0);
        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping', 'user', 'order_metrics'])->whereDate('created_at', '<=', $duration)->whereIn('id', $seller_orders->pluck('order_id'))->paginate(10);

        return view('seller.admin.customer_orders', ['orders'=> $orders]);
    }


    public function profile_settings(Request $request){
        if($request->isMethod('post')){
            $user = Seller::where('id', $this->auth->user()->id)->first();

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|min:4|unique:sellers,username,' . $user->id,
                'firstname' => 'max:255|min:2',
                'lastname' => 'max:255|min:2',
                'phone' => 'digits:11',
                'bio' => 'max:300|min:20',
            ]);

            if ($validator->fails()) {

                return response()->json(['errors'=> $validator->errors()], 500);

            } else {
                $seller = Seller::where('id', $this->auth->user()->id)->first();
                $seller->username = $request->username;
                $seller->slug = str_slug($request->username, '-');
                $seller->phone = $request->phone;
                $seller->firstname = $request->firstname;
                $seller->lastname = $request->lastname;
                $seller->bio = $request->bio;

                if ($request->hasFile('image') && $request->file('image')->isValid()) {
                    if(Storage::exists($seller->image_path)){
                        Storage::delete($seller->image_path);
                    }
                    $image_path = Storage::putFile('sellers', $request->file('image'));
                    $seller->photo_url = Storage::url($image_path);
                    $seller->image_path = $image_path;
                }

                if ($seller->save()) {
                    return response()->json(['message'=> 'Account settings updated successfully', 'redirect_url'=> route('app:seller:dashboard:profile_settings')], 200);
                }
             }
           }

        return view('seller.admin.profile_settings', []);
    }

    public function change_password(Request $request){

        if($request->isMethod('post')){
            $seller = Seller::where('id', $this->auth->user()->id)->first();

            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'password' => ['max:255', 'min:4', function($attribute, $value, $fail)use($seller) {
                        if (!Hash::check($value, $seller->password)) {
                            return $fail('Password does not match your old password.');
                        }
                    }],
                    'confirm_password' => 'required|min:4|same:password',
                ]);

                if ($validator->fails()) {
                    return response()->json(['errors'=> $validator->errors()], 500);
                } else {

                    $seller = Seller::where('id', $this->auth->user()->id)->first();
                    $seller->password = bcrypt($request->password);
                    if ($seller->save()) {
                        return response()->json(['message'=> 'Password setting updated successfully', 'redirect_url'=> route('app:seller:dashboard:change_password')], 200);
                    }
                }

            }
        }

        return view('seller.admin.change_password', []);
    }

    public function bank_details(Request $request){
        if($request->isMethod('post')){
			$utils = new Utils();
			$seller = Seller::where('id', $this->auth->user()->id)->first();
			$validator = Validator::make($request->all(), [
				'bank' => 'required',
				'bank_name' => 'required',
				//'bank_bvn' => 'required|digits:11',
				'bank_account' => 'required|digits:10',
			]);

			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);

			}else{

            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));

            try
			{
			  $tranx = $paystack->transferrecipient->create([
				'name' => $request->bank_name,
                 'description'=> 'Transfer Reciepent',
                'bank_code'=> $request->bank,
                'currency'=> 'NGN',
                'account_number'=> $request->bank_account
			  ]);

               if($tranx->status === true){
               $seller->bank_sort = $request->bank;
				$seller->bank_name = $request->bank_name;
				//$user->bank_bvn = $request->bank_bvn;
				$seller->bank_account = $request->bank_account;
                $seller->transfer_code = $tranx->data->recipient_code;
				if($seller->save()){
					return response()->json(['message'=> 'Your Profile updated was successfull.', 'redirect_url'=> route('app:seller:dashboard:bank_details')], 200);
				}
               }

			}catch(\Yabacon\Paystack\Exception\ApiException $e){
				//die($e->getMessage());
                return response()->json(['errors'=> str_replace("'", " ", str_after($e->getMessage(), ':'))], 500);
			}

		}
      }
        return view('seller.admin.bank_details', []);
    }

    public function payment_history(Request $request){
        $payments = Payment::where('user_id', Auth::user()->id)->paginate(10);
        return view('seller.admin.payment_history', ['payments'=> $payments]);
    }

    public function withdrawal(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                 'amount' => ['required', new NumberBetween(1000, 50000), new AmountEquals($this->auth->user()->balance)]
             ]);
             if ($validator->fails()) {
                 return response()->json(['errors'=> $validator->errors()], 500);
             }else{
                 $withdraw = new Withdraw();
                 $withdraw->user_id = $this->auth->user()->id;
                 $withdraw->code = $this->auth->user()->transfer_code;
                 $withdraw->amount = $request->amount;
                 $withdraw->ref = 'SUV-' . str_random(20);
                 $withdraw->status = 1;
                 $withdraw->save();
                 $userQuery = \App\Seller::where('id', $this->auth->user()->id)->first();
                 $userQuery->decrement('balance', $request->amount);

                 return response()->json(['message'=> 'Your request has been recieved! After validation, your payment would be made within 5 working days.', 'redirect_url'=> route('app:seller:dashboard:withdraw')], 200);
             }
        }

        $seller_orders = SellerOrder::where([['user_id', $this->auth->user()->id], ['is_paid', 2]])->get();

        $dimers = [
            [
                'title'=> 'Current Balance',
                'value'=> 'N' . $this->auth->user()->balance
            ],
            [
                'title'=> 'Lifetime Balance',
                'value'=> 'N' . $seller_orders->sum('user_profit')
            ],
        ];
        $withdrawals = Withdraw::where('user_id', $this->auth->user()->id)->paginate(10);

        return view('seller.admin.withdraw', ['withdrawals'=> $withdrawals, 'dimers'=> json_decode(json_encode($dimers), false)]);
    }



    public function design_services(Request $request){
        $orders = \App\DesignerOrder::with('gig', 'seller', 'plan')->where([['user_id', $this->auth->user()->id], ['user_type', 'SELLER']])->paginate(20);
        return view('seller.admin.design_orders', ['active_orders'=> $orders]);
    }

    public function single_design_service(Request $request, $id){
        $order = \App\DesignerOrder::with('gig','plan', 'order_request', 'designer')->where('id', $id)->firstOrFail();
        return view('seller.admin.single_design_order', ['order'=> $order]);
    }

    public function design_request(Request $request){
        $req = \App\DesignerRequest::where([['user_id', $this->auth->user()->id], ['user_type', 'SELLER'], ['gig_id', null]])->paginate(15);
        return view('seller.admin.design_request', ['requests'=> $req]);
    }

    public function single_design_request(Request $request, $id){
        $offers = \App\DesignerOffer::with(['gig'=>function($q){
            $q->with(['plans', 'user']);
        }])->where('request_id', $id)->paginate(15);
        return view('seller.admin.single_design_request', ['offers'=> $offers]);
    }


    //For Notifications

    public function all_notifications(Request $request){
        if($request->isMethod('get')){
            AllNotifications::where('id', $request->id)->update(['read_at'=> true]);
            $notifications = DB::table('all_notifications')->where('owner_id', Auth::guard('sellers')->user()->id)->latest()->paginate(10);
            return view('seller.admin.notifications', ['notifications'=> $notifications]);
        }
        $notifications = DB::table('all_notifications')->where('owner_id', Auth::guard('sellers')->user()->id)->latest()->paginate(10);
        return redirect()->route('seller.admin.notifications', ['notifications'=> $notifications]);

    }

    public function delete_notifications(Request $request, $id)
    {
        AllNotifications::where('id', $id)->delete();
        return back()->with('success', 'Notification deleted sucessfully!');
    }


}
