<?php
namespace App\Repository\Influencer;

interface InfluencerInterface{

    /**
     * Save influencer application data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
     public function influencer_apply($data);

    /**
     * Save influencer application data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
     public function sign_up($data);

    /**
     * Save forgot password data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function forgot_password($data);

    /**
     * Save reset password data
     *
     * @param Illuminate\Http\Request $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function reset_password($data);

    /**
     * Confirms the influencers
     *
     * @param Illuminate\Http\Request $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function confirm_influencer($id);


}