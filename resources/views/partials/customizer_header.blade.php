<div class="uk-navbar-container middle-nav">
        <div class="uk-container">
        <div class="uk-navbar">
                <div class="uk-navbar-left">
                        <a href="{{ route('app:base:index') }}" class="uk-navbar-item uk-logo"><img src="{{ $utils->get_image('site.logo') }}" uk-responsive width="120"></a>
                </div>


                <div class="uk-navbar-right">

                        
                        <div class="uk-navbar-item">
                            <a href="{{ route('app:ecommerce:cart_review') }}" class="text-link-cart">
                                    <div class="d-inline"><span class="icon-cart"></span> <div class="cart-counter" id="__data_cart_counter">{{ LaraCart::count() }}</div></div> Cart
                            </a>
                        </div>

                       

                              
                </div>

        </div>
        </div>
</div>