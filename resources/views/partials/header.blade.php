@php $categories = $utils->get_categories(); @endphp

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="uk-navbar-container top-nav">
    <div class="uk-container uk-container-small">
        <div class="row nav-ext">
            <div class="col-md-3">
            <a href="{{ route('app:base:index') }}" class="{{ $utils->makeActive(['app:base:index'], 'uk-active') }}"><span class="icon-suvenia" style="font-size:18px;"></span> Marketplace</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:seller:index') }}" class="{{ $utils->makeActive(['app:seller:index'], 'uk-active') }} "><span class="icon-seller" style="font-size:18px;"></span>seller</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:designer:landing_page') }}" class="{{ $utils->makeActive(['app:designer:landing_page', 'app:designer:sign_up'], 'uk-active') }}"><span class="icon-bulb" style="font-size:18px;"></span> designer</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:influencer:landing_page') }}" class="{{ $utils->makeActive(['app:influencer:landing_page', 'app:influencer:apply'], 'uk-active') }}"><span class="icon-influencer" style="font-size:18px;"></span> influencer</a>
            </div>

        </div>
    </div>
</div>


<div class="uk-navbar-container middle-nav">
    <div class="uk-container">
        <div class="uk-navbar">
            <div class="uk-navbar-left">
                <a href="{{ route('app:base:index') }}" class="uk-navbar-item uk-logo"><img src="{{  asset($utils->get_image('site.logo')) }}" uk-responsive width="120"></a>
            </div>

            <div class="uk-navbar-right">

                <div class="uk-navbar-item">
                    <form action="{{ route('app:ecommerce:product_search') }}" method="post" autocomplete="off">
                        @csrf
                        <div class="input-group  app-search-form boundary-align">
                            <input type="search" class="form-control" placeholder="What are you looking for today?" name="q" id="appWideSearchInp" data-url="{{ route('app:ecommerce:product_search') }}">
                            <div uk-dropdown="pos: bottom-justify; boundary: .boundary-align; boundary-align: true; mode: click" id="appWideSearchContainer">
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-info" id="app-search-btn">Search</button>
                            </div>
                        </div>
                    </form>

                </div>

                @auth('buyers')
                <div class="uk-navbar-item"></div>
               @else
               <div class="uk-navbar-item">
                <a href="#" uk-toggle="target: #modal-close-default" class="text-link">Sell on suvenia</a>
               </div>
               @endauth

                <div class="uk-navbar-right">
                    @auth('buyers')
                    <div class="uk-navbar-item">
                    <a href="javascript:;" class="text-link-auth text-link-auth-logged">
                            <span class="m-0">{{ Auth::guard('buyers')->user()->username  }}</span>
                            <span uk-icon="chevron-down"></span>
                        </a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav app-user-drop-down">
                                @includeIf('partials.acl_menu.user', ['type'=> 'front_end'])
                                <li><a href="{{ route('app:user:logout') }}" class="post-link">Logout</a></li>

                            </ul>
                        </div>
                    </div>
                  @else
                  <div class="uk-navbar-item">
                    <a href="{{ route('app:user:login') }}" class="text-link-auth">
                        <span class="m-0">Login</span>
                        <div class="uk-navbar-subtitle m-0">Sign up</div>
                    </a>
                    </div>
                    @endauth
                </div>


                <div class="uk-navbar-item">
                        <a href="{{ route('app:ecommerce:cart_review') }}" class="text-link-cart">
                            <div class="d-inline"><span class="icon-cart"></span>
                                <div class="cart-counter" id="__data_cart_counter">{{ LaraCart::count() }}</div>
                            </div> Cart
                        </a>
                    </div>

            </div>

        </div>
    </div>
</div>

@if(!is_null($show_category) && $show_category)
<div class="uk-navbar-container bottom-nav">
    <div class="uk-container">
        <div class="uk-navbar">
            <div class="uk-navbar-center">
                <ul class="uk-navbar-nav">
                    @foreach ($categories as $category)
                    <li class=""><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $category->slug ]) }}">{{ $category->name }}
                                            @if(count($category->children) > 0)
                                            &nbsp;<span uk-icon="chevron-down"></span>
                                            @endif
                                        </a> @if(count($category->children) > 0)
                        <div uk-dropdown="offset: 0">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                @foreach ($category->children as $child)
                                <li><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $child->slug ]) }}" class="uk-text-capitalize">{{ $child->name }}
                                                        @if(count($child->children) > 0)
                                                        <span uk-icon="chevron-right" style="float:right;"></span>
                                                        @endif
                                                    </a> @if(count($child->children) > 0)
                                    <div uk-dropdown="pos: right-top; offset: 0;">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            @foreach($child->children as $child_item)

                                            <li><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $child_item->slug ]) }}" class="uk-text-capitalize">#{{ $child_item->name }}
                                                                    </a>
                                            </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>
@endif

@else

<div class="uk-navbar-container mobile-nav">
        <div class="uk-container">
            <nav class="uk-navbar">

                <div class="uk-navbar-left">
                    <a href="javascript:;" class="uk-navbar-item mobile-nav-text-color" uk-toggle="target: #NavOffcanvas" style="focus: text-decoration:none;"><span class="icon-menu" style="font-size:18px; font-weight:bold;"></span></a>
                </div>

                <div class="uk-navbar-left ml-1">
                    <a href="{{ route('app:base:index') }}" class="uk-navbar-item uk-logo">
                        <img src="{{ $utils->get_image('site.logo') }}" width="100" uk-responsive>
                    </a>
                </div>

                <div class="uk-navbar-right">



                            <div class="uk-navbar-item">
                                    <a href="{{ route('app:ecommerce:cart_review') }}" class="text-link-cart">
                                        <div class="d-inline"><span class="icon-cart"></span>
                                            <div class="cart-counter" id="__data_cart_counter">{{ LaraCart::count() }}</div>
                                        </div>
                                    </a>
                                </div>
                </div>

            </nav>
        </div>

        <div class="uk-container mt-2 mb-2">
            <div class="mobile_search_nav">
                <form action="{{ route('app:ecommerce:product_search') }}" method="post" autocomplete="off" class="row">
                    @csrf
                    <div class="col-12">
                        <div class="uk-search uk-search-default app-search-mobile">
                            <span class="uk-search-icon-flip text-link" uk-search-icon></span>
                            <input class="uk-search-input" type="search" placeholder="Search..." name="q">
                        </div>
                    </div>
                 </form>
            </div>
        </div>

</div>


<div id="NavOffcanvas" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <div class="user-jar uk-text-center">
                @auth
                    @if(Auth::user()->photo_url)
                    <div class="mb-2"><img src="{{ asset(Auth::user()->photo_url) }}" alt="{{ Auth::user()->username }}" width="100" class="uk-border-pill"></div>
                    @else
                    <div class="mb-2"><span class="canvas-user-icon icon-round-user"></span></div>
                    @endif
                 @else
                 <div class="mb-2"><span class="canvas-user-icon icon-round-user"></span></div>
                @endif

                <div class="mt-3 p-3 d-flex justify-content-between">
                        @auth
                        <div class="">
                        <a href="{{ route('app:user:logout') }}" class="canvas-auth-link">{{ strtoupper(Auth::user()->username) }}</a>
                            </div>
                        <div class="">
                                <a href="{{ route('app:user:logout') }}" class="canvas-auth-link post-link">LOGOUT</a>
                            </div>
                        @else
                        <div class="">
                                <a href="{{ route('app:user:login') }}" class="canvas-auth-link">LOGIN</a>
                            </div>
                            <div class="">
                                <a href="{{ route('app:user:register') }}" class="canvas-auth-link">SIGN UP</a>
                            </div>
                        @endauth
                </div>
            </div>
            <div class="menu-pane">
                @auth
                    <ul uk-accordion class="canvas-accordion">
                            <li >
                                <a class="uk-accordion-title p-0 m-0" href="#"><span class="canvas-icon icon-user  mr-2"></span> ACCOUNT</a>
                                <div class="uk-accordion-content">
                                        <ul class="list-unstyled canva-sub-menu">
                                            @includeIf('partials.acl_menu.user', ['type'=> 'front_end'])
                                        </ul>
                                </div>
                            </li>
                    </ul>
                @endauth
                    <ul uk-accordion class="canvas-accordion">
                            <li >
                                <a class="uk-accordion-title p-0 m-0" href="#"><span class="canvas-icon icon-home mr-2"></span> BUY</a>
                                <div class="uk-accordion-content">
                                        <ul class="list-unstyled canva-sub-menu">
                                                @foreach ($categories as $category)
                                                <li class=""><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $category->slug ]) }}">{{ title_case($category->name) }} </a>
                                                </li>
                                                @endforeach
                                        </ul>
                                </div>
                            </li>
                    </ul>

                    <ul uk-accordion class="canvas-accordion">

                            <li class="acc-border">
                                <a class="uk-accordion-title p-0 m-0" href="#"><span class="canvas-icon icon-pencil mr-2"></span> Create</a>
                                <div class="uk-accordion-content">
                                        <ul class="list-unstyled canva-sub-menu">
                                        <li><a href="{{ route('app:ecommerce:product_list') }}" class="text-link">Get Started</a></li>
                                        </ul>
                                </div>
                            </li>

                    </ul>
                    <ul class="uk-nav uk-nav-default canvas-menu mt-1">
                        <li><a href="{{ route('app:seller:index') }}"><span class="canvas-icon icon-seller mr-2"></span> SELLER</a></li>
                        <li><a href="{{ route('app:designer:landing_page') }}"><span class="canvas-icon icon-bulb mr-2"></span> DESIGNER</a></li>
                        <li><a href="{{ route('app:influencer:landing_page') }}"><span class="canvas-icon icon-influencer mr-2"></span> INFLUENCER</a></li>
                    </ul>

            </div>
        </div>
</div>

@endif
