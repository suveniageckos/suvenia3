@extends('layouts.dashboard')

@section('title', 'My Orders' )

@php
$qry_duration = request()->query('dur');
@endphp

@section('content')

         
  <div class="row justify-content-center">
        <div class="col-12">
          <div class="card p-4 uk-animation-slide-top-small">
                <div class="row">
                        <div class="col-12">
                              <div uk-form-custom="target: > * > span:first-child">
                                  <select class="_order_item_filter_select_input">
                                      <option value="{{ route('app:seller:dashboard:my_orders') }}" {{ base64_decode($qry_duration) == null ? 'selected': '' }}>All Orders</option>
                                      <option value="{{ route('app:seller:dashboard:my_orders', ['dur'=> base64_encode(30)]) }}" {{ base64_decode($qry_duration) == 30 ? 'selected': '' }}>Last 30 days</option>
                                      <option value="{{ route('app:seller:dashboard:my_orders', ['dur'=> base64_encode(60)]) }}" {{ base64_decode($qry_duration) == 60 ? 'selected': '' }}>Last 60 days</option>
                                      <option value="{{ route('app:seller:dashboard:my_orders', ['dur'=> base64_encode(365)]) }}" {{ base64_decode($qry_duration) == 365 ? 'selected': '' }}>Last 1 year</option>
                                      
                                  </select>
                                  <button class="_order_item_filter" type="button" tabindex="-1">
                                      <span></span>
                                      <span uk-icon="icon: chevron-down"></span>
                                  </button>
                              </div>
                        </div>
                    </div>

            <div class="table-responsive">
                <table class="uk-table uk-table-middle user_order_table">
                        <thead>
                            <tr class="">
                                <th>ORDER NUMBER</th>
                                <th>DATE</th>
                                <th>TOTAL</th>
                                <th>ORDER STATUS</th>
                                <th>DETAILS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($orders as $order)

                            <tr>
                                <td>#{{ $order->order_ref }} </td>
                                <td>{{ $order->created_at->toDateTimeString() }} </td>
                                <td>&#8358;{{  number_format($order->price, 2) }}</td>
                                 <td><span class="_delivery_status">{{ isset($order->order_shipping) ? $utils->set_shipping_status($order->order_shipping->status) : '' }} </span></td>
                                <td><a data-toggle="collapse" href="#accordion{{ $order->id }}" role="button" aria-expanded="false" aria-controls="accordion" class="_cart_items_expand">Order Details <span uk-icon="chevron-down"></span></a>
                                </td>
                                
                            </tr>

                                 <tr  id="accordion{{ $order->id }}" class="collapse uk-animation-slide-top-small cart_item-detail">
                                    <td colspan="5">
                                        <div class="bg-white p-3">
                                            <div class="row">
                                                <div class="col-3">
                                                    <p class="_cart_item_product_title m-0">PRODUCTS</p>
                                                </div>
                                                <div class="col-9">
                                                    <div class="row">
                                                        @foreach($order->order_items as $item)
                                                        @php $photos = $utils->get_photos($item->product->id); @endphp
                                                            <div class="col-6 mb-2">
                                                                    <div class="media">
                                                                    <img class="mr-3" src="" alt="{{ $item->product->name }}" width="70">
                                                                            <div class="media-body">
                                                                             <p class="_item_product_desc mt-1">{{ $item->quantity }}X {{ $item->product->name }}</p>
                                                                            <p class="_item_product_desc mt-1">&#8358;{{ number_format($item->price, 2) }}</p>
                                                                             </div>
                                                                          </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-5">
                                                <div class="col-3">
                                                    <p class="_cart_item_product_title m-0">SHIPPING ADDRESS</p>
                                                </div>
                                                <div class="col-9">
                                                    <p class="m-0 _item_product_shipping_desc">
                                                        @php $address = $utils->get_address($order->order_shipping); @endphp
                                                        {{ $address["name"] }}
                                                        {{ $address["address"] }}
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" style="background: #fff; text-align: center;">
                                        @if(request()->dur)
                                        <p>Oops.. No result found</p>
                                        @else
                                        <p>Oops.. You have not made any order yet</p>
                                        @endif
                                        
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                </table>
            </div>

                <div class="row">
                        <div class="col-12 user_orders_pagination">
                            {{ $orders->appends(['dur'=> $qry_duration])->links() }}
                        </div>
                </div>
          </div>
        </div>
  </div>
@endsection