<form class="app-form data-post border-0" action="{{ route('app:dashboard:edit_shipping_address', ['id'=> $id, 'address_id'=> $address_id]) }}" method="POST" data-post>
        @csrf 
        <div class="form-group firstname">
                <label for="" class="ship_modal_label">First Name</label>
        <input type="text" class="form-control" placeholder="Firstname" name="firstname"  value="{{ $address->firstname }}">
            <p class="form-text help-block"></p>
        </div>
        <div class="form-group lastname">
                <label for="" class="ship_modal_label">Last Name</label>
            <input type="text" class="form-control" placeholder="Lastname" name="lastname"  value="{{ $address->lastname }}">
            <p class="form-text help-block"></p>
        </div>
        <div class="form-group email">
                <label for="" class="ship_modal_label">Email</label>
            <input type="text" class="form-control" placeholder="Email" name="email"  value="{{ $address->email }}">
            <p class="form-text help-block"></p>
        </div>
        
        @if($states->count() > 0)
        <div class="form-group">
                <label for="" class="ship_modal_label">What state do you want to ship to?</label>
            <label for="">Select State</label>
            <select class="form-control ship_inp_grey" name="location">
                @foreach($states as $state)
                @php $selectedState = $state->id == $address->location_id ? 'selected' : '' ;@endphp
            <option value="{{ $state->id }}" {{ $selectedState }}>{{ $state->name }}</option>
                @endforeach
            </select>
        </div>
        @else
    
        <div class="form-group">
                <label for="" class="ship_modal_label">What country do you want to ship to?</label>
                <select class="form-control ship_inp_grey" name="country" id="__country_select">
                    @foreach(config('ecommerce_config.countries') as $key => $value)
                    @php $chooseNigeria = $key == strtoupper($address->country) ? 'selected' : ''; @endphp
                <option value="{{ strtolower($key) }}" {{ $chooseNigeria }}>{{ $value }}</option>
                    @endforeach
                </select>
         </div>
    
        @endif
    
        <div class="form-group address">
                <label for="" class="ship_modal_label">Shipping Address</label>
            <textarea class="form-control" name="address" placeholder="Your Address">{{ $address->address }}</textarea>
            <p class="form-text help-block"></p>
        </div>
    
        <div class="form-group phone_number">
                <label for="" class="ship_modal_label d-block">Phone Number</label>
                <input type="tel" class="form-control" placeholder="phone Number" name="phone_number"  value="{{ $address->phone_number }}" id="___phone" style="width:100%;">
                <p class="form-text help-block"></p>
        </div>
        <button type="submit" class="btn btn-info float-right">SAVE</button>
    
    </form>
    
    <script>
    ___setNumber('#___phone', '#__country_select');
    </script>