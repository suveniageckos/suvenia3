@extends('vendor.voyager.layouts.app')

@section('page_title', __('Stores we like blocks'))
@section('templcss')
<link rel="stylesheet" href="{{ mix('css/admin_templates.css') }}">
@endsection

@section('content')
<div class="container-fluid">
             <h1 class="page-title">
                    <i class="voyager-star-half"></i> Edit Stores We Like Blocks
                </h1>

<div class="page-content browse container-fluid">

<div class="row">
<div class="col-md-12">

<div class="panel panel-bordered">
<div class="panel-body">

<div class="row">
<form action="{{ route('admin:stores_we_like_block:edit', ['id'=> $store_block->id]) }}" method="POST">
   @csrf
        {{-- <div class="form-group">
            <label for="">Block Title</label>
            <input type="text" class="form-control" placeholder="Block Title" required name="title" value="{{ $product->title }}">
        </div> --}}

        <div class="form-group">
            <label for="">Store IDS <small>(Seperate Product ID by comma)</small></label>

        <textarea class="form-control" placeholder="Store IDS" name="store_ids">{{ $store_block->store_ids }}</textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-info">SUBMIT</button>
        </div>

    </form>
</div>
</div>

</div>


</div>
</div>


</div>
</div>

@endsection

@push('javascript')

@endpush
