@extends('layouts.layout')

@section('title', 'My Shipping Address' )

@section('content')

<div class="uk-container mt-2 mb-3">
  <div class="row justify-content-center">
    <div class="col-8">
        @includeIf('partials.user_nav')
    </div>
  </div>

  <div class="row justify-content-center">
  <div class="col-8">
      <div class="bg-white mb-3 shadowed-box">
          <div class="p-2 cart_shipping_zone_header_bottom">
              <p class="cart_shipping_zone_header m-0">ADDRESS</p>
          </div>
          <div class="row no-gutters">
              @foreach($zones as $zone)
              @php $currentLocation = \App\ShippingAddress::where([['zone_id', $zone->id], ['is_default', true]])->first(); 
               //$selectZone = $selectedZoneID == $zone->id ? 'checked': '';
               $checkedZone = $zone->is_default ? 'checked' : '';
              @endphp
              <div class="col-12 shipping_border-righ p-3 cart_ship_border_bottom">
                  <div class="row no-gutters">
                      <div class="col-1 d-table uk-text-right p-1">
                          <div class="custom-control custom-radio align-middle">
                          @if(!$zone->has_option || $currentLocation)
                          <input type="radio" id="zone_radio_{{ $zone->id }}" name="zone_radio" class="custom-control-input _zone_selector" data-url="{{ route('app:user:select_zone', ['zone_id'=> $zone->id]) }}" {{ $checkedZone }}>
                          @else
                          <input type="radio" id="zone_radio_{{ $zone->id }}" name="zone_radio" class="custom-control-input _zone_selector" data-url="{{ route('app:user:select_zone', ['zone_id'=> $zone->id]) }}" disabled {{ $checkedZone }}>
                         
                          @endif
                          <label class="custom-control-label" for="zone_radio_{{ $zone->id }}"></label>
                          </div>
                      </div>
                      <div class="col-11">
                          <p class="m-1 cart-shipping-header">{{ $zone->name }}</p>
                          <p class="m-1 cart-shipping-text">
                          @if($zone->addresses()->count() > 0)
                      @php $selectedAddress =  $zone->addresses()->where('is_default', true)->first(); @endphp
                      {{ !isset($selectedAddress->address) ? $zone->description : $selectedAddress->address }}
                          @else
                          {!! $zone->description  !!}
                          @endif
                          </p> 
                          @if($zone->has_option)
                          @if($zone->addresses()->count() > 0)
                          <p class="m-1"><a href="" class="__ship_select_address_def __change_address_link_style" data-url="{{ route('app:user:set_default_zone_modal', ['id'=> $zone->id])}}">SELECT ADDRESS</a></p> 
                          @else
                          <p class="m-1"><a href="" class="__ship_add_address_def __change_address_link_style" data-url="{{ route('app:user: add_address_zone_modal', ['id'=> $zone->id])}}">ADD ADDRESS</a></p> 
                          @endif

                          @endif
                      </div>
                  </div>
                  
                </div>
              @endforeach
          </div> 
      </div>

  </div>
  </div>

</div>

@endsection

@push('cart_shipping_page')
<script src="{{ asset('suv/s/pg_ship.bundle.js') }}"></script>
@endpush