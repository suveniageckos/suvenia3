@php
  $menus = collect([
      [
          'title'=> 'My Dashboard',
          'route'=> 'app:designer:dashboard:index',
          'icon'=> 'icon-speedometer'
      ],
      [
          'title'=> 'Gigs',
          'route'=> 'app:designer:dashboard:all_gigs',
          'icon'=> 'icon-camrecorder'
      ],
      [
          'title'=> 'Orders',
          'route'=> 'app:designer:dashboard:all_orders',
          'icon'=> 'icon-basket'
      ],
      [
          'title'=> 'Requests',
          'route'=> 'app:designer:dashboard:requests',
          'icon'=> 'icon-rocket'
      ],
      [
          'title'=> 'Withdrawal',
          'route'=> 'app:designer:dashboard:withdrawal',
          'icon'=> 'icon-wallet'
      ],
      [
          'title'=> 'Settings',
          'route'=> 'app:designer:dashboard:profile_settings',
          'icon'=> 'icon-user'
      ],

  ]);
@endphp

@if(isset($type) && $type == 'admin_dropdown')

@foreach ($menus->all() as $item)
    <a href="{{ route($item['route']) }}" class="dropdown-item">
        <i class="icon {{ $item['icon']}}"></i> {{ $item['title']}}
    </a>
@endforeach

@endif

@if(isset($type) && $type == 'admin_sidebar_header')

<li class="nav-item super-header">
    <div class="uk-text-center pt-3 uk-light" style="border-bottom: 1px solid #09628a !important;">
        <img src="{{ Auth::guard('designers')->user()->photo_url ? asset(Auth::guard('designers')->user()->photo_url) : asset('user/default.png') }}" class="uk-border-circle m-auto" uk-responsive width="70">
        <p class="text-capitalize mt-2 mb-0">{{ Auth::guard('designers')->user()->username }}</p>
        <p class="text-capitalize mt-0" style="font-size: 12px;">Designer</p>
    </div>
 </li>

<li class="nav-item super-header">
    <a href="" class="nav-link active">
        My Designer Profile <i class="icon icon-minus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endif

@if(isset($type) && $type == 'admin_sidebar_content')
@foreach ($menus->all() as $item)
<li class="nav-item">
        <a href="{{ route($item['route']) }}" class="nav-link">
            <i class="icon {{ $item['icon']}}" style="font-size: 14px;"></i> {{ $item['title']}}
        </a>
</li>
@endforeach

@endif

@if(isset($type) && $type == 'front_end')
@foreach ($menus->all() as $item)
<li class=""><a href="{{ route($item['route']) }}" target="_blank">{{ $item['title'] }}</a></li>
@endforeach
@endif

@if(isset($type) && $type == 'admin_sidebar_footer')

<li class="nav-item super-header">
    <a href="{{ route('app:dashboard:my_orders') }}" class="nav-link active">
        Buyer Dashboard
    </a>
</li>
@endif
