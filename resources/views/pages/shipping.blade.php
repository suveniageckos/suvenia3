@extends('layouts.layout')

@section('title', 'Shipping')

@section('content')
<div class="uk-container uk-container-small uk-margin-top">

        <div class="" uk-grid>

            <div class="uk-width-1-1">
                <h4 class="uk-heading-line uk-text-center"><span><b>Shipping And Delivery</b></span></h4>

                <div class="uk-text-left">
                <p> We provide shipping to all 36 states of Nigeria using our partnered logistics partners, we process your orders to verify availability of the product(s), shipping selections and payment information.</p>

        <h5><b>Lagos</b></h5>
        <ul>
        <li>1-100 Items = 3-5 days </li>
        <li>101-500 Items= 5-7 days </li>
        <li>501-1000 Items = 7-9 days</li>
        <li>1000+ Items= As Agreed Offline</li>
        </ul>

        <h5 class="uk-margin-remove-vertical">Southwest Nigeria</h5>
        <ul>
        <li>1-100 items= 4-7 days</li>
        <li>101-500 Items=7-9 days</li>
        <li>501-1000 Items= 9-11 days</li>
        <li>1000+ Items = As agreed Offline</li>
        </ul>

        <h5 class="uk-margin-remove-vertical">Southeast Nigeria</h5>
        <ul>
        <li>1-100 Items= 5-9 days</li>
        <li>101-500 Items= 8-10 days</li>
        <li>501-1000 Items= 10-12 days</li>
        <li>1000+ Items= As agreed Offline</li>
        </ul>

        <h5 class="uk-margin-remove-vertical">Northern Nigeria</h5>
        <ul>
        <li>1- 100 Items= 6-11 days</li>
        <li>101-500 Items= 10-12 days</li>
        <li>501-1000 Items=12-14 days</li>
        <li>1000+Items+As agreed Offline</li>
        </ul>

        <h5 class="uk-margin-remove-vertical">Undeliverable Packages</h5>
        <p>Occasionally packages are returned to us undeliverable. When a carrier returns an undeliverable package to us, we will immediately contact you for an updated reshipment address and time. If we do not receive any contact within a week of the package being returned, we will cancel your order, and you will not be entitled to any refunds.</p>
        <h5 class="uk-margin-remove-vertical">Incorrect Address</h5>
        <p>We ship all packages based on the address that you entered during checkout. Please make sure that the address you enter is correct.</p>
        <p>Advice on Receiving Your Package
        If you will be unavailable to receive your package, please ensure that someone else will be there to receive, inspect, and confirm your package. Our dispatch man will not leave your package at the door or around your house if no one is home. On the day of delivery, our dispatch man will give you a few moments to inspect your package for you to make sure you have the correct sizes, and correct items.</p>

        <h5 class="uk-margin-remove-vertical">International Orders</h5>
        <p>We ship internationally to locations outside Nigeria. Simply confirm your location upon checkout. Delivery time for international orders are currently on a pre-agreed basis for each order with Suvenia.com.</p>

        </ul>

         </div>

            </div>

        </div>
        </div>



<div id="modal-close-default" uk-modal>
        <div class="uk-modal-dialog uk-modal-body big-dialog">
            {{-- <div class=" uk-modal-header"> --}}
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h4 class="uk-modal-title" style="font-size: 24px; font-weight: 500; font-family: Poppins;">3 WAYS TO SELL ON SUVENIA</h4>
            {{-- </div> --}}

                        <div class="row">
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <span class="fas fa-store"></span>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px; padding-bottom: 19px;">Create a store to start selling your creative designs on suvenia</p>
                                        <a href="{{ route('app:seller:index') }}" class="btn sell_on_suvenia_buttons">OWN A STORE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fas fa-paint-brush"></i>
                                            </div>

                                            <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Create awesome designs on Suvenia as a designer and make money. </p>
                                            <a href="{{ route('app:designer:landing_page') }}" class="btn sell_on_suvenia_buttons">FREELANCE</a>
                                 </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="service-box">
                                            <div class="icon-ball d-inline-block">
                                                <i class="fab fa-accessible-icon"></i>
                                            </div>

                                             <p class="mt-1 mb-0 service-desc" style="font-size: 0.8em; text-align:left; padding-top:10px;">Promote any product on Suvenia and earn referral on any purchase.</p>
                                            <a href="{{ route('app:influencer:landing_page') }}" class="btn sell_on_suvenia_buttons">BE AN INFLUENCER</a>
                                 </div>
                            </div>

                        </div>

            </div>
            </div>
@endsection
