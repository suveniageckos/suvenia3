import fabric from 'fabric';
import Navigo = require("navigo");
import Dropzone = require("dropzone");
import 'fabric-customise-controls';
import * as _ from "lodash";
import Sortable = require("sortablejs");
import "bootstrap-slider";
import "bootstrap-tagsinput";
import "spectrum-colorpicker";

Dropzone.autoDiscover = false;

class Customizer {

	canvas_front: any;

	canvas_back: any;

	active_canvas_side: any; 
 
	activeCanva: any;

	baseUrl: any; 

	activeCanvaDiv: any;

	hasBack: boolean = true;

	uploadedImage: any = [];

	isDirty: boolean = false;

	activeData: any;

	front_design: any;

	back_design: any;

	DesignTags: any;

	DesignTagsBox: any;

	PrimaryColor: any = ['#FFFFFF'];

	rowBucket: any = [];

	baseCategory: any;

	DesDiv: String = 'canvas';

	Layers: any = [
	];

	designData: any = {
		front: {},
		back: {}
	}

	public constructor(
		private $route = new Navigo(null, true, '#/'),
	) {
		this.setRoutes();
		this.baseUrl = $('body').attr('BaseAppUrl');
		this.InitCanvas();
		if (!localStorage.getItem("mappedData")) {
			this.LoadProduct();
		} else {
			this.LoadProductFromMapped();
		}
		let cls = this;
		cls.changeProduct();
		cls.switchView();
		cls.setControls();
		cls.changeProductColor();
		cls.addText();
		cls.uploadImage();
		cls.setArtPagination();
		cls.loadChildArts();
		cls.placeArt();
		cls.placeTemplate();
		cls.sortLayers();
		cls.process_design();
		cls.setToolBarItem();
		cls.changeFont();
		cls.addTextOutline();
	}

	public InitCanvas() {
		this.canvas_front = new fabric.fabric.Canvas('canvas_front');
		this.canvas_back = new fabric.fabric.Canvas('canvas_back');
	}

	public LoadProductFromMapped() {
		let _data: any = JSON.parse(<any>localStorage.getItem('mappedData'));
		this.activeData = _data;
		if (!_data.backViewEnabled) {
			this.setFrontView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			$('.flip-container').addClass('flipped_front');
			$('#_FlipToggles').hide();
			$('#ProductSelect option[data-name=' + this.slugify(_data.name) + ']').prop("selected", true);
			this.hasBack = false;
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
		} else {
			this.setFrontView(_data);
			this.setBackView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
			this.hasBack = true;
			$('#_FlipToggles').show();
			$('#ProductSelect option[data-name=' + this.slugify(_data.name) + ']').prop("selected", true);
		}
	}

	public LoadProductFromSelect(data: any) {
		let _data = JSON.parse(data);
		this.activeData = _data;
		if (!_data.backViewEnabled) {
			this.setFrontView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.active_canvas_side = 'front';
			$('#_FlipToggles').hide();
			this.hasBack = false;
			this.isDirty = true;
			//this.PrimaryColor = '#FFFFFF';
			this.baseCategory = _data.name;
		} else {
			this.setFrontView(_data);
			this.setBackView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.active_canvas_side = 'front';
			$('#_FlipToggles').show();
			this.isDirty = true;
			this.hasBack = true;
			//this.PrimaryColor = '#FFFFFF';
			this.baseCategory = _data.name;
		}
	}

	public changeProduct(): void {
		let cls = this;
		$('#ProductSelect').on('change', function () {
			localStorage.removeItem("mappedData");
			const __data = $(this).val();
			cls.LoadProductFromSelect(__data);

		});
	}

	public LoadProduct() {
		let _data = JSON.parse(<any>$('#ProductSelect').val());
		this.activeData = _data;
		if (!_data.backViewEnabled) {
			this.setFrontView(_data);
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			this.active_canvas_side = 'front';
			$('.flip-container').addClass('flipped_front');
			$('#_FlipToggles').css('display', 'none');
			this.hasBack = false;
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
		} else {
			this.setFrontView(_data);
			this.setBackView(_data);
			this.hasBack = true;
			this.activeCanva = this.canvas_front;
			this.activeCanvaDiv = $('#side_front');
			$('#_FlipToggles').css('display', 'block');
			this.active_canvas_side = 'front';
			this.baseCategory = _data.name;
			//this.PrimaryColor = '#FFFFFF';
		}
	}

	public switchView() {
		let cls = this;
		$('#_flipFront').on('change', function () {
			if (!$('.flip-container').hasClass('flipped_front')) {
				$('.flip-container').removeClass('flipped_back');
				$('.flip-container').addClass('flipped_front');
				cls.activeCanva = cls.canvas_front;
				cls.activeCanvaDiv = $('#side_front');
				cls.active_canvas_side = 'front';
			}

		});

		$('#_flipBack').on('change', function () {
			if (!$('.flip-container').hasClass('flipped_back')) {
				$('.flip-container').removeClass('flipped_front');
				$('.flip-container').addClass('flipped_back');
				cls.activeCanva = cls.canvas_back;
				cls.activeCanvaDiv = $('#side_back');
				cls.active_canvas_side = 'back';
			}
		});

	}

	public setFrontView(data: any): void {
		const _Pdiv = $('#side_front');
		const _CanvD = $('#' + this.DesDiv + '_front');
		_Pdiv.find('.ProductFacing').attr('src', this.baseUrl + '/brandables/' + data.front.image_url.split('/').pop());
		_CanvD.css({
			'border': '2px solid #ABABAB'
		});
		_CanvD.after('<div class="CanvaDimension"></div>');
		_CanvD.css({
			position: 'relative',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'originX': 'center',
			'originY': 'center'
		});
		_Pdiv.find('.canvas-container').css({
			'position': 'absolute',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'left': data.front.left + 'px',
			'top': data.front.top + 'px',
		});
		_Pdiv.find('.CanvaDimension').text(data.front.width + 'x' + data.front.height + ' Pixels Printable Area. 120 Dpi');
		this.canvas_front.setWidth(data.front.width);
		this.canvas_front.setHeight(data.front.height);
		this.canvas_front.renderAll();
		this.designData.front.img_url = data.front.image_url;
	}


	public setBackView(data: any): void {
		const _PdivB = $('#side_back');
		const _CanvB = $('#' + this.DesDiv + '_back');
		_PdivB.find('.ProductFacing').attr('src', data.back.image_url);
		_CanvB.css({
			'border': '2px solid #ABABAB'
		});
		_CanvB.after('<div class="CanvaDimension"></div>');
		_CanvB.css({
			'position': 'relative',
			'width': data.back.width + 'px',
			'height': data.back.height + 'px',
			'originX': 'center',
			'originY': 'center'
		});
		_PdivB.find('.canvas-container').css({
			'position': 'absolute',
			'width': data.back.width + 'px',
			'height': data.back.height + 'px',
			'left': data.back.left + 'px',
			'top': data.back.top + 'px',
		});
		_PdivB.find('.CanvaDimension').text(data.back.width + 'x' + data.back.height + ' Pixels Printable Area. 120 Dpi');
		this.canvas_back.setWidth(data.back.width);
		this.canvas_back.setHeight(data.back.height);
		this.canvas_back.renderAll();
		this.designData.back.img_url = data.back.image_url;
	}



	public backgroundSetter(front: any, back: any, ChosenColor: any) {
		$(front).find('.ProductFacing').css({
			'background': ChosenColor,
		});
		$(back).find('.ProductFacing').css({
			'background': ChosenColor,
		});

	}


	public changeProductColor() {
		let cls = this;
		const validate = (el: any) => {
			if ($(el).length >= 4) {
				return false;
			}
			return true;
		}
		$(document).on('change', '.product-color-ball', (e) => {
			let Icon = $(e.target).next();

			if ($(e.target).is(':checked')) {
				if (validate($('.icon-checked'))) {
					Icon.addClass('icon-checked');
					var ChosenColor = $(e.target).val();
					cls.backgroundSetter('#side_front', '#side_back', ChosenColor);
					cls.isDirty = true;
					cls.PrimaryColor.push(ChosenColor);
				} else {
					toastr.error('You must select only 4 colours');
				}

			} else {
				Icon.removeClass('icon-checked');
				_.pull(cls.PrimaryColor, $(e.target).val());
				var lastCol = _.last(cls.PrimaryColor);
				cls.backgroundSetter('#side_front', '#side_back', lastCol);
			}

		});

	}

	public addText() {
		const cls = this;

		$('#AddText').on('click', (e) => {
			let Text = new fabric.fabric.IText(<any>'Add Text', {
				fontFamily: < any > $('#FontSelect').val(),
				fill: '#333',
				fontSize: 40,
			});
			cls.isDirty = true;
			cls.activeCanva.centerObject(Text);
			cls.activeCanva.add(Text);
			cls.activeCanva.setActiveObject(Text);
			cls.activeCanva.renderAll();
			let thisObject = cls.activeCanva.getObjects().indexOf(Text);
			cls.setObjectLayers({ index: thisObject, icon: 'add-text', text: 'Text' });
		});
	}

	public addTextOutline(){
		    const cls = this;
			$('#TextOutline').on('change', function(e){
				let inp = $(e.currentTarget);
				var activeObject = cls.activeCanva.getActiveObject();
				if (activeObject && activeObject.type === 'i-text') {
					activeObject.stroke = < any > $('#AppWideOutlineColor').val();
					activeObject.strokeWidth = parseInt(<any>inp.val());
					cls.activeCanva.renderAll();
				}
			});
	}

	public setControls() {
		let cls = this;
		let BaseUrl = $('body').attr('BaseAppUrl');
		//(<any>$('#AppWideOutlineColor') )
		(<any>fabric).fabric.Canvas.prototype.customiseControls({
			tr: {
				action: 'rotate',
				cursor: 'default'
			},
			tl: {
				action: 'moveUp',
				cursor: 'default'
			},
			bl: {
				action: 'remove',
				cursor: 'default'
			},
			br: {
				action: 'scale',
				cursor: 'nwse-resize'
			},

		}, function () {
			cls.activeCanva.renderAll();
		});

		// basic settings
		(<any>fabric).fabric.Object.prototype.customiseCornerIcons({
			settings: {
				borderColor: '#4B4B4B',
				cornerSize: 20,
				cornerShape: '',
				cornerBackgroundColor: '#AFAEAE',
				cornerPadding: 10 
			},
			tl: {
				icon: BaseUrl + '/customize-icons/move-up.svg',
			},
			tr: {
				icon: BaseUrl + '/customize-icons/rotate-left.svg',
			},

			/*bl: {
				icon: BaseUrl + '/customize-icons/delete.svg',
			},*/
			br: {
				icon: BaseUrl + '/customize-icons/resize-d.svg',
			},
			/* mb:{
			     icon: BaseUrl + '/src/customizer/customize-icons/move-down.svg',
			 }*/
		}, function () {
			cls.activeCanva.renderAll();
		});

		fabric.fabric.Object.prototype.setControlsVisibility({
			mt: false,
			mb: false,
			mr: false,
			ml: false,
			mtr: false,
			bl: false
		});
	}

	public uploadImage() {
		/*$('#InitUpload').on('click', ()=>{
			
		});*/
		let imgData: any, imgFilePath: any, imgName: any;
		let cls = this;

		let uploadArt = new Dropzone("div#ImgUploader", {
			url: $("div#ImgUploader").attr('uploadUrl'),
			maxFilesize: 10,
			paramName: "file",
			method: 'post',
			maxFiles: 5,
			addRemoveLinks: true,
			//parallelUploads:1,
			uploadMultiple: false,
			acceptedFiles: 'image/*,application/pdf',
			//autoProcessQueue: false,
			params: {
				_token: $('meta[name="csrf-token"]').attr('content')
			},
			dictDefaultMessage: '<span class="uk-text-middle app-text-upload">Drag Art Here or </span><span class="uk-link">Select File</span>',
			/*init: function () {

			}*/

		});

		uploadArt.on('success', function (file: any, res: any) {
			imgData = res.file;
			imgFilePath = res.image_path;
			imgName = res.name;
		});

		$('#SaveUpload').on('click', () => {
			fabric.fabric.Image.fromURL(imgData, function (myImg: any) {
				var scale = 300 / myImg.width;
				
				myImg.set({});
				myImg.scaleToWidth(150);
				myImg.scaleToHeight(150);
				cls.activeCanva.centerObject(myImg);
				cls.activeCanva.add(myImg);
				cls.activeCanva.setActiveObject(myImg);
				cls.activeCanva.renderAll();
				let thisObject = cls.activeCanva.getObjects().indexOf(myImg);
				cls.setObjectLayers({ index: thisObject, icon: 'image', text: imgName });
				cls.isDirty = true;
				UIkit.modal('#ImageModal').hide();
			});
		});
	}

	public setArtPagination() {
		$(document).on('click', '.pagination > .page-item > .page-link', function (e) {
			e.preventDefault();
			let curTarg: any = $(e.currentTarget);
			let url = $('#PaginLink').attr('data-link');
			let page = curTarg.attr('href').split('page=')[1];
			let pageLink = $(this).attr('href');
			$.ajax({
				url: pageLink,
				beforeSend: function () {
					//$('#Artscontent').append('<div class="loading">Loading…</div>');
					$('#Artscontent').find('.loader').remove();
					$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

				}
			}).done(function (res) {
				$('#Artscontent').html(" ");
				$('#Artscontent').html(res);
			});
			//console.log(url);
		});
	}

	public setToolBarItem(): void {
		let cls = this;

		let tools: any = {};

		cls.activeCanva.on('object:selected', function (evt: any) {
			var activeObject = evt.target.get('type');
			$('.ToolBar').removeClass('hide');
			if (activeObject == 'image') {
				$('#OtherColorCont').hide();
				$('#outlineColorCont').hide();
			} else {
				$('#OtherColorCont').show();
				$('#outlineColorCont').show();
			}
		});
		cls.activeCanva.on('selection:cleared', function (evt: any) {
			$('.ToolBar').addClass('hide');
		});
		cls.activeCanva.on('object:removed', function (evt: any) {
			var activeObject = evt.target;
			// console.log(activeObject);
			// console.log(cls.uploadedImage);
			$('.ToolBar').addClass('hide');
		});

		$('#DuplicateApp').on('click', function (e: any) {
			e.preventDefault();
			var activeObject = cls.activeCanva.getActiveObject();
			activeObject.clone(function (cloned: any) {
				cls.activeCanva.discardActiveObject();
				cloned.set({
					top: cloned.top + 20,
					evented: true
				});
				if (cloned.type === 'activeSelection') {
					// active selection needs a reference to the canvas.
					cloned.canvas = cls.activeCanva;
					cloned.forEachObject(function (obj: void) {
						cls.activeCanva.add(obj);
					});
					cloned.setCoords();
				} else {
					cls.activeCanva.add(cloned);
				}

				let setType = cloned.type == 'i-text' ? 'add-text' : 'image' ;
				cls.setObjectLayers({ index: cloned, icon: setType, text: setType });
				//console.log(cloned.type);
				cls.activeCanva.setActiveObject(cloned);
				cls.activeCanva.renderAll();
			});
		});

		$('#FlipVertical').on('click', function (e: any) {
			e.preventDefault();
			var activeObject = cls.activeCanva.getActiveObject();
			if (activeObject) {
				if (!activeObject.flipY) {
					activeObject.set('flipY', true);
				} else {
					activeObject.set('flipY', false);
				}
				cls.activeCanva.centerObject(activeObject);
				cls.activeCanva.renderAll();

			}
		});

		$('#FlipHorizontal').on('click', function (e: any) {
			e.preventDefault();
			var activeObject = cls.activeCanva.getActiveObject();
			if (activeObject) {

				if (!activeObject.flipX) {
					activeObject.set('flipX', true);
				} else {
					activeObject.set('flipX', false);
				}

				cls.activeCanva.centerObject(activeObject);
				cls.activeCanva.renderAll();
			}
		});

		(<any>$('#AppWideColor') ).spectrum({
			color: "#f00",
			change: function(color: any) {
				//color.toHexString(); // #ff0000
					var activeObject = cls.activeCanva.getActiveObject();
					if (activeObject && activeObject.type == 'i-text') {
						activeObject.fill = color.toHexString();
						activeObject.dirty = true;
						cls.activeCanva.renderAll();
					} else if (activeObject && activeObject.type == 'path-group') {
						activeObject.set('fill', color.toHexString());
						activeObject.dirty = true;
						cls.activeCanva.renderAll();
					}

			}
		});

		(<any>$('#AppWideOutlineColor') ).spectrum({
			color: "#f00",
			change: function(color: any) {
				var activeObject = cls.activeCanva.getActiveObject();
			if (activeObject && activeObject.type === 'i-text') {
				activeObject.stroke = color.toHexString();
				//activeObject.dirty = true;
				cls.activeCanva.renderAll();
			}
			}
		});

	}

	public changeFont() {
		let fontSelected = $('#FontSelect').val();
		let cls = this;
		$('#FontSelect').css('font-family', < any > fontSelected);
		$('#FontSelect').on('change', function () {
			var cl: any = $(this).val();
			$(this).css('font-family', cl);
			var activeObject = cls.activeCanva.getActiveObject();
			if (activeObject && activeObject.type === 'i-text') {
				activeObject.fontFamily = (activeObject.fontFamily == cl ? '' : cl);
				activeObject.dirty = true;
				cls.activeCanva.renderAll();
				cls.isDirty = true;
			}

		});
	}


	

	public loadChildArts() {
		$('.ArtPicParent').each(function () {
			$(this).on('click', function () {
				let url = $(this).attr('data-url');
				$.ajax({
					url: url,
					beforeSend: function () {
						$('#Artscontent').find('.loader').remove();
						$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

					}
				}).done(function (res) {
					$('#Artscontent').html(" ");
					$('#Artscontent').html(res);
				});
			});
		});

		$(document).on('click', '#GoBack', function (e) {
			e.preventDefault();
			let url = $(this).attr('href');
			$.ajax({
				url: url,
				beforeSend: function () {
					$('#Artscontent').find('.loader').remove();
					$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

				}
			}).done(function (res) {
				$('#Artscontent').html(" ");
				$('#Artscontent').html(res);
				$('.ArtPicParentDyn').each(function () {
					$(this).on('click', function () {
						//alert('hey');
						let url = $(this).attr('data-url');
						$.ajax({
							url: url,
							beforeSend: function () {
								$('#Artscontent').find('.loader').remove();
								$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

							}
						}).done(function (res) {
							$('#Artscontent').html(" ");
							$('#Artscontent').html(res);
						});
					});
				});
			});
		});

	}

	public placeArt(): void {
		let cls = this;
		// $('.ArtPic').each(function(ind, val){
		$(document.body).on('click', '.ArtPic', function () {
			var pic = $(this).attr('src');
			fabric.fabric.loadSVGFromURL(<any>pic, function (objects: any, options: any) {
				var svg = fabric.fabric.util.groupSVGElements(objects, options);
				svg.scaleToWidth(150);
				svg.scaleToHeight(150);
				cls.activeCanva.centerObject(svg);
				cls.activeCanva.add(svg);
				cls.activeCanva.setActiveObject(svg);
				cls.activeCanva.renderAll();
				cls.isDirty = true;
				UIkit.modal('#GraphicModal').hide();
				// console.log(cls.activeCanva.toJSON());


			});


		});
		//});
	}

	public placeTemplate() {
		let cls = this;
		$(document.body).on('click', '.TemplateSingle', function (e) {
			let pic = $(e.target).attr('src');
			let data = JSON.parse(<any>$(e.target).attr('data'));
			cls.activeCanva.loadFromJSON($(e.target).attr('data'), function (r: any) {
				//console.log(r);
				//cls.activeCanva.setActiveObject();
				//cls.activeCanva.centerObject(cls.activeCanva);

			}, function (o: any, object: any) {

				if (o.type == 'i-text') {
					o.fontFamily = o.fontFamily;
					let Text = new fabric.fabric.IText(<any>o.text, o);
					cls.activeCanva.setActiveObject(object);
				}

				cls.isDirty = true;
				cls.activeCanva.renderAll();
			});

			UIkit.modal('#TemplateModal').hide();
		});
	}

	public setObjectLayers(obj: any) {
		const cls = this;
		const ID = cls.generateId(5);
		cls.Layers.push(ID);
		let latest: any = _.last(cls.Layers);
		
		$('#LayerPane').append(
			'<div class="layer layerID" data-id="'+ID+'">' +
			'<ul class="list-unstyled m-0 d-flex">' +
			'<li class="d-inline flex-fill"><span class="icon-' + obj['icon'] + ' layer-type mr-1 mt-5"></span> <span class="layer-name">' + obj['text'] + '</span></li>' +
			'<li class="d-inline flex-fill uk-text-right"><span class="icon-move icon-shade move-layer mr-2"></span><span class="icon-delete delete-layer icon-shade" data-fabric="w"><textarea style="display: none;">'+ JSON.stringify(obj) +'</textarea></span></li>' +
			'</ul></div>'
		);

		$('.layerID').each(function(ind, val){
			let delBtn = $(this).find('.delete-layer');
			delBtn.off().on('click', function(e){
				let pos: any = _.indexOf(cls.Layers, $(val).attr('data-id'));
				let toBeDel = cls.activeCanva.item(pos);
				cls.activeCanva.remove(toBeDel);
				cls.activeCanva.renderAll();
				$(val).remove();
				_.pull(cls.Layers, $(val).attr('data-id'));
				
			});
		});
		

	}

	public sortLayers() {
		var el = document.getElementById('LayerPane');
		let sort = new Sortable(<any>el, {

		});
	}

	public generateId(len: any){
        var text = "";

            var charset = "abcdefghjklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ";

            for( var i=0; i < len; i++ )
            text += charset.charAt(Math.floor(Math.random() * charset.length));

            return text;
    }

	public process_design() {
		let cls = this;
		$('#SellPageTrigger').on('click', () => {

			cls.$route.navigate('sell');

		});
	}

	

	public setPriceSlider() {
		let cls = this;

		let PriceSlider = $('#PriceSlider').slider({
			formatter: function (value: any) {
				return value + '%';
			}
		}).on('slide change', function () {
			//console.log(PriceSlider.getValue());
			setInputs(PriceSlider.getValue());

		}).data('slider');

		let setEarning = function () {
			$('#data_earning').on('keydown keyup', function () {
				var base_price = parseInt(cls.activeData.price);
				var percentage = <any>$(this).val() / base_price * 100;

				if (percentage < 200) {
					$('#PriceSlider').slider('setValue', percentage);
					setInputs(percentage, true);
				} else {
					toastr.remove()
					toastr.warning('Maximum percentage exceeded!');
				}

			});
		}
		setEarning();

		let setQuantity = function () {
			$('#data_quantity').on('keydown keyup', function () {
				setInputs(PriceSlider.getValue());
			});
		}
		setQuantity();

		let setInputs = function (percentage: any, noEarning: boolean = false) {
			var qty = parseInt(<any>$('#data_quantity').val());
			var base_price = parseInt(cls.activeData.price);
			var earning = percentage / 100 * base_price;
			var totalEarning = earning * qty;
			var totalPrice = base_price + percentage / 100 * base_price;
			var user_price = totalPrice - base_price;
			$('#data_base_price').val(base_price);
			$('#display_base_price').text(base_price);
			$('#data_price').val(totalPrice);
			$('#data_user_price').val(user_price);
			$('#display_price').text(totalPrice);
			if (!noEarning) {
				$('#data_earning').val(earning.toFixed(2));
			}
			$('#data_total_earning').val(totalEarning.toFixed(2));
			$('#display_total_earning').text(totalEarning.toFixed(2));
			$('#data_percentage').val(PriceSlider.getValue());
		}

	}

	public setTags() {
		$("#TagsInput").tagsinput({
			tagClass: 'badge badge-info'
		});
	}

	public setDesign() {
		let cls = this;
		let data = cls.activeData;

		$('.frontimageHolder').find('.ProductFacing').attr('src', cls.designData.front.img_url);
		$('.frontimageHolder').find('.drawingArea').css({
			'position': 'absolute',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'left': data.front.left + 'px',
			'top': data.front.top + 'px',
		});
		this.designData.front.left = data.front.left;
		this.designData.front.top = data.front.top;
		this.designData.front.height = this.canvas_front.height;
		this.designData.front.width = this.canvas_front.width;
		this.designData.front.design = this.canvas_front.toDataURL();
		$('.frontimageHolder').find('.drawingArea').html('<img src="' + this.canvas_front.toDataURL() + '">');

		if (cls.hasBack == true) {
			$('.backimageHolder').find('.ProductFacing').attr('src', cls.designData.back.img_url);
			$('.backimageHolder').find('.drawingArea').css({
				'position': 'absolute',
				'width': data.back.width + 'px',
				'height': data.back.height + 'px',
				'left': data.back.left + 'px',
				'top': data.back.top + 'px',
			});
			this.designData.back.left = data.back.left;
			this.designData.back.top = data.back.top;
			this.designData.back.height = this.canvas_back.height;
			this.designData.back.width = this.canvas_back.width;
			this.designData.back.design = this.canvas_back.toDataURL();
			$('.backimageHolder').find('.drawingArea').html('<img src="' + this.canvas_back.toDataURL() + '">');
		} else {
			this.designData.back.design = '';
			$('.backimageHolder').find('.drawingArea').html('');
			$('.backimageHolder').find('.ProductFacing').attr('src', '');
		}
		cls.backgroundSetter('.frontimageHolder', '.backimageHolder', cls.PrimaryColor);
	}

	public sellPage() {
		let cls = this;
		if (!cls.isDirty) {
			toastr.error('You have not made any changes');
			//	window.location.href = '#/create';
			cls.$route.navigate('home');
		} else {

			cls.setDesign();
			$('#data_base_price').val(cls.activeData.price);
			$('#display_base_price').text(cls.activeData.price);
			$('#display_price').text(cls.activeData.price);

			$('#CategorySelect option[value=' + cls.activeData.category_id + ']').prop("selected", true);
			$('#CategorySelect').val(cls.activeData.category_id);
			$('#CategorySelect').prop('disabled', true);
			/*if(!cls.hasBack){
				$('#SizeInput').prop('disabled', true);
			}else{
				$('#SizeInput').prop('disabled', false);
			}*/

			cls.postForm(cls);
		}
	}

	public postForm(cls: any): any {
		$(document).on('submit', '[data-post-customizer]', function (e) {
			e.preventDefault();
			let productId: any = $('#ProductSelect option:selected').attr('data-id');
			var form = $(this);
			var btnText = form.find(':input[type="submit"]').text();
			//console.log(cls.designData);
			//if (e.isDefaultPrevented()) {
			var data = form.serializeArray();
			data.push({
				name: 'design_id',
				value: productId
			});
			data.push({
				name: 'design_data',
				value: JSON.stringify(cls.designData)
			});
			data.push({
				name: 'colors',
				value: JSON.stringify(cls.PrimaryColor)
			});
			data.push({
				name: 'has_back',
				value: cls.hasBack
			});
			data.push({
				name: 'sub_category',
				value: <any>$('#DesignTags').val()
			});
			data.push({
				name: 'category',
				value: cls.activeData.category_id
			});
			data.push({
				name: 'brand_description',
				value: cls.activeData.description
			});
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: data,
				beforeSend: function (xhr) {
					//console.log(data);
					form.find(':input').prop('disabled', true);
					form.find(':input[type="submit"]').text('Working...');
				},
				timeout: 20000,
			})
				.done(function (data, textStatus, jqXHR) {
					if (jqXHR.status == 200) {
						form.find('.has-error').removeClass('has-error');
						form.find('.help-block').text('');
						form.find(':input[type="submit"]').text(btnText);
						if (data.message) {
							toastr.success(data.message);
						}
						var wait = setInterval(function () {
							window.location.href = data.redirect_url;
						}, 1000);
						form.find(':input[type="submit"]').text(btnText);

					}
				})
				.fail(function (jqXHR, textStatus, errorThrown) {

					if (jqXHR.status == 500) {
						var data = JSON.parse(jqXHR.responseText);
						if (typeof data.error === 'string' || data.error instanceof String) {
							toastr.error(data.error, "Error");
							form.find(':input').prop('disabled', false);
							form.find(':input[type="submit"]').text(btnText);

						} else {

							form.find('.has-error').removeClass('has-error');
							form.find('.help-block').text('');
							$.each(data.error, function (ind: any, val: any) {
								form.find('.' + ind).addClass('has-error');
								form.find('.' + ind + ' .help-block').text(val[0]);
							});
							form.find(':input').prop('disabled', false);
							form.find(':input[type="submit"]').text(btnText);
						}

					} else if (jqXHR.statusText == "timeout") {
						form.find('.has-error').removeClass('has-error');
						form.find('.help-block').text('');
						toastr.error('Request Timeout! Please try again.');
						form.find(':input').prop('disabled', false);
						form.find(':input[type="submit"]').text(btnText);
					}
				});


		});
	}

	public slugify(str: string) {
		str = str.replace(/^\s+|\s+$/g, ''); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
		var to = "aaaaaeeeeeiiiiooooouuuunc------";
		for (var i = 0, l = from.length; i < l; i++) {
			str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		}

		str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes

		return str;
	}

	public setRoutes() {
		const cls = this;
		let render = (name: any) => {
			let pages = $('page');
			let page = pages.filter('[data-route=' + name + ']');
			if (page.length) {
				pages.removeClass('page-show').addClass('page-hide');
				page.removeClass('page-hide').addClass('page-show');
			}
		};

		this.$route.on({
			'home': () => {
				render('home');

			},
			'sell': () => {
				cls.sellPage();
				cls.setPriceSlider();
				cls.setTags();
				render('sell');

			},
			'buy': () => {
				render('buy');

			}
		});

		this.$route.on(() => {
			render('home');


		});

		this.$route.resolve();
	}

}

$(function () {
	const ____init_customizer_page = new Customizer();
}); 