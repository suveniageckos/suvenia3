@extends('layouts.dashboard_designer')

@section('title', 'My Design Orders' )

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
            
                    <div class="card-header card-header-info">
                        <div class="row">
                            <div class="col-md-4">
                                    <div><h4 class="header_text m-0">My Design Orders</h4></div>
                            </div>
                            
                            <div class="col-md-4"></div>
                            <div class="col-md-4 uk-text-right">
                            </div> 
                        </div>
                    </div> 
 
                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead> 
                                <tr>
                                    <th>GIG</th>
                                    <th></th>
                                    <th>ORDER NUMBER</th>
                                    <th>DURATION</th>
                                    <th>PRICE</th>
                                    <th></th>
                                </tr> 
                                </thead>
                                <tbody>
                                    @forelse($active_orders as $order)
                                    <tr>
                                        <td>{{ $order->gig->title }}</td>
                                        <td>
                                            <ul class="list-unstyled">
                                                <li class="d-inline-block"><img src="{{ asset($order->designer->photo_url) }}" class="mr-1 uk-border-circle" alt="{{ $order->designer->username }}" width="50"></li>
                                                <li class="d-inline-block">
                                                    <p class="text-secondary mb-0 uk-text-capitalize" style="font-size:12px;">{{ $order->designer->username }}</p>
                                                    @php $rateValues = [1, 2, 3, 4, 5] ;@endphp
                                                    <select class="bar-rated" data-url="{{ route('app:designer:dashboard:rate_designer', ['id'=> $order->id]) }}">
                                                    @foreach ($rateValues as $item)
                                                    <option value="{{ $item }}" {{ $order->designer->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                                                    @endforeach
                                                    </select>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>{{ $order->reference }}</td>
                                        <td>{{ $order->plan->duration }}</td>
                                        <td>&#8358;{{ $order->amount }}</td>
                                        <td><a href="{{ route('app:seller:dashboard:single_design_service', ['id'=> $order->id]) }}" class="text-muted">View</a></td>
                                    </tr>
                                    @empty 
                                    <tr>
                                        <td colspan="100">You Do Not Have Orders Yet</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                       {{ $active_orders->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection