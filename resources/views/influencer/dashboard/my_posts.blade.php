@extends('layouts.dashboard_influencer')

@section('title', 'My Posts' )

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
            <div class="card-header card-header-info">
                    <h4 class="header_text m-0">My Post</h4>
            </div>

            <div class="card-body">
             @foreach($posts as $post)
                <div class="mb-2 border p-3">
                    <div class="row">
                        <div class="col-md-9">
                                <div class="d-flex justify-content-start">
                                    <div class="w-75">
                                           
                                    <img src="{{ asset($post->product->photos->first()->public_url) }}" width="100">
                                    </div>
                                    <div class="">
                                    <h4 class="m-0 gig-media-title">{{ $post->title }}</h4>
                                    <p class="mt-1 mb-0 gig-media-user">{{ $post->product->user->username }}</p>
                                    <p class="mt-1 mb-0 gig-media-user">{{ $post->content }}</p>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-3">
                            <ul class="list-unstyled m-0">
                                <li class="d-block mb-1">
                                    <span class="gig-list-header">Category: </span><span class="gig-list-desc">{{ $post->type }}</span>
                                </li>
                                <li class="d-block mb-1">
                                <span class="gig-list-header">Type: </span><span class="gig-list-desc">{{ $post->type }}</span>
                                </li>
                               
                            </ul>
                           
                        </div>
                    </div>
                </div>
             @endforeach
            </div>
        </div>
    </div>
</div>

@endsection