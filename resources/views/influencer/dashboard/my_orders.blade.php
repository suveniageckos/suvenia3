@extends('layouts.dashboard_influencer')

@section('title', 'My Orders' )

@section('content')
<div class="">
    <h5>My Orders</h5>
</div>
<div class="bg-white uk-box-shadow-small p-2">
   <div class="row justify-content-center">
       @forelse($gigs as $gig)
       <div class="col-md-3">
            <div class="uk-card ">
                @if($gig->promotion_format == "VIDEO")
                <div class="bg bg-success p-1 uk-text-center w-25 text-white font-weight-bold uk-position uk-position-top-right " style="background: #1f7100;">Video</div>
                @else
                <div class="bg p-1 uk-text-center w-25 text-white font-weight-bold uk-position uk-position-top-right " style="background: #a36a3d;">Photo</div>
                @endif
                    <div class="uk-card-media-top">
                        <img src="{{ asset($gig->product->photos->first()->public_url) }}" alt="">
                    </div>
                    <div class="uk-card-body p-3 bg-light">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <p class="font-weight-bold m-0 text-dark" style="font-size:14px;">{{ title_case($gig->product->name) }}</p>
                                    <p class="text-muted m-0" style="font-size:14px;">{{ title_case($gig->product->name) }}</p>
                                </div>
                                <div>
                                    <p class="text-muted m-0" style="font-size:14px;">Amount</p>
                                    <p class="font-weight-bold m-0 text-dark" style="font-size:14px;">&#8358;{{ title_case($gig->product->price) }}</p>
                                </div>
                            </div> 
                            <div class="mt-1">
                                <p class="text-muted m-0" style="font-size:14px;">{{ str_limit($gig->description, 30) }}</p>
                            </div>
                            <div class="d-flex justify-content-between mt-2">
                                <div>
                                </div>
                                <div>
                                    @if($gig->job_status == 0)
                                    <a href="javascript:;" class="btn btn-info rounded-pill" modal-form data-title="Complete Job" data-url="{{ route('app:influencer:dashboard:gig_complete_modal', ['id'=> $gig->id]) }}">Complete</a>
                                    @endif
                                    @if($gig->job_status == 1)
                                    <button class="btn rounded-pill" style="background: #9b9b9b; font-size:13px; color: #fff;">Pending</button>
                                    @endif

                                </div>
                            </div>
                    </div>
                </div>
        </div>
        @empty
        <div class="col-md-12 uk-text-center">
            <p>Ooops.. no jobs at this time!</p>
        </div>
       @endforelse
   </div>
</div>
@endsection