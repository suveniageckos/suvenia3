<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(InfluencersTableSeeder::class);
        $this->call(SellersTableSeeder::class);
        $this->call(BuyersTableSeeder::class);
        $this->call(DataTypesTableSeeder::class);
        $this->call(DataRowsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(EcoBrandablesTableSeeder::class);
        $this->call(DesignersTableSeeder::class);
        $this->call(EcoShippingZonesTableSeeder::class);
        $this->call(EcoShippingLocationsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
