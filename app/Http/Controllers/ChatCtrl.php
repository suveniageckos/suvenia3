<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\DesignerOrder;
use App\DesignerAttachment as Photo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ChatCtrl extends Controller
{ 
    public function new_chat(Request $request, $id, $senderId){
        if($request->isMethod('POST')){
            $chat = new Chat();
            $order = DesignerOrder::where('id', $id)->first();
            $sender = $senderId;
           // $reciever = $order->user_id;
            $reciever = (($senderId == $order->user_id) && ($request->user_type != $order->user_type)) ? $order->designer_id : $order->user_id;
           // $buyer = $this->get_user($order->user_type)::where('user_id', $order->user_type)->first();
           // $designer = \App\Designer::where('user_id', $order->designer_id)->first();

            $chat->sender = $sender; 
            $chat->reciever = $reciever;
            $chat->model_id = $id;
            $chat->content = $request->message;
            $chat->type = $request->chat_type;
            $chat->sender_type = $request->user_type;
            $chat->save();
 
            $respData = [
                "avatar"=> $this->get_sender_data($order, $sender)->photo_url,
                "message"=> $chat->content,
                "time"=> $chat->created_at,
                "id"=> $chat->id,
                //"line_class"=> $chat->sender == $senderId ? "replies" : " sent",
                "line_class"=> $chat->sender_type == $request->user_type ? "replies" : " sent",
                "chat_type"=> $chat->type,
                "attachment_url"=> $chat->type == "MEDIA" ? asset($chat->attachment_url) : ""
            ];
            
           return response()->json(['data'=> $respData], 200);
        }
        
    }

    public function all_chats(Request $request, $id, $senderId){
        if($request->isMethod('POST')){
            $chats = Chat::where('model_id', $id)->get();
            $order = DesignerOrder::where('id', $id)->first();
            $sender = $senderId;
            $reciever = (($senderId == $order->user_id) && ($request->user_type != $order->user_type)) ? $order->designer_id : $order->user_id;
            $data = [];
            foreach($chats as $chat){
                $data[] = [
                    "avatar"=> $this->get_sender_data($order, $sender)->photo_url,
                    "message"=> $chat->content,
                    "time"=> $chat->created_at,
                    "id"=> $chat->id,
                    "line_class"=> $chat->sender_type == $request->user_type ? "replies" : " sent",
                    "chat_type"=> $chat->type,
                    "attachment_url"=> $chat->type == "MEDIA" ? asset($chat->attachment_url) : "",
                    "background"=> asset('img/plh.jpg')
                ];
            }
            
           return response()->json(['data'=>$data], 200);
        }
    }

    public function get_user($user_type){
        if($user_type === 'SELLER'){
            return \App\Seller;
        }elseif($user_type === 'BUYER'){
            return \App\User;
        }
    }

    public function get_sender_data($order, $sender){
        $isDesigner = $order->designer_id == $sender ? true : false;
        if($isDesigner){
            return \App\Designer::where('id', $order->designer_id)->first();
        }elseif(!$isDesigner && $order->user_type === 'SELLER'){
            return \App\Seller::where('id', $order->user_id)->first();
        }elseif(!$isDesigner && $order->user_type === 'BUYER'){
            return \App\User::where('id', $order->user_id)->first();
        }
    }

    public function update_chat(Request $request, $id, $senderId){
        if($request->isMethod('POST')){
            if(!$request->last_id){
                return response()->json(['data'=>[]], 200);
            }else{
                $chats = Chat::where([['id', '>', $request->last_id], ['model_id', $id]])->get();
                $order = DesignerOrder::where('id', $id)->first();
                $sender = $senderId;
                $reciever = (($senderId == $order->user_id) && ($request->user_type != $order->user_type)) ? $order->designer_id : $order->user_id;
                $data = [];
                foreach($chats as $chat){
                    $data[] = [
                        "avatar"=> $this->get_sender_data($order, $sender)->photo_url,
                        "message"=> $chat->content,
                        "time"=> $chat->created_at,
                        "id"=> $chat->id,
                        "line_class"=> $chat->sender_type == $request->user_type ? "replies" : " sent",
                        "chat_type"=> $chat->type,
                        "attachment_url"=> $chat->type == "MEDIA" ? asset($chat->attachment_url) : "",
                        "background"=> asset('img/plh.jpg')
                    ];
                }
                
               return response()->json(['data'=>$data], 200);
            }
           
        }
    }

    public function upload_chat_file(Request $request, $id, $senderId){
        if($request->isMethod('POST')){
            $chat = new Chat();
            $order = DesignerOrder::where('id', $id)->first();
            $sender = $senderId;
            $reciever = (($senderId == $order->user_id) && ($request->user_type != $order->user_type)) ? $order->designer_id : $order->user_id;

            $uploadFile = $this->upload_design($request);
            $photo = new Photo();
            $photo->user_id = $senderId;
            $photo->order_id = $id;
            $photo->path = $uploadFile;
           // $photo->public_url = 'storage/' . $uploadFile;
            $photo->public_url = Storage::url($uploadFile);
            $photo->save();

            $chat->sender = $sender; 
            $chat->reciever = $reciever;
            $chat->model_id = $id;
            $chat->content = $photo->id;
            $chat->type = $request->chat_type;
            $chat->sender_type = $request->user_type;
            $chat->attachment_id = $photo->id;
            $chat->attachment_url = asset($photo->public_url);
            $chat->save();

            $respData = [
                "avatar"=> $this->get_sender_data($order, $sender)->photo_url,
                "message"=> $chat->content,
                "time"=> $chat->created_at,
                "id"=> $chat->id,
                "line_class"=> $chat->sender_type == $request->user_type ? "replies" : " sent",
                "chat_type"=> $chat->type,
                "attachment_url"=> $chat->type == "MEDIA" ? asset($chat->attachment_url) : "",
                "background"=> asset('img/plh.jpg')
            ];
            
           return response()->json(['data'=> $respData], 200);

        }
    }

    public function upload_design($request)
    {
            if ($request->file('file')->isValid()) {
                $file = $request->file('file');
                $path = Storage::putFile(
                    'designers_graphs', $request->file('file')
                );
                return $path;
            }
    }

}
