<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $mail_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $mail_type)
    {
        $this->user = $user;
        $this->mail_type = $mail_type;
    } 
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(setting('site.site_email'), setting('site.title') . ' ' . setting('site.description'))
        ->subject(setting('site.title') . ' - Email Verification')

             ->view('mails.verify_email');
    }
}
