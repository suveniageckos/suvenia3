@extends('layouts.dashboard_designer')

@section('title', 'Update Profile' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:designer:dashboard:profile_settings') }}" class="card_head_nav_item {{  $utils->makeActive(['app:designer:dashboard:profile_settings'], 'active') }}">Personal Details</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:designer:dashboard:change_password') }}" class="card_head_nav_item {{  $utils->makeActive(['app:designer:dashboard:change_password'], 'active') }}">Change Password</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:designer:dashboard:bank_details') }}" class="card_head_nav_item {{  $utils->makeActive(['app:designer:dashboard:bank_details'], 'active') }}">Bank Details</a>
                        </div>
                    </div> 
                </div>
                <div class="card-body">
                <form action="{{ url()->current() }}" method="POST" data-post>
                        <div class="row justify-content-center mb-4">
                            <div class="col-md-4 uk-text-center">
                                <img src="{{ Auth::guard('designers')->user()->photo_url ? asset(Auth::guard('designers')->user()->photo_url) : asset('user/default.png') }}" width="100" uk-responsive class="m-auto" id="DisplayPreviewDiv">
                                <div class="mt-2">
                                <h4 class="usernames m-0">{{ Auth::guard('designers')->user()->lastname . ' ' . Auth::guard('designers')->user()->firstname}}</h4>

                                <div class="custom-file mt-2">
                                    <input type="file" class="custom-file-input" id="customFile" name="image" data-preview preview="#DisplayPreviewDiv">
                                    <label class="custom-file-label" for="customFile">Change Photo</label>
                                </div>

                                </div> 
                            </div>
                        </div>
                        <div class="w-75 m-auto">
                            <div class="row justify-content-center">
                                <div class="col-md-6 form-group username">
                                    <label class="app-label">UserName</label>
                                <input type="text" class="form-control app-form" name="username" value="{{ Auth::guard('designers')->user()->username }}">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group firstname">
                                    <label class="app-label">FirstName</label>
                                    <input type="text" class="form-control app-form" name="firstname" value="{{ Auth::guard('designers')->user()->firstname }}">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group lastname">
                                        <label class="app-label">LastName</label>
                                        <input type="text" class="form-control app-form" name="lastname" value="{{ Auth::guard('designers')->user()->lastname }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group phone">
                                        <label class="app-label">Phone Number</label>
                                        <input type="number" class="form-control app-form" name="phone" value="{{ Auth::guard('designers')->user()->phone }}">
                                        <span class="help-block"></span>
                                </div>
                                
                                <div class="col-md-12 form-group bio">
                                        <label class="app-label">Bio</label>
                                <textarea class="form-control app-form" name="bio" rows="4">{{ Auth::guard('designers')->user()->bio }}</textarea>
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-12 form-group uk-text-center">
                                    <button class="btn btn-info" type="submit">UPDATE</button>
                                </div>
                            
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
   
</div>


@endsection

 