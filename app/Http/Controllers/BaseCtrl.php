<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory as Category;
use DB;

class BaseCtrl extends Controller{

    public function index(Request $request){
       // $url = parse_url('mysql://b7680eb9dd827b:eb42f85b@us-cdbr-iron-east-03.cleardb.net/heroku_724285d14f5f0ea?reconnect=true');

        $categories = Category::with(['children'=> function($q){
            $q->with('children');
        }])->where('parent_id', 0)->get();

        $showcase_categories = Category::where('is_sub', true)->get();
        $banner_grids = DB::table('display_block')->where('block_code', 'ID:MASONRY')->get();

        // $connected = @fsockopen('https://www.blog.suvenia.com/wp-json/wp/v2/posts?per_page=1');
        // if($connected){
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://www.blog.suvenia.com/wp-json/wp/v2/posts?per_page=1');
        $response = $request->getBody();
        $blog_details = json_decode($response);

        foreach($blog_details  as $value){
          $id = $value->featured_media;

        $url = 'http://blog.suvenia.com/wp-json/wp/v2/media/'. $id;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = json_decode($res->getBody(), true);
        $imgUrl = $data['media_details']['sizes']['medium']['source_url'];
        $image_url = str_replace('https://', 'http://', $imgUrl);

     }

        $client2 = new \GuzzleHttp\Client();
        $request2 = $client2->get('https://www.vzine.suvenia.com/wp-json/wp/v2/posts?per_page=1');
        $response2 = $request2->getBody();
        $vzine_details = json_decode($response2);

        // }else{
        //     $vzine_details = [];
        //     $blog_details = [];
        //     $image_url = "";
        // }

        //   return dd($url);

        return view('shop.index', [
            'categories'=> $categories,
            'showcase_categories'=> $showcase_categories,
            'banner_grids'=> $banner_grids,
            'blog_details' => $blog_details,
            'vzine_details' => $vzine_details,
            'image_url' => $image_url
            ]);
    }

    public function seeder(Request $request){
        /*
        $block = DB::table('display_block')->insert([
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/1.png',
                'block_value'=> 'Birthday Celebration',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/2.png',
                'block_value'=> 'Coloful Phone Cases',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/3.png',
                'block_value'=> 'Fashion',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/4.png',
                'block_value'=> 'Game Of Thrones',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/5.png',
                'block_value'=> 'Fun  Throw Pillows',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/6.png',
                'block_value'=> 'Foodie',
            ],
        ]);
        */
    }

    public function migrator(Request $request){

    }

    public function faqs(){
        return view('pages.faqs');
    }
    public function return_policy(){
        return view('pages.return_policy');
    }
    public function terms(){
        return view('pages.terms');
    }
    public function shipping(){
        return view('pages.shipping');
    }
    public function about(){
        return view('pages.about');
    }
    public function contact(){
        return view('pages.contact');
    }
}
