@extends('layouts.influencer')

@section('title', 'Become An Influencer' )

@section('content')


<div class="uk-cover-container banner-height">
    <img src="{{ $utils->get_image('site.influencer_landing_page_banner') }}" alt="" uk-cover>
    <!--<div class="uk-overlay-primary uk-position-cover"></div>-->
    <div class="uk-section uk-position-center">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="uk-text-center">
                <div class="n-centered-hero">
                    <b style="color: #fff;">EARN WITH SUVENIA</b>
                    <p class="mt-1 n-centered-sub-hero" style="color: #fff;">Are you an influencer? Get paid a percentage on products you share and promote on social media. </p>
                </div>
                <div class="mt-4">
                <a class="btn btn-info" style="color: #fff;">GET STARTED</a>
                </div>
                </div>

            </div>
        </div>
    </div>

</div>

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="borde bg-white" style="position: relative;">
        {{-- <div class="float-slider" style="padding: 20px 60px; left:20%; right: 20%; top: -80px;" uk-slider> --}}
                <div class="float-slider" style="padding: 20px 60px;" uk-slider>
                <div class="row">
                    <div class="col-12 uk-text-center">
                    <h2 class="m-0 header">Complete tasks and get paid</h2>
                    </div>
                </div>
                <div class="row mt-3">
                        <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >

                                <ul class="uk-slider-items uk-child-width-1-3 uk-child-width-1-3@m uk-grid">
                                    @foreach($service_grids as $service_grid)
                                        <li class="uk-text-center">
                                            <a href="javascript:;">
                                                 <div class="uk-panel">
                                                    <img src="{{ asset($service_grid->block_key) }}" alt="" width="200" uk-responsive>

                                                </div>
                                            </a>
                                        <a href="javascript:;" class="_prod_title mt-2">{{ title_case($service_grid->block_value) }}</a>
                                        </li>
                                    @endforeach
                                </ul>

                        </div>
                </div>
                <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
                <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
        </div>
</div>


<div class="uk-section bg-white p-5">
        <div class="uk-container uk-container-small">
            <div class="row justify-content-center">
                <div class="col-6">
                    <p class="m-0 after_cat_slide_desc">Enjoy trying new products and taking pictures? Why not earn from them while you’re at it!</p>
                </div>
            </div>
        </div>
</div>
@endif

<div class="uk-section bg-grey p-4">
        <div class="uk-container uk-container-small">
            <div class="app-sec-header sec-center mb-4">
                    <h1 class="title">HOW IT WORKS</h1>
            </div>
            <div class="row mt-2">

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-enter"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                                Register
                            </h4>
                        <p class="mt-1 mb-0 service-desc">Register by filling short details about you as an influencer</p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-verified"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                            Verify
                        </h4>
                        <p class="mt-1 mb-0 service-desc">
                                Get your account verified as an influencer by following the verification process
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-share"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                            Share
                        </h4>
                        <p class="mt-1 mb-0 service-desc">
                            Choose from our products and share with your link.
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-email_money"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                            Get Paid
                        </h4>
                        <p class="mt-1 mb-0 service-desc">
                            Get your earnings paid directly to your preffered bank on all sales made
                        </p>
                    </div>
                </div>

            </div>
        </div>
</div>

<div class="uk-section bg-white">
    <div class="uk-container">
        <div class="app-sec-header sec-center mb-4">
            <h1 class="title">THESE BRANDS WANT TO WORK WITH YOU</h1>
        </div>
        <div class="row justify-content-center">
            @foreach ($utils->getDirectoryImages('storage/influencer/brands') as $item)
                <div class="col-md-2 col-6 mb-4">
                <img src="{{ asset($item->path) }}" alt="Suvenia Influencer" class="m-auto" uk-responsive width="170">
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="uk-section bg-grey p-4">
    <div class="uk-container">
            <div class="app-sec-header sec-center mb-4">
                    <h1 class="title">Join Our Influencer Community</h1>
            </div>

            <div class="row justify-content-center">
              @foreach($influencers as $influencer)
                <div class="col-md-3 mb-2">
                    <a href="javascript:;">
                        <div class="uk-cover-container influncer-grid-single ">
                                <canvas width="280" height="250"></canvas>
                                <img src="{{ asset($influencer->cover_photo_url) }}" alt="" uk-cover>
                                <div class="uk-overlay-prima uk-position-cover"></div>
                                <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">{{ $influencer->username }}</p>
                                <p class="mt-1 mb-0 influencer_title">{{ is_null($influencer->socials)  ? $influencer->socials->name : ''}} Influencer</p>
                                </div>
                        </div>
                    </a>
                </div>
              @endforeach
            </div>

            <div class="row justify-content-center mt-4">
                <div class="col-md-6 col-12 uk-text-center">
                        <i class="mb-0 sign-in-info">Start creating awesome content by promoting a product</i>
                        <p><a href="{{ route('app:influencer:apply') }}" class="btn btn-info">GET STARTED</a></p>
                </div>
            </div>
    </div>


</div>

@endsection
