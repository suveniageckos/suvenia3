@extends('layouts.dashboard')

@section('title', 'My Notifications')

@section('content')

<div class="uk-container">
        <div class="row">
        <div class="col-md-12">


                <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h4 class="border-bottom border-gray pb-2 mb-0">Notifications</h4>

                        @forelse($notifications as $notification)
                        {{-- @foreach($notifications as $notification) --}}
                <?php $notificationData = json_decode($notification->message_body, true); ?>
@if ($notification->read_at == true)
<div class="container text-muted" style="margin-top:20px;">
        <div class="row">
          <div class="col-md-1">
            <img class="uk-preserve-width uk-border-circle" src="{{ asset('storage/'. $notificationData['photo_url']) }}" width="80" alt="">
          </div>
          <div class="col-md-9">
              <p style="font-size: 1.2em; font-weight:700; text-transform:capitalize;">{{$notificationData['name']}} <small>({{$notificationData['data']}} by {{$notificationData['influencer_username']}})</small></p>
            {{ str_limit($notificationData['description'], 180) }}
          </div>

          <div class="col-md-2 p-4">
            <a href="{{ route('app:dashboard:delete_notifications', ['id'=> $notification->id]) }}" style="font-size: 2em; color:red; "><i class="fa fa-trash"></i></a>
          </div>
        </div>
</div>
@else
<div class="container" style="margin-top:20px;">
        <div class="row">
          <div class="col-md-1">
            <img class="uk-preserve-width uk-border-circle" src="{{ asset('storage/'. $notificationData['photo_url']) }}" width="80" alt="">
          </div>
          <div class="col-md-9">
              <p style="font-size: 1.2em; font-weight:700; text-transform:capitalize;">{{$notificationData['name']}} <small>({{$notificationData['data']}} by {{$notificationData['influencer_username']}})</small></p>
            {{ str_limit($notificationData['description'], 180) }}
          </div>

          <div class="col-md-2 p-4">
            <a href="{{ route('app:dashboard:delete_notifications', ['id'=> $notification->id]) }}" style="font-size: 2em; color:red; "><i class="fa fa-trash"></i></a>
          </div>
        </div>
</div>
@endif



                        @empty

                        <div class="media text-muted pt-3">
                        <p>There are no notifications</p>
                        </div>

                        @endforelse

                        <div class="row pt-5">
                            <div class="col-12 user_orders_pagination">
                                {!! $notifications->render() !!}
                            </div>
                    </div>

                      </div>




        </div>
        </div>
        </div>


                </div>
            </div>
        </div>
</div>

@endsection
