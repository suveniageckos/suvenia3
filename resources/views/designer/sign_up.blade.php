@extends('layouts.login')

@section('title', 'Sign Up As A Designer' )
@section('header')
 @include('partials.login_header', ['route_name'=> 'app:designer:landing_page'])
@endsection
@section('content')


<div class="uk-container mt-3 mb-3">
    <div class="row justify-content-center">

   <div class="col-md-4">

     <div class="card app-box">
       <div class="box-header">
         <p class="title">SIGN UP AS DESIGNER</p>
         <p class="sub-title">Sign up to join to create, buy or request for product of your choice.</p>
       </div>
       <div class="box-body">
           <form class="app-form" action="{{ route('app:designer:sign_up') }}" method="POST" >
               @csrf
               <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                  <input type="text" class="form-control form-control-lg" placeholder="Username" name="username"  value="{{ old('username') }}">
                  <p class="form-text help-block">{{ $errors->first('username') }}</p>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                  <input type="email" class="form-control form-control-lg" placeholder="Email Address" name="email"  value="{{ old('email') }}">
                  <p class="help-block">{{ $errors->first('email') }}</p>
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                  <input type="password" class="form-control form-control-lg" placeholder="Password" name="password"  value="{{ old('password') }}">
                  <p class="form-text help-block">{{ $errors->first('password') }}</p>
                </div>

                <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                  <input type="password" class="form-control form-control-lg" placeholder="Confirm Password" name="confirm_password"  value="{{ old('confirm_password') }}">
                  <p class="form-text help-block">{{ $errors->first('confirm_password') }}</p>
                </div>

                <div class="form-group {{ $errors->has('portfolio_link') ? 'has-error' : ''}}">
                    <input type="url" class="form-control form-control-lg" placeholder="Portfolio Link" name="portfolio_link"  value="{{ old('portfolio_link') }}">
                    <p class="form-text help-block">{{ $errors->first('portfolio_link') }}</p>
                </div>

                <div class="form-group form-row {{ $errors->has('tos') ? 'has-error' : ''}}">
                    <div class="col-md-12">
                       <div class="custom-control custom-checkbox mr-sm-2">
                           <input type="checkbox" class="custom-control-input" id="customControlAutosizing" value="1" name="tos" readonly checked>
                           <label class="custom-control-label remember-text" for="customControlAutosizing">I agree to the terms</label>
                         </div>
                    </div>
                    <p class="help-block">{{ $errors->first('tos') }}</p>
            </div>

                  <button type="submit" class="btn btn-info btn-lg btn-block rounded">SIGN UP</button>
                </form>

                <p class="body-info">Already have an account? <a href="{{ route('app:designer:login') }}">Sign in now</a></p>
       </div>
       <div class="box-footer mt-2">
         <div class="fab-info">OR</div>
         <p class="m-1 info">Sign In with social media</p>
         <div class="mt-3">
             <a href="" class="uk-icon-button uk-margin-small-right app-auth-icon" uk-icon="google-plus"></a>
             <a href="" class="uk-icon-button  uk-margin-small-right app-auth-icon" uk-icon="facebook"></a>
             <a href="" class="uk-icon-button app-auth-icon" uk-icon="twitter"></a>
         </div>
       </div>
     </div>

   </div>

    </div>
</div>

@endsection
