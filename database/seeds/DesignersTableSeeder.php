<?php

use Illuminate\Database\Seeder;

class DesignersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('designers')->delete();
        
        \DB::table('designers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'ref' => 'CHxik93NgN',
                'username' => '1',
                'slug' => 'designers',
                'firstname' => 'ukandu',
                'lastname' => 'michael',
                'email' => 'admin@ppp.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$3Uc9krBFKxUNRNeysi.B/.s84.OEA.QeLf3mJerGTv3a0CBsxy.n2',
                'password_token' => NULL,
                'password_token_expiry' => NULL,
                'email_token' => 'GaISWcuDnE1RFANfb5qKJQJbloIAgxQBAHvN',
                'email_token_expiry' => '2019-06-02 04:49:36',
                'has_password' => 1,
                'email_verified' => 1,
                'facebook' => NULL,
                'twitter' => NULL,
                'linkedin' => NULL,
                'photo_url' => 'http://127.0.0.1:8000/storage/designers/rHWVt88ISMm7RaqkRLMjXFaTXZFtHcMF8zQIPoPo.jpeg',
                'image_path' => 'designers/rHWVt88ISMm7RaqkRLMjXFaTXZFtHcMF8zQIPoPo.jpeg',
                'cover_photo_url' => NULL,
                'phone' => '07061244001',
                'balance' => '2000.00',
                'earnings' => '0.00',
                'bank_sort' => '011',
                'bank_bvn' => NULL,
                'bank_account' => '3032439851',
                'bank_name' => 'ukandu michaell',
                'transfer_code' => 'RCP_0k4xb42tjaar54w',
                'portfolio_link' => NULL,
                'bio' => 'The bio must be at least  The bio must be at least',
                'accepted_payment' => NULL,
                'minimum_amount' => NULL,
                'is_approved' => NULL,
                'created_at' => '2019-06-01 04:49:36',
                'updated_at' => '2019-07-31 18:43:13',
            ),
        ));
        
        
    }
}