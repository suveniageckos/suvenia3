<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Template as Template;
use App\Helpers\Utils as Utils;
use App\Http\Controllers\Controller;
use App\ProductCategory as Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TemplateCtrl extends Controller
{
    public function index(Request $request){
        $templates = Template::paginate(12);
        return view('vendor.voyager.templates.index', ['templates'=> $templates]);
    } 
 
    public function add(Request $request){
        if ($request->isMethod('post')) { 
           // print_r($request->all());
           $utils = new Utils();
           $template = new Template();
           $template->data = $request->data;
           $path = 'design_templates/' . str_random(36) . '.png';

           if (preg_match('/^data:image\/(\w+);base64,/', $request->photo_url)) {
            $data = str_replace('data:image/png;base64,', '', $request->photo_url);
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
            Storage::put($path, $data);
            $template->photo_url = Storage::url($path);
            $template->image_path = $path;
        }
          // $template->photo_url = $utils->base64_img($request->photo_url, 'template', 'design_templates');
           $template->save();
           $red = action([TemplateCtrl::class, 'index']);
           return redirect($red);
        } 
        return view('vendor.voyager.templates.add', []);
    }

    public function edit(Request $request, $id){
        $template = Template::where('id', $id)->first();
        if ($request->isMethod('post')) {
            // print_r($request->all());
            $old_photo = basename($template->photo_url);
            $link_photo = public_path('design_templates/') . $old_photo;
            
            $utils = new Utils();
            $template = Template::where('id', $id)->first();
            $template->data = $request->data;
            $path = 'design_templates/' . str_random(36) . '.png';
            if (preg_match('/^data:image\/(\w+);base64,/', $request->photo_url)) {
                if(Storage::exists($template->image_path)){
                    Storage::delete($template->image_path);
                }
               
                $data = str_replace('data:image/png;base64,', '', $request->photo_url);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                Storage::put($path, $data);
                $template->photo_url = Storage::url($path);
                $template->image_path = $path;
            }
            $template->save();
            $red = action([TemplateCtrl::class, 'index']);
           return redirect($red);
         }
        return view('vendor.voyager.templates.edit', ['template'=> $template]);
    } 
    
    public function delete(Request $request, $id){
        $template = Template::where('id', $id)->first();
        if(Storage::exists($template->image_path)){
            Storage::delete($template->image_path);
        }
        $template->delete();
        return back();
    }

}
