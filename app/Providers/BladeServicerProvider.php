<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BladeServicerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    } 

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('userIsAn', function ($roleName) {
            $condition = false;
            if(Auth::check()){
                $role = DB::table('roles')->where('id', Auth::user()->role_id)->first();
                if($role->name == $roleName){
                    $condition = true;
                }
            }else{
                $condition = false;
            }
            
            return $condition;
        });

    }
}
