<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\ProductCategory as Category;
use App\Product;
use App\Analytic;
use App\SavedItem;
use App\Attachment as Attachment;
use App\Notification;
use Illuminate\Support\Str;

class Utils{
    
    public function mark_active($data, $check, $class="active"){
        if($data === $check){
            return $class;
        }
        return " ";
    }

    public function get_designer_duration_count($duration){
        if($duration < 1){
            $plans = \App\DesignerPlan::count();
        }else{
            $plans = \App\DesignerPlan::where('duration', '<=', $duration)->count();
        }
        
        return $plans;
    }


    public function get_design_buyer($order){
        if($order->user_type == 'SELLER'){
            return \App\Seller::where('id', $order->user_id)->first();
        }elseif($order->user_type == 'INFLUENCER'){
            return \App\Influencer::where('id', $order->user_id)->first();
        }elseif($order->user_type == 'BUYER'){
            return \App\User::where('id', $order->user_id)->first();
        }
    }

    public function get_chat_user($user_type, $user_id){
        if($user_type == '@SELLER'){
            return \App\Seller::where('id', $user_id)->first();
        }elseif($user_type == '@INFLUENCER'){
            return \App\Influencer::where('id', $user_id)->first();
        }elseif($user_type == '@BUYER'){
            return \App\User::where('id', $user_id)->first();
        }elseif($user_type == '@DESIGNER'){
            return \App\Designer::where('id', $user_id)->first();
        }
    }

    public function isActive($routes = []){
        if(in_array(Route::currentRouteName(), $routes)){
            return true;
        }
        
        return false;
    }

    public function makeActive($routes = [], $class){
        if(in_array(Route::currentRouteName(), $routes)){
            return $class;
        } 
        return '';
    }
     
    public function can($abilities){
        $user_perm = Auth::user()->getAbilities()->pluck('name')->toArray();
        if(array_intersect($user_perm, $abilities)){
            return true;
        }
        return false;
    }

    public function userRoles(){
        $selected_roles = DB::table('user_roles')->where('user_id', Auth::user()->id)->get();
        $roles = DB::table('roles')->whereIn('id', $selected_roles->pluck('role_id'))->get();
        return $roles->pluck('name')->toArray();
    }

    public function device(){
        $agent = new \Jenssegers\Agent\Agent();
        return $agent;
    }

    public function get_categories(){
        $categories = Category::with(['children'])->where('is_parent', true)->get();
        return $categories;
    }


    public function set_shipping_status($status){
        switch ($status){ 
            case 0:
            return 'Queued';
            break;
            case 1:
            return 'In Production';
            break;
            case 2:
            return 'Awaiting Delivery';
            case 3:
            return 'Delivered';
            default:
            return 'Cancelled';
        }
    }


    public function get_photos($id){
        $photos = \App\Photo::where('product_id', $id)->get();
        return isset($photos) ? $photos : [] ;
    }

    public function get_address($order_shipping){
        $zone_id = $order_shipping->zone_id;
        $address_id = $order_shipping->address_id;
        $data = [
            "name"=>'',
            "address"=>'',
        ];

        if(isset($address_id) && $address_id != 0){
            $address = \App\ShippingAddress::where('id', $address_id)->first();
            if(is_null($address)){
                return [
                    "name" => json_encode($address),
                    "address"=> ''
                ];
            }else{
                return [
                    "name" => $address->firstname . ' ' . $address->lastname,
                    "address"=> $address->address
                ];
            }
            
        }else{
            $zone = \App\ShippingZone::where('id', $zone_id)->first();
            return [
                "name" => $zone->name,
                "address"=> $zone->description
            ];
        }
    }

    /**
     * save base 64 string to image
     *
     * @param [type] $data
     * @param [type] $name
     * @param [type] $folder
     * @return void
     */
    public function base64_img($data, $name, $folder){
		$data = str_replace('data:image/png;base64,', '', $data);
		$data = str_replace(' ', '+', $data);
		$data = base64_decode($data);
		$url_setup = $name . '-' . str_random(10) . '.png';
		$url = public_path() . '/'. $folder .'/' . $url_setup;
		file_put_contents($url, $data);
		return $folder .'/' . $url_setup; 
     }

    /**
     * save base 64 string to image
     *
     * @param [type] $data
     * @param [type] $name
     * @param [type] $folder
     * @return void
     */
    public function full_base64_upload($data, $name, $ref, $folder){
		$data = $this->resolve_base64_name($data);
		$data = str_replace(' ', '+', $data);
		$data = base64_decode($data);
        $url_setup = $name . '-' . str_random(10) . '.png';
        if(!file_exists(public_path('storage') . '/'. $folder .'/' . $ref)){
            mkdir(public_path('storage') . '/'. $folder .'/' . $ref);
        }
		$url = public_path('storage') . '/'. $folder .'/' . $ref . '/' . $url_setup;
        file_put_contents($url, $data);
        return collect([
            'public_url'=> 'storage/' . $folder .'/' . $ref . '/'  . $url_setup,
            'path'=> $url
        ])->all();
    } 

    public function resolve_base64_name($data){
        if(Str::contains($data, 'png')){
            return str_replace('data:image/png;base64,', '', $data);
        }elseif(Str::contains($data, 'jpg')){
            return str_replace('data:image/jpg;base64,', '', $data);
        }elseif(Str::contains($data, 'jpeg')){
            return str_replace('data:image/jpeg;base64,', '', $data);
        }
           
    }

    public function getDirectoryImages($dir){
        $path = public_path($dir);
        if(file_exists($path)){
            $images = glob("$path/*.{jpg,jpeg,png,bmp}", GLOB_BRACE);
            $allImages = [];
            foreach($images as $image)
            {
                $allImages[] = ['path'=> $dir . '/' . basename($image)];
            }
            return json_decode(json_encode($allImages), FALSE);
        }else{
            return json_decode(json_encode([]), FALSE);
        }
    }



    public function browsing_history(){
         $current_user = request()->ip();
         $analytics = Analytic::where('client_id', request()->ip())->get();
         $ids = $analytics->pluck('model_id');
         $products = Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->whereIn('id', $ids)->get();
         return $products;
     }

     public function get_product_view_count($id){
        $analytics = Analytic::where('model_id', $id)->count();
        return $analytics;
     }

     public function fetch_related_product($id){
        $category = Category::with('products')->where('id', $id)->first();
        $ids = $category->products->pluck('id');
        $products = Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->limit(12)->whereIn('id', $ids)->get();
         return $products;
     }

     public function check_if_favorited($id){
            $saveditem = SavedItem::where('product_id', $id)->first();
            if($saveditem){
                return true;
            }else{
                return false;
            }
     }

     public function get_attachment($model, $model_id){
        $att = Attachment::where([['model', $model], ['model_id', $model_id]])->first();
        return $att;
     }

     public function fetchBanks(){
        $data = [
            '044'=> 'Access Bank',
            '063'=> 'Diamond Bank',
            '050'=> 'Ecobank',
            '070'=> 'Fidelity Bank',
            '011'=> 'First Bank',
            '214'=> 'First City Monument Bank',
            '058'=> 'Guaranty Trust Bank',
            '076'=> 'Skye Bank',
            '221'=> 'Stanbic IBTC Bank',
            '068'=> 'Standard Chartered Bank',
            '232'=> 'Sterling Bank',
            '032'=> 'Union Bank',
            '215'=> 'Unity bank',
            '035'=> 'Wema Bank',
            '057'=> 'Zenith Bank'
        ];

		return $data;
     }
     
    
     public function get_image($key){
        return str_replace('\\', '/', '/storage/' . setting($key));
     }
 
     public function get_notifications(){
       return Notification::where([['reciever_id', Auth::user()->id], ['is_read', false]]);
     }

     public function deleteAllFiles($folder) {
        foreach( new \RecursiveIteratorIterator( 
            new \RecursiveDirectoryIterator( $folder, \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS ), 
            \RecursiveIteratorIterator::CHILD_FIRST ) as $value ) {
                $value->isFile() ? unlink( $value ) : rmdir( $value );
        }
         
    }

}