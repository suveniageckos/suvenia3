@inject('states', '\App\ShippingLocation')

@php
$design_colors = json_decode(json_decode($product->design->colors));
$get_color = collect($design_colors)->random();
@endphp
@php
$state = $states::first();
$shipping_price = collect(json_decode($state->data, true))->first();
$ff = $followers->first();
@endphp

<input type="hidden" value="{{ route('app:seller:dashboard:promote_filter') }}" id="promoteFilterUrl">
<div id="modal-promote-item">
    <form action="{{ route('app:seller:dashboard:promote') }}" method="POST" data-post>
        <section class="" id="initPromote">
                <div class="row">
                        <div class="col-md-6">
                                <input type="hidden" name="product_id" value="{{ $id }}">

                                <div class="row no-gutters">
                                        <div class="col-4">
                                                <div style="width:100px">
                                                    <img src="{{ Storage::url($product->photos->first()->public_url) }}" width="100" style="background: {{ $get_color }};">
                                                </div>
                                        </div>
                                        <div class="col-8">
                                                <p class="m-1 text-capitalize promote-product-name-title">{{ $product->name  }}</p>
                                        </div>
                                </div>
                                <div class="row mt-2">
                                        <div class="col-md-12 form-group description">
                                        <label class="app-label">Describe Your Request</label>
                                        <textarea class="form-control app-form" name="description" value="" placeholder="Give a brief summary about your product"></textarea>
                                        <span class="help-block"></span>
                                        </div>

                                        <div class="col-md-12 form-group gig_type">
                                        <label class="app-label">Ship Product to Influencer</label>
                                        <p class="text-muted m-0" style="font-size:11px;">(We will produce the Item and send it to the influencer to boost your promotion.)</p>
                                        <select class="form-control app-form" name="is_shippable" placeholder="Select preffered option" id="isPromoteShippable">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        </select>
                                        <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-12 form-group content_link wizardry-hide" id="PromoteContentLink">
                                        <label class="app-label">Content Link</label>
                                        <p class="text-muted m-0" style="font-size:11px;">(Kindly send a link to your video or picture of the content Influencer will work with)</p>
                                        <input type="url" name="content_link" id="" class="form-control app-form">
                                        <span class="help-block"></span>
                                        </div>
                                </div>
                                <div class="row mt-2">
                                        <div class="col-6"><!--<a href="" class="promote-back-link">BACK</a>--></div>
                                        <div class="col-6 uk-text-right"><a href="" class="btn btn-info btn-sm" wgo="#completePromote" wparent="#initPromote">NEXT</a></div>
                                </div>
                        </div>


                        <div class="col-sm-6">
                                <div class="uk-margin-medium-top bg-promotion-filter-result p-3">
                                <div class="uk-text-medium uk-nav-center tip-text-style">QUICK TIPS</div>
                                <br>
                                <div class="row uk-margin-bottom">
                                <div class="col-2">
                                <span class="uk-badge uk-border-circle badge-small circle_format_style_small">1</span>
                                </div>
                                <div class="col-10">
                                <span style="font-size:0.8em; text-align:justify;">When you promote a product, your request will be sent to the most effective influencer(s) that match the criteria you provide. </span>
                                </div>
                                </div>
                                <div class="row uk-margin-bottom">
                                <div class="col-2">
                                <span class="uk-badge uk-border-circle badge-large circle_format_style_small">2</span>
                                </div>
                                <div class="col-10">
                                <span style="font-size:0.8em; text-align:justify;">Influencers are matched based on your budget, audience category and desired number of followers  </span>
                                </div>
                                </div>
                                <div class="row uk-margin-bottom">
                                <div class="col-2">
                                <span class="uk-badge uk-border-circle badge-large circle_format_style_small">3</span>
                                </div>
                                <div class="col-10">
                                <span style="font-size:0.8em; text-align:justify;">You can choose to ship the promoted Item to Influencer(s) or allow Influencer(s) use default content you provide.</span>
                                </div>
                                </div>
                                </div>
                                <div class="pt-3">
                                    <p class="text-con-muted">Note that item production cost and shipping cost will be added to your promotion fee if you choose to Ship product to Influencer </p>
                                 </div>
                            </div>


                        {{-- <div class="col-md-5">
                                <div class="bg-promotion-filter-result p-3">
                                        <p class="text-con m-1">Describe what you want</p>
                                        <p class="text-con m-1">Include every necessary details needed for the promotion</p>
                                </div>
                                <div class="p-1">
                                <p class="text-con-muted">Note that item production cost and shipping cost will be added to your promotion fee if you choose to Ship product to Influencer </p>
                                </div>

                        </div> --}}
                </div>
        </section>

        <section class="wizardry-hide" id="completePromote">
                <div class="row">
                <div class="col-md-7">
                 <div class="row mt-2">
                                <div class="col-md-12 form-group">
                                        <label class="app-label">Post Platform</label>
                                        <select class="form-control app-form" name="promotion_platform" placeholder="Select preffered option">
                                        <option value="instagram">Instagram</option>
                                        </select>
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-12 form-group">
                                        <label class="app-label">Post Format</label>
                                        <select class="form-control app-form" name="promotion_format" placeholder="Select preffered option" id="promoteFormatInput">
                                        <option value="photo_price">Photo</option>
                                        <option value="video_price">Video</option>
                                        </select>
                                        <span class="help-block"></span>
                                </div>
                        </div>
                        <div class="row mt-2">
                                <div class="col-md-6 form-group">
                                        <label class="app-label">Duration</label>
                                        <select class="form-control app-form" name="duration" placeholder="Select prefered option" id="totalPromoteDuration">
                                        <option value="7">1 Week</option>
                                        <option value="14">2 Weeks</option>
                                        <option value="21">3 Weeks</option>
                                        <option value="30">1 Month</option>
                                        </select>
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group">
                                        <label class="app-label">No. Of Influencer</label>
                                        <input type="number" value="1" class="form-control app-form" placeholder="E.g 2" min="1" id="InfluencerCount" name="number_of_influencers">
                                        <span class="help-block"></span>
                                </div>
                        </div>
                        <div class="row mt-2">
                                <div class="col-md-8 form-group">

                                        <label class="app-label">Number Of Followers</label>
                                <select class="form-control app-form" name="number_of_followers" placeholder="Select preffered option" data-filter  id="FollowSelect">
                                        @foreach($followers as $follower)
                                <option value="{{ $follower->id }}" data="{{ json_encode($follower) }}">{{ $follower->follower_range}}</option>
                                        @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                </div>
                        </div>
                        {{-- <div class="row mt-2" id="promoteShippingLocationDiv">
                                <div class="col-md-12 form-group">
                                        <label class="app-label">Audience Category</label>
                                        <select class="form-control app-form" name="audience_location" placeholder="Select preffered option" id="promoteShippingLocationInput">
                                                @foreach($states::get() as $state)
                                                 @php $shipping_price = collect(json_decode($state->data, true))->first() @endphp
                                                <option value="{{ $state->id}}" data-price="{{ $shipping_price }}"> {{ $state->name }}</option>
                                                @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                </div>
                        </div> --}}

                        <div class="row mt-2">
                            <div class="col-md-12 form-group">
                                    <label class="app-label">Audience Category</label>
                                    <select class="form-control app-form" name="audience_category" placeholder="Select preffered option">
                                       <option value=""> --Choose Category--</option>
                                       <option value="Food"> Food</option>
                                       <option value="Religion"> Religion</option>
                                       <option value="Fashion"> Fashion</option>
                                       <option value="Technology"> Technology</option>

                                    </select>
                                    <span class="help-block"></span>
                            </div>
                    </div>

                    <div class="row mt-2" id="promoteShippingLocationDiv">
                        <div class="col-md-12 form-group">
                                <label class="app-label">Location to Target</label>
                                <select class="form-control app-form" name="audience_location" placeholder="Select preffered option" id="promoteShippingLocationInput">
                                        @foreach($states::get() as $state)
                                         @php $shipping_price = collect(json_decode($state->data, true))->first() @endphp
                                        <option value="{{ $state->id}}" data-price="{{ $shipping_price }}"> {{ $state->name }}</option>
                                        @endforeach
                                </select>
                                <span class="help-block"></span>
                        </div>
                </div>
                        <input type="hidden" value="{{ $ff->photo_price + $shipping_price }}" id="PromotetotalPriceInput" name="total_price">
                        <div class="row mt-2">
                                <div class="col-6"><a href="" class="promote-back-link" wgo="#initPromote" wparent="#completePromote">Back</a></div>
                                <div class="col-6 uk-text-right"><button class="btn btn-info btn-sm" type="submit" {{ $influencers->count() < 1 ? 'disabled' : '' }} id="PromoteSubmitButton">PROMOTE</button></div>
                        </div>
                </div>
                <div class="col-md-5">

                        <div class="bg-promotion-filter-result p-3">
                            <p class="text-con m-1">Estimated Result for your budget</p>
                        <p class="text-con m-1 text-primary" id="influencerCountDisplay">{{ $influencers->count() }}</p>
                            <p class="text-con m-1 text-primary">Influencer with <span id="influencerRangeDisplay">{{ $ff->follower_range }}</span> followers</p>
                            <p class="text-con m-1 mt-2 font-weight-bold">Potential reach</p>
                            <p class="text-con m-1" id="PotentialReach">{{ $influencers->avg('instagram_followers') }}</p>
                            <p class="text-con m-1 mt-2 font-weight-bold">Estimated engagements</p>
                            <p class="text-con m-1" id="EstimatedEngagements">{{ $influencers->avg('instagram_comments') +  $influencers->avg('instagram_likes')}}</p>
                            <!--<p class="text-con m-1">(600 - 2400 per engagement)</p>-->
                            <p class="text-con m-1 mt-2 font-weight-bold is_shippable_divs">Shipping Fee</p>
                            <p class="text-con m-1 is_shippable_divs">&#8358;<span id="promoteShippingLocationDiv">{{ $shipping_price }}</span></p>
                            <p class="text-con m-1 mt-2 font-weight-bold">Total Expense</p>

                        <p class="text-con m-1 text-primary font-weight-bold">&#8358;<span id="PromotetotalPrice">{{ $ff->photo_price + $shipping_price }}</span></p>

                        </div>
                </div>
                </div>
        </section>
    </form>
</div>
