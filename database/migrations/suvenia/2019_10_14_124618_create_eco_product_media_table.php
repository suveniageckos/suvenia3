<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoProductMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_product_media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('user_type', 225)->nullable();
			$table->integer('product_id');
			$table->text('path', 65535);
			$table->text('public_url', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_product_media');
	}

}
