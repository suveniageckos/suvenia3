<div class="uk-section p-4 bg-app">
        <div class="uk-container">
            <div class="row">
                <div class="col-md-9">
                    <ul class="list-unstyled menu-footer">
                        <li><a href="{{ route('app:pages:faqs') }}">FAQ</a></li>
                        <li><a href="{{ route('app:pages:return-policy') }}">return policy</a></li>
                        {{-- <li><a href="">seller terms</a></li> --}}
                        <li><a href="{{ route('app:pages:terms') }}">terms &amp; conditions</a></li>
                        <li><a href="{{ route('app:pages:shipping') }}">shipping</a></li>
                        <li><a href="{{ route('app:pages:about') }}">why suvenia</a></li>
                        <li><a href="{{ route('app:pages:contact') }}">contact us</a></li>
                        <li><a href="https://blog.suvenia.com" target="_blank">blog</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="">
                            <a href="https://twitter.com/Suveniadotcom" target="_blank" class="uk-icon-button uk-margin-small-right app-list-icon" uk-icon="twitter"></a>
                            <a href="https://www.facebook.com/suveniadotcom" target="_blank" class="uk-icon-button  uk-margin-small-right app-list-icon" uk-icon="facebook"></a>
                            <a href="https://www.instagram.com/suveniadotcom/" target="_blank" class="uk-icon-button app-list-icon" uk-icon="instagram"></a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <p class="app-copyright">&copy; {{ date('Y') }} Suvenia.com</p>
                </div>
            </div>

        </div>
    </div>
