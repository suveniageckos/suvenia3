<?php
namespace App\Admin\Actions\Seller;

use TCG\Voyager\Actions\AbstractAction;

class Transfer extends AbstractAction{

    public function getTitle()
    {
        return 'Transfer Selected';
    }

    public function getIcon()
    {
        return 'voyager-paper-plane';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getDefaultRoute()
    {
        //return route('admin:seller:transfer');
        return 'javascript:;';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success'
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'seller-withdrawals';
    }

    public function massAction($ids, $comingFrom)
    {
        // Do something with the IDs
        //return redirect($comingFrom);
        if(count($ids) > 0){
            $withdrawals = \App\Withdraw::whereIn('id', $ids)->get();
            $customers = [];
            foreach($withdrawals as $with){
                $customers[] = [
                            'amount'=> $with->amount * 100,
                            'recipient'=> $with->transfer_code
                     ];  
            }

            $client = new \GuzzleHttp\Client();
            $post = $client->request('POST', 'https://api.paystack.co/transfer/bulk', [
                'headers'=> [
                'Authorization' => 'Bearer ' . config('ecommerce_config.paystack_code'),
                'Content-Type' => 'application/json',
                ], 
                'json'=> [
                'currency'=> 'NGN',
                'source'=> 'balance',
                'transfers'=> $customers
                ]
            ]);
            $msg = json_decode($post->getBody(), true);
            if(isset($msg['status']) &&  $msg['status'] === true){
                $sucMsg = isset($msg['message']) ? $msg['message'] : '' ;
                $withUpdate = \App\Withdraw::whereIn('id', $ids);
				 $withUpdate->update(['status' => true]);
                return back()->with('success', $sucMsg );
            }else{
                 return back()->with('error', $sucMsg);
            }



        }else{
            return back()->with('error', 'Please select atleast 1 user');
        }
        
    }


}