class Post{
    public constructor(private form: String){
        this.sendAjax();
    }

    private beforeSend(xhr: any, currentForm: JQuery): void{
        const Spinner : any = $('body').attr("data-sinner");
        currentForm.find(':input').prop('disabled', true);
        //currentForm.find(':input[type="submit"]').html('<img src="'+Spinner+'" width="20" uk-responsive>');
    }

    private sendAjax(){
        let form = $(this.form);
        const cls = this;
        $(document).on('submit', this.form, function(e){
            e.preventDefault();
            let currentForm: any = $(e.currentTarget);
            let btnText: any = currentForm.find(':input[type="submit"]').text();
            let dataForm = new FormData(<any>$(e.currentTarget).get(0));
           // let arr = currentForm.serializeArray();
           /* $.each(arr, function(key:any, val:any){
                dataForm.append(val['name'], val['value']);
            });
            if($('input[type="file"]').length > 0){
                $('input[type="file"]').each(function(ind, file){
                    console.log($('input[type="file"]'));
                });
                console.log((<any>$('input[type="file"]')[0]));
            }*/
           
            $.ajax({
                url: currentForm.attr('action'),
                type: currentForm.attr('method'),
                data: dataForm,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(xhr){
                    cls.beforeSend(xhr, currentForm);
                },
                timeout: 20000
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
            })
            .done(function(data, textStatus, jqXHR){
                cls.success(data, textStatus, jqXHR, currentForm, btnText);
            });
        });
    }

    private success(data: any, textStatus: any, jqXHR: any, curForm: JQuery, btnText: any){
        if(jqXHR.status == 200){
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            curForm.find(':input[type="submit"]').text(btnText);
                if(data.message){
                toastr.success(data.message);
                }
                curForm.find(':input[type="submit"]').text(btnText);
                if(!data.isPaused){
                    var wait = setTimeout(function() {
                        window.location.href = data.redirect_url;
                    }, 1000);
                }else{
                    window.location.href = data.redirect_url;
                }
                
        }
    }

    private errorHandler(jqXHR: any, textStatus: any, errorThrown: any, btnText: any, curForm: JQuery){
       // console.log(jqXHR.status);
        if(jqXHR.status == 500 || jqXHR.status == 422){
            var data = JSON.parse(jqXHR.responseText);
                if(typeof data.errors === 'string' || data.errors instanceof String){
                    toastr.error(data.errors, "Error");
                    curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
                    
                }else{
                    
                    curForm.find('.has-error').removeClass('has-error');
                    curForm.find('.help-block').text('');
                    $.each(data.errors, function(ind: any, val: any){
                        curForm.find('.'+ind).addClass('has-error');
                        curForm.find('.'+ind+' .help-block').text(val[0]);
                    });
                    curForm.find(':input').prop('disabled', false);
                    curForm.find(':input[type="submit"]').text(btnText);
                }
        }else if(jqXHR.statusText == "timeout"){
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            toastr.error('Request Timeout! Please try again.');
            curForm.find(':input').prop('disabled', false);
            curForm.find(':input[type="submit"]').text(btnText);
        }
    }

}

$(function(){
    const __initPost = new Post("[data-post]");
});
