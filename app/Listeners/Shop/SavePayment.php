<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrdered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\Payment;

class SavePayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrdered  $event
     * @return void
     */
    public function handle(ProductOrdered $event)
    {
        try {
            $decrypted_type = decrypt(request('type'));
        } catch (DecryptException $e) {
            return back()->with('error', 'Invalid token!');
        }

        $order = $event->order;
        $decrypted_type = decrypt(request('type'));
        //save payment
        $payment = new Payment();
        // $payment->user_id = Auth::user()->id;
        $payment->user_id = Auth::guard('buyers')->user()->id;
        $payment->order_id = $order->id;
        $payment->amount = $order->price;
        $payment->reference = $order->order_ref;
        $payment->status = 1;
        $payment->gateway = $decrypted_type;
        $payment->save();
    }
}
