<?php
namespace App\Helpers\Social;

class Facebook{

	static public function Init($config){
		$provider = new \League\OAuth2\Client\Provider\Facebook([
			'clientId'          => config('user_config.facebook.client_id'),
			'clientSecret'      => config('user_config.facebook.client_secret'),
			'redirectUri'       => config($config['redirect_url']),
			'graphApiVersion'   => 'v2.10',
		]);

		return $provider;
	}

	static public function driver($request, $config){
		$provider = self::Init($config);
		if(is_null($request->code) || !isset($request->code)){
			
			$authUrl = $provider->getAuthorizationUrl([
				'scope' => ['email'],
			]);
			$request->session()->put('FACEBOOK_AUTH_STATE', $provider->getState());
			//$this->authRedirectUrl = $authUrl;
			return redirect($authUrl);
			
		}elseif(!is_null($request->state) || ($request->state !== $request->session()->get('FACEBOOK_AUTH_STATE'))){
			$request->session()->forget('FACEBOOK_AUTH_STATE');
			return redirect()->route('home')->with('error', 'Error loggin in with Facebook, please try again.');
		}

	}

	static public function authenticate($request){
		$provider = self::Init();
		if(!is_null($request->code) || isset($request->code)){
            $token = $provider->getAccessToken('authorization_code', [
				'code' => $request->code
			]);
			try{
				$user = $provider->getResourceOwner($token);
				$userData = [
					'id'=> $user->getId(),
					'email'=> $user->getEmail(),
					'username'=> str_slug($user->getFirstName() . ' ' . $user->getLastName()),
					'firstname'=> $user->getFirstName(),
					'lastname'=> $user->getLastName(),
					'profile_url'=> $user->getLink(),
					'photo_url'=> $user->getPictureurl(),
				  ];
				  $request->session()->forget('FACEBOOK_AUTH_STATE');
				  return json_decode(json_encode($userData));
			} catch(\Exception $e){
				return redirect()->route('home')->with('error', 'Unable to get profile details from facebook.');				
			}

		}
	}


}