<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEcoDesignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eco_designs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('user_type', 225)->nullable();
			$table->integer('product_id');
			$table->integer('brandable_id');
			$table->string('name', 191)->nullable();
			$table->string('earnings', 191)->nullable();
			$table->string('target_quantity', 191)->nullable();
			$table->string('total_earning', 191)->nullable();
			$table->string('percentage', 191)->nullable();
			$table->text('colors', 65535)->nullable();
			$table->text('raw_design', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eco_designs');
	}

}
