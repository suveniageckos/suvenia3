<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllNotifications extends Model
{
    protected $casts = ['message_body' => 'array'];
}
