@extends('layouts.designer')

@section('title', '' )

@section('content')
@includeIf('partials.designer_tags', ['categories'=> $categories, 'cat_ref'=> $gig->category->ref])

<div class="uk-container  mt-0 mb-3">
        <div class="designer-top-card p-3">
                <div class="row justify-content-center">
                    <div class="col-md-5 mb-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="profile_pic_holder m-auto"><!--<div class="logged_stat"></div>--><img src="{{ $gig->user->photo_url ? asset($gig->user->photo_url) : asset('user/default.png')}}" width="100" uk-responsive class="uk-border-circle m-auto "></div>
                            <div class="des_profile_name mt-1 uk-text-center">{{ title_case($gig->user->username) }}</div>
                                <div class="des_profile_desc mt-1 uk-text-center">
                                       {{ title_case($gig->title) }}
                                </div> 
                                <div class="mt-4 gig-tab"> 
                                        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                            @foreach ($gig->plans as $plan)
                                                <li class="nav-item">
                                                <a class="nav-link {{ $loop->first ? 'active' : ''}}" id="gig_tab_ar{{ $plan->id }}" data-toggle="tab" href="#gig_tab{{ $plan->id }}" role="tab" aria-controls="{{ $plan->title }}" aria-selected="true">{{ $plan->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            @foreach ($gig->plans as $plan)
                                            <div class="tab-pane fade {{ $loop->first ? 'show active' : ''}}" id="gig_tab{{ $plan->id }}" role="tabpanel" aria-labelledby="gig_tab_ar{{ $plan->id }}">
                                                <div class="p-3">
                                                    <div class="tab-gig-price">&#8358; {{ $plan->price }}</div>
                                                    <div class="tab-gig-desc">{{ $plan->description }}</div>
                                                    <div class="tab-gig-desc mb-0">
                                                        <ul class="list-unstyled mb-0 mt-1">
                                                            <li class="d-inline-block"><i class="icon-clock mr-2"></i>Time: {{ $plan->duration }} Days</li>
                                                        <li class="d-inline-block ml-2"><i class="icon-refresh mr-2"></i>Revisions: {{ $plan->revisions }}</li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-gig-desc mt-2">
                                                        <ul class="list-unstyled">
                                                            <li class="d-block"><i class="icon-checked mr-2 color-check"></i>Time: {{ $plan->duration }} Days</li>
                                                            <li class="d-block">
                                                                    @if($plan->is_printable)
                                                                    <i class="icon-checked mr-2 color-check"></i>
                                                                    @else
                                                                    <i class="icon-cancel mr-2 color-cancel"></i>
                                                                    @endif
                                                                    Print-Ready
                                                            </li>
                                                            <li class="d-block">
                                                                @if($plan->has_source_file)
                                                                <i class="icon-checked mr-2 color-check"></i>
                                                                @else
                                                                <i class="icon-cancel mr-2 color-cancel"></i>
                                                                @endif
                                                                Source Files</li>
                                                        </ul>
                                                    </div>
                                                    <div class="mt-1">
                                                        <a href="{{ route('app:designer:buy_plan', ['id'=>$plan->id]) }}" class="btn btn-info-dark btn-sm">HIRE</a>
                                                    </div>
    
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                </div>
                            </div>
                        </div>
                </div>

                    <div class="col-md-7">
                        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
    
                                <ul class="uk-slideshow-items">
                                    @forelse($gig->photos as $photo)
                                        <li>
                                            <img src="{{ asset($photo->public_url) }}" alt="" uk-cover>
                                        </li>
                                    @empty
                                        <li>
                                            <img src="{{ asset('storage/designers/plh.jpg') }}" alt="" uk-cover>
                                        </li>
                                    @endforelse
                            
                                </ul>
                            
                                <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                                <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
                                <ul class="uk-thumbnav mt-3">
                                        @foreach ($gig->photos as $photo)
                                        <li class="uk-active" uk-slideshow-item="{{ $loop->index }}"><a href="javascript:;"><img src="{{ asset($photo->public_url) }}" width="70" alt=""></a></li>
                                        @endforeach
                                </ul>
                        </div>        
                    </div>
                </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-6 mt-3">
            <div class="plan-card">
                <ul uk-accordion>
                        <li class="">
                            <a class="uk-accordion-title card-header" href="#">About</a>
                            <div class="uk-accordion-content border m-0">
                                <div class="about_content p-3">
                                    {!! $gig->description !!}
                                </div>
                            </div>
                        </li>
                </ul>
            </div>
            </div>

            <div class="col-md-6">
                    <div class="card mt-3 plan-card">
                            <div class="card-header">
                                Compare Plans
                            </div>
                    </div>
                    @php
                        $price_compare_data = [
                            ['name'=> '', 'key'=> 'name'],
                            ['name'=> 'Description', 'key'=> 'description'],
                            ['name'=> 'Print-Ready', 'key'=> 'is_printable'],
                            ['name'=> 'Source File', 'key'=> 'has_source_file'],
                            ['name'=> 'Design Concepts', 'key'=> 'design_concepts'],
                            ['name'=> 'Revision', 'key'=> 'revisions'],
                            ['name'=> 'Delivery Time', 'key'=> 'duration'],
                            ['name'=> '', 'key'=> 'button'],
                        ];
                        $__compare_data = json_decode(json_encode($price_compare_data));
                    @endphp
                    <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered compare_table mb-0">
                                        
                                    <tbody>
                                    @foreach($__compare_data as $compare_data)
                                    <tr>
                                        <th class="compare_title">{{ $compare_data->name }} </th>
                                        @foreach($gig->plans as $plan)
                                        @php $kkey = $compare_data->key @endphp
                                            @if($compare_data->key == 'title')
                                            <td class="cell_price">
                                                <div class="cell_gig_title">{{ $plan->$kkey }}</div>
                                            <span class="d-block">N{{ $plan->amount }}</span>
                                            </td>
                                            @elseif($compare_data->key == 'duration')
                                            <td class="cell_con">{{ $plan->$kkey }} Days</td>
                                            @elseif($compare_data->key == 'is_printable' || $compare_data->key ==  'has_source_file')
                                                <td class="cell_con">
                                                        @if($plan->$kkey)
                                                        <i class="icon-checked mr-2 color-check"></i>
                                                        @else
                                                        <i class="icon-cancel mr-2 color-cancel"></i>
                                                        @endif
                                                </td>
                                            @elseif($compare_data->key == 'button')
                                                <td class="cell_con">
                                                    <a class="btn btn-info-dark btn-sm" style="color: #fff;" href="{{ route('app:designer:buy_plan', ['id'=>$plan->id]) }}">SELECT</a>
                                                </td>
                                            @else
                                                <td class="cell_con">{{ $plan->$kkey }}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
            </div>

        </div>

        
<div class="uk-container  mt-5">
        <div class="app-sec-header sec-center">
                <h1 class="title">Recommended for you</h1>
                <div class="underline"></div>
        </div>
    <div class="row mt-5">
            <div uk-slider="center: true">

                    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                
                        <ul class="uk-slider-items uk-child-width-1-4@s uk-grid" uk-height-match="target: > li > .uk-card">
                            @forelse($related->gigs as $rgig)
                            <li>
                              @includeIf('partials.single_gig', ['gig' => $rgig])
                            </li>
                            @empty
                               @foreach ($defaults as $rgig)
                               <li>
                                @includeIf('partials.single_gig', ['gig' => $rgig])
                               </li>
                               @endforeach
                            @endforelse
                        </ul>
                
                        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                
                    </div>
                
                    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
                
                </div>
    </div>
</div>

</div>

@endsection