@extends('layouts.dashboard_designer')

@section('title', 'Overviews' )

@section('content')


<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card uk-animation-slide-top-small">
            <div class="card-body">
                <div class="row dime_row">
                @foreach(json_decode(json_encode($dimers), FALSE) as $dimer)
                <div class="col-md-3 mb-2">
                        <div class="uk-text-center">
                        <p class="dime_desc">{{ $dimer->title }}</p>
                        <p class="dime_counter m-0">{{ $dimer->value }}</p>
                        </div>
                </div>
                @endforeach
                </div>
                    </div>
            </div>
        </div>
    </div>



<div class="row">
  <div class="col-md-9">
    <div class="mt-2 p-2 bg-white">
        <h3>Gigs</h3>
    <div class="uk-child-width-expand@s uk-text-center uk-padding-small" uk-grid>
        @foreach ($orders as $order)

        <div class="row">
            @if($order)
                <div class="uk-card uk-card-default col-md-5">
                        <div class="uk-card-media-top">
                        <img src="{{  asset($order->designer->photo_url) }}" alt="">
                        </div>
                        <div class="uk-card-body col-12 uk-padding-remove-bottom">
                                <p>{{ str_limit($order->gig->description, 45, '...') }}</p>

                            <div class="uk-width-expand">
                                <div class="float-left col-md-7">
                                    <p style="font-size:0.9em;" class="text-muted-little uk-text-left">{{ $order->gig->orders->count() }} ORDERS </p>
                                </div>
                                <div class="float-right col-md-5">
                                    <div><span><a href="{{ route('app:designer:dashboard:edit_gig', ['id'=> $order->gig->id]) }}"><i style="font-size: 1.5em; color:blue;"  class="color-item-blue fa fa-pencil"></i></a> | <a href="{{ route('app:designer:dashboard:delete_gigs', ['id'=> $order->gig->id]) }}"><i style="font-size: 1.5em; color:red;" class="color-item-red fa fa-trash"></i></a></span></div>
                                </div>
                            </div>

                            </div>
                </div>
                @else
                <div class="col-md-9">No Gigs created</div>
                @endif
            </div>
        @endforeach

            <div class="col-md-3"><a href="{{ route('app:designer:dashboard:add_gig') }}">
                <div class="uk-card uk-card-default uk-card-body col-12">
                    <i class="font-bold-resize fa fa-plus"></i>
                    <p>Create a new Gig</p>
                </div>
                </a>
            </div>
        </div>
    </div>
            </div>

            <div class="col-md-3">
                <div class="p-3 bg-white">
                    <p class="active_order_title m-0">Active Order - {{ $active_orders->count() }}</p>
                </div>
            <div class="ScrollStyle scroll-other-details">
                @foreach ($active_orders as $active_order)

                    <div class="uk-card uk-card-default uk-width-1-1@m card-demarcation">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto uk-padding-remove-left">
                                    <img class="uk-border-circle" width="40" height="40" src="{{ $active_order->designer->photo_url }}">
                                    </div>
                                    <div class="uk-width-expand">
                                            <div class="float-left  uk-padding-remove-left col-md-8">
                                            <h6 class="uk-margin-remove-bottom">{{ $active_order->gig->title }}</h6>
                                            <p class="uk-text-meta uk-margin-remove-top">
                                                <span style="font-weight:600; font-size:1.2em;">{{ $active_order->designer->username }}</span>
                                            </p>
                                        </div>

                                        <div class="float-right col-md-4 uk-padding-remove-right">
                                                <small class="text-muted-little text-sizer uk-container-item-padding-remove-right"><i class="fa fa-clock-o"></i> {{ $active_order->created_at->diffForHumans() }}</small>
                                                <p class="text-muted-little text-sizer uk-container-item-padding-remove-right"><i class="fa fa-money"></i> {{ $active_order->amount }} </p>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-body uk-padding-small">
                                <p>{{ str_limit($active_order->gig->description, 45, '...') }} <span>&nbsp;<a href="#">Read more</a></span></p>

                                <div class="float-right">
                                {{-- <a href="{{ route('app:designer:dashboard:all_orders', ['id'=> $order->gig->id]) }}" class="btn btn-app-fill btn-sm">View Order</a> --}}
                                <a href="{{ route('app:seller:dashboard:single_design_service', ['id'=> $active_order->id]) }}" class="btn btn-app-fill btn-sm">View Order</a>
                                </div>
                            </div>
                        </div>
                        @endforeach


                    </div>


                    <div class="uk-margin-top uk-card uk-card-default uk-width-1-1@m">
                            <div class="uk-card-header">
                               <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand ">
                                      <div class="d-flex">
                                         <div class="p-0 text-bold-order-bigger">Hot News</div>
                                         </div>
                                    </div>
                                 </div>
                            </div>
                            <div class="uk-card-body uk-padding-remove-bottom">
                                    <ul class="uk-list uk-list-divider">

                                            <li>
                                                @foreach ($vzine_details as $vzine)
                                                <div class=" uk-padding-small uk-padding-remove-top" uk-grid>
                                                        <div class="uk-width-auto uk-padding-remove-left">
                                                        <a href="{{ $vzine->link }}" target="_blank">
                                                        <img class="" width="60" height="60" src="{{ $vzine->jetpack_featured_media_url }}" alt="image">
                                                        </a>
                                                    </div>
                                                        <div class="uk-width-expand uk-padding-remove">
                                                            <div class="col-12 uk-padding-remove-right">
                                                            <a href="{{ $vzine->link }}" target="_blank">
                                                                <p class="text-bold-order-news ">{{ str_limit( strip_tags($vzine->title->rendered), 35) }} </p>
                                                                <p class="text-muted-small">{{ str_limit( strip_tags($vzine->content->rendered), 50) }}</p>
                                                            </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    @endforeach
                                            </li>

                                            <li>
                                                    @foreach ($blog_details as $blog)
                                                   <div class=" uk-padding-small uk-padding-remove-top" uk-grid>
                                                           <div class="uk-width-auto uk-padding-remove-left">
                                                          <a href="{{ $blog->link }}" target="_blank">
                                                           <img class="" width="60" height="120" src="{{ $image_url }}" alt="image">
                                                          </a>
                                                       </div>
                                                           <div class="uk-width-expand uk-padding-remove">
                                                               <div class="col-12 uk-padding-remove-right">
                                                               <a href="{{ $blog->link }}" target="_blank">
                                                                   <p class="text-bold-order-news ">{{ str_limit( strip_tags($blog->title->rendered), 35) }} </p>
                                                                   <p class="text-muted-small">{{ str_limit( strip_tags($blog->content->rendered), 50) }}</p>
                                                               </a>
                                                               </div>

                                                           </div>
                                                       </div>
                                                       @endforeach
                                                   </li>

                                    </ul>
                             </div>
                            </div>







            </div>
    </div>

</div>

@endsection



{{--
@foreach($active_orders as $order)
<div class="mt-2 p-2 bg-white">
    <div class="row">
        <div class="col-md-2">
        <img src="{{ asset($order->designer->photo_url) }}" uk-responsive width="100">
        </div>
        <div class="col-md-4">
        <p class="m-0 text-bold-order">{{ $order->reference }}</p>
        </div>
        <div class="col-md-3">
            <p class="m-0 text-bold-order">&#8358; {{ $order->amount }}</p>
        </div>
        <div class="col-md-2">
            <a href="{{ route('app:designer:dashboard:view_order', ['id'=> $order->id]) }}" class="btn btn-app-border btn-sm">View Order</a>
        </div>
    </div>
</div>
@endforeach


<div class="uk-width-expand">
                <div class="float-left col-md-7">
                    <p class="text-muted-little uk-text-left">20 ORDERS </p>
                </div>
                <div class="float-right col-md-5">
                    <div><span><i class="color-item-blue fa fa-pencil"></i> | <i class="color-item-red fa fa-trash"></i></span></div>
                </div>
            </div> --}}
