@extends('layouts.layout_cart')

@section('title', 'Shipping')
@php $selectedZoneID = session('CartZoneID'); @endphp

@section('content')
<div id="ShipPage"></div>
<div class="uk-container  uk-container-small mt-3 mb-3">
    <div class="app-sec-header sec-center">
        <h1 class="title">SHIPPING ADDRESS</h1>
        <div class="underline"></div>
    </div>

    <div class="row mt-4">
        <div class="col-8">
            <div class="bg-white mb-3 shadowed-box">
                <div class="p-2 cart_shipping_zone_header_bottom">
                    <p class="cart_shipping_zone_header m-0">ADDRESS</p>
                </div>
                <div class="row no-gutters">
                    @foreach($zones as $zone)
                    @php $currentLocation = \App\ShippingAddress::where([['zone_id', $zone->id], ['is_default', true]])->first();
                     $selectZone = $selectedZoneID == $zone->id ? 'checked': '';
                     $checkedZone = $zone->is_default ? 'checked' : '';
                    @endphp
                    <div class="col-12 shipping_border-righ p-3 cart_ship_border_bottom">
                        <div class="row no-gutters">
                            <div class="col-1 d-table uk-text-right p-1">
                                <div class="custom-control custom-radio align-middle">
                                @if(!$zone->has_option || $currentLocation)
                                <input type="radio" id="zone_radio_{{ $zone->id }}" name="zone_radio" class="custom-control-input _zone_selector" data-url="{{ route('app:ecommerce:select_zone', ['zone_id'=> $zone->id]) }}" {{ $checkedZone }}>
                                @else
                                <input type="radio" id="zone_radio_{{ $zone->id }}" name="zone_radio" class="custom-control-input _zone_selector" data-url="{{ route('app:ecommerce:select_zone', ['zone_id'=> $zone->id]) }}" disabled {{ $checkedZone }}>

                                @endif
                                <label class="custom-control-label" for="zone_radio_{{ $zone->id }}"></label>
                                </div>
                            </div>
                            <div class="col-11">
                                <p class="m-1 cart-shipping-header">{{ $zone->name }}</p>
                                <p class="m-1 cart-shipping-text">
                                @if($zone->addresses()->count() > 0)
                            @php $selectedAddress =  $zone->addresses()->where('is_default', true)->first(); @endphp
                            {{ !isset($selectedAddress->address) ? $zone->description : $selectedAddress->address }}
                                @else
                                {{ $zone->description  }}
                                @endif
                                </p>
                                @if($zone->has_option)
                                @if($zone->addresses()->count() > 0)
                                <p class="m-1"><a href="" class="__ship_select_address_def __change_address_link_style" data-url="{{ route('app:ecommerce:set_default_zone_modal', ['id'=> $zone->id])}}">SELECT ADDRESS</a></p>
                                @else
                                <p class="m-1"><a href="" class="__ship_add_address_def __change_address_link_style" data-url="{{ route('app:ecommerce: add_address_zone_modal', ['id'=> $zone->id])}}">ADD ADDRESS</a></p>
                                @endif

                                @endif
                            </div>
                        </div>

                      </div>
                    @endforeach
                </div>
            </div>

            <div class="bg-white shadowed-box">
                <div class="p-2 cart_shipping_zone_header_bottom">
                    <p class="cart_shipping_zone_header m-0">SHIPPING DETAILS</p>
                </div>

                <div class="row no-gutters p-4">

                    @forelse(LaraCart::getItems() as $key => $item)
                    @php
                     $keys =array_keys(LaraCart::getItems());

                    @endphp
                    <div class="col-12 cart-shipping-text cart_ship_border_bottom">
                            <p class="shipping_detail_item_head mt-2">Product {{ array_search($key, $keys) + 1 }} of {{ count(LaraCart::getItems()) }}</p>
                            <p>{{ $item->qty }} x {{ $item->name }} </p>
                            <!--<p>Package to arrive within <span class="shipping_detail_item_emp">3-7 business days</span> after ordering, allow up to <span class="shipping_detail_item_emp">4 additional business days</span> for other cities</p>-->
                    </div>
                    @empty
                    <div class="col-12 cart-shipping-text">
                        <p class="shipping_detail_item_head">There are no products in the cart!</p>
                    </div>
                    @endforelse

                    <div class="col-12 mt-3">
                    <p class="shipping_sub_total mb-2">Subtotal: <span style="float: right;">&#x20a6; {{ number_format(LaraCart::total($formatted = false), 2) }}</span></p>
                    <p class="shipping_sub_total mb-0 mt-3">Shipping fee: <span style="float: right;">&#x20a6; {{ number_format(session('CartShippingPrice'), 2) }}</span></p>
                    <p class="shipping_total mt-1">Total: <span style="float: right;">&#x20a6; {{ number_format(LaraCart::total($formatted = false) + session('CartShippingPrice'), 2) }}</span></p>
                    <form action="{{ route('app:ecommerce:process_shipping_price_for_payment') }}" method="POST">
                        {{ csrf_field() }}
                        @php $__choosedZonedID = $zones->where('is_default', true)->first(); @endphp
                        <input type="hidden" value="{{ !session('CartZoneID') ? $__choosedZonedID->id : session('CartZoneID') }}" name="__zone_id">
                    <p class="mb-0 mt-4"><button href="" class="btn btn-info-dark" type="submit">PROCEED TO PAYMENT</button></p>
                    </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-4">
            <div class="bg-white shadowed-box">
                    <div class="p-2 cart_shipping_zone_header_bottom">
                        <p class="cart_shipping_zone_header2 m-0">YOUR ORDER ({{ count(LaraCart::getItems()) }} {{ str_plural('Item', count(LaraCart::getItems()) ) }})</p>

                    </div>
                    <div class="row no-gutters">
                        @foreach(LaraCart::getItems() as $item)
                        @php $photos = $utils->get_photos($item->id); @endphp
                        <div class="col-12 cart-shipping-text cart_ship_listing p-3">
                                <div class="media uk-postion-relative">
                                        <img src="{{ asset('storage/'.$photos->first()->public_url) }}" alt="" class="mr-2" width="132" height="152" style="background-color: {{$item->color}}">
                                        <div class="media-body">
                                            <p class="ship_prod_name mt-1 mb-1 uk-text-capitalize">{{ $item->name }}</p>
                                            <p class="ship_prod_qty m-1">Qty: {{ $item->qty }}</p>
                                            <p class="ship_prod_price m-2" style="font-size:14px;">&#x20a6; {{ number_format($item->price, 2) }}</p>
                                        </div>
                                        <p class="d-inline-block" style="margin-top:-10px;">
                                                <a class="cart_delete_order_link" href="javascript:;" data-url="{{ route('app:ecommerce:delete_cart_item', ['hash'=> $item->getHash()]) }}"><span class="icon-delete"></span></a>
                                        </p>
                                </div>
                        </div>
                        @endforeach

                        <div class="col-12 mt-3 p-3">
                            <p class="shipping_sub_total mb-2">Subtotal: <span style="float: right;">&#x20a6; {{ number_format(LaraCart::total($formatted = false), 2) }}</span></p>
                            <p class="shipping_sub_total mb-0 mt-3">Shipping fee: <span style="float: right;">&#x20a6; {{ number_format(session('CartShippingPrice'), 2) }}</span></p>
                            <p class="shipping_total mt-1">Total: <span style="float: right;">&#x20a6; {{ number_format(LaraCart::total($formatted = false) + session('CartShippingPrice'), 2) }}</span></p>
                        </div>

                    </div>
            </div>

        </div>
    </div>

</div>
@endsection

@push('cart_shipping_page')
<script src="{{ asset('suv/s/pg_ship.bundle.js') }}"></script>
@endpush
