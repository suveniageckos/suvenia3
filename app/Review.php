<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $table = 'eco_reviews';

    protected $fillable = ['product_id'];

    public function user(){
        return $this->belongsTo("App\User");
    }
    
    public function product(){
        return $this->belongsTo("App\Product");
    }
}
