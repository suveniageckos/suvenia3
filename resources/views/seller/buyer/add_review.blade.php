<form action="{{ route('app:dashboard:add_product_review', ['id'=> $product->id])}}" method="POST" class="data-post" data-post>
    @csrf
<div class="row">
        <div class="col-12">
                <div class="media">
                        <img src="https://suvenia.com/p-upl/prod-Xv2BKWXbRY.png" alt="" class="mr-2" width="100" height="130">
                        <div class="media-body">
                            <p class="saved_item_prod_name m-0 mb-0 uk-text-capitalize">{{ $product->name }}</p>
                        <p class="saved_item_prod_seller m-0">{{ $product->order_item->order_ref }}</p>
                        <div class="mt-1">
                                <select class="bar-rated">
                                        <option value="1" data-html="Poor">1</option>
                                        <option value="2" data-html="Fair">2</option>
                                        <option value="3" data-html="Good">3</option>
                                        <option value="4" data-html="Very Good">4</option>
                                        <option value="5" data-html="Excellent!">5</option>
                                        
                                    </select>
                                
                                <small class="text-muted" id="rateMsg"></small>
                                <input type="hidden" id="ratingValue" name="rating" value="{{ $product->avgRating }}">
                        </div>
                           
                        </div>
                </div>
                
        </div>

        <div class="col-12">
            <div class="form-group title">
            <label for="">Review Title</label>
            <input type="text" class="form-control" placeholder="Review Title" name="title" value="{{ isset($review) ? $review->title : '' }}">
            <p class="form-text help-block"></p>
            </div>
        </div>

        <div class="col-12">
        <div class="form-group review_content">
        <label for="">Write your Review</label>
        <textarea class="form-control" rows="3" name="review_content">{{ isset($review) ? $review->content : '' }}</textarea>
        <p class="form-text help-block"></p>
        </div>
        </div>

        <div class="col-12">
        <div class="form-group uk-text-center">
        <button class="btn btn-info">SUBMIT REVIEW</button>
        </div>
        </div>


    </div>
</form>

    <script>
$(document).ready(function(){
    
    $('.bar-rated').barrating({
        theme: 'css-stars',
        readonly: false,
        allowEmpty: true,
        initialRating:$('#ratingValue').val(),
        hoverState: true,
        showValues: false,
        onSelect:function(value, text, event){
            $('#ratingValue').val(value);
            $('#rateMsg').text(text);
        }
    });

});


 </script>