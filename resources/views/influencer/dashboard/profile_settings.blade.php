@extends('layouts.dashboard_influencer')

@section('title', 'Update Profile' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:influencer:dashboard:profile_settings') }}" class="card_head_nav_item {{  $utils->makeActive(['app:influencer:dashboard:profile_settings'], 'active') }}">Personal Details</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:influencer:dashboard:change_password') }}" class="card_head_nav_item {{  $utils->makeActive(['app:influencer:dashboard:change_password'], 'active') }}">Change Password</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:influencer:dashboard:bank_details') }}" class="card_head_nav_item {{  $utils->makeActive(['app:influencer:dashboard:bank_details'], 'active') }}">Bank Details</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                <form action="{{ url()->current() }}" method="POST" data-post>
                        <div class="row justify-content-center">
                            <div class="col-md-4 uk-text-center">
                                <img src="{{ Auth::guard('influencers')->user()->photo_url ? asset(Auth::guard('influencers')->user()->photo_url) : asset('user/default.png') }}" width="100" uk-responsive class="m-auto" id="DisplayPreviewDiv">
                                <div class="mt-2">
                                <h4 class="usernames m-0">{{ Auth::guard('influencers')->user()->lastname . ' ' . Auth::guard('influencers')->user()->firstname}}</h4>

                                <div class="uk-margin">
                                    <div uk-form-custom>
                                        <input type="file" name="image" data-preview preview="#DisplayPreviewDiv">
                                        <span class="change_image_link uk-link">Change Image</span>
                                    </div>
                                </div>

                                </div>
                            </div>
                        </div>
                        <div class="w-75 m-auto">
                            <div class="row justify-content-center">
                                <div class="col-md-6 form-group username">
                                    <label class="app-label">UserName</label>
                                <input type="text" class="form-control app-form textfield-format" name="username" value="{{ Auth::guard('influencers')->user()->username }}">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group firstname">
                                    <label class="app-label">FirstName</label>
                                    <input type="text" class="form-control app-form textfield-format" name="firstname" value="{{ Auth::guard('influencers')->user()->firstname }}">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group lastname">
                                        <label class="app-label">LastName</label>
                                        <input type="text" class="form-control app-form textfield-format" name="lastname" value="{{ Auth::guard('influencers')->user()->lastname }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group phone">
                                        <label class="app-label">Phone Number</label>
                                        <input type="number" class="form-control app-form textfield-format" name="phone" value="{{ Auth::guard('influencers')->user()->phone }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group instagram">
                                        <label class="app-label">Instagram username</label>
                                        <input type="url" class="form-control app-form textfield-format" name="instagram" value="{{ Auth::guard('influencers')->user()->instagram }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="col-md-6 form-group address">
                                    <label class="app-label">Address</label>
                                    <textarea class="form-control app-form textfield-format" name="address" rows="2">{{ Auth::guard('influencers')->user()->address }}</textarea>
                                    <span class="help-block"></span>
                                </div>


                                <div class="col-md-6 form-group bio">
                                        <label class="app-label">Bio</label>
                                <textarea class="form-control app-form" name="bio" rows="6">{{ Auth::guard('influencers')->user()->bio }}</textarea>
                                        <span class="help-block"></span>
                                </div>

                                <div class="col-md-6">
                                <div class="form-group state">
                                    <label class="app-label">State</label>
                                    <input type="url" class="form-control app-form textfield-format" name="state" value="{{ Auth::guard('influencers')->user()->state }}">
                                    <span class="help-block"></span>
                               </div>

                               <div class="form-group preffered-post">
                                <label class="app-label">Preffered Post</label>
                                <input type="url" class="form-control app-form textfield-format" name="post" value="{{ Auth::guard('influencers')->user()->post }}">
                                <span class="help-block"></span>
                           </div>
                              </div>

                                <div class="col-md-12 form-group uk-text-center uk-padding">
                                    <button class="uk-button button-large-format uk-button-large  " type="submit">UPDATE</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>

</div>


@endsection

