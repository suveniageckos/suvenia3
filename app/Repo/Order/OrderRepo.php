<?php
namespace App\Repo\Order;

use App\Repo\Order\OrderInterface;
use App\Order;
use Auth;

class OrderRepo implements OrderInterface{

    /**
     * saves inline order
     *
     * @param int $total_amount
     * @return App\User
     */
    public function quick_order($total_amount, $order_id){
        $order = new Order();
        // $order->user_id = Auth::user()->id;
        $order->user_id = Auth::guard('buyers')->user()->id;
        $order->order_ref = $order_id;
        $order->price = $total_amount;
        $order->save();
        return $order;
    }
}
