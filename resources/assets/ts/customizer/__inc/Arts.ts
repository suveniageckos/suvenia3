class Arts{

    constructor(public Canva: any){
        this.setArtPagination();
        this.loadChildArts();
        this.placeArt();
    }

    public setArtPagination() {
		$(document).on('click', '.pagination > .page-item > .page-link', function (e) {
			e.preventDefault();
			let curTarg: any = $(e.currentTarget);
			let url = $('#PaginLink').attr('data-link');
			let page = curTarg.attr('href').split('page=')[1];
			let pageLink = $(this).attr('href');
			$.ajax({
				url: pageLink,
				beforeSend: function () {
					//$('#Artscontent').append('<div class="loading">Loading…</div>');
					$('#Artscontent').find('.loader').remove();
					$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

				}
			}).done(function (res) {
				$('#Artscontent').html(" ");
				$('#Artscontent').html(res);
			});
			//console.log(url);
		});
    }
    
    public loadChildArts() {
		$('.ArtPicParent').each(function () {
			$(this).on('click', function () {
				let url = $(this).attr('data-url');
				$.ajax({
					url: url,
					beforeSend: function () {
						$('#Artscontent').find('.loader').remove();
						$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

					}
				}).done(function (res) {
					$('#Artscontent').html(" ");
					$('#Artscontent').html(res);
				});
			});
		});

		$(document).on('click', '#GoBack', function (e) {
			e.preventDefault();
			let url = $(this).attr('href');
			$.ajax({
				url: url,
				beforeSend: function () {
					$('#Artscontent').find('.loader').remove();
					$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

				}
			}).done(function (res) {
				$('#Artscontent').html(" ");
				$('#Artscontent').html(res);
				$('.ArtPicParentDyn').each(function () {
					$(this).on('click', function () {
						//alert('hey');
						let url = $(this).attr('data-url');
						$.ajax({
							url: url,
							beforeSend: function () {
								$('#Artscontent').find('.loader').remove();
								$('#Artscontent').append('<div class="loader loader-default is-active" data-text></div>');

							}
						}).done(function (res) {
							$('#Artscontent').html(" ");
							$('#Artscontent').html(res);
						});
					});
				});
			});
		});

	}

	public placeArt(): void {
		let cls = this;
		// $('.ArtPic').each(function(ind, val){
		$(document.body).on('click', '.ArtPic', function () {
			var pic = $(this).attr('src');
			cls.Canva.fab.fabric.loadSVGFromURL(<any>pic, function (objects: any, options: any) {
				var svg = cls.Canva.fab.fabric.util.groupSVGElements(objects, options);
				svg.scaleToWidth(150);
				svg.scaleToHeight(150);
				cls.Canva.activeCanva.centerObject(svg);
				cls.Canva.activeCanva.add(svg);
				cls.Canva.activeCanva.setActiveObject(svg);
				cls.Canva.activeCanva.renderAll();
				cls.Canva.isDirty = true;
				UIkit.modal('#GraphicModal').hide();
				// console.log(cls.Canva.activeCanva.toJSON());


			});


		});
	}

}

export default Arts;