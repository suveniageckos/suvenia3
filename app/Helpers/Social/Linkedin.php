<?php
namespace App\Helpers\Social;

class Linkedin{

	static public function Init(){
		$provider = new \League\OAuth2\Client\Provider\LinkedIn([
			'clientId'          => config('user_config.linkedin.client_id'),
			'clientSecret'      => config('user_config.linkedin.client_secret'),
			'redirectUri'       => config('user_config.linkedin.redirect'),
		]);

		return $provider;
	}

	static public function driver($request){
		$provider = self::Init();
		if(is_null($request->code) || !isset($request->code)){
			
			$authUrl = $provider->getAuthorizationUrl();
			$request->session()->put('LINKEDIN_AUTH_STATE', $provider->getState());
			//$this->authRedirectUrl = $authUrl;
			return redirect($authUrl);
			
		}elseif(!is_null($request->state) || ($request->state !== $request->session()->get('LINKEDIN_AUTH_STATE'))){
			$request->session()->forget('LINKEDIN_AUTH_STATE');
			return redirect()->route('home')->with('error', 'Error loggin in with Linkedin, please try again.');
		}

	}

	static public function authenticate($request){
		$provider = self::Init();
		if(!is_null($request->code) || isset($request->code)){
            $token = $provider->getAccessToken('authorization_code', [
				'code' => $request->code
			]);
			try{
				$user = $provider->getResourceOwner($token);
				$userData = [
				  'id'=> $user->getId(),
				  'email'=> $user->getEmail(),
				  'username'=> str_slug($user->getFirstName() . ' ' . $user->getLastName()),
				  'firstname'=> $user->getFirstName(),
				  'lastname'=> $user->getLastName(),
				  'profile_url'=> $user->getUrl(),
				  'photo_url'=> $user->getImageurl(),
				];
				$request->session()->forget('LINKEDIN_AUTH_STATE');
				return json_decode(json_encode($userData));
			} catch(\Exception $e){
				return redirect()->route('home')->with('error', 'Unable to get profile details from linkedin.');				
			}

		}
	}


}