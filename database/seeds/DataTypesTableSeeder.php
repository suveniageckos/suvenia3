<?php

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_types')->delete();
        
        \DB::table('data_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'TCG\\Voyager\\Models\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-07-25 18:33:05',
                'updated_at' => '2019-07-25 18:33:05',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-07-25 18:33:06',
                'updated_at' => '2019-07-25 18:33:06',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-07-25 18:33:06',
                'updated_at' => '2019-07-25 18:33:06',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'designers',
                'slug' => 'designers',
                'display_name_singular' => 'Designer',
                'display_name_plural' => 'Designers',
                'icon' => 'voyager-paint-bucket',
                'model_name' => 'App\\Designer',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"username","order_display_column":"username","order_direction":"asc","default_search_key":"username"}',
                'created_at' => '2019-07-26 08:38:57',
                'updated_at' => '2019-07-26 08:38:57',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'influencers',
                'slug' => 'influencers',
                'display_name_singular' => 'Influencer',
                'display_name_plural' => 'Influencers',
                'icon' => 'voyager-tv',
                'model_name' => 'App\\Influencer',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"id","order_display_column":"id","order_direction":"asc","default_search_key":null}',
                'created_at' => '2019-07-26 18:47:16',
                'updated_at' => '2019-07-26 18:47:16',
            ),
            5 => 
            array (
                'id' => 7,
                'name' => 'eco_brandables',
                'slug' => 'eco-brandables',
                'display_name_singular' => 'Brandable',
                'display_name_plural' => 'Brandables',
                'icon' => 'voyager-images',
                'model_name' => 'App\\Brandable',
                'policy_name' => NULL,
                'controller' => 'App\\Http\\Controllers\\Admin\\BrandableCtrl',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"id","order_display_column":"id","order_direction":"asc","default_search_key":null}',
                'created_at' => '2019-07-27 08:27:41',
                'updated_at' => '2019-07-27 08:27:41',
            ),
            6 => 
            array (
                'id' => 8,
                'name' => 'eco_categories',
                'slug' => 'product-categories',
                'display_name_singular' => 'Product Category',
                'display_name_plural' => 'Product Categories',
                'icon' => 'voyager-list',
                'model_name' => 'App\\ProductCategory',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"id","order_display_column":"id","order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-07-27 19:59:01',
                'updated_at' => '2019-07-27 20:51:20',
            ),
            7 => 
            array (
                'id' => 9,
                'name' => 'eco_templates',
                'slug' => 'templates',
                'display_name_singular' => 'Template',
                'display_name_plural' => 'Templates',
                'icon' => 'voyager-photo',
                'model_name' => 'App\\Template',
                'policy_name' => NULL,
                'controller' => 'App\\Http\\Controllers\\Admin\\TemplateCtrl',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                'created_at' => '2019-07-27 21:22:35',
                'updated_at' => '2019-07-27 21:22:35',
            ),
            8 => 
            array (
                'id' => 11,
                'name' => 'eco_withdrawals',
                'slug' => 'seller-withdrawals',
                'display_name_singular' => 'Seller Withdrawal',
                'display_name_plural' => 'Seller Withdrawals',
                'icon' => 'voyager-wallet',
                'model_name' => 'App\\Withdraw',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":"user_id","scope":"activeRequest"}',
                'created_at' => '2019-07-29 11:57:24',
                'updated_at' => '2019-07-29 15:34:57',
            ),
            9 => 
            array (
                'id' => 12,
                'name' => 'eco_orders',
                'slug' => 'product-orders',
                'display_name_singular' => 'Product Order',
                'display_name_plural' => 'Product Orders',
                'icon' => 'voyager-basket',
                'model_name' => 'App\\Order',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"id","order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-01 11:51:00',
                'updated_at' => '2019-08-01 12:58:12',
            ),
        ));
        
        
    }
}