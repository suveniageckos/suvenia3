@if(!$utils->device()->isMobile() || $utils->device()->isTablet())
<div class="uk-navbar-container top-nav">
    <div class="uk-container uk-container-small">
        <div class="row nav-ext">
            <div class="col-md-3">
            <a href="{{ route('app:base:index') }}" class="{{ $utils->makeActive(['app:base:index'], 'uk-active') }}"><span class="icon-suvenia" style="font-size:18px;"></span> Marketplace</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:seller:index') }}" class="{{ $utils->makeActive(['app:seller:index'], 'uk-active') }}"><span class="icon-seller" style="font-size:18px;"></span>seller</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:designer:landing_page') }}" class="{{ $utils->makeActive(['app:designer:landing_page', 'app:designer:sign_up'], 'uk-active') }}"><span class="icon-bulb" style="font-size:18px;"></span> designer</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:influencer:landing_page') }}" class="{{ $utils->makeActive(['app:influencer:landing_page', 'app:influencer:apply'], 'uk-active') }}"><span class="icon-influencer" style="font-size:18px;"></span> influencer</a>
            </div>

        </div>
    </div>
</div>


<div class="uk-navbar-container middle-nav">
    <div class="uk-container">
        <div class="uk-navbar">
            <div class="uk-navbar-left">
                <a href="{{ route('app:influencer:landing_page') }}" class="uk-navbar-item uk-logo"><img src="{{  asset($utils->get_image('site.logo')) }}" uk-responsive width="120"></a>

            </div>

            <div class="uk-navbar-right">

            @auth('influencers')
            <div class="uk-navbar-item">
            <a href="javascript:;" class="text-link-auth text-link-auth-logged">
                    <span class="m-0">{{ Auth::guard('influencers')->user()->username  }}</span>
                    <span uk-icon="chevron-down"></span>
                </a>
                <div uk-dropdown>
                    <ul class="uk-nav uk-dropdown-nav app-user-drop-down">
                         @includeIf('partials.acl_menu.influencer', ['type'=> 'front_end'])
                        <li><a href="{{ route('app:influencer:logout') }}" class="post-link">Logout</a></li>
                    </ul>
                </div>
            </div>
          @else
          <div class="uk-navbar-item">

                <a href="{{ route('app:influencer:login') }}" class="text-designer-nav">SIGN IN</a>
            </div>
            <div class="uk-navbar-item">
                <a href="{{ route('app:influencer:apply') }}" class="btn btn-sm btn-designer-join">APPLY</a>
            </div>

        </div>

          @endauth

        </div>
    </div>
</div>

@else


<div class="uk-navbar-container mobile-nav">
        <div class="uk-container">
            <nav class="uk-navbar">

                {{-- @if(isset($showOffCanvas) AND $showOffCanvas) --}}
                <div class="uk-navbar-left">
                    <a href="javascript:;" class="uk-navbar-item mobile-nav-text-color" uk-toggle="target: #NavOffcanvas" style="focus: text-decoration:none;"><span class="icon-menu" style="font-size:18px; font-weight:bold;"></span></a>
                </div>
                {{-- @endif --}}

                <div class="uk-navbar-left ml-1">
                    <a href="{{ route('app:influencer:landing_page') }}" class="uk-navbar-item uk-logo">
                        <img src="{{ $utils->get_image('site.logo') }}" width="100" uk-responsive>
                    </a>
                </div>

                <div class="uk-navbar-right">
                        @auth('influencers')
                        <div class="uk-navbar-item">
                        <a href="javascript:;" class="text-link-auth text-link-auth-logged">
                                <span class="m-0">{{ Auth::guard('influencers')->user()->username  }}</span>
                                <span uk-icon="chevron-down"></span>
                            </a>
                            <div uk-dropdown>
                                <ul class="uk-nav uk-dropdown-nav app-user-drop-down">
                                     @includeIf('partials.acl_menu.influencer', ['type'=> 'front_end'])
                                    <li><a href="{{ route('app:influencer:logout') }}" class="post-link">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                      @else
                      <div class="uk-navbar-item">
                            @php $redirectUrl = url()->current(); @endphp
                            <a href="{{ route('app:influencer:login') }}" class="text-designer-nav">SIGN IN</a>
                        </div>
                        <div class="uk-navbar-item">
                            <a href="{{ route('app:influencer:apply') }}" class="btn btn-sm btn-designer-join">APPLY</a>
                        </div>

                    </div>

                      @endauth
                </div>

            </nav>
        </div>

</div>



<!-- Mobile side bar  -->
<div id="NavOffcanvas" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <div class="user-jar uk-text-center">
            @auth
                @if(Auth::user()->photo_url)
                <div class="mb-2"><img src="{{ asset(Auth::user()->photo_url) }}" alt="{{ Auth::user()->username }}" width="100" class="uk-border-pill"></div>
                @else
                <div class="mb-2"><span class="canvas-user-icon icon-round-user"></span></div>
                @endif
             @else
             <div class="mb-2"><span class="canvas-user-icon icon-round-user"></span></div>
            @endif

            <div class="mt-3 p-3 d-flex justify-content-between">
                    @auth
                    <div class="">
                    <a href="#" class="canvas-auth-link">{{ strtoupper(Auth::user()->username) }}</a>
                        </div>
                    <div class="">
                            <a href="{{ route('app:influencer:logout') }}" class="canvas-auth-link post-link">LOGOUT</a>
                        </div>
                    @else
                    <div class="">
                            <a href="{{ route('app:influencer:login') }}" class="canvas-auth-link">SIGNIN</a>
                        </div>
                        <div class="">
                            <a href="{{ route('app:influencer:apply') }}" class="canvas-auth-link">APPY</a>
                        </div>
                    @endauth
            </div>
        </div>
        <div class="menu-pane">
            @auth
                <ul uk-accordion class="canvas-accordion">
                        <li >
                            <a class="uk-accordion-title p-0 m-0" href="#"><span class="canvas-icon icon-user  mr-2"></span> ACCOUNT</a>
                            <div class="uk-accordion-content">
                                    <ul class="list-unstyled canva-sub-menu">
                                        @includeIf('partials.acl_menu.user', ['type'=> 'front_end'])
                                    </ul>
                            </div>
                        </li>
                </ul>
            @endauth


                <ul uk-accordion class="canvas-accordion">


                <ul class="uk-nav uk-nav-default canvas-menu mt-1">
                    <li><a href="{{route('app:base:index')}}"><span class="canvas-icon icon-suvenia mr-2"></span> MARKETPLACE</a></li>
                    <li><a href="{{ route('app:seller:index') }}"><span class="canvas-icon icon-seller mr-2"></span> SELLER</a></li>
                    <li><a href="{{ route('app:designer:landing_page') }}"><span class="canvas-icon icon-bulb mr-2"></span> DESIGNER</a></li>

                </ul>

        </div>
    </div>
</div>

@endif

