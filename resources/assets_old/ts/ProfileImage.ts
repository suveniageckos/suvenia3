class ProfileImage{
    mimetypes: Array<string> = ['image/jpg', 'image/png', 'image/jpeg'];
    Errors: Array<string> = [];
    ImageDiv: JQuery = $('[data-preview]');

    public constructor(){
        this.process_image();
    }

    public process_image(){
        $(document).on('change', '[data-preview]', (e)=>{
            let file = e.target.files[0];
            let FileToMb: any = file.size/1024/1020 //to MB
            FileToMb = FileToMb.toFixed(2);
            $(e.currentTarget).next('.ImageErrors').remove();
            this.Errors = [];

            if(FileToMb > 5){
                this.Errors.push('Image cannot be than 5MB');
            }
            
            if(_.indexOf(this.mimetypes, file.type) == -1){
                this.Errors.push('You can only upload jpg, png and jpeg');
            }
            if(this.Errors.length){
                $('<ul class="list-unstyled ImageErrors mt-2"></ul>').insertAfter($(e.currentTarget));
                $.each(this.Errors, (ind, val)=>{
                    $(e.currentTarget).next('.ImageErrors').append(
                        '<li class="text-danger" style="font-size:12px;">'+val+'</li>'
                    );
                });
                $(e.currentTarget).val("");
            }else{
                const reader = new FileReader();
                const filePreview = $(e.currentTarget).attr('preview');
                reader.onload = function (e: any) {
                    $(<string>filePreview).attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
            }

        });
    }
}

$(()=>{
    const __profile_image = new ProfileImage();
});