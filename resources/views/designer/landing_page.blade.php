@extends('layouts.designer')

@section('title', 'Become A Designer' )

@section('content')
@php $redirectUrl = url()->current(); @endphp

{{-- <div class="uk-cover-container uk-height-larg" uk-height-viewport> --}}
<div class="uk-cover-container banner-height">
        <img src="{{ $utils->get_image('site.designer_landing_page_banner') }}" alt="" uk-cover>
        <!--<div class="uk-overlay-primary uk-position-cover"></div>-->
        <div class="uk-section uk-position-center">
            <div class="row uk-justify-center p-2">
                <div class="col-md-12">

                    <div class="uk-text-center">
                    <div class="n-centered-hero">
                        <b>WORK AS A FREELANCE DESIGNER</b>
                        <p class="mt-1 n-centered-sub-hero">Earn Online with your Creative Skill.</p>
                    </div>
                    <div class="mt-4">
                    <a class="btn btn-info" style="color: #fff;" href="{{ route('app:designer:sign_up') }}">GET STARTED</a>
                    </div>
                    </div>

                </div>
            </div>
        </div>

</div>

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="borde" style="position: relative; background: #fff;">
        <div class="float-slider" style="padding: 20px 60px;" uk-slider>
                <div class="row">
                    <div class="col-12 uk-text-center">
                    <h2 class="m-0  header-main text-body-black">Do what you love, and get paid for it!</h2>
                    </div>
                </div>
                <div class="row mt-3">
                        <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >

                                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                                @foreach($categories as $category)
                                    <li class="uk-text-center">
                                        <a href="">
                                            <div class="uk-panel">
                                                <img src="{{ asset($category->photo_url) }}" alt="" width="150" uk-responsive>

                                            </div>
                                        </a>
                                    <a href="" class="_prod_title mt-2">{{ title_case($category->title) }}</a>
                                    </li>
                                @endforeach
                                </ul>

                        </div>
                </div>
                <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
                <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
        </div>
</div>

@endif
<div class="uk-section bg-white p-5">
        <div class="uk-container uk-container-small">
            <div class="row">
                <div class="col-12 ml-5">
                    <p class="mt-5 col-11 after_cat_slide_desc" style="text-align:center;">Find awesome clients and become a part of our global community of talented designers—all in a safe, secure workspace.</p>
                </div>
            </div>
        </div>
</div>


<div class="uk-section">
        <div class="uk-container uk-container-medium">
                <div class="lite-bg-header uk-text-center">
                    <h1 class="m-0 header-main text-body-black">HOW IT WORKS</h1>
                    <p class="mt-4 sub-header">An overview of freelancing as a designer on Suvenia</p>
                </div>
                @php
                $how_it_works = [
                    [
                        'icon'=>'designer_img/icons/register.png',
                        'title'=> 'sign up',
                        'width'=> '53',
                        'content'=> 'Sign up for free, set up your Gig, and offer your work.'
                    ],
                    [
                        'icon'=>'designer_img/icons/package.png',
                        'title'=> 'RECEIVE REQUEST',
                        'width'=> '70',
                        'content'=> 'You get notified when you get design request from customers'
                    ],
                    [
                        'icon'=>'designer_img/icons/file-submit.png',
                        'title'=> 'FULLFIL REQUEST',
                        'width'=> '40',
                        'content'=> 'Complete the design request and deliver to customer for approval.'
                    ],
                    [
                        'icon'=>'designer_img/icons/paid.png',
                        'title'=> 'GET PAID',
                        'width'=> '30',
                        'content'=> 'Get paid on time, every time you complete an Order or request.'
                    ],
                ]
                @endphp
                <div class="row justify-content-center">
                    @foreach (json_decode(json_encode($how_it_works), false) as $item)
                        <div class="col-md-3 col-6 uk-text-center mb-2">
                        <img src="{{ asset($item->icon) }}" width="{{ $item->width }}" uk-responsive class="m-auto">
                        <h1 class="des_howitworks_title m-0 uk-text-uppercase mt-4">{{ $item->title }}</h1>
                        <div class="mt-1 des_howitworks_content">
                            {{ $item->content }}
                        </div>
                        </div>
                    @endforeach
                </div>
        </div>
</div>

<div class="uk-section" style="background: #fff;">
        <div class="uk-container uk-container-meduim">
                <div class="lite-bg-header uk-text-center">
                    <h1 class="header-main text-body-black mb-5">TWO WAYS TO EARN AS A DESIGNER</h1>
                 </div>

                <div class="row justify-content-center mt-3 mb-3" uk-grid uk-height-match="target: > div > .card_earn">
                   <div class="col-md-4">
                       <div class="card_earn pt-5 pb-3 uk-text-center ">
                           <h1 class="m-0 header text-body-black">CREATE GIG</h1>
                           <p class="mt-2 p-3 content uk-padding-small">Set up your Gigs and state the services you offer and list your pricing packages for customers to see. </p>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card_earn pt-5 pb-3 uk-text-center">
                           <h1 class="m-0 header text-body-black">ACCEPT BRIEFS</h1>
                           <p class="mt-2 p-3 content">Explore and send offer to customers request and get paid</p>
                       </div>
                   </div>
                </div>

                <div class="row justify-content-center mt-5">
                    <div class="col-md-7 desi_call uk-text-center">
                        <h2 class="title m-0" style="font-size:1.3em; font-weight:600; font-style:normal;">Ready to Join Us and create your first Gig?</h2>
                        <p class="mt-1 content">Suvenia makes it easy to connect with customers by earning doing great work you love.</p>
                        <div class="mt-2">
                            <a href="{{ route('app:designer:sign_up') }}" class="btn btn-info-dark">GET STARTED</a>
                        </div>
                    </div>
                </div>
        </div>
</div>


<div class="uk-section bg-app">
        <div class="uk-container uk-container-small">
                <div class="bg-banner-header uk-text-center mb-5">
                        <h1 class="m-0 text-body-white">FAQ</h1>
                    </div>
            <div class="row justify-content-center">
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content1" aria-expanded="false" aria-controls="faq-collapse-content1">
                            <div class="d-flex justify-content-between">
                                <div>What can Suvenia.com do for me?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content1">
                            <p class="faqs_text text-muted style="text-align:justify;">Suvenia.com allows you to discover, create, buy and sell personlaized items such as apparels, drinkwares, hats and accessories. If you are looking for any specific item category, and do not find, do not hesitate to contact us, as we would often go out of our way to make it available for you. Our goal is to make Suvenia.com a platform that allows you create products at the speed of thought.</p>
                           <p  class="faqs_text text-muted" style="text-align:justify;">You can buy products from the marketplace simply by searching through the site, create your own desisgns and order when you don't find what you like or sell ideas and designs on products if you are a creative individual or brand. You can also search for your favorite stores directly to find products you love.</p>
                    </div>

                </div>
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content2" aria-expanded="false" aria-controls="faq-collapse-content2">
                            <div class="d-flex justify-content-between">
                                <div>What are the design specifications you accept?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content2">
                        <p  class="faqs_text text-muted" style="text-align:justify;">All product design specifications are listed on the product customizer page. You can use our customizer using JPEG, PNG or SVG files with the sizes indicated. If you are unable to edit your design to fit our specification, please kindly chat with us in the live chat icon page. We are able to accept Adobe Illustrator files (vector art with paths) in an AI or EPS format with all fonts converted to outlines. We will also accept vector PDFs and Jpeg for orders placed off the platform. We would contact you if there are any complexities concerning your submitted artwork that require modification.</p>
                    </div>

                </div>
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content3" aria-expanded="false" aria-controls="faq-collapse-content3">
                            <div class="d-flex justify-content-between">
                                <div>What kind of product quality can I expect?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content3">
                        <p class="faqs_text text-muted" style="text-align:justify;">All products are made in Nigeria, and sourced from the highest grade suppliers and factories. We also ensure that all products are made ethically and in compliance with environmental laws.</p>
                    </div>

                </div>
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content4" aria-expanded="false" aria-controls="faq-collapse-content4">
                            <div class="d-flex justify-content-between">
                                <div>What print quality can I expect?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content4">
                        <p class="faqs_text text-muted" style="text-align:justify;">Except explicitly stated otherwise, we use screen printing process for apparels. Learn more about screen printing process <a href="https://www.youtube.com/watch?v=Aru1wZEKrkE" target="_blank">here</a>. For hats, we use either <a href="https://www.youtube.com/watch?v=d6mqndD9xok" target="_blank">vinyl printing</a> or <a href="https://www.youtube.com/watch?v=CcYC_7rqyN0" target="_blank">embroidery</a>. For accessories & drinkwares, we use a digital printing process. Please factor this in when also creating or uploading your designs.</p>
                    </div>

                </div>

            </div>
        </div>
</div>


<div class="uk-section" style="background: #fff;">
        <div class=" uk-text-center">
         <h1 class="header-main text-body-black mb-5">Customer Feedback</h1>
        </div>
        <div class=" uk-margin-large-top" uk-slider >
                <div>
                    <div class=" uk-container uk-container-small uk-slider-container">
                        <ul class="uk-slider-items uk-child-width-1-1@m uk-grid">

                            <li>
                                <div class="uk-card uk-card-default">
                                    <div class="uk-card-body p-2">
                                    <div class="d-flex">
                                        <div>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2tgeRjqSG63pd2ZiiPUi814IwvsDNSxo3158OSw6THxaPOsIqKg" class="uk-border-circle uk-padding-small" alt="" width="200">
                                        </div>
                                        <div>
                                            <p class="ml-3 mt-3 store_text_content">Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua labore et dolore magna aliqua.</p>
                                            <p class="ml-3 mt-0" style="font-weight:600;">Chidiebere Ezeka</p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                    <div class="uk-card uk-card-default">
                                        <div class="uk-card-body p-2">
                                        <div class="d-flex">
                                            <div>
                                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2tgeRjqSG63pd2ZiiPUi814IwvsDNSxo3158OSw6THxaPOsIqKg" class="uk-border-circle uk-padding-small" alt="" width="200">
                                            </div>
                                            <div>
                                                <p class="ml-3 mt-3 store_text_content">Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua labore et dolore magna aliqua.</p>
                                                <p class="ml-3 mt-0" style="font-weight:600;">Chidiebere Ezeka</p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </li>
                        </ul>


                    </div>
                </div>

                 <div class=" uk-margin-medium-top">
                   <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
                 </div>
            </div>

    </div>


<div class="uk-section">
        <div class="uk-container uk-container-small">
                <div class="lite-bg-header uk-text-center ml-5">
                    <h1 class="header-main text-body-black col-10 mb-0 ml-5">Our freelancers have done awesome work for many great companies.</h1>
                </div>
                <div class="row mt-4">
                    @php
                        $companies = [
                            'https://www.freelogodesign.org/Content/img/logo-ex-7.png',
                            'https://www.freelogodesign.org/Content/img/logo-ex-7.png',
                            'https://www.freelogodesign.org/Content/img/logo-ex-7.png',
                            'https://www.freelogodesign.org/Content/img/logo-ex-7.png'
                            // 'https://ya-webdesign.com/images/adidas-logo-png-4.png',
                            // 'https://ya-webdesign.com/images/adidas-logo-png-4.png',
                            // 'https://ya-webdesign.com/images/adidas-logo-png-4.png'

                        ];
                    @endphp
                    @foreach ($companies as $item)
                        <div class="col-md-3 col-6 uk-text-center">
                        <img src="{{ $item }}" uk-responsive width="100">
                        </div>
                    @endforeach
                </div>

        </div>
</div>


@endsection
