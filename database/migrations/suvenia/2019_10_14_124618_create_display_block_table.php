<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisplayBlockTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('display_block', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('block_code', 191);
			$table->text('block_key', 65535)->nullable();
			$table->text('block_value', 65535)->nullable();
			$table->string('link', 191)->nullable();
			$table->text('block_meta', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('display_block');
	}

}
