@extends('layouts.login')

@section('title', 'Reset Password' )
@section('header')
 @include('partials.login_header', ['route_name'=> 'app:designer:landing_page'])
@endsection

@section('content')

<div class="uk-container mt-3 mb-3">
  <div class="row justify-content-center">
 
 <div class="col-md-4">
 
   <div class="card app-box">
     <div class="box-header">
       <p class="title">Reset Password</p>
       <p class="sub-title">Please provide your new password</p>
     </div>
     <div class="box-body">
         <form class="app-form" action="{{ route('app:designer:reset_password', ['token'=> $token]) }}" method="POST" >
             @csrf 
             <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
              <input type="password" class="form-control form-control-lg" placeholder="Password" name="password"  value="{{ old('password') }}">
              <p class="form-text help-block">{{ $errors->first('password') }}</p>
            </div>

            <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
              <input type="password" class="form-control form-control-lg" placeholder="Confirm Password" name="confirm_password"  value="{{ old('confirm_password') }}">
              <p class="form-text help-block">{{ $errors->first('confirm_password') }}</p>
            </div>

                <button type="submit" class="btn btn-info btn-lg btn-block rounded">SUBMIT</button>
              </form>
 
              <p class="body-info">Already have an account? <a href="{{ route('app:designer:login') }}">Sign in now</a></p>
     </div>
    
   </div>
     
 </div>
 
  </div>
</div>


@endsection