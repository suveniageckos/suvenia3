<?php
namespace App\Admin\Actions\Influencer;

use TCG\Voyager\Actions\AbstractAction;

class ApproveAction extends AbstractAction{


    public function getTitle()
    {
        return 'Approve';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        if(!$this->data->is_approved){
            return [
                'class' => 'btn btn-sm btn-primary pull-right'
            ];
        }else{
            return [
                'class' => 'btn btn-sm btn-primary pull-right',
                'style'=> 'display:none;'
            ];
        }
    }

    public function getDefaultRoute()
    {
        return route('admin:influencer:confirm', ['id'=> $this->data->{$this->data->getKeyName()}]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'influencers';
    }

}