class Delete{

    public constructor(){
        
        this.delete_item();
        this.ajax_delete_item();
        this.ajax_delete_action();
        
    }

    public delete_item(): any{
            $(document).on('click', '.data-delete-item', function(e){
                e.preventDefault();
                let title = $(e.currentTarget).attr('title') ? $(e.currentTarget).attr('title') : 'DELETE ITEM';
                let model = $(e.currentTarget).attr('content') ? $(e.currentTarget).attr('content') : 'Do you really want to delete this Item';
                $('body').prepend(
                    '<div id="__delete_item_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center">'+
                    '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                    '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 24px; font-weight: 600;">'+title+'</h2>'+
                    '<p class="m-1" style="color: #333; font-size: 17px;">'+model+'?</p>'+
                    '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>'+
                    '<a class="btn btn-info post-link" href="'+$(this).attr('data-url')+'">YES</a>'+
                    '</div></div>'
                );
                UIkit.modal("#__delete_item_modal").show();
            });
    }

    public ajax_delete_item(): any{
            $(document).on('click', '[ajax-delete-item]', function(e){
                e.preventDefault();
                let title = $(e.currentTarget).attr('title') ? $(e.currentTarget).attr('title') : 'DELETE ITEM';
                let message = $(e.currentTarget).attr('message') ? $(e.currentTarget).attr('message') : 'Do you really want to delete this Item?';
                let callback_type = $(e.currentTarget).attr('data-callback-type');
                let target_element = $(e.currentTarget).attr('data-target-element');
                $('body').prepend(
                    '<div id="__delete_item_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center">'+
                    '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                    '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 24px; font-weight: 600;">'+title+'</h2>'+
                    '<p class="m-1" style="color: #333; font-size: 17px;">'+message+'?</p>'+
                    '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>'+
                    '<a class="btn btn-info ajax-post-link" href="'+$(this).attr('data-url')+'" callback-type="'+callback_type+'" target-element="'+target_element+'">YES</a>'+
                    '</div></div>'
                );
                UIkit.modal("#__delete_item_modal").show();
            });
    }

    public ajax_delete_action(){
        $(document).on('click', '.ajax-post-link', (e)=>{
            e.preventDefault();
            let ele = $(e.currentTarget);
            $.ajax({
                url: ele.attr('href'),
                type: 'POST',
                data: {},
                beforeSend: function(xhr){
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                timeout: 20000
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                //cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
                toastr.error(textStatus);
            })
            .done(function(data, textStatus, jqXHR){
                if(ele.attr('callback-type') == 'DELETE'){
                    $(<any>ele.attr('target-element')).remove();
                }else if(ele.attr('callback-type') == 'UPDATE'){
                    //server returns html string
                    $(<any>ele.attr('target-element')).html(data);
                }
                UIkit.modal("#__delete_item_modal").hide();
            });

        });
    }

}

$(function(){
    let ___deleteItem = new Delete();
})

