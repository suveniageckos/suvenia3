<?php

namespace App\Http\Controllers;

use DB;
use App\Seller;
use App\Mail\VerifyEmail;
use App\Mail\ForgotPassword;
use Illuminate\Http\Request;
use App\ProductCategory as Category;
use App\Repo\User\Auth as AuthGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SellerCtrl extends Controller
{
    public $user; 

    public function __construct(){
        $this->user = new AuthGuard('@SELLER');
    }

    public function seller_home(){
        $showcase_categories = Category::where('is_sub', true)->get();
        return view('seller.seller_home', [
            'showcase_categories'=> $showcase_categories,
        ]); 
    }  

   
    public function login(Request $request){
       
        if ($request->isMethod('post')){
           $login = $this->user->login(); 
           $login->enter();
           if($login->status){ 
               return redirect()->route('app:seller:dashboard:index')->with('success', $login->message);
           }else{
               return back()->withInput()->with('error', $login->message);
           }
        }
        return view('seller.auth.login'); 
    }

    public function sign_up(Request $request){
        if ($request->isMethod('post')) {
            $role = DB::table('roles')->where('name', 'user')->first();

            $data = $request->all();

            $validator = Validator::make($data, [
                'username' => 'required|max:255|min:4|unique:sellers',
                'password' => 'required|min:4',
                'confirm_password' => 'required|min:4|same:password',
                'email' => 'required|min:4|email|unique:sellers',
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user = new Seller();
                $data = $request->all();

                $user->username = $data['username'];
                $user->ref = str_random(10);
                $user->slug = str_slug($data['username'], '-');
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->has_password = true;
                $user->email_token = str_random(36);
                $user->email_token_expiry = \Carbon\Carbon::now()->addDay(1);

                if ($user->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new VerifyEmail($user, 'SELLER'));
                    //dispatch onUserSignUp() event
                    return redirect()->route('app:seller:index')->with('success', 'Your registration was successful! A verification link has been sent to your mail, follow the link to verify your email address.');
                }

            }

        }
        return view('seller.auth.sign_up');
    }

    /**
     * This verifies user email address.
     * This happens either after sign up or after account update.
     *
     * @param Request $request
     * @param String $token
     * @return void
     */
    public function verify_email(Request $request, $token)
    {

        if ($request->isMethod('get')) {
            $currentTime = \Carbon\Carbon::now();
            $user = Seller::where('email_token', $token)->first();
            if (!$user) {
                return redirect()->route('app:seller:login')->with('error', 'Invalid Email Verification Token.');

            } elseif ($user and $currentTime > \Carbon\Carbon::parse($user->email_token_expiry)) {

                return redirect()->route('app:seller:index')->with('error', 'Email token has expired, please re-verify your email address.');

            } else {

                $user->email_verified = true;
                $user->email_token = null;
                $user->email_token_expiry = null;
                if ($user->save()) {
                    return redirect()->route('app:seller:login')->with('success', 'Your email has been verified! you may now login.');
                }
            }

        }

        return view('user.verify_email');
    }

    /**
     * Handles the forgot password callback
     *
     * @param Request $request
     * @return void
     */
    public function forgot_password(Request $request)
    {

        if ($request->isMethod('post')) { 

            $data = $request->all();

            $validator = Validator::make($data, [
                'email' => ['required', 'min:4', 'email', 'exists:sellers'],
            ]);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();
 
            } else {

                $user = Seller::where('email', $data['email'])->first();
                $user->password_token = str_random(36);
                $user->password_token_expiry = \Carbon\Carbon::now()->addDay(1);

                if ($user->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new ForgotPassword($user, 'SELLER'));
                    return redirect()->route('app:seller:login')->with('success', 'A password reset link has been successfully sent to your email address.!');

                }
            }

        }

        return view('seller.auth.forgot_password');
    }

    /**
     * Handles the reset password callback
     * This verifies token and saves the new password
     *
     * @param Request $request
     * @param String $token
     * @return void
     */
    public function reset_password(Request $request, $token)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();

            $currentTime = \Carbon\Carbon::now();
            $user = Seller::where('password_token', $token)->first();
            if (!$user) {

                return back()
                    ->withInput()
                    ->with('error', 'Invalid Password Token.');

            } elseif ($user and $currentTime > \Carbon\Carbon::parse($user->password_token_expiry)) {

                return back()
                    ->withInput()
                    ->with('error', 'Your Password Request Token has expired, please request a password reset link again.');

            } else {

                $validator = Validator::make($data, [
                    'password' => 'required|min:4',
                    'confirm_password' => 'required|min:4|same:password',
                ]);

                if ($validator->fails()) {

                    return back()
                        ->withErrors($validator)
                        ->withInput();

                } else {
                    $user->password = bcrypt($data['password']);
                    $user->password_token = null;
                    $user->password_token_expiry = null;
                    if ($user->save()) {

                        return redirect()->route('app:seller:login')->with('success', 'Your password reset was successful you can now login with your new password.!');
                    }

                }

            }
        }

        return view('seller.auth.reset_password', ['token' => $token]);
    }



    public function logout(Request $request){
        if ($request->isMethod('post')){
            $this->user->logout('app:seller:index');
            return redirect()->route('app:seller:index')->with('success', 'You have logged out successfully!');
         }
    }

     /**
     * Handles user social authentication and redirects
     *
     * @param Request $request
     * @param String $provider
     * @return void
     */
    public function social(Request $request, $provider)
    {
        return \App\Helpers\Social\SocialAuth::driver($request, [
            'redirect_url'=> 'user_config.facebook.redirect_seller'
        ]);
    }

    /**
     * Handles the social callback and saves user data or log user in if already signed up.
     *
     * @param Request $request
     * @param String $provider
     * @return void
     */
    public function social_callback(Request $request, $provider)
    {
        $auth = \App\Helpers\Social\SocialAuth::authenticate($request);
        $userCheck = _User::where($provider, $auth->id)->orWhere('email', $auth->email)->first();
        if ($userCheck) {
            if (!$userCheck->$provider) {
                $userCheck->$provider = $auth->id;
                if ($userCheck->save()) {
                    Auth::guard('sellers')->loginUsingId($userCheck->id, true);
                }
            } else {
                Auth::guard('sellers')->loginUsingId($userCheck->id, true);
            }
            return redirect()->route('app:seller:index')->with('success', 'You have successfully logged in with ' . $provider);

        } else {
            $user = new _User();
            $user->name = $auth->username;
            $user->username = $auth->username;
            $user->slug = str_slug($auth->username);
            $user->ref = str_random(10);
            $user->email = $auth->email;
            $user->email_verified = true;
            $user->photo_url = $auth->photo_url;
            $user->password = 'Not Available';
            $user->$provider = $auth->id;
            $user->has_password = false;
            if ($user->save()) {
                //Bouncer::assign('none_admin')->to($user);
               
                Auth::guard('sellers')->loginUsingId($user->id, true);
                return redirect()->route('app:seller:index')->with('success', 'You have successfully logged in with ' . $provider);

            }

        }
    }


}
 