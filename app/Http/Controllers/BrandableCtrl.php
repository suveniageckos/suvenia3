<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brandable as Brandable;
use App\ProductCategory as Category;
use App\Template as Template;
use App\Product as Product;
use App\Design as Design;
use App\Photo as Photo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use \Imagine\Image\Box;
use \Imagine\Image\Point;
use LaraCart;


class BrandableCtrl extends Controller
{

    public $auth;
    public $auth_type;

    public function __construct(){
        $this->auth = !Auth::guard('sellers')->check() ? Auth::guard('web') : Auth::guard('sellers');
        $this->auth_type = !Auth::guard('sellers')->check() ? 'BUYER' : 'SELLER';
    }

    public function product_list(Request $request)
    {
        $brandables = Brandable::get();
        return view('customizer.product_list', ['brandables' => $brandables]);
    }

    public function design(Request $request)
    {
        $brandables = Brandable::get();
        $arts = DB::table('eco_arts')->where('is_parent', true)->paginate(10);
        $templates = Template::paginate(20);
        $sub_categories = Category::where('is_sub', true)->get();

        return view('customizer.design', ['brandables' => $brandables, 'sub_categories' => $sub_categories, 'arts'=> $arts, 'templates'=> $templates]);
    }

    public function upload_design(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->file('file')->isValid()) {
                $file = $request->file('file');
                $ff = file_get_contents($request->file('file')->path());
                $mime = $request->file('file')->getMimeType();
                $FName = $request->file('file')->getClientOriginalName();
                $encodedFile = 'data: ' . $mime . ';base64,' . base64_encode($ff);
                return response()->json(['file' => $encodedFile, 'image_path' => $encodedFile, 'name'=> $FName], 200);
            }
        }
    }

    public function fetch_arts(Request $request){
        if($request->pid){
            $arts = DB::table('eco_arts')->where('parent_ref', $request->pid)->simplePaginate(6);
            return View::make('customizer.arts_paginate')->with(['arts'=> $arts, 'parent_ref'=> $request->pid])->render();
        }else{
            $arts = DB::table('eco_arts')->where('is_parent', true)->simplePaginate(12);
            return View::make('customizer.art_paginate_base')->with('arts', $arts)->render();
        }
    }

    public function design_success(Request $request, $type)
    {
        return view('customizer.design_success', ['type'=> base64_decode($type)]);
    }

    public function sell_design(Request $request){
        if($request->isMethod('post')){
            $foldername = str_random(10);
            $validator = Validator::make($request->all(), [
				'name' => 'required|max:255|min:2',
				'description' => 'required|min:2',
				'user_price' => 'required',
				//'target_quantity' => 'required',
            ]);
            if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
			}else{
                $product = new Product();
                $product->name = $request->name;
                $product->slug = str_slug($request->name, '-');
                $product->ref = str_random(10);
                //$product->user_id = Auth::user()->id;
                $product->user_id = Auth::guard('sellers')->user()->id;
                $product->user_type = 'SELLER';
                $product->is_published = false;
                $product->description = $request->description;
                $product->price = $request->user_price;
                $product->brandable_id = $request->design_id;
                $product->temp_data_design = $request->design_data;
                $product->temp_data_photos = $request->photos;
                $product->save();
                $this->saveProductMetas($product, $request);
                $this->sortCategory($request->all(), $product);
                //$this->uploadDesigned($request, $product->ref, $product->id);
                return response()->json(['message'=> 'Product was saved successfully', 'redirect_url'=> route('app:ecommerce:process_design', ['id'=> $product->id, 'type'=> base64_encode('sell')])], 200);

            }

        }
    }

    public function buy_design(Request $request){
        if($request->isMethod('post')){
            $foldername = str_random(10);
            $validator = Validator::make($request->all(), [
				'name' => 'required|max:255|min:2',
				'description' => 'required|min:2',
				'user_price' => 'required',
				//'target_quantity' => 'required',
            ]);
            if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
			}else{
                $product = new Product();
                $product->name = $request->name;
                $product->slug = str_slug($request->name, '-');
                $product->ref = str_random(10);
                //$product->user_id = Auth::user()->id;
                $product->user_id = Auth::guard('buyers')->user()->id;
                $product->user_type = 'BUYER';
                $product->is_published = false;
                $product->description = $request->description;
                $product->price = $request->user_price;
                $product->brandable_id = $request->design_id;
                $product->temp_data_design = $request->design_data;
                $product->temp_data_photos = $request->photos;
                $product->save();
                $order_vars = json_decode($request->order_variant, true);
                $this->saveProductMetas($product, $request);

                foreach($order_vars as $item){
                    $this->add_to_cart($product->id, $item['quantity'], $item['size'], $item['color']);
                }
                //$this->sortCategory($request->all(), $product);
               // return response()->json(['message'=> 'Product was saved successfully, you would be redirected shortly to the cart review page to complete your order.', 'redirect_url'=> route('app:ecommerce:cart_review')], 200);

                return response()->json(['message'=> 'Product was saved successfully', 'redirect_url'=> route('app:ecommerce:process_design', ['id'=> $product->id, 'type'=> base64_encode('buy')])], 200);

            }

        }
    }

    public function process_design(Request $request, $id, $type){
        if($request->isMethod('get')){
            $product = \App\Product::where('id', $id)->first();
           $this->uploadDesigned($product, $product->ref, $product->id);

                    $product->temp_data_design = null;
                    $product->temp_data_photos = null;
                    $product->save();


           return redirect()->route('app:ecommerce:design_success', ['type'=> $type]);
        }
    }


    public function add_to_cart($id, $qty, $size, $color)
    {
           // $__data = json_decode(base64_decode($request->item), true);

            $product = Product::with(['photos' => function ($q) {
                $q->first();
            }, 'user', 'brandable', 'design'])->where('id', $id)->first();

            LaraCart::addLine(
                $product->id,
                $product->name,
                $qty,
                $product->brandable->price + $product->design->total_earning,
                [
                    "size" => $size,
                    "color" => $color,
                ],
                $taxable = false
            );
    }

    public function uploadDesigned($product, $foldername, $id){
        $designs =  json_decode($product->temp_data_design, true);
         mkdir(storage_path('app/public/products/'. $foldername));
            if(isset($designs['back']) && !empty($designs['back']['design'])){
                foreach($designs as $key=>$design){
                $imagine = new \Imagine\Gd\Imagine();
                $imagine_design = new \Imagine\Gd\Imagine();
                $imgDir = basename($design['img_url']);
                $image = $imagine->open(storage_path('app/public/brandables/' . $imgDir));

                //save main design
                $SaveAndGetPath = $this->DataToPNG($design['design'], storage_path('app/public/products/') , $foldername);

                $user_design = $imagine_design->open($SaveAndGetPath);

                //$user_design = $imagine_design->open($design['design']);
                $art_position = new \Imagine\Image\Point($design['left'], $design['top']);
                $image->paste($user_design, $art_position);

                //save user art
                /*$position = new \Imagine\Image\Point(0, 0);

                $palette = new \Imagine\Image\Palette\RGB();
                $size  = new \Imagine\Image\Box(530, 630);
                $color = $palette->color($request->color);
                $new_image = $imagine->create($size, $color);

                $new_image->paste($image, $position);*/

                $user_design->save(storage_path('app/public/products/'. $foldername . '/design-' . $key . '-' . $foldername . '.png'));
                $image->save(storage_path('app/public/products/'. $foldername . '/' . $key . '-' . $foldername . '.png'));

                //save meta datas
                $imgPathsArray = [
                    'url'=> 'products/'. $foldername . '/' . $key . '-' . $foldername . '.png',
                    'path'=> storage_path('app/public/products/'. $foldername . '/' . $key . '-' . $foldername . '.png')
                ];
                $this->uploadUserImages($product,storage_path('app/public/products/') , $foldername);
                $this->saveUserDesignAndPhoto($product, $imgPathsArray);
                }
            }else{
                unset($designs['back']);
                $imagine = new \Imagine\Gd\Imagine();
                $imagine_design = new \Imagine\Gd\Imagine();
                $imgDir = basename($designs['front']['img_url']);
                $image = $imagine->open(storage_path('app/public/brandables/' . $imgDir));

                //save main design
                $SaveAndGetPath = $this->DataToPNG($designs['front']['design'], storage_path('app/public/products/') , $foldername);

                $user_design = $imagine_design->open($SaveAndGetPath);
                $art_position = new \Imagine\Image\Point($designs['front']['left'], $designs['front']['top']);
                $image->paste($user_design, $art_position);
                /*$position = new \Imagine\Image\Point(0, 0);

                $palette = new \Imagine\Image\Palette\RGB();
                $size  = new \Imagine\Image\Box(530, 630);
                $color = $palette->color($request->color);
                $new_image = $imagine->create($size, $color);

                $new_image->paste($image, $position);*/
                $user_design->save(storage_path('app/public/products/'. $foldername . '/design-front' . '-' . $foldername . '.png'));
                $image->save(storage_path('app/public/products/'. $foldername . '/front' . '-' . $foldername . '.png'));

                $imgPathsArray = [
                    'url'=> 'products/'. $foldername . '/front' . '-' . $foldername . '.png',
                    'path'=> storage_path('app/public/products/'. $foldername . '/front' . '-' . $foldername . '.png')
                ];
                $this->uploadUserImages($product,storage_path('app/public/products/') , $foldername);
                $this->saveUserDesignAndPhoto($product, $imgPathsArray);
            }

    }

    /**
     * saves the product meta datas to database
     *
     * @param integer $product_id
     * @param array $photos
     * @return void
     */
    public function saveProductMetas($product, $request){
        $design = new Design;
        $design->product_id = $product->id;
        $design->user_id = $product->user_id;
        $design->user_type = $product->user_type;
        $design->brandable_id = $request->design_id;
        $design->name = $request->name;
        $design->earnings = $request->earnings;
        $design->target_quantity = $request->target_quantity;
        $design->total_earning = $request->total_earning;
        $design->percentage = $request->percentage;
        $design->raw_design = $request->design_data;
        $design->colors = json_encode($request->colors);
        $design->save();

    }

    public function uploadUserImages($product, $path, $filename){
        $photos = json_decode($product->temp_data_photos, true);
        if(!empty($photos)){
            foreach($photos as $photo){
                file_put_contents($path . $filename . '/' . $photo['name'], base64_decode(explode(',', $photo['tmp_name'])[1]));
            }
        }

    }


    public function saveUserDesignAndPhoto($product, $photos = []){
        $photoModel = new Photo();
         //save photo to product photo DB
         $photoModel->user_id = $product->user_id;
         $photoModel->user_type = $product->user_type;
         $photoModel->product_id = $product->id;
         $photoModel->path = $photos['path'];
         $photoModel->public_url = $photos['url'];
         $photoModel->save();
    }



    public function DataToPNG($data, $path, $filename){
        $image_path = file_put_contents($path . $filename . '/' . $filename . '.png', base64_decode(explode(',', $data)[1]));
        return $path . $filename . '/' . $filename . '.png';
    }

    public function sortCategory($data, $product){
        $allKats = \App\ProductCategory::get();
					//$fcat = \App\Category::where('parent_id', $data['category'])->get();
					$tagCats = explode(',', $data['sub_category']);
					$oldcat = [];
					///$oldIDS = [];
					$newIDS = [];
					$sortcat = [];
					$allKatData = [];

					/*foreach($fcat as $fca){
					$oldIDS[] = $fca->id;
					}*/

					foreach($allKats as $allKat){
						$allKatData[] = strtolower($allKat->name);
					}
					//$newcat = array_diff($tagCats,$oldcat);
					foreach($tagCats as $tagCat){
						if(!in_array(strtolower($tagCat), $allKatData)){
							$sortcat[] = $tagCat;
						}
					}

					foreach($sortcat as $newca){
					$catd = new \App\ProductCategory();
					$catd->name = $newca;
					$catd->slug = str_slug($newca);
					$catd->parent_id = null;
					$catd->ref = str_random(10);
					$catd->save();
					$newIDS[] = $catd->id;
					}

					$allIDS = array_merge([$data['category']], $newIDS);

                    //$att = \App\Product::find($product->id);
					$product->categories()->attach($allIDS);

    }


    public function populateArts(){
        $artDirectory = glob(storage_path('/Arts') . '/*' , GLOB_ONLYDIR);
        foreach($artDirectory as $dir){
            $childImages = glob(storage_path('/Arts/' .  basename($dir)) . '/*');
            $parentRef = str_random(10);
            //print_r($childImages);
            $artt = DB::table('eco_arts')->insertGetId(
                [
                    'name'=> basename($dir),
                    'ref'=> $parentRef,
                    'parent_ref'=> null,
                    'is_parent'=> true,
                    'photo_url'=> asset('Arts/' .  basename($dir) . '/' . basename($childImages[0]))
                ]
            );

            foreach($childImages as $childImage){
                $arttcild = DB::table('eco_arts')->insertGetId(
                    [
                        'name'=> basename($childImage),
                        'ref'=> str_random(10),
                        'parent_ref'=> $parentRef,
                        'is_parent'=> false,
                        'photo_url'=> asset('Arts/' .  basename($dir) . '/' . basename($childImage))
                    ]
                );
            }
        }

    }

}
