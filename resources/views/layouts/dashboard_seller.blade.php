<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favicons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('favicons/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="theme-color" content="#ffffff">

    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">

    <link rel="stylesheet" href="{{ mix('css/dashboard_vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/dashboard_main.css') }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/js/all.js"></script>
    @stack('PAGE_STYLES')

    <script src="{{ mix('js/dashboard_vendor.js') }}"></script>
    <script src="{{ mix('js/dashboard_main.js') }}"></script>
    @stack('PAGE_SCRIPTS')

</head>
<body class="sidebar-fixed header-fixed">
@includeIf('partials.notify')
<div class="page-wrapper">

    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none">
            <!--<i class="fa fa-bars"></i>-->
            <i class="icon-menu" style="font-size:20px;"></i>
        </a>

        <a class="navbar-brand d-md-down-none" href="{{ route('app:seller:index') }}">
            <img src="{{ $utils->get_image('site.logo') }}" alt="logo" width="100">
        </a>
        <a class="navbar-brand navbar-brand-mobile d-md-none" href="{{ route('app:seller:index') }}">
            <img src="{{ asset('img/logo-icon.png') }}" alt="logo" width="36">

        </a>

        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="icon-menu" style="font-size:20px;"></i>
        </a>

        <ul class="navbar-nav ml-auto">


            <li class="nav-item ">

                <i class="icon-bell" style="font-size: 20px;"></i>
                @php $notify = DB::table('all_notifications')->where([['owner_id', Auth::guard('sellers')->user()->id], ['read_at', 0]])->count(); @endphp
                @if($notify > 0)
                <span class="badge badge-pill badge-danger">
                {{ $notify }}
                </span>
                @endif

                <div uk-dropdown="mode: click" class="p-0">
                    <ul class="list-unstyled">
                 @php $notifications = DB::table('all_notifications')->where([['owner_id', Auth::guard('sellers')->user()->id], ['read_at', 0]])->get()->all(); @endphp
                    @foreach($notifications as $notification)
                        <?php $notificationData = json_decode($notification->message_body, true); ?>
                        <li class="nav-item p-2 divider">
                                <a href="{{ route('app:seller:dashboard:all_notifications', ['id'=> $notification->id]) }}">{{ $notificationData['data'] }}</a>
                        </li>
                   @endforeach
                    </ul>
                    </div>
            </li>


            {{-- <li class="nav-item ">
                <a href="#">
                    <i class="icon-envelope" style="font-size: 20px;"></i>
                    <span class="badge badge-pill badge-danger">5</span>
                </a>
                <div uk-dropdown="mode: click" class="p-0">
                    <ul class="list-unstyled">
                        <li class="d-inline-block border-bottom p-3">
                            <div class="d-flex justify-content-between">
                                <div >
                                    <i class="icon-envelope" style="font-size:20px;"></i>
                                </div>
                                <div class="ml-3">
                                        Set the direction of flex items in a flex container with direction utilities
                                </div>
                            </div>
                        </li>
                        <li class="d-inline-block border-bottom p-3">
                            <div class="d-flex justify-content-between">
                                <div >
                                    <i class="icon-envelope" style="font-size:20px;"></i>
                                </div>
                                <div class="ml-3">
                                        Set the direction of flex items in a flex container with direction utilities
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </li> --}}

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="avatar avatar-sm generalProfilePic" alt="{{ Auth::guard('sellers')->user()->username }}" src="{{ Auth::guard('sellers')->user()->photo_url ? asset(Auth::guard('sellers')->user()->photo_url) : asset('user/default.png') }}" alt="" width="100">
                    <span class="small ml-1 d-md-down-none capitalize">{{ Auth::guard('sellers')->user()->username }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    @include('partials.acl_menu.seller', ['type'=>'admin_dropdown'])
                    <a href="{{ route('app:seller:logout') }}" class="dropdown-item post-link">
                            <i class="fa fa-lock"></i> Logout
                    </a>
                </div>
            </li>

        </ul>
    </nav>

    <div class="main-container">

        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    @include('partials.acl_menu.seller', ['type'=>'admin_sidebar_header'])
                </ul>
                <ul class="nav children">
                    @include('partials.acl_menu.seller', ['type'=>'admin_sidebar_content'])
                </ul>
                <ul class="nav">
                    @include('partials.acl_menu.seller', ['type'=>'admin_sidebar_footer'])
                </ul>
            </nav>
        </div>

        <div class="content">

           @yield('content')

        </div>

    </div>
</div>






{{-- Sigup/Login Popup first screen --}}
<div id="modal-close-default3" uk-modal>
        <div class="uk-modal-dialog uk-modal-body medium-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="">

            <div class="row">
            <div class="col-sm-12">

                            <div class="login-header-title-popup">
                                    <p class="mt-5 mb-5" >MAKE MONEY ON SUVENIA</p>
                         </div>
                         <p class="mt-5 mb-5" style="text-align:center;">Suvenia allows you sell your designs and ideas on everyday products like apparels, drinkwares and accessories, at no hassle to you. </p>
                        </div>
                        <div class="col-sm-3" style="text-align:center">
                                <img src="{{ asset('img/a_a.jpg') }}"/>
                                <p>SnapBack</p>
                        </div>

                        <div class="col-sm-3" style="text-align:center">
                                <img src="{{ asset('img/a_a2.png') }}"/>
                                <p>T-Shirt</p>
                        </div>
                        <div class="col-sm-3" style="text-align:center">
                                <img src="{{ asset('img/a_a3.png') }}"/>
                                <p>Hoodies</p>
                        </div>
                        <div class="col-sm-3" style="text-align:center">
                                <img src="{{ asset('img/a_a4.jpg') }}"/>
                                <p>Mug</p>
                        </div>
        </div>
        <div class="row">
            <div class="col-sm-6 mt-5 pt-5 mb-5">
                    <a href="{{ route('app:seller:index') }}" style="color:#1896a9; font-family: Poppins; font-size: 16px;font-weight: norma" >Learn More </a>
            </div>
            <div class="col-sm-6 mt-5 mb-5" style="text-align:right;">
                    <a href="#" class="btn sell_on_suvenia_buttons" uk-toggle="target: #modal-close-default4">SWITCH TO BUYER</a>
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>



{{-- Login main Popup screen --}}
    <div id="modal-close-default4" uk-modal>
        <div class="uk-modal-dialog uk-modal-body small-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="">
                <div class="row">
                <div class="col-sm-12">
                    <div class="login-header-title-popup">
                                <p class="mt-3" style="color:teal;" >SIGN IN</p>
                        </div>
                        <p class="mb-5" style="text-align:center;">Log in to your account to buy products that interest you </p>
                        <form action="{{ route('app:seller:dashboard:create_store') }}" method="POST" data-post>
                                @csrf
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                <input type="email" class="form-control textfield-format text-border-radius" id="email" placeholder="Email Address" name="email">
                                <p class="help-block m-0"></p>
                                </div>

                                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                <input type="password" class="form-control textfield-format text-border-radius" id="password" placeholder="Password" name="password">
                                <p class="help-block m-0"></p>
                                </div>

                                <div class="form-group form-row">
                                        <div class="col-md-7">
                                           <div class="custom-control custom-checkbox mr-sm-2">
                                               <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                               <label class="custom-control-label remember-text" style="padding-left:20px;" for="customControlAutosizing">Remember me</label>
                                             </div>
                                        </div>
                                        <div class="col-md-5">
                                          <a href="{{ route('app:seller:forgot_password') }}" class="forgot-link">Forgot password?</a>
                                        </div>
                                </div>

                                <button type="submit" class="btn btn-info btn-lg btn-block rounded">SIGN IN</button>
                            </form>
                            <p class="body-info uk-margin-small-top">Don’t have an account? <a href="#" uk-toggle="target: #modal-close-default5">Create Account</a></p>
                    </div>
                    <div class="box-footer mt-2">
                            <div class="fab-info">OR</div>
                            <p class="m-1 info">Sign In with social media</p>
                            <div class="mt-3">
                                <a href="{{ route('app:user:social', ['provider'=> 'facebook']) }}" class="uk-icon-button uk-margin-small-right app-auth-icon" uk-icon="facebook"></a>
                                <a href="{{ route('app:user:social', ['provider'=> 'twitter']) }}" class="uk-icon-button  uk-margin-small-right app-auth-icon" uk-icon="twitter"></a>
                                <a href="{{ route('app:user:social', ['provider'=> 'linkedin']) }}" class="uk-icon-button app-auth-icon" uk-icon="linkedin"></a>
                            </div>
                    </div>
            </div>

                </div>
                </div>
            </div>



{{-- Sign-up main Popup screen --}}
<div id="modal-close-default5" uk-modal>
        <div class="uk-modal-dialog uk-modal-body small-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="">

                <div class="row">
                <div class="col-sm-12">
                    <div class="login-header-title-popup">
                                <p class="mt-3" style="color:teal;" >SIGN UP</p>
                        </div>
                        <p class="mb-5" style="text-align:center;">Sign up to join to create, buy or request for product of your choice.</p>
                        <form action="{{ route('app:seller:dashboard:create_store') }}" method="POST" data-post>
                                @csrf
                                <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                                <input type="text" class="form-control textfield-format text-border-radius" id="username" placeholder="Username" name="username">
                                <p class="help-block m-0"></p>
                                </div>

                                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                <input type="email" class="form-control textfield-format text-border-radius" id="email" placeholder="Email Address" name="email">
                                <p class="help-block m-0"></p>
                                </div>

                                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                <input type="password" class="form-control textfield-format text-border-radius" id="password" placeholder="Password" name="password">
                                <p class="help-block m-0"></p>
                                </div>
                                <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                                <input type="password" class="form-control textfield-format text-border-radius" id="confirm_password" placeholder="Confirm Password" name="confirm_password">
                                <p class="help-block m-0"></p>
                                </div>

                                <button type="submit" class="btn btn-info btn-lg btn-block rounded">SIGN IN</button>
                            </form>
                            <p class="body-info">Already have an account? <a href="#" uk-toggle="target: #modal-close-default4">Sign in now</a></p>
                    </div>

                    <div class="box-footer mt-2">
                            <div class="fab-info">OR</div>
                            <p class="m-1 info">Sign In with social media</p>
                            <div class="mt-3">
                                <a href="{{ route('app:user:social', ['provider'=> 'facebook']) }}" class="uk-icon-button uk-margin-small-right app-auth-icon" uk-icon="facebook"></a>
                                <a href="{{ route('app:user:social', ['provider'=> 'twitter']) }}" class="uk-icon-button  uk-margin-small-right app-auth-icon" uk-icon="twitter"></a>
                                <a href="{{ route('app:user:social', ['provider'=> 'linkedin']) }}" class="uk-icon-button app-auth-icon" uk-icon="linkedin"></a>
                            </div>
                    </div>
            </div>

                </div>
            </div>
            </div>


            </div>







<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(function() {
    $('input[id$=tb1]').keyup(function() {
        var txtClone = $(this).val().toLowerCase();
        $slug = txtClone.replace(/[^a-z0-9-]/gi, '-').
        replace(/-+/g, '-').
        replace(/^-|-$/g, '');
        $('input[id$=tb2]').val($slug);
    });
});

$('.li-modal').on('click', function(e){
    e.preventDefault();
    $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
});

// $(document).ready(function(){
//         $('.launch-modal').click(function(){
//             $('#modal-close-default4').modal({
//                 backdrop: 'static'
//             });
//         });
//     });

</script>

</body>
</html>
