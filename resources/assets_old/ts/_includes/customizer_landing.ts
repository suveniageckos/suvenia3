class customizerLanding{
 
    public constructor(){
        this.set_ajax();
        this.set_page();
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    } 

    public set_page(){
        $('[data-mapper]').each(function(ind, val){
            $(val).on('click', function(e){
            e.preventDefault();
               var info : any = $(this).attr('data-mapper');   
                 if (typeof(Storage) !== "undefined") {
                   localStorage.setItem("mappedData", info);
                   localStorage.setItem("isMapped", '1');
                } else {
                    alert('you must use a browser that supports localstorage.')
                }
                window.location.href = <any>$(this).attr('href')
                
            });
            
        });
    }
 

}

$(function(){ 
    const ____init_customizerLanding_page = new customizerLanding();
}); 