<?php

use Illuminate\Database\Seeder;

class SellersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sellers')->delete();
        
        \DB::table('sellers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'ref' => '5PkH2sezHy',
                'username' => 'ukandu',
                'slug' => 'ukandu',
                'firstname' => 'ukandud',
                'lastname' => 'michael',
                'email' => 'admin@ppp.com',
                'email_verified_at' => '2019-05-28 00:00:00',
                'password' => '$2y$10$Hn7DrjSsIKjzVkFdvm80CeaxeirkJVslaDbDpQpSjG5fUB6O6wNOO',
                'password_token' => NULL,
                'password_token_expiry' => NULL,
                'email_token' => NULL,
                'email_token_expiry' => NULL,
                'has_password' => 1,
                'email_verified' => 1,
                'facebook' => NULL,
                'twitter' => NULL,
                'linkedin' => NULL,
                'photo_url' => 'http://127.0.0.1:8000/storage/sellers/jkEFHzXqfgiBFxBVGKCORYAs7aep0wFWx0PoaHq0.jpeg',
                'image_path' => 'sellers/jkEFHzXqfgiBFxBVGKCORYAs7aep0wFWx0PoaHq0.jpeg',
                'cover_photo_url' => NULL,
                'phone' => '07061244001',
                'balance' => '0.00',
                'earnings' => '0.00',
                'bank_sort' => '011',
                'bank_bvn' => NULL,
                'bank_account' => '3032439851',
                'bank_name' => 'ukandu michael',
                'transfer_code' => 'RCP_0k4xb42tjaar54w',
                'bio' => 'The lastname must be at least 2 characters.',
                'created_at' => NULL,
                'updated_at' => '2019-07-31 18:04:16',
            ),
            1 => 
            array (
                'id' => 5,
                'ref' => 'xV9QE9tbYE',
                'username' => 'vcordss',
                'slug' => 'vcordss',
                'firstname' => NULL,
                'lastname' => NULL,
                'email' => 'adminss@app.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$wgYMFzlPUPVXzt6RUYmP7uuQC1r1HO04rW/a8.aMPHFiXG1QopJ6a',
                'password_token' => 'CqMhme40JlIheDzooUivY3Gki9PY94B70WQH',
                'password_token_expiry' => '2019-08-01 17:41:36',
                'email_token' => 'eSDM1RlVILV0eSx160FaZo7xhy2FmYNu3ckg',
                'email_token_expiry' => '2019-08-01 15:04:28',
                'has_password' => 1,
                'email_verified' => 0,
                'facebook' => NULL,
                'twitter' => NULL,
                'linkedin' => NULL,
                'photo_url' => NULL,
                'image_path' => NULL,
                'cover_photo_url' => NULL,
                'phone' => NULL,
                'balance' => '0.00',
                'earnings' => '0.00',
                'bank_sort' => NULL,
                'bank_bvn' => NULL,
                'bank_account' => NULL,
                'bank_name' => NULL,
                'transfer_code' => NULL,
                'bio' => NULL,
                'created_at' => '2019-07-31 15:04:28',
                'updated_at' => '2019-07-31 17:41:36',
            ),
            2 => 
            array (
                'id' => 6,
                'ref' => 'PBUd5ZUYtH',
                'username' => 'michea',
                'slug' => 'michea',
                'firstname' => NULL,
                'lastname' => NULL,
                'email' => 'michael@ukandumichael.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$d7z0WebSoEF6RGlbdIAS8OBfdMtOSxifA8aXVRyoly5NqfVGXsV/G',
                'password_token' => NULL,
                'password_token_expiry' => NULL,
                'email_token' => NULL,
                'email_token_expiry' => NULL,
                'has_password' => 1,
                'email_verified' => 1,
                'facebook' => NULL,
                'twitter' => NULL,
                'linkedin' => NULL,
                'photo_url' => NULL,
                'image_path' => NULL,
                'cover_photo_url' => NULL,
                'phone' => NULL,
                'balance' => '0.00',
                'earnings' => '0.00',
                'bank_sort' => NULL,
                'bank_bvn' => NULL,
                'bank_account' => NULL,
                'bank_name' => NULL,
                'transfer_code' => NULL,
                'bio' => NULL,
                'created_at' => '2019-07-31 15:22:54',
                'updated_at' => '2019-07-31 15:26:00',
            ),
            3 => 
            array (
                'id' => 7,
                'ref' => 'KzLXmiSSaW',
                'username' => 'killbuhari',
                'slug' => 'killbuhari',
                'firstname' => NULL,
                'lastname' => NULL,
                'email' => 'kill@buhari.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$uLkwQowjdZ/X3578QcjAbuqNiUV6/xHRzr2Em04TSkODpor7U02QK',
                'password_token' => NULL,
                'password_token_expiry' => NULL,
                'email_token' => NULL,
                'email_token_expiry' => NULL,
                'has_password' => 1,
                'email_verified' => 1,
                'facebook' => NULL,
                'twitter' => NULL,
                'linkedin' => NULL,
                'photo_url' => NULL,
                'image_path' => NULL,
                'cover_photo_url' => NULL,
                'phone' => NULL,
                'balance' => '0.00',
                'earnings' => '0.00',
                'bank_sort' => NULL,
                'bank_bvn' => NULL,
                'bank_account' => NULL,
                'bank_name' => NULL,
                'transfer_code' => NULL,
                'bio' => NULL,
                'created_at' => '2019-08-08 15:01:41',
                'updated_at' => '2019-08-08 15:03:24',
            ),
        ));
        
        
    }
}