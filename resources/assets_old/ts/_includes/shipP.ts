import { CartItem } from "./includes/CartItem";
class CartShippingPage{

    public constructor(public __cartItem = new CartItem){
        this.set_ajax();
        if($('#ShipPage').length){
            this.__cartItem.__initiate_delete();
            this.__cartItem._delete_cart_item();
        }
        
        this.add_ship_modal();
        this.set_default_ship_modal();
        this.set_zone();
        this.edit_ahipping_address();
    }
 
    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    }  
 
    public add_ship_modal(){
        let cls = this;
        $(document).on('click', '.__ship_add_address_def', function(e){
            e.preventDefault();
            $.ajax({
                url:  $(e.currentTarget).attr('data-url'),
                type: 'GET',
                data: {},
                beforeSend: function(){
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    //console.log(data);
                    $('body').prepend(
                        '<div id="__zone_address_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">'+
                        '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Add Address</h2>'+
                        '<p class="m-1" style="color: #333; font-size: 17px;">'+data+'</p>'+
                        '</div></div>'
                    );
                    UIkit.modal("#__zone_address_modal").show();
                }
            });

        });
    }

    public set_default_ship_modal(){
        $(document).on('click', '.__ship_select_address_def', function(e){
            e.preventDefault();
            $.ajax({
                url:  $(e.currentTarget).attr('data-url'),
                type: 'GET',
                data: {},
                beforeSend: function(){
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    //console.log(data);
                    $('body').prepend(
                        '<div id="__zone_address_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">'+
                        '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Add Address</h2>'+
                        '<p class="m-1" style="color: #333; font-size: 17px;">'+data+'</p>'+
                        '</div></div>'
                    );
                    UIkit.modal("#__zone_address_modal").show();
                }
            });

        });
    }

    public set_zone(){
        $(document).on('change', '._zone_selector', function(e){
            let inp = $(e.target);
            $.ajax({
                url:  $(e.currentTarget).attr('data-url'),
                type: 'POST',
                data: {},
                beforeSend: function(){
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    if(data.message){
                        toastr.success(data.message);
                        }

                        var wait = setTimeout(function() {
                            window.location.href = data.redirect_url;
                        }, 1000);
                        
                }
            });

        });
    }

    public edit_ahipping_address(){
        $(document).on('click', '.__ship_edit_trigger', function(e){
            e.preventDefault();
            $.ajax({
                url:  $(e.currentTarget).attr('href'),
                type: 'GET',
                data: {},
                beforeSend: function(){
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    $('body').prepend(
                        '<div id="__zone_address_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">'+
                        '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Edit Address</h2>'+
                        '<p class="m-1" style="color: #333; font-size: 17px;">'+data+'</p>'+
                        '</div></div>'
                    );
                    UIkit.modal("#__zone_address_modal").show();
                }
            });
        });
    }


}

$(function(){ 
    const ____init_cSP_page = new CartShippingPage();
}); 