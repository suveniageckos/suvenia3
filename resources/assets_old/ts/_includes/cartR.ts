import { CartItem } from "./includes/CartItem";

class CartReviewPage{

    public constructor(public __cartItem = new CartItem){
        this.set_ajax();
        if($('#CartPage').length){
            this.__cartItem.__initiate_delete();
            this.__cartItem._delete_cart_item();
        }
        this.__initiate_update();
 
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    } 
 

    public __initiate_update(){
        $(document).on('change', '.cart_prod_quantity_input', function(e){
            let curTarg : any = $(e.currentTarget);
            $.post(curTarg.attr('data-url'), {qty: curTarg.val()});
             window.location.reload();
        });
    }

}

$(function(){ 
    const ____init_cR_page = new CartReviewPage();
}); 