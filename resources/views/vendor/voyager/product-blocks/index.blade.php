@extends('voyager::master')

@section('page_title', __('All Brandables'))

@section('content')

<div class="container-fluid">
        <h1 class="page-title">
                <i class="voyager-categories"></i> Product Blocks
            </h1>
            @can('add', app('App\ProductBlock'))
            <a href="{{ route('admin:product_block:add') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
             @endcan

<div class="page-content browse container-fluid">

<div class="row">
<div class="col-md-12">

<div class="panel panel-bordered">
<div class="panel-body">
<div class="row">
    @forelse($product_blocks as $product_block)
        <div class="col-md-3 text-center">
            <div class="card p-3">
                <p style="margin:2px; text-transform:capitalize;"><b>{{ $product_block->title }}</b></p>
                <p style="margin:2px; text-transform:capitalize;"><input type="text" class="form-control" readonly value="{{$product_block->code }}"></p>
                    <p style="margin:2px; text-transform:capitalize;">
                            <a href="{{route('admin:product_block:edit', ['id'=> $product_block->id ])}}" class="btn btn-info btn-sm">Edit this</a>
                            <form action="{{route('admin:product_block:delete', ['id'=> $product_block->id ])}}" method="POST">
                                @csrf
                                <button class="btn btn-danger btn-sm">Delete this</button>
                            </form>
                    </p>
            </div>
        </div>
    @empty
        <div class="col-md-12 text-center">
            <p><i class="voyager-bag" style="font-size:100px;"></i></p>
             <b>You do not have any template</b>
        </div>
    @endforelse
</div>
</div>
</div>


</div>
</div>


</div>
</div>

@endsection
