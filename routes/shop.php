<?php
Route::group(['prefix' => 'shop', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/product/search', 'ShopCtrl@product_search')->name('app:ecommerce:product_search');
    Route::match(['post', 'get'], '/product/favorite/{task}', 'ShopCtrl@favorite_product')->name('app:ecommerce:favorite_product');
    Route::match(['post', 'get'], '/product/ask/question', 'ShopCtrl@ask_question')->name('app:ecommerce:ask_question');
    Route::match(['post', 'get'], '/buy-direct/{code?}', 'ShopCtrl@buy_direct')->name('app:ecommerce:buy_direct');
    Route::match(['post', 'get'], '/add-to-cart/{id}', 'ShopCtrl@add_to_cart')->name('app:ecommerce:add_to_cart');
    Route::match(['post', 'get'], '/step/cart-review', 'ShopCtrl@cart_review')->name('app:ecommerce:cart_review');
    Route::match(['post', 'get'], '/step/apply-coupon', 'ShopCtrl@apply_coupon')->name('app:ecommerce:cart_apply_coupon');
    Route::match(['post', 'get'], '/step/shipping', 'ShopCtrl@step_shipping')->name('app:ecommerce:step_shipping');
    // Route::match(['post', 'get'], '/step/pay', 'ShopCtrl@step_pay')->name('app:ecommerce:step_pay');
    Route::match(['post', 'get'], '/step/pay', 'ShopCtrl@step_pay')->name('app:ecommerce:step_pay');
    Route::match(['post', 'get'], '/step/payment/verify', 'ShopCtrl@payment_verify')->name('app:ecommerce:payment_verify');
    Route::match(['post', 'get'], '/step/payment/notify/{status}', 'ShopCtrl@payment_notify')->name('app:ecommerce:payment_notify_status');
    Route::match(['post', 'get'], '/step/{id}/complete-payment', 'ShopCtrl@complete_paylater_payment')->name('app:ecommerce:complete_payment');

    /**
     * Tasks routes
     */
    Route::match(['post', 'get'], '/action/delete-cart-item/{hash}', 'ShopCtrl@delete_cart_item')->name('app:ecommerce:delete_cart_item');
    Route::match(['post', 'get'], '/action/update-cart-item/{hash}', 'ShopCtrl@update_cart_item')->name('app:ecommerce:update_cart_item');
    Route::match(['post', 'get'], '/set-modal/add-address/{id}', 'ShopCtrl@add_address_zone_modal')->name('app:ecommerce: add_address_zone_modal');
    Route::match(['post', 'get'], '/set-modal/edit-address/{id}/{address_id}', 'ShopCtrl@edit_address_zone_modal')->name('app:ecommerce: edit_address_zone_modal');
    Route::match(['post', 'get'], '/set-modal/select-address/{id}', 'ShopCtrl@set_default_zone_modal')->name('app:ecommerce:set_default_zone_modal');
    Route::match(['post', 'get'], '/add-shipping-address/{zone_id}', 'ShopCtrl@add_shipping_address')->name('app:ecommerce:add_shipping_address');
    Route::match(['post', 'get'], '/edit-shipping-address/{zone_id}/{address_id}', 'ShopCtrl@edit_shipping_address')->name('app:ecommerce:edit_shipping_address');
    Route::match(['post', 'get'], '/delete-shipping-address/{id}', 'ShopCtrl@delete_shipping_address')->name('app:ecommerce:delete_shipping_address');
    Route::match(['post', 'get'], '/shipping/select-address/{zone_id}', 'ShopCtrl@select_shipping_address')->name('app:ecommerce:select_shipping_address');
    Route::match(['post', 'get'], '/shipping/select-zone/{zone_id}', 'ShopCtrl@select_zone')->name('app:ecommerce:select_zone');
    Route::match(['post', 'get'], '/shipping/process-price/verify', 'ShopCtrl@process_shipping_price_for_payment')->name('app:ecommerce:process_shipping_price_for_payment');
    Route::match(['post', 'get'], '/shipping/process-payment/{type}', 'ShopCtrl@process_payment')->name('app:ecommerce:process_payment');
});
