<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInfluencerPricelistingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('influencer_pricelistings', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('follower_range', 191);
			$table->float('video_price');
			$table->float('photo_price');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('influencer_pricelistings');
	}

}
