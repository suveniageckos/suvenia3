<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $table = 'eco_customers';

    public function user(){
        return $this->belongsTo("App\User");
    }
}
