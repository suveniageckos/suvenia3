<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInfluencerGigBidsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('influencer_gig_bids', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('influencer_id');
			$table->integer('gig_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('influencer_gig_bids');
	}

}
