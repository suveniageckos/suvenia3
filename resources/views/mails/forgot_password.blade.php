@extends('mails.layouts.base')

@section('content')

<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
		<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
            <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
                <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
                    <tbody>
                        <tr>
                            <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                                <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                                    <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:5px 5px 5px 5px;" align="center">
                                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;">
                                                        <p><span style="font-size:16px;"><strong>Hi {{ $user->username }}</strong></span>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
		<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
		<div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
			<table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
				<tbody>
					<tr>
						<td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
							<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
							<div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
								<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
									<tbody>
										<tr>
											<td style="word-wrap:break-word;font-size:0px;padding:1px 1px 1px 1px;" align="left">
												<div style="cursor:auto;color:#000000;font-family:Cabin, sans-serif;font-size:11px;line-height:1.5;text-align:center;">
													<p><span style="font-size:14px;">We recently received a request to recover your Suvenia.com Account Password. If you have not requested this password reset, please disregard the email.</span>
													</p>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
						</td>
					</tr>
				</tbody>
			</table>
        </div>
        

            <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
		<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
		<div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
			<table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
				<tbody>
					<tr>
						<td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
							<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
							<div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
								<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
									<tbody>
										<tr>
											<td style="word-wrap:break-word;font-size:0px;padding:15px 15px 15px 15px;" align="center">
												<table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;width:auto;" align="center" border="0">
													<tbody>
														<tr>
															<td style="border:0px solid #000;border-radius:5px;color:#fff;cursor:auto;padding:14px 40px;" align="center" valign="middle" bgcolor="#1896a9"><a href="{{ route('app:'. strtolower($type) .':reset_password', ['token'=> $user->password_token]) }}" style="text-decoration:none;background:#1896a9;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:20px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Reset Password</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
						</td>
					</tr>
				</tbody>
			</table>
        </div>

        <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
		<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
		
@endsection