@extends('layouts.dashboard_seller')

@section('title', 'Promotions' )

@section('content')

{{-- <div class="row border-botto">
        <div class="col-md-12">
            <h2 class="dash-heading">Promotions</h2>
        </div>
        </div> --}}


        <div class="row">
                <div class="col-md-12">
                    <div class="card uk-animation-slide-top-small">
                            <div class="card-header card-header-lite">
                                <div class="row">
                                    <div class="col-md-4 uk-text-center">
                                    <a href="{{ route('app:seller:dashboard:promotions') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:promotions'], 'active') }}">Influencer's Promotions</a>
                                    </div>
                                    <div class="col-md-4 uk-text-center">
                                    {{-- <a href="{{ route('app:seller:dashboard:discounts') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:discounts'], 'active') }}">Discounts</a> --}}
                                    <a href="#" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:discounts'], 'active') }}">Discounts</a>
                                    </div>
                                    <div class="col-md-4 uk-text-center">
                                         {{-- <a href="{{ route('app:seller:dashboard:advertising') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:advertising'], 'active') }}">Advertising</a> --}}
                                    <a href="#" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:advertising'], 'active') }}">Advertising</a>
                                      </div>
                                </div>
                            </div>
                            <div class="card-body">


                                    <div class="uk-card uk-card-default uk-card-body uk-width-1-0@m">
                                    <table class="uk-table uk-table-divider">

                                            <thead>
                                                <tr>
                                                    <th width="40%">Products</th>
                                                    <th width="20%">Influencers</th>
                                                    <th width="40%">Links</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($promotions as $promotion)
                                                @php
                                                $user_state = DB::table('inflencer_gigs')->whereNotNull('influencer_id')->first();

                                                $influencer = DB::table('influencers')->where([['id', $promotion->influencer_id]])->first();
                                                $photo = DB::table('eco_product_media')->where([['product_id', $promotion->product_id]])->first();
                                               @endphp
                                               @if (! empty($user_state))
                                               <tr>
                                                    <td width="40%"><img height="80" width="80" src="{{ asset('storage/'.$photo->public_url) }}"> {{$promotion->product->name }}</td>
                                                    <td width="20%">{{$influencer->username }}</td>
                                                    <td width="40%"><a href="{{ $promotion->content_link }}">{{ str_limit($promotion->content_link, 80) }}</a></td>
                                                </tr>
                                               @else
                                               <tr>
                                                   <td>You do not have any promotion(s) yet</td>
                                                   <td></td>
                                                   <td></td>
                                               </tr>
                                               @endif

                                                @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                            </div>
                    </div>
                </div>

            </div>

@endsection
